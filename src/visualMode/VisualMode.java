package visualMode;

import visualToken.TextArea;
import android.graphics.Rect;
import android.widget.Button;
import dataToken.DataToken;
import displayLayout.Container;
import displayLayout.TextComponent;
import displayLayoutOld.Part;
import displayLayoutOld.TextPart;

public abstract class VisualMode {
	
	protected Container rootLayout;
	protected Rect displayArea;
	protected DataToken resultDataToken;

	public VisualMode(Container rootLayout) {
		// TODO Auto-generated constructor stub
		this.rootLayout = rootLayout;
		displayArea = new Rect();
		resultDataToken = null;
	}
	
	public abstract void onDestroy();
	
	public abstract void onDisplayAreaChanged();
	
	public abstract void onFocused();
	
	public abstract int onClick(Button b);
	
	public abstract void onResultReceived(TextArea result, DataToken resultDataToken);
	
	public abstract TextComponent getInputTextComponentToWorkWith();

}
