package visualMode;

import com.example.calculator03.Calculator03;
import com.example.calculator03.CommonButtonCommands_Visual;
import com.example.calculator03.ComputingThread;
import com.example.calculator03.DisplayManager;
import com.example.calculator03.MyButton;

import visualToken.FractionToken;
import visualToken.StringToken;
import visualToken.TextArea;
import visualToken.VisualToken;
import android.graphics.Bitmap;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.Log;
import android.widget.Button;
import dataToken.DataToken;
import dataToken.ResultToken;
import displayLayout.Container;
import displayLayout.TextComponent;
import displayLayoutOld.InputPart;
import displayLayoutOld.LayoutBox;
import displayLayoutOld.Part;
import displayLayoutOld.TextPart;


public class BasicVisualMode extends VisualMode {
	
	public static final int RRE_DISPLAY = 0;
	public static final int DECIMAL_DISPLAY = 1;
	
	public static final int INPUT = 0;
	public static final int OUTPUT = 1;

	private CommonButtonCommands_Visual commonButtonCommands_Visual;
	private TextComponent input, output;
	private DisplayManager displayManager;
	private int displayType;
	
	public BasicVisualMode(Container rootLayout) {
		// TODO Auto-generated constructor stub
		super(rootLayout);
		output = (TextComponent) rootLayout.getBoxList().get(1);
		input = (TextComponent) rootLayout.getBoxList().get(0);
		
		output.setDisabled(true);
		rootLayout.updateBoxList();
		
		commonButtonCommands_Visual = new CommonButtonCommands_Visual();
		displayType = -1;
	}
	
	public void setReferences(Calculator03 mainActivity){
		commonButtonCommands_Visual.setReferences(mainActivity);
		displayManager = mainActivity.getDisplayManager();
	}
	
	public void updateDisplayArea(Rect displayArea){
		if (rootLayout.getWidth() != displayArea.width() && rootLayout.getHeight() != displayArea.height()){
			Log.d("","Basic mode updating layout");
			rootLayout.updateLayout(displayArea);
			this.displayArea = new Rect(displayArea);
			alignTextAreas();
		}
	}
	
	public Bitmap getDrawnBitmap(){
		Bitmap bitmap = rootLayout.getDrawnBitmap();
		return bitmap;
	}

	public void initParts(){
		
	}
	
	public void partsAlignmentParamsChanged(){
		
	}
	
	public void setupParts_withParams(){
		
	}
	
	public void updateTextSize(){
		
	}
	
	public void alignOutputText(){
		
	}
	
	public void alignInputText(){
		
	}
	
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onDisplayAreaChanged() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onFocused() {
		// TODO Auto-generated method stub

	}

	/**
	 * 
	 * <p><b><i>   </i></b></p>
	 * return 1 if the command was executed and consumed, 0 if it was not executed but was consumed.
	 * -1 if it was not executed and not consumed. -1 means that the thread that calls this command should continue calling this command 
	 * until 0 or 1 is returned.
	 * @param b
	 * @return
	 */
	
	@Override
	public int onClick(Button b) {
		// TODO Auto-generated method stub
		MyButton mb = (MyButton)b;
		String bText = (String) mb.getNameTag();
		
		if (!bText.equals("=") && !output.isDisabled()){
			displayManager.dataUpdateStatus = DisplayManager.UPDATING_DATA;
			input.setDisabled(false);
			output.setDisabled(true);
			input.toggleCursor(true);
			rootLayout.updateBoxList();
			rootLayout.updateLayout(displayArea);
			input.setTextAreaAlignment(TextComponent.TOP_LEFT);
			alignTextAreas();
			displayManager.dataUpdateStatus = DisplayManager.WAITING_FOR_INPUT;
		}
		commonButtonCommands_Visual.onClick(b);
		return 1;
	}

	public int onTouch(PointF p){
		RectF inputRect = new RectF(input.getLocation().x, input.getLocation().y, input.getLocation().x + input.getWidth(), input.getLocation().y + input.getHeight());
		RectF outputRect = new RectF(output.getLocation().x, output.getLocation().y, output.getLocation().x + output.getWidth(), output.getLocation().y + output.getHeight());
		int val = -1;
		if (inputRect.contains(p.x, p.y)){
			val = INPUT;
		}else if (outputRect.contains(p.x , p.y)){
			val = OUTPUT;
			toggleDisplayResultType();
		}
		return val;
	}
	
	@Override
	public void onResultReceived(TextArea result, DataToken resultDataToken) {
		// TODO Auto-generated method stub
		//get the result token
		if (resultDataToken != null){
			this.resultDataToken = resultDataToken;
			if (resultDataToken.type == DataToken.RESULT_TOKEN){
				ResultToken resultToken = (ResultToken)resultDataToken;
				if (resultToken.isDecimal()){
					displayType = DECIMAL_DISPLAY;
				}else if (resultToken.isRRE()){
					displayType = RRE_DISPLAY;
				}
			}
		}
		
		output.clear();
		for (VisualToken token:result.getTokenList()){
			token.setParent(output.getRootTextArea());
			output.getRootTextArea().getTokenList().add(token);
			
			
			/*TextArea ta = output.getRootTextArea();
			FractionToken ft = new FractionToken(ta);
			TextArea num = ft.getNumerator();
			TextArea den = ft.getDenominator();
			
			StringToken s1 = new StringToken(num , "1");
			StringToken s2 = new StringToken(den , "2");
			
			ft.getNumerator().getTokenList().add(s1);
			ft.getDenominator().getTokenList().add(s2);
			
			ta.getTokenList().add(ft);
			ta.update_pass_1_chain_down();
			ta.update_pass_2();*/
		}
		output.getRootTextArea().update_pass_1_chain_down();
		output.getRootTextArea().update_pass_2();
		
		//output.getRootTextArea().update_pass_1(false);
		//output.getRootTextArea().update_pass_2();
		
		output.setDisabled(false);
		input.setDisabled(false);
		input.toggleCursor(false);
		
		rootLayout.updateLayout(displayArea);
		
		input.setTextAreaAlignment(TextComponent.BOTTOM_LEFT);
		alignTextAreas();
	}

	public void toggleDisplayResultType(){
		if (resultDataToken.type != DataToken.RESULT_TOKEN) return;
		
		ResultToken temp = (ResultToken) resultDataToken.clone(null);
		
		if (displayType == DECIMAL_DISPLAY && temp.isRRE()){
			displayType = RRE_DISPLAY;
			TextArea ta = ComputingThread.makeDisplayResult(ComputingThread.prepairDisplayResult(temp));
			onResultReceived(ta, null);
		}else if (displayType == RRE_DISPLAY){
			displayType = DECIMAL_DISPLAY;
			temp.setResultType(ResultToken.DECIMAL);
			TextArea ta = ComputingThread.makeDisplayResult(ComputingThread.prepairDisplayResult(temp));
			onResultReceived(ta, null);
		}
	}
	
	@Override
	public TextComponent getInputTextComponentToWorkWith() {
		// TODO Auto-generated method stub
		return (TextComponent) rootLayout.getBoxList().get(0);
	}
	
	public void alignTextAreas(){
		if (!input.isDisabled()){
			input.alignTextArea();
			input.getVCM().refreshCursor();
		}
		if (!output.isDisabled()){
			output.alignTextArea();
			output.getVCM().refreshCursor();
		}
	}
	

	/*public void test() {
		// TODO Auto-generated method stub
		LayoutBox input = rootLayout.getFullBoxList().get(0);
		LayoutBox output = rootLayout.getFullBoxList().get(1);
		input.setDisabled(true);
		output.setDisabled(false);
		rootLayout.updateBoxList();
		rootLayout.computeMinimumSize();
		rootLayout.placeTestLayout(displayArea);
		
		Log.d("","In Basic Visual Mode test(): displayArea size: " + displayArea.width() + " " + displayArea.height() );
		
		alignTextAreas();
	}*/

}
