package computing;

import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;

import android.util.Log;
import visualToken.FractionToken;
import visualToken.TextArea;
import visualToken.VisualToken;

public class RationalRootExpression {
	
	public static final int NUMERATOR = 0;
	public static final int DENOMINATOR = 1;
	
	private RootExpression numeratorExpression;
	private RootExpression denominatorExpression;

	public RationalRootExpression() {
		// TODO Auto-generated constructor stub
		numeratorExpression = new RootExpression();
		denominatorExpression = new RootExpression();
	}
	
	public RationalRootExpression(RationalRootExpression rre){
		numeratorExpression = new RootExpression(rre.numeratorExpression);
		denominatorExpression = new RootExpression(rre.denominatorExpression);
	}
	
	public RationalRootExpression(RootExpression numeratorExpression, RootExpression denominatorExpression){
		this.numeratorExpression = new RootExpression(numeratorExpression);
		this.denominatorExpression = new RootExpression(denominatorExpression);
	}
	
	public void add(SquareRootNumber term, int expression){
		RootExpression temp = null;
		switch (expression) {
		case NUMERATOR:
			temp = numeratorExpression;
			break;
		case DENOMINATOR:
			temp = denominatorExpression;
			break;
		default:
			break;
		}
		if (temp != null) temp.add(term);
	}
	public void remove(int index, int expression){
		RootExpression temp = null;
		switch (expression) {
		case NUMERATOR:
			temp = numeratorExpression;
			break;
		case DENOMINATOR:
			temp = denominatorExpression;
			break;
		default:
			break;
		}
		if (temp != null) temp.remove(index);
	}
	
	public boolean isRationalizeable(){
		if (denominatorExpression.size() <= 2) return true;
		return false;
	}
	
	public RationalRootExpression simplify() throws PrimeLimitReachedException, EmptyExpressionException{
		if (numeratorExpression.size() == 0 || denominatorExpression.size() == 0) throw new EmptyExpressionException("numerator or denominator is empty");
		RationalRootExpression res = new RationalRootExpression();
		res.numeratorExpression = new RootExpression(numeratorExpression);
		res.denominatorExpression = new RootExpression(denominatorExpression);
		
		
		if (res.onlyContainsRationalNumbers()){
			RationalNumber r = res.toRational().simplify();
			SquareRootNumber sr = new SquareRootNumber(RationalNumber.ONE, r);
			res.numeratorExpression = new RootExpression();
			res.denominatorExpression = new RootExpression();
			res.add(sr, NUMERATOR);
			res.add(SquareRootNumber.ONE, DENOMINATOR);
		}else{ 
			if ((denominatorExpression.size() == 1 && !denominatorExpression.get(0).simplify().isRational())
				|| denominatorExpression.size() == 2){
			RootExpression conj = denominatorExpression.getFactorToRationalize();
			res.numeratorExpression = res.numeratorExpression.multiply(conj).simplify();
			res.denominatorExpression = res.denominatorExpression.multiply(conj).simplify();
			}
			res.denominatorExpression = res.denominatorExpression.convertToSameDenominators();
			BigInteger bcd = res.denominatorExpression.get(0).getCoefficient().getDenominator();
			SquareRootNumber sr = new SquareRootNumber(RationalNumber.ONE, new RationalNumber(bcd, BigInteger.ONE));
			res.numeratorExpression = res.numeratorExpression.multiply(sr);
			res.denominatorExpression = res.denominatorExpression.multiply(sr);
			
			SquareRootNumber gcd = new SquareRootNumber(RationalNumber.ONE, res.numeratorExpression.gcd(res.denominatorExpression));
			res.numeratorExpression = res.numeratorExpression.divide(gcd).simplify();
			res.denominatorExpression = res.denominatorExpression.divide(gcd).simplify();
		}
		if (res.denominatorExpression.get(0).signum() < 0){
			res.numeratorExpression = res.numeratorExpression.negate();
			res.denominatorExpression = res.denominatorExpression.negate();
		}
		
		return res;
	}
	
	public RationalRootExpression add(RationalRootExpression ex){
		RootExpression den = denominatorExpression.multiply(ex.denominatorExpression);
		RootExpression num1 = numeratorExpression.multiply(ex.denominatorExpression);
		RootExpression num2 = denominatorExpression.multiply(ex.numeratorExpression);
		RootExpression num = num1.add(num2);
		RationalRootExpression res = new RationalRootExpression(num, den);
		return res;
	}
	
	public RationalRootExpression substract(RationalRootExpression ex){
		RootExpression den = denominatorExpression.multiply(ex.denominatorExpression);
		RootExpression num1 = numeratorExpression.multiply(ex.denominatorExpression);
		RootExpression num2 = denominatorExpression.multiply(ex.numeratorExpression);
		RootExpression num = num1.subtract(num2);
		RationalRootExpression res = new RationalRootExpression(num, den);
		return res;
	}
	
	public RationalRootExpression multiply(RationalRootExpression ex){
		RootExpression den = denominatorExpression.multiply(ex.denominatorExpression);
		RootExpression num = numeratorExpression.multiply(ex.numeratorExpression);
		RationalRootExpression res = new RationalRootExpression(num, den);
		return res;
	}
	
	public RationalRootExpression divide(RationalRootExpression ex){
		RootExpression den = denominatorExpression.multiply(ex.numeratorExpression);
		RootExpression num = numeratorExpression.multiply(ex.denominatorExpression);
		RationalRootExpression res = new RationalRootExpression(num, den);
		return res;
	}
	
	public BigDecimal toBigDecimal(){
		BigDecimal numBD = numeratorExpression.toBigDecimal();
		BigDecimal denBD = denominatorExpression.toBigDecimal();
		BigDecimal res = numBD.divide(denBD, DataProcessor.BIG_DECIMAL_SCALE, DataProcessor.BIG_DECIMAL_DEFAULT_ROUNDING_MODE);
		return res;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		String s = new String();
		s += numeratorExpression.toString();
		s += " / ";
		s += denominatorExpression.toString();
		return s;
	}
	
	public ArrayList<VisualToken> toVisualTokens(TextArea parent, int expression){
		RootExpression temp = null;
		switch (expression) {
		case NUMERATOR:
			temp = numeratorExpression;
			break;
		case DENOMINATOR:
			temp = denominatorExpression;
			break;
		default:
			break;
		}
		ArrayList<VisualToken> list = new ArrayList<VisualToken>();
		if (temp != null){
			list = temp.toVisualTokens(parent);
		}
		return list;
	}
	
	public FractionToken toFractionToken(TextArea parent){
		FractionToken ft = new FractionToken(parent);
		ArrayList<VisualToken> numList = numeratorExpression.toVisualTokens(ft.getNumerator());
		ArrayList<VisualToken> denomList = denominatorExpression.toVisualTokens(ft.getDenominator());

		ft.getNumerator().getTokenList().addAll(numList);
		ft.getDenominator().getTokenList().addAll(denomList);
		return ft;
	}
	
	public boolean denominatorIsOne(){
		if (denominatorExpression.size() == 1 && 
				denominatorExpression.get(0).compareTo(SquareRootNumber.ONE) == 0){
			return true;
		}
		return false;
	}

	public boolean onlyContainsRationalNumbers(){
		for (int i = 0; i < numeratorExpression.size(); i++){
			SquareRootNumber term = numeratorExpression.get(i);
			if (!term.isRational()) return false;
		}
		for (int i = 0; i < denominatorExpression.size(); i++){
			SquareRootNumber term = denominatorExpression.get(i);
			if (!term.isRational()) return false;
		}
		return true;
	}
	
	public boolean isTooLong(){
		if (numeratorExpression.size() > DataProcessor.RRE_MAX_TERMS_NUMBER || 
				denominatorExpression.size() > DataProcessor.RRE_MAX_TERMS_NUMBER){
			return true;
		}
		
		for (int i = 0; i < numeratorExpression.size(); i++){
			SquareRootNumber term = numeratorExpression.get(i);
			if (term.toBigDecimal().compareTo(new BigDecimal("1e" + DataProcessor.RRE_MAX_TERM_VALUE_EXP)) > 0
					|| term.toBigDecimal().compareTo(new BigDecimal("1e-" + DataProcessor.RRE_MAX_TERM_VALUE_EXP)) < 0){
				return true;
			}
		}
		
		for (int i = 0; i < denominatorExpression.size(); i++){
			SquareRootNumber term = denominatorExpression.get(i);
			if (term.toBigDecimal().compareTo(new BigDecimal("1e" + DataProcessor.RRE_MAX_TERM_VALUE_EXP)) > 0
					|| term.toBigDecimal().compareTo(new BigDecimal("1e-" + DataProcessor.RRE_MAX_TERM_VALUE_EXP)) < 0){
				return true;
			}
		}
		return false;
	}
	
	public RationalNumber toRational(){
		RationalNumber numSum = RationalNumber.ZERO;
		RationalNumber denomSum = RationalNumber.ZERO;
		for (int i = 0; i < numeratorExpression.size(); i++){
			SquareRootNumber term = numeratorExpression.get(i);
			if (term.compareTo(SquareRootNumber.ZERO) != 0){
				numSum = numSum.add(term.getCoefficient());
			}
		}
		for (int i = 0; i < denominatorExpression.size(); i++){
			SquareRootNumber term = denominatorExpression.get(i);
			if (term.compareTo(SquareRootNumber.ZERO) != 0){
				denomSum = denomSum.add(term.getCoefficient());
			}
		}
		RationalNumber res = numSum.divide(denomSum);
		return res;
	}
	public static void main(String[] args) throws FileNotFoundException, PrimeLimitReachedException, EmptyExpressionException {
		PrimeMath.loadData();
		SquareRootNumber sr1 = new SquareRootNumber("2", "6");
		SquareRootNumber sr2 = new SquareRootNumber("5", "8");
		SquareRootNumber sr3 = new SquareRootNumber("1", "4");
		SquareRootNumber sr4 = new SquareRootNumber("3", "2");
		SquareRootNumber sr5 = new SquareRootNumber("5", "10");
		
		RootExpression re = new RootExpression();
		RootExpression re1 = new RootExpression();
		re.add(sr1);
		re.add(sr2);
		re1.add(sr3);
		//re1.add(sr4);
		//re1.add(sr5);
		
		System.out.println(re);
		System.out.println(re1);
		
		RationalRootExpression rre = new RationalRootExpression(re, re1);
		System.out.println(rre);
		System.out.println(rre.simplify());
	}

}
