package computing;

public class EmptyExpressionException extends Exception {

	public EmptyExpressionException() {
		// TODO Auto-generated constructor stub
	}

	public EmptyExpressionException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public EmptyExpressionException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public EmptyExpressionException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
