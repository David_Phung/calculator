package computing;


import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.ArrayList;

import visualToken.FractionToken;
import visualToken.StringToken;
import visualToken.TextArea;
import visualToken.VisualToken;

/**
 * 
 * <p><b><i>   </i></b></p>
 * Both numerator and denominator are positive integers.
 * Sign determined by "sign" attribute.
 * @author ThePhuong
 *
 */
public class RationalNumber implements Comparable<RationalNumber>{
	
	public static int MAX_ACCEPTABLE_DECIMAL_LENGTH = 100;
	public static final RationalNumber ZERO = new RationalNumber(BigInteger.ZERO, BigInteger.ONE);
	public static final RationalNumber ONE = new RationalNumber(BigInteger.ONE, BigInteger.ONE);
	
	public static RationalNumber convertDecimalToRational_withoutPatternRecognition(BigDecimal val){
		BigDecimal remainder = val.remainder(BigDecimal.ONE).abs();
		int length = remainder.toPlainString().length() - 2;
		if (length > MAX_ACCEPTABLE_DECIMAL_LENGTH) throw new IllegalArgumentException("Input decimal part longer than MAX_ACCEPTABLE_DECIMAL_LENGTH");
		RationalNumber res = new RationalNumber(val.toBigInteger().abs(), new BigInteger("1"));
		System.out.println("l = " + length + " " + remainder);
		if (length > 0 && remainder.signum() != 0){
			String s = "1";
			for (int i = 0; i < length; i++){
				s += "0";
			}
			remainder = remainder.multiply(new BigDecimal(s));
			BigInteger num = remainder.toBigIntegerExact();
			BigInteger den = new BigInteger(s);
			RationalNumber temp = new RationalNumber(num, den);
			res = res.add(temp);
		}
		res.sign = val.signum();
		return res;
	}
	
	
	private BigInteger numerator, denominator;
	private int sign;
	public RationalNumber(BigInteger numerator, BigInteger denominator) {
		super();
		this.numerator = numerator.abs();
		this.denominator = denominator.abs();
		if (denominator.signum() == 0) throw new ArithmeticException("Denominator is 0");
		else sign = numerator.signum() * denominator.signum();
	}
	
	public RationalNumber(String numerator, String denominator){
		BigInteger num = new BigInteger(numerator);
		BigInteger den = new BigInteger(denominator);
		this.numerator = num.abs();
		this.denominator = den.abs();
		if (den.signum() == 0) throw new ArithmeticException("Denominator is 0");
		else sign = num.signum() * den.signum();
	}

	public RationalNumber simplify(){
		if (numerator.signum() == 0){
			RationalNumber res = new RationalNumber(numerator, denominator);
			res.sign = 0;
			return res;
		}else{
			BigInteger gcd = numerator.gcd(denominator);
			BigInteger newNum = numerator.divide(gcd);
			BigInteger newDen = denominator.divide(gcd);
			if (sign == -1) newNum = newNum.negate();
			return new RationalNumber(newNum, newDen);
		}
	}
	
	public RationalNumber add(RationalNumber val){
		BigInteger gcd = denominator.gcd(val.denominator);
		BigInteger bcd = denominator.multiply(val.denominator).divide(gcd);
		BigInteger newNum1 = bcd.divide(denominator).multiply(numerator).multiply(BigInteger.valueOf(sign));
		BigInteger newNum2 = bcd.divide(val.denominator).multiply(val.numerator).multiply(BigInteger.valueOf(val.sign));
		BigInteger newNum = newNum1.add(newNum2);
		RationalNumber res = new RationalNumber(newNum, bcd);
		return res;
	}
	
	public RationalNumber subtract(RationalNumber val){
		BigInteger gcd = denominator.gcd(val.denominator);
		BigInteger bcd = denominator.multiply(val.denominator).divide(gcd);
		BigInteger newNum1 = bcd.divide(denominator).multiply(numerator).multiply(BigInteger.valueOf(sign));
		BigInteger newNum2 = bcd.divide(val.denominator).multiply(val.numerator).multiply(BigInteger.valueOf(val.sign));
		BigInteger newNum = newNum1.subtract(newNum2);
		RationalNumber res = new RationalNumber(newNum, bcd);
		return res;
	}
	
	public RationalNumber multiply(RationalNumber val){
		BigInteger newNum = numerator.multiply(val.numerator);
		BigInteger newDen = denominator.multiply(val.denominator);
		newNum = newNum.multiply(BigInteger.valueOf(sign)).multiply(BigInteger.valueOf(val.sign));
		RationalNumber res = new RationalNumber(newNum, newDen);
		return res;
	}
	
	public RationalNumber divide(RationalNumber val){
		BigInteger newNum = numerator.multiply(val.denominator);
		BigInteger newDen = denominator.multiply(val.numerator);
		newNum = newNum.multiply(BigInteger.valueOf(sign)).multiply(BigInteger.valueOf(val.sign));
		RationalNumber res = new RationalNumber(newNum, newDen);
		return res;
	}
	
	public RationalNumber mod(BigInteger val){
		BigInteger temp = val.multiply(denominator);
		temp = numerator.mod(temp);
		return new RationalNumber(temp, denominator);
	}
	
	public RationalNumber negate(){
		RationalNumber res = new RationalNumber(numerator, denominator);
		res.sign = -sign;
		return res;
	}
	
	public RationalNumber gcd(RationalNumber val){
		BigInteger gcdNum = numerator.gcd(val.numerator);
		BigInteger gcdDen = denominator.gcd(val.denominator);
		return new RationalNumber(gcdNum, gcdDen);
	}
	
	public boolean isNumeratorEven(){
		BigInteger r = numerator.mod(BigInteger.valueOf(2));
		if (r.signum() == 0) return true;
		return false;
	}
	
	public boolean isDenominatorEven(){
		BigInteger r = denominator.mod(BigInteger.valueOf(2));
		if (r.signum() == 0) return true;
		return false;
	}
	
	public int compareTo(RationalNumber o) {
		// TODO Auto-generated method stub
		RationalNumber diff = this.subtract(o);
		return diff.sign;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		if (sign == 0) return "0";
		String s = new String();
		if (sign == -1) s+= "-";
		if (denominator.compareTo(BigInteger.ONE) == 0){
			s+= numerator.toString();
		}else{
			s += numerator.toString() + "/" + denominator.toString();
		}
		return s;
	}
	
	public ArrayList<VisualToken> toVisualTokens(TextArea parent){
		ArrayList<VisualToken> list = new ArrayList<VisualToken>();
		if (sign < 0) list.add(new StringToken(parent, "-"));
		
		if (denominator.compareTo(BigInteger.ONE) == 0){
			list.add(new StringToken(parent, numerator.toString()));
		}else if(numerator.compareTo(BigInteger.ZERO) == 0){
			list.add(new StringToken(parent, "0"));
		}else{
			FractionToken ft = new FractionToken(parent);
			StringToken s1 = new StringToken(ft.getNumerator() , numerator.toString());
			StringToken s2 = new StringToken(ft.getDenominator() , denominator.toString());
			ft.getNumerator().getTokenList().add(s1);
			ft.getDenominator().getTokenList().add(s2);
			list.add(ft);
		}
		return list;
	}
	
	public BigDecimal toBigDecimal(){
		BigDecimal numBD = new BigDecimal(numerator);
		BigDecimal denBD = new BigDecimal(denominator);
		BigDecimal res = numBD.divide(denBD, DataProcessor.BIG_DECIMAL_SCALE, DataProcessor.BIG_DECIMAL_DEFAULT_ROUNDING_MODE);
		res = res.multiply(BigDecimal.valueOf(sign));
		return res;
	}
	
	public int signum(){
		return sign;
	}
	
	public BigDecimal toBigDecimal(int scale, RoundingMode roundingMode){
		BigDecimal numBD = new BigDecimal(numerator.toString());
		BigDecimal denBD = new BigDecimal(denominator.toString());
		return numBD.divide(denBD, scale, roundingMode).multiply(BigDecimal.valueOf(sign));
	}
	
	
	public BigInteger getNumerator() {
		return numerator;
	}

	public BigInteger getDenominator() {
		return denominator;
	}

	public static void main(String[] args) {
		BigInteger a = new BigInteger("1");
		BigInteger b = new BigInteger("3");
		BigInteger c = new BigInteger("-3");
		BigInteger d = new BigInteger("-4");
		RationalNumber r1 = new RationalNumber(a, b);
		RationalNumber r2 = new RationalNumber(c, d);
		System.out.println(r1.divide(r2).simplify());
		
		System.out.println(convertDecimalToRational_withoutPatternRecognition(new BigDecimal("1.123")));
	}

}
