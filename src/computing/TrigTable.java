package computing;

public class TrigTable {
	
	public static RationalRootExpression[] sinTable;
	public static RationalRootExpression[] cosTable;
	public static RationalRootExpression[] tanTable;

	public TrigTable() {
		// TODO Auto-generated constructor stub
	}
	
	public static void initTables(){
		sinTable = new RationalRootExpression[7];
		cosTable = new RationalRootExpression[7];
		tanTable = new RationalRootExpression[6];
		
		SquareRootNumber root_6 = new SquareRootNumber("6", "1");
		SquareRootNumber root_2 = new SquareRootNumber("2", "1");
		SquareRootNumber root_3 = new SquareRootNumber("3", "1");
		SquareRootNumber integer_2 = new SquareRootNumber("1", "2");
		SquareRootNumber integer_3 = new SquareRootNumber("1", "3");
		SquareRootNumber integer_4 = new SquareRootNumber("1", "4");
		
		RationalRootExpression sin_0 = new RationalRootExpression();
		sin_0.add(SquareRootNumber.ZERO, RationalRootExpression.NUMERATOR);
		sin_0.add(SquareRootNumber.ONE, RationalRootExpression.DENOMINATOR);
		
		RationalRootExpression sin_15 = new RationalRootExpression();
		sin_15.add(root_6, RationalRootExpression.NUMERATOR);
		sin_15.add(root_2.negate(), RationalRootExpression.NUMERATOR);
		sin_15.add(integer_4, RationalRootExpression.DENOMINATOR);
		
		RationalRootExpression sin_30 = new RationalRootExpression();
		sin_30.add(SquareRootNumber.ONE, RationalRootExpression.NUMERATOR);
		sin_30.add(integer_2, RationalRootExpression.DENOMINATOR);
		
		RationalRootExpression sin_45 = new RationalRootExpression();
		sin_45.add(root_2, RationalRootExpression.NUMERATOR);
		sin_45.add(integer_2, RationalRootExpression.DENOMINATOR);
		
		RationalRootExpression sin_60 = new RationalRootExpression();
		sin_60.add(root_3, RationalRootExpression.NUMERATOR);
		sin_60.add(integer_2, RationalRootExpression.DENOMINATOR);
		
		RationalRootExpression sin_75 = new RationalRootExpression();
		sin_75.add(root_6, RationalRootExpression.NUMERATOR);
		sin_75.add(root_2, RationalRootExpression.NUMERATOR);
		sin_75.add(integer_4, RationalRootExpression.DENOMINATOR);
		
		RationalRootExpression sin_90 = new RationalRootExpression();
		sin_90.add(SquareRootNumber.ONE, RationalRootExpression.NUMERATOR);
		sin_90.add(SquareRootNumber.ONE, RationalRootExpression.DENOMINATOR);
		
		RationalRootExpression cos_0 = new RationalRootExpression(sin_90);
		RationalRootExpression cos_15 = new RationalRootExpression(sin_75);
		RationalRootExpression cos_30 = new RationalRootExpression(sin_60);
		RationalRootExpression cos_45 = new RationalRootExpression(sin_45);
		RationalRootExpression cos_60 = new RationalRootExpression(sin_30);
		RationalRootExpression cos_75 = new RationalRootExpression(sin_15);
		RationalRootExpression cos_90 = new RationalRootExpression(sin_0);
		
		RationalRootExpression tan_0 = new RationalRootExpression();
		tan_0.add(SquareRootNumber.ZERO, RationalRootExpression.NUMERATOR);
		tan_0.add(SquareRootNumber.ONE, RationalRootExpression.DENOMINATOR);
		
		RationalRootExpression tan_15 = new RationalRootExpression();
		tan_15.add(integer_2, RationalRootExpression.NUMERATOR);
		tan_15.add(root_3.negate(), RationalRootExpression.NUMERATOR);
		tan_15.add(SquareRootNumber.ONE, RationalRootExpression.DENOMINATOR);
		
		RationalRootExpression tan_30 = new RationalRootExpression();
		tan_30.add(root_3, RationalRootExpression.NUMERATOR);
		tan_30.add(integer_3, RationalRootExpression.DENOMINATOR);
		
		RationalRootExpression tan_45 = new RationalRootExpression();
		tan_45.add(SquareRootNumber.ONE, RationalRootExpression.NUMERATOR);
		tan_45.add(SquareRootNumber.ONE, RationalRootExpression.DENOMINATOR);
		
		RationalRootExpression tan_60 = new RationalRootExpression();
		tan_60.add(root_3, RationalRootExpression.NUMERATOR);
		tan_60.add(SquareRootNumber.ONE, RationalRootExpression.DENOMINATOR);
		
		RationalRootExpression tan_75 = new RationalRootExpression();
		tan_75.add(integer_2, RationalRootExpression.NUMERATOR);
		tan_75.add(root_3, RationalRootExpression.NUMERATOR);
		tan_75.add(SquareRootNumber.ONE, RationalRootExpression.DENOMINATOR);
		
		sinTable[0] = sin_0;
		sinTable[1] = sin_15;
		sinTable[2] = sin_30;
		sinTable[3] = sin_45;
		sinTable[4] = sin_60;
		sinTable[5] = sin_75;
		sinTable[6] = sin_90;
		
		cosTable[0] = cos_0;
		cosTable[1] = cos_15;
		cosTable[2] = cos_30;
		cosTable[3] = cos_45;
		cosTable[4] = cos_60;
		cosTable[5] = cos_75;
		cosTable[6] = cos_90;
		
		tanTable[0] = tan_0;
		tanTable[1] = tan_15;
		tanTable[2] = tan_30;
		tanTable[3] = tan_45;
		tanTable[4] = tan_60;
		tanTable[5] = tan_75;
	}

	public static RationalRootExpression checkSinTable(TrigExpression angleInRadian){
		TrigExpression simplifiedAngle = angleInRadian.simplifyAngle();
		if (simplifiedAngle == null || angleInRadian.getConstant().compareTo(RationalNumber.ZERO) != 0) return null;
		RationalNumber one_twelvth = new RationalNumber("1", "12");
		RationalNumber temp = simplifiedAngle.getCoeff().divide(one_twelvth).simplify();
		int index = toIndex(temp, 0 , 6);
		if (index != -1){
			return new RationalRootExpression(sinTable[index]);
		}
		return  null;
	}
	
	public static RationalRootExpression checkCosTable(TrigExpression angleInRadian){
		TrigExpression simplifiedAngle = angleInRadian.simplifyAngle();
		if (simplifiedAngle == null || angleInRadian.getConstant().compareTo(RationalNumber.ZERO) != 0) return null;
		RationalNumber one_twelvth = new RationalNumber("1", "12");
		RationalNumber temp = simplifiedAngle.getCoeff().divide(one_twelvth).simplify();
		int index = toIndex(temp, 0, 6);
		if (index != -1){
			return new RationalRootExpression(cosTable[index]);
		}
		return  null;
	}
	
	public static RationalRootExpression checkTanTable(TrigExpression angleInRadian){
		TrigExpression simplifiedAngle = angleInRadian.simplifyAngle();
		if (simplifiedAngle == null || angleInRadian.getConstant().compareTo(RationalNumber.ZERO) != 0) return null;
		RationalNumber one_twelvth = new RationalNumber("1", "12");
		RationalNumber temp = simplifiedAngle.getCoeff().divide(one_twelvth).simplify();
		int index = toIndex(temp, 0, 5);
		if (index != -1){
			return new RationalRootExpression(tanTable[index]);
		}
		return  null;
	}
	
	private static int toIndex(RationalNumber val, int from, int to){
		for (int i = from; i <= to; i++){
			RationalNumber r = new RationalNumber(Integer.toString(i), "12");
			if (val.compareTo(r) == 0) return i;
		}
		return -1;
	}
}
