package computing;

import java.math.BigDecimal;
import java.math.BigInteger;

public class TrigExpression {
	
	private RationalNumber coeff;
	private RationalNumber constant;

	public TrigExpression(RationalNumber coeff, RationalNumber constant) {
		// TODO Auto-generated constructor stub
		this.coeff = coeff;
		this.constant = constant;
	}
	
	public TrigExpression(String coeff, String constant){
		String temp[] = constant.split("[/]");
		RationalNumber constantR = new RationalNumber(temp[0], "1");
		if (temp.length == 2) constantR = new RationalNumber(temp[0], temp[1]);
		
		temp = coeff.split("[/]");
		RationalNumber co = new RationalNumber(temp[0], "1");
		if (temp.length == 2) co = new RationalNumber(temp[0], temp[1]);
		
		this.constant = constantR;
		this.coeff = co;
	}
	
	public TrigExpression(TrigExpression trigEx){
		this.coeff = trigEx.coeff;
		this.constant = trigEx.constant;
	}
	
	public TrigExpression simplify(){
		RationalNumber newCoeff = coeff.simplify();
		RationalNumber newConstant = constant.simplify();
		TrigExpression res = new TrigExpression(newCoeff, newConstant);
		return res;
	}
	
	public TrigExpression add(TrigExpression ex){
		RationalNumber newCoeff = coeff.add(ex.coeff);
		RationalNumber newConstant = constant.add(ex.constant);
		TrigExpression res = new TrigExpression(newCoeff, newConstant);
		return res;
	}
	
	public TrigExpression subtract(TrigExpression ex){
		RationalNumber newCoeff = coeff.subtract(ex.coeff);
		RationalNumber newConstant = constant.subtract(ex.constant);
		TrigExpression res = new TrigExpression(newCoeff, newConstant);
		return res;
	}
	
	public TrigExpression add(RationalNumber val){
		RationalNumber newConstant = constant.add(val);
		TrigExpression res = new TrigExpression(coeff, newConstant);
		return res;
	}
	
	public TrigExpression subtract(RationalNumber val){
		RationalNumber newConstant = constant.subtract(val);
		TrigExpression res = new TrigExpression(coeff, newConstant);
		return res;
	}
	
	public TrigExpression multiply(RationalNumber val){
		RationalNumber newCoeff = coeff.multiply(val);
		RationalNumber newConstant = constant.multiply(val);
		TrigExpression res = new TrigExpression(newCoeff, newConstant);
		return res;
	}
	
	public TrigExpression divide(RationalNumber val){
		RationalNumber newCoeff = coeff.divide(val);
		RationalNumber newConstant = constant.divide(val);
		TrigExpression res = new TrigExpression(newCoeff, newConstant);
		return res;
	}
	
	public TrigExpression negate(){
		RationalNumber newCoeff = coeff.negate();
		RationalNumber newConstant = constant.negate();
		TrigExpression res = new TrigExpression(newCoeff, newConstant);
		return res;
	}
	
	public BigDecimal toDegree(){
		RationalNumber one_eighty = new RationalNumber("180", "1");
		RationalNumber degree = coeff.multiply(one_eighty);
		degree = degree.add(constant);
		return degree.toBigDecimal(DataProcessor.BIG_DECIMAL_SCALE, DataProcessor.BIG_DECIMAL_DEFAULT_ROUNDING_MODE);
	}
	
	public BigDecimal toBigDecimal(){
		BigDecimal res = coeff.toBigDecimal(DataProcessor.BIG_DECIMAL_SCALE, DataProcessor.BIG_DECIMAL_DEFAULT_ROUNDING_MODE);
		res = res.multiply(BigDecimalFunctions.PI);
		res = res.add(constant.toBigDecimal(DataProcessor.BIG_DECIMAL_SCALE, DataProcessor.BIG_DECIMAL_DEFAULT_ROUNDING_MODE));
		return res;
	}
	
	public TrigExpression simplifyAngle(){
		if (constant.compareTo(RationalNumber.ZERO) == 0){
			RationalNumber newCoeff = coeff.mod(new BigInteger("2"));
			return new TrigExpression(newCoeff, constant);
		}
		return null;
	}

	public RationalNumber getCoeff() {
		return coeff;
	}

	public RationalNumber getConstant() {
		return constant;
	}
	
	
}
