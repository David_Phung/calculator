package computing;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;


public class PrimeMath {
	
	public static int[] primeList_100_000 = new int[100000];

	private ArrayList<BigInteger> primeList;

	public PrimeMath() {
		// TODO Auto-generated constructor stub
		primeList = new ArrayList<BigInteger>();

	}
	
	public static void loadData() throws FileNotFoundException{
		Scanner sc = new Scanner(new File("primes_100_000.txt"));
		for (int i = 0; i < 100000; i++){
			primeList_100_000[i] = sc.nextInt();
		}
		int[][] f = null;
		try {
			f = primeFactorize(new BigInteger("72"));
		} catch (PrimeLimitReachedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static Iterator<Integer> prime_100_000_Iterator(){
		return new Prime_100_000_Iterator();
	}

	public static boolean isPrime(BigInteger val) {
		if (val.signum() < 0)
			throw new IllegalArgumentException("val < 0");
		if (val.compareTo(BigInteger.ZERO) == 0
				|| val.compareTo(BigInteger.ONE) == 0)
			return false;
		if (val.compareTo(new BigInteger("2")) == 0)
			return true;

		BigDecimal sqrtValBD = BigDecimalFunctions.sqrt(new BigDecimal(val), 9);
		BigInteger sqrtVal = sqrtValBD.toBigInteger();
		sqrtVal = sqrtVal.add(new BigInteger("1"));
		BigInteger i = new BigInteger("2");

		while (i.compareTo(sqrtVal) <= 0) {
			BigInteger remainder = val.mod(i);
			if (remainder.signum() == 0)
				return false;
			i = i.add(BigInteger.ONE);
		}
		return true;
	}

	/**
	 * 
	 * <p><b><i>  public static int[][] primeFactorize(BigInteger val) </i></b></p>
	 * return a 2d array with 2 lines. First line contains prime numbers. Second line contains corresponding exponent.
	 * throws LimitReachedException if the prime factors exceed the limit allowed by the PrimeMath class.
	 * @param val
	 * @return
	 * @throws PrimeLimitReachedException 
	 */
	public static int[][] primeFactorize(BigInteger val) throws PrimeLimitReachedException{
		Iterator<Integer> ite = prime_100_000_Iterator();
		BigInteger temp = val;
		ArrayList<Integer> factors = new ArrayList<Integer>();
		ArrayList<Integer> exponents = new ArrayList<Integer>();
		int index = -1;
		
		while (temp.compareTo(BigInteger.ONE) > 0){
			
			if (!ite.hasNext()){
				throw new PrimeLimitReachedException("Max prime number reached");
			}
			
			int p = ite.next();
			if (temp.mod(BigInteger.valueOf(p)).signum() == 0){
				index++;
				factors.add(p);
				exponents.add(0);
			}
			while (temp.mod(BigInteger.valueOf(p)).signum() == 0){
				int ex = exponents.get(index);
				exponents.set(index, ex + 1);
				temp = temp.divide(BigInteger.valueOf(p));
			}
		}
		
		int[][] res = new int[2][factors.size()];
		for (int i = 0; i < factors.size(); i++){
			res[0][i] = factors.get(i);
			res[1][i] = exponents.get(i);
		}
		return res;
	}
	
	public void generatePrimes(BigInteger cap) {

	}
	
	private static class Prime_100_000_Iterator implements Iterator<Integer>{
		
		private int count;
		
		public Prime_100_000_Iterator() {
			// TODO Auto-generated constructor stub
			count = 0;
		}

		public boolean hasNext() {
			// TODO Auto-generated method stub
			return count < 100000;
		}

		public Integer next() {
			// TODO Auto-generated method stub
			int p = primeList_100_000[count];
			count++;
			return new Integer(p);
		}

		public void remove() {
			// TODO Auto-generated method stub
			throw new UnsupportedOperationException();
		}
		
	}
	
	/*public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException {
		PrintWriter writer = new PrintWriter("primes_100_000.txt", "UTF-8");
		int nr = 2;
		int count = 0;
		while (count < 100000){
			BigInteger val = new BigInteger(Integer.toString(nr));
			if (isPrime(val)) {
				writer.println(nr);
				count ++;
			}
			nr++;
		}
		System.out.println(count);
		writer.close();
	}*/
	
	public static void main(String[] args) throws FileNotFoundException {
		
		Scanner sc = new Scanner(new File("primes_100_000.txt"));
		for (int i = 0; i < 100000; i++){
			primeList_100_000[i] = sc.nextInt();
		}
		int[][] f = null;
		try {
			f = primeFactorize(new BigInteger("72"));
		} catch (PrimeLimitReachedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(f.length);
		System.out.println(f[0].length);
		System.out.println();
		for (int i = 0; i < f.length; i++){
			for (int j = 0; j < f[i].length; j++){
				System.out.print(f[i][j] + " ");
			}
			System.out.println();
		}
	}

}
