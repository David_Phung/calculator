package computing;

import java.io.BufferedReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Stack;

import com.example.calculator03.Calculator03;

import android.util.Log;
import dataToken.BinaryOperator;
import dataToken.ContainerToken;
import dataToken.DataToken;
import dataToken.DigitToken;
import dataToken.Expression;
import dataToken.FlagToken;
import dataToken.FunctionToken;
import dataToken.NumberToken;
import dataToken.OperatorToken;
import dataToken.ParenthesisDataToken;
import dataToken.PowerToDataToken;
import dataToken.ResultToken;
import dataToken.UnaryOperator;

public class DataProcessor {
	
	public static final int BIG_DECIMAL_SCALE = 64;
	public static final int BIG_DECIMAL_DEFAULT_ROUNDING = BigDecimal.ROUND_HALF_EVEN;
	public static final RoundingMode BIG_DECIMAL_DEFAULT_ROUNDING_MODE = RoundingMode.HALF_EVEN;
	public static final int MAX_SHORT_DECIMAL_DIGIT = 4;
	
	public static final int DISPLAY_RESULT_SCALE = 4;
	public static final RoundingMode DISPLAY_RESULT_ROUNDING = RoundingMode.HALF_UP;
	
	public static final int DEFAULT_RESULT_LENGTH = 10;
	public static final int MAX_INT_LENGTH = 100;
	
	public static final int RRE_MAX_TERM_VALUE_EXP = 5;
	public static final int RRE_MAX_TERMS_NUMBER = 4;
	
	public static final String NUM_TRIG_EXCEPTION = "Num-trig";
	
	public DataProcessor() {
		// TODO Auto-generated constructor stub
	}
	
	public void setReferences(Calculator03 mainActivity){
		BufferedReader br = mainActivity.getBr_primes_100_000();
		for (int i = 0; i < 100000; i++){
			String line = null;
			try {
				line = br.readLine();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (line == null) break;
			int p = Integer.parseInt(line);
			PrimeMath.primeList_100_000[i] = p;
		}
	}
	
	public void combineDigitToken(Expression inputExpression){
		int a = -1; //index of start digit, also a flag to show if we have a pending number token
		int b = -1; //b - 1 is index of end digit.
		
		//Loop through inputExpression
		for (int i = 0; i < inputExpression.getTokenList().size(); i++){
			DataToken token = inputExpression.getTokenList().get(i);
			//CASE DIGIT TOKEN
				if (token.type == DataToken.DIGIT_TOKEN){
				//if a == -1 means we don't have a pending token, make this token the start of a new number token
				if (a == -1){
					a = i;
				}else if (a >= 0){ // if a>=0 means we have a pending token
					
				}
				
				//CASE END OF EXPRESSION
				if (i == inputExpression.getTokenList().size() - 1){
					String number = new String();
					for (int j = a; j <= i;){
						DigitToken digit = (DigitToken) inputExpression.getTokenList().get(j);
						number += digit.getDigit();
						inputExpression.getTokenList().remove(j);
						i--;
					}
					NumberToken numberToken = new NumberToken(inputExpression, number);
					inputExpression.getTokenList().add(numberToken);
					i++;
					a = -1;
				}
			}else{//CASE NON_DIGIT TOKEN
				//if a == -1 means we dont have a pending token, do nothing
				if (a == -1){
					
				}else if (a >= 0){// if a>=0 means we have a pending token, we create a new number token and put digits in it
					String number = new String();
					for (int j = a; j < i;){
						DigitToken digit = (DigitToken) inputExpression.getTokenList().get(j);
						number += digit.getDigit();
						inputExpression.getTokenList().remove(j);
						i--;
					}
					NumberToken numberToken = new NumberToken(inputExpression, number);
					inputExpression.getTokenList().add(i, numberToken);
					i++;
					a = -1;
				}
				
				//if function token then ask it to combine recursively
				if (token.type == DataToken.FUNCTION_TOKEN){
					FunctionToken functionToken = (FunctionToken) token;
					for (int j = 0; j < functionToken.getNumberExpression(); j++){
						Expression ex = functionToken.getExpression(j);
						combineDigitToken(ex);
					}
				}
			}
		}
	}
	
	public void combineSignIntoEXPNumber(Expression inputExpression){
		for (int i = 0; i < inputExpression.getTokenList().size(); i++){
			DataToken token = inputExpression.getTokenList().get(i);
			if (token.type == DataToken.NUMBER_TOKEN){
				NumberToken nbrToken = (NumberToken)token;
				char lastChar = nbrToken.getNumber().charAt(nbrToken.getNumber().length() - 1);
				if (lastChar == DigitToken.EXP){
					DataToken tokenAfter = inputExpression.getTokenList().get(i + 1);
					if (tokenAfter.identifier.contains(ContainerToken.PRE_UNARY_NEGATIVE_SIGN_CONTAINER_TYPE_ID)
							|| tokenAfter.identifier.contains(ContainerToken.ADD_SUB_CONTAINER_TYPE_ID)){
						ContainerToken container = (ContainerToken)tokenAfter;
						if (container.getTokenList().size() == 1){
							DataToken tokenInside = container.getTokenList().get(0);
							if (tokenInside.identifier.contains(UnaryOperator.NEGATIVE_SIGN_ID)
									|| tokenInside.identifier.contains(BinaryOperator.SUBTRACTION_ID)){
								DataToken tokenAfter2 = inputExpression.getTokenList().get(i + 2);
								if (tokenAfter2.type == DataToken.NUMBER_TOKEN){
									NumberToken nbrToken2 = (NumberToken)tokenAfter2;
									String nbr2 = nbrToken2.getNumber();
									if (nbr2.charAt(0) == '-'){
										nbr2 = nbr2.substring(1);
									}else{
										nbr2 = "-" + nbr2;
									}
									nbrToken.setNumber(nbrToken.getNumber() + nbr2);
									inputExpression.getTokenList().remove(i + 2);
									inputExpression.getTokenList().remove(i + 1);
								}
							}
						}
					}
				}
			}else if (token.identifier.contains(ContainerToken.FUNCTION_CONTAINER_TYPE_ID)){
				ContainerToken container = (ContainerToken)token;
				for (int j = 0; j < container.getTokenList().size(); j++){
					DataToken tempToken = container.getTokenList().get(j);
					if (tempToken.type == DataToken.FUNCTION_TOKEN){
						FunctionToken functionToken = (FunctionToken)tempToken;
						for (int z = 0; z < functionToken.getNumberExpression(); z++){
							Expression ex = functionToken.getExpression(z);
							combineSignIntoEXPNumber(ex);
						}
					}
				}
			}
		}
	}
	
	public void checkSignAtBeginningOfExpression(Expression inputExpression){
		if (inputExpression.getTokenList().size() >= 2){
		if (inputExpression.getTokenList().get(1).type == DataToken.NUMBER_TOKEN
			&& inputExpression.getTokenList().get(0).identifier.contains(OperatorToken.BINARY_OPERATOR_ID)){
				BinaryOperator op = (BinaryOperator) inputExpression.getTokenList().get(0);
				if (op.binaryOperatorType == BinaryOperator.ADDITION){
					inputExpression.getTokenList().remove(0);
				}else if (op.binaryOperatorType == BinaryOperator.SUBTRACTION){
					NumberToken num = (NumberToken) inputExpression.getTokenList().get(1);
					num.setNumber("-" + num.getNumber());
					inputExpression.getTokenList().remove(0);
				}
			}
		}
		for (DataToken token:inputExpression.getTokenList()){
			if (token.type == DataToken.FUNCTION_TOKEN){
				FunctionToken functionToken = (FunctionToken) token;
				for (int j = 0; j < functionToken.getNumberExpression(); j++){
					Expression ex = functionToken.getExpression(j);
					checkEmptyExpressions(ex);
				}
			}
		}
	}
	
	private int replaceTokensWithContainer(Expression inputExpression, int pendingIndex, int currentIndex, int storedContainerType){
		int a = pendingIndex;
		int i = currentIndex;
		ContainerToken containerToken = new ContainerToken(inputExpression, storedContainerType);
		for (int j = a; j < i;){
			containerToken.getTokenList().add(inputExpression.getTokenList().get(j));
			inputExpression.getTokenList().remove(j);
			i--;
		}
		inputExpression.getTokenList().add(i, containerToken);
		i++;
		return i;
	}

	public void putTokensInContainers(Expression inputExpression){
		//pending flags and precedence
		int a = -1;
		int storedContainerType = -1;
		
		//LOOP
		for (int i = 0; i < inputExpression.getTokenList().size(); i++){
			DataToken token = inputExpression.getTokenList().get(i);
			//CASE END OF EXPRESSION
			if (i == inputExpression.getTokenList().size() - 1){
			
			}
			
			//CASE OPERATOR OR FUNCTION
			if ((token.type == DataToken.OPERATOR_TOKEN && !token.identifier.contains(OperatorToken.PARENTHES_ID)) 
					|| token.type == DataToken.FUNCTION_TOKEN){
				//case no pending
				if (a == -1){
					a = i;
					storedContainerType = ContainerToken.getContainerType(token);
				}else{	//case pending
					//case same container type
					if (storedContainerType == ContainerToken.getContainerType(token)){
						
					}else{//case difference container type
						i = replaceTokensWithContainer(inputExpression, a, i, storedContainerType);
						a = i;
						storedContainerType = ContainerToken.getContainerType(token);
					}
				}		
				//CASE END OF EXPRESSION
				if (i == inputExpression.getTokenList().size() - 1){
					i = replaceTokensWithContainer(inputExpression, a, i + 1, storedContainerType);
					a = -1;
				}
			}else{	//CASE NOT OPERATOR NOR FUNCTION
				//case no pending
				if (a == -1){

				}else{	//case pending
					i = replaceTokensWithContainer(inputExpression, a, i, storedContainerType);
					a = -1;
					storedContainerType = -1;
				}
			}	
			if (token.type == DataToken.FUNCTION_TOKEN){
				FunctionToken functionToken = (FunctionToken) token;
				for (int j = 0; j < functionToken.getNumberExpression(); j++){
					Expression ex = functionToken.getExpression(j);
					putTokensInContainers(ex);
				}
			}

		}
	}
	
	private boolean isInList(String stringToCheck, ArrayList<String> list){
		for (String s:list){
			if (stringToCheck.contains(s)) return true;
		}
		return false;
	}
	
	private boolean markSyntaxErrorByFilter(ArrayList<String> filter, Expression inputExpression, 
			DataToken tokenAfter, int currentIndex){
		String temp = "Nothing";
		if (tokenAfter == null && isInList(temp, filter)){
			FlagToken syntaxError = new FlagToken(inputExpression, FlagToken.SYNTAX_ERROR);
			inputExpression.getTokenList().add(syntaxError);
			return true;
		}else if (tokenAfter == null){
			return false;
		}
		
		String s = new String(tokenAfter.identifier);
		if (isInList(s, filter)){
			if (isInList(NUM_TRIG_EXCEPTION, filter)){
				ContainerToken container = (ContainerToken)tokenAfter;
				if (!container.getTokenList().isEmpty() && container.getTokenList().get(0).identifier.contains(UnaryOperator.IS_TRIG_ID)){
					return false;
				}
			}
			FlagToken syntaxError = new FlagToken(inputExpression, FlagToken.SYNTAX_ERROR);
			if (currentIndex >= inputExpression.getTokenList().size() - 1){
				inputExpression.getTokenList().add(syntaxError);
			}else{
				inputExpression.getTokenList().add(currentIndex + 1, syntaxError);
			}
			return true;
		}
		
		return false;
	}
	
	public void controlSyntaxErrorsOfContainers(Expression inputExpression){
		
		for (int i = 0; i < inputExpression.getTokenList().size(); i++){
			DataToken token = inputExpression.getTokenList().get(i);
			if (token.type != DataToken.CONTAINER_TOKEN){
				continue;
			}
			
			DataToken tokenAfter = null;
			if (i < inputExpression.getTokenList().size() - 1){
				tokenAfter = inputExpression.getTokenList().get(i + 1);
			}
			
			ArrayList<String> filter = new ArrayList<String>();
			
			ContainerToken containerToken = (ContainerToken) token;
			
			switch (containerToken.containerType) {
			case ContainerToken.ADD_SUB_CONTAINER_TYPE:
				filter.add(ContainerToken.MULT_DIV_MOD_CONTAINER_TYPE_ID);
				filter.add(ContainerToken.POWER_TO_CONTAINER_TYPE_ID);
				filter.add(ContainerToken.POST_UNARY_CONTAINER_TYPE_ID);
				filter.add(ParenthesisDataToken.CLOSE_ID);
				filter.add("Nothing");
				break;
			case ContainerToken.MULT_DIV_MOD_CONTAINER_TYPE:
				filter.add(ContainerToken.POWER_TO_CONTAINER_TYPE_ID);
				filter.add(ContainerToken.POST_UNARY_CONTAINER_TYPE_ID);
				filter.add(ParenthesisDataToken.CLOSE_ID);
				filter.add("Nothing");
				break;
			case ContainerToken.POWER_TO_CONTAINER_TYPE:
				filter.add(ContainerToken.MULT_DIV_MOD_CONTAINER_TYPE_ID);
				filter.add(ContainerToken.POST_UNARY_CONTAINER_TYPE_ID);
				filter.add(ParenthesisDataToken.CLOSE_ID);
				filter.add("Nothing");
				break;
			case ContainerToken.POST_UNARY_CONTAINER_TYPE:
				filter.add(ContainerToken.FUNCTION_CONTAINER_TYPE_ID);
				filter.add(ContainerToken.PRE_UNARY_CONTAINER_TYPE_ID);
				filter.add(ParenthesisDataToken.CLOSE_ID);
				break;
			case ContainerToken.PRE_UNARY_NEGATIVE_SIGN_CONTAINER_TYPE:
				
			case ContainerToken.PRE_UNARY_CONTAINER_TYPE:
				filter.add(ParenthesisDataToken.CLOSE_ID);
				filter.add(ContainerToken.ADD_SUB_CONTAINER_TYPE_ID);
				filter.add(ContainerToken.MULT_DIV_MOD_CONTAINER_TYPE_ID);
				filter.add(ContainerToken.POWER_TO_CONTAINER_TYPE_ID);
				filter.add(ContainerToken.POST_UNARY_CONTAINER_TYPE_ID);
				filter.add("Nothing");
				break;
			case ContainerToken.FUNCTION_CONTAINER_TYPE:
				filter.add(DataToken.NUMBER_TOKEN_ID);
				filter.add(ContainerToken.PRE_UNARY_CONTAINER_TYPE_ID);
				filter.add(ContainerToken.PRE_UNARY_NEGATIVE_SIGN_CONTAINER_TYPE_ID);
				filter.add(NUM_TRIG_EXCEPTION);
				
				ContainerToken containerFunctionToken = (ContainerToken)token;
				for (int j = 0; j < containerFunctionToken.getTokenList().size(); j++){
					FunctionToken functionToken = (FunctionToken) containerFunctionToken.getTokenList().get(j);
					for (int k = 0; k < functionToken.getNumberExpression(); k++){
						controlSyntaxErrorsOfContainers(functionToken.getExpression(k));
					}
				}
				break;
			
			default:
				break;
			}
			
			if (markSyntaxErrorByFilter(filter, inputExpression, tokenAfter, i)){
				i++;
			}
		}
	}

	
	public void autoAddTokenBetweenTokens(Expression inputExpression){
		for (int i = 0; i < inputExpression.getTokenList().size(); i++){
			DataToken token = inputExpression.getTokenList().get(i);
			DataToken tokenAfter = null;
			if (i < inputExpression.getTokenList().size() - 1){
				tokenAfter = inputExpression.getTokenList().get(i + 1);
			}
			
			if (token.identifier.contains(ContainerToken.FUNCTION_CONTAINER_TYPE_ID) ){
				if (tokenAfter != null && tokenAfter.identifier.contains(ParenthesisDataToken.OPEN_ID)){
					BinaryOperator mult = new BinaryOperator(inputExpression, BinaryOperator.MULTIPLCATION);
					inputExpression.getTokenList().add(i + 1, mult);
					i++;
				}else if (tokenAfter != null && tokenAfter.identifier.contains(ContainerToken.PRE_UNARY_CONTAINER_TYPE_ID)){
					ContainerToken containerToken = (ContainerToken)tokenAfter;
					if (!containerToken.getTokenList().isEmpty() && containerToken.getTokenList().get(0).identifier.contains(UnaryOperator.IS_TRIG_ID)){
						BinaryOperator mult = new BinaryOperator(inputExpression, BinaryOperator.MULTIPLCATION);
						inputExpression.getTokenList().add(i + 1, mult);
						i++;
					}
				}
				ContainerToken containerToken = (ContainerToken)token;
				for (int j = 0; j < containerToken.getTokenList().size(); j++){
					FunctionToken functionToken = (FunctionToken) containerToken.getTokenList().get(j);
					for (int k = 0; k < functionToken.getNumberExpression(); k++){
						autoAddTokenBetweenTokens(functionToken.getExpression(k));
					}
				}
			}
			
			if (token.identifier.contains(DataToken.NUMBER_TOKEN_ID) && tokenAfter != null){
				if (tokenAfter.identifier.contains(ContainerToken.FUNCTION_CONTAINER_TYPE_ID) || 
						tokenAfter.identifier.contains(ParenthesisDataToken.OPEN_ID)){
					BinaryOperator mult = new BinaryOperator(inputExpression, BinaryOperator.MULTIPLCATION);
					inputExpression.getTokenList().add(i + 1, mult);
					i++;
				}else if (tokenAfter.identifier.contains(ContainerToken.PRE_UNARY_CONTAINER_TYPE_ID)){
					ContainerToken containerToken = (ContainerToken)tokenAfter;
					if (!containerToken.getTokenList().isEmpty() && containerToken.getTokenList().get(0).identifier.contains(UnaryOperator.IS_TRIG_ID)){
						BinaryOperator mult = new BinaryOperator(inputExpression, BinaryOperator.MULTIPLCATION);
						inputExpression.getTokenList().add(i + 1, mult);
						i++;
					}
				}
			}
			
			if (token.identifier.contains(ParenthesisDataToken.CLOSE_ID) && tokenAfter != null){
				if (tokenAfter.identifier.contains(ParenthesisDataToken.OPEN_ID)){
					BinaryOperator mult = new BinaryOperator(inputExpression, BinaryOperator.MULTIPLCATION);
					inputExpression.getTokenList().add(i + 1, mult);
					i++;
				}
			}
			
			if (token.identifier.contains(ParenthesisDataToken.CLOSE_ID) && tokenAfter != null){
				if (tokenAfter.identifier.contains(ContainerToken.FUNCTION_CONTAINER_TYPE_ID) ||
						tokenAfter.identifier.contains(DataToken.NUMBER_TOKEN_ID)){
					BinaryOperator mult = new BinaryOperator(inputExpression, BinaryOperator.MULTIPLCATION);
					inputExpression.getTokenList().add(i + 1, mult);
					i++;
				}
			}
		}
	}
	
	public void autoAddTokenInsideContainers(Expression inputExpression){
		for (int i = 0; i < inputExpression.getTokenList().size(); i++){
			DataToken token = inputExpression.getTokenList().get(i);
			if (token.type != DataToken.CONTAINER_TOKEN){
				continue;
			}
			ContainerToken container = (ContainerToken)token;
			if (container.identifier.contains(ContainerToken.FUNCTION_CONTAINER_TYPE_ID)){
				for (int j = 0; j < container.getTokenList().size(); j++){
					DataToken curr = container.getTokenList().get(j);
					if ( j < container.getTokenList().size() - 1){
						DataToken after = container.getTokenList().get(j + 1); 
						if (curr.identifier.contains(DataToken.FUNCTION_TOKEN_ID)
								&& after.identifier.contains(DataToken.FUNCTION_TOKEN_ID) 
								&& !after.identifier.contains(FunctionToken.POWER_TO_DATA_TOKEN_ID)){
							BinaryOperator mult = new BinaryOperator(inputExpression, BinaryOperator.MULTIPLCATION);
							container.getTokenList().add(j + 1, mult);
							j++;
						}
					}
					if (curr.identifier.contains(DataToken.FUNCTION_TOKEN_ID)){
						for (int k = 0; k < ((FunctionToken)curr).getNumberExpression(); k++){
							Expression ex = ((FunctionToken)curr).getExpression(k);
							autoAddTokenInsideContainers(ex);
						}
					}
				}
			}
		}
	}

	public void controlSyntaxErrorsNumbers(Expression inputExpression){
		for (int i = 0; i < inputExpression.getTokenList().size(); i++){
			DataToken token = inputExpression.getTokenList().get(i);
			if (token.type == DataToken.NUMBER_TOKEN){
				
				if (i < inputExpression.getTokenList().size() - 1){
					DataToken tokenAfter = inputExpression.getTokenList().get(i + 1);
					ArrayList<String> filter = new ArrayList<String>();
					filter.add(ContainerToken.PRE_UNARY_CONTAINER_TYPE_ID);
					filter.add(ContainerToken.PRE_UNARY_NEGATIVE_SIGN_CONTAINER_TYPE_ID);
					filter.add(NUM_TRIG_EXCEPTION);
					markSyntaxErrorByFilter(filter, inputExpression, tokenAfter, i);
				}
				
				//Inside number
				NumberToken number = (NumberToken) token;
				int count = 0;
				boolean bPointAtBeginning = false;
				boolean bPointAtEnd = false;
				boolean bOnlyPoint = false;
				int indexOfSecondPoint = -1;
				int indexOfFirstPoint = -1;
				
				boolean bExpAtBeginning = false;
				boolean bExpAtEnd = false;
				int indexOfSecondExp = -1;
				int indexOfFirstExp = -1;
				int expCount = 0;
				
				String numberString = new String(number.getNumber());
				for (int j = 0; j < numberString.length(); j++){
					char c = numberString.charAt(j);
					if (c == DigitToken.POINT_CHAR){
						count++;
						if (count == 1) indexOfFirstPoint = j;
						if (count == 2) indexOfSecondPoint = j;
						
						if (number.getNumber().length() == 1) bOnlyPoint = true;
						if (j == 0) bPointAtBeginning = true;
						if (j == numberString.length() - 1) bPointAtEnd = true;
					}
					
					if (c == DigitToken.EXP){
						expCount++;
						if (expCount == 1) indexOfFirstExp = j;
						if (expCount == 2) indexOfSecondExp = j;
						
						if (j == 0) bExpAtBeginning = true;
						if (j == numberString.length() - 1) bExpAtEnd = true;
						
					}
				}
				
				if (count > 1 ){
					number.addFlagToken(indexOfSecondPoint, FlagToken.SYNTAX_ERROR);
				}else if (bOnlyPoint){
					number.setNumber("0");
				}else if (bPointAtBeginning){
					number.setNumber("0" + numberString);
				}else if (bPointAtEnd){
					number.setNumber(numberString + "0"); 
				}
				
				if (expCount > 1){
					number.addFlagToken(indexOfSecondExp, FlagToken.SYNTAX_ERROR);
				}else if (bExpAtBeginning){
					number.addFlagToken(0, FlagToken.SYNTAX_ERROR);
				}else if (bExpAtEnd){
					number.addFlagToken(numberString.length() - 1, FlagToken.SYNTAX_ERROR);
				}
				
				if (count == 1 && expCount == 1 && indexOfFirstPoint > indexOfFirstExp){
					number.addFlagToken(indexOfFirstPoint, FlagToken.SYNTAX_ERROR);
				}
				
			}else if (token.identifier.contains(ContainerToken.FUNCTION_CONTAINER_TYPE_ID)){
				ContainerToken containerToken = (ContainerToken)token;
				for (int j = 0; j < containerToken.getTokenList().size(); j++){
					FunctionToken functionToken = (FunctionToken) containerToken.getTokenList().get(j);
					for (int k = 0; k < functionToken.getNumberExpression(); k++){
						controlSyntaxErrorsNumbers(functionToken.getExpression(k));
					}
				}
			}
		}
	}

	public void controlSyntaxErrorInsideContainers(Expression inputExpression){
		for (DataToken token: inputExpression.getTokenList()){
			if (token.type == DataToken.CONTAINER_TOKEN){
				ContainerToken container = (ContainerToken)token;
				switch (container.containerType) {
				case ContainerToken.ADD_SUB_CONTAINER_TYPE:
					handleAddSubContainers(inputExpression, container);
					break;
				case ContainerToken.MULT_DIV_MOD_CONTAINER_TYPE:
					handleMultDivPowerToContainer(inputExpression, container);
					break;
				case ContainerToken.PRE_UNARY_NEGATIVE_SIGN_CONTAINER_TYPE:
					handlePreUnaryNegativeContainer(inputExpression, container);
					break;
				case ContainerToken.PRE_UNARY_CONTAINER_TYPE:
					
					break;
				case ContainerToken.POST_UNARY_CONTAINER_TYPE:
					handlePostUnaryContainer(inputExpression, container);
					break;
				case ContainerToken.FUNCTION_CONTAINER_TYPE:
					handleFunctionContainer(inputExpression, container);
					
					ContainerToken containerFunctionToken = (ContainerToken)token;
					for (int j = 0; j < containerFunctionToken.getTokenList().size(); j++){
						DataToken temp = containerFunctionToken.getTokenList().get(j);
						if (temp.identifier.contains(DataToken.FUNCTION_TOKEN_ID)){
							FunctionToken functionToken = (FunctionToken) temp;
							for (int k = 0; k < functionToken.getNumberExpression(); k++){
								controlSyntaxErrorInsideContainers(functionToken.getExpression(k));
							}
						}
					}
					break;
				default:
					break;
				}
			}
		}
	}
	
	private void handleAddSubContainers(Expression inputExpression, ContainerToken container){
		
		if (container.getTokenList().size() <= 1) return;
		
		int count = 0;
		for (DataToken token:container.getTokenList()){
			if (token.identifier.contains(BinaryOperator.SUBTRACTION_ID)){
				count++;
			}
		}
		int type = BinaryOperator.ADDITION;
		if (count % 2 != 0) type = BinaryOperator.SUBTRACTION;
		
		BinaryOperator binToken = new BinaryOperator(inputExpression, type);
		container.getTokenList().clear();
		container.getTokenList().add(binToken);
	}
	
	private void handleMultDivPowerToContainer(Expression inputExpression, ContainerToken container){
		if (container.getTokenList().size() <= 1) return;
		
		FlagToken flagToken = new FlagToken(inputExpression, FlagToken.SYNTAX_ERROR);
		
		if (container.getTokenList().size() == 2){
			container.getTokenList().add(flagToken);
		}else{
			container.getTokenList().add(2, flagToken);
		}
	}
	
	private void handlePreUnaryNegativeContainer(Expression inputExpression, ContainerToken container){
		if (container.getTokenList().size() <= 1) return;
		
		boolean bOnlyNegativeSigns = true;
		
		for (DataToken token:container.getTokenList()){
			if (!token.identifier.contains(UnaryOperator.NEGATIVE_SIGN_ID)){
				bOnlyNegativeSigns = false;
			}
		}
		
		if (bOnlyNegativeSigns){
			int type = BinaryOperator.ADDITION;
			if (container.getTokenList().size() % 2 != 0) type = BinaryOperator.SUBTRACTION;
			
			BinaryOperator binToken = new BinaryOperator(inputExpression, type);
			container.getTokenList().clear();
			container.getTokenList().add(binToken);
		}
	}
	
	private void handlePostUnaryContainer(Expression inputExpression, ContainerToken container){
		
	}

	private void handleFunctionContainer(Expression inputExpression, ContainerToken container){
		if (container.getTokenList().size() <= 1) return;
		
		for (int i = 0; i < container.getTokenList().size(); i++){
			FunctionToken token = (FunctionToken) container.getTokenList().get(i);
			if (token.identifier.contains(FunctionToken.POWER_TO_DATA_TOKEN_ID)){
				PowerToDataToken pt = (PowerToDataToken)token;
				if (pt.isbTempBaseEnabled()){
					Expression tempBase = pt.getTempBase();
					FlagToken flagToken = new FlagToken(inputExpression, FlagToken.SYNTAX_ERROR);
					tempBase.getTokenList().add(flagToken);
				}
			}
		}
	}
	
	public void checkSignOfNumbers(Expression inputExpression){
		for (int i = 0; i < inputExpression.getTokenList().size(); i++){
			DataToken token = inputExpression.getTokenList().get(i);
			if (token.type == DataToken.NUMBER_TOKEN && i > 0){
				DataToken tokenBefore = inputExpression.getTokenList().get(i - 1);
				if (tokenBefore.identifier.contains(ContainerToken.ADD_SUB_CONTAINER_TYPE_ID)){
					if (i - 2 >= 0){
						DataToken token_2_before = inputExpression.getTokenList().get(i - 2);
						if (token_2_before.identifier.contains(DataToken.NUMBER_TOKEN_ID)
								|| token_2_before.identifier.contains(ContainerToken.FUNCTION_CONTAINER_TYPE_ID)
								|| token_2_before.identifier.contains(ParenthesisDataToken.CLOSE_ID)){
							continue;
						}
					}
					ContainerToken container = (ContainerToken)tokenBefore;
					NumberToken number = (NumberToken)token;
					if (container.getTokenList().get(0).identifier.contains(OperatorToken.BINARY_OPERATOR_ID)){
						BinaryOperator bin = (BinaryOperator) container.getTokenList().get(0);
						char c0 = number.getNumber().charAt(0);
						if (bin.binaryOperatorType == BinaryOperator.SUBTRACTION && c0 == '-'){
							String s = number.getNumber();
							s = s.substring(1);
							number.setNumber(s);
						}else if (bin.binaryOperatorType == BinaryOperator.SUBTRACTION){
							String s = number.getNumber();
							s = "-" + s;
							number.setNumber(s);
						}
						inputExpression.getTokenList().remove(i - 1);
						i--;
					}
				}
			}else if (token.identifier.contains(ContainerToken.FUNCTION_CONTAINER_TYPE_ID)){
				ContainerToken functionContainer = (ContainerToken)token;
				for (DataToken tempToken:functionContainer.getTokenList()){
					if (tempToken.type == DataToken.FUNCTION_TOKEN){
						FunctionToken functionToken = (FunctionToken) tempToken;
						for (int j = 0; j < functionToken.getNumberExpression(); j++){
							Expression ex = functionToken.getExpression(j);
							checkSignOfNumbers(ex);
						}
					}
				}
			}
		}
	}
	
	public void checkSignOfFunctions(Expression inputExpression){
		for (int i = 0; i < inputExpression.getTokenList().size(); i++){
			DataToken token = inputExpression.getTokenList().get(i);
			if (token.identifier.contains(ContainerToken.FUNCTION_CONTAINER_TYPE_ID) && i > 0){
				DataToken tokenBefore = inputExpression.getTokenList().get(i - 1);
				if (tokenBefore.identifier.contains(ContainerToken.ADD_SUB_CONTAINER_TYPE_ID)){
					if (i - 2 >= 0){
						DataToken token_2_before = inputExpression.getTokenList().get(i - 2);
						if (token_2_before.identifier.contains(DataToken.NUMBER_TOKEN_ID)
								|| token_2_before.identifier.contains(ContainerToken.FUNCTION_CONTAINER_TYPE_ID)
								|| token_2_before.identifier.contains(ParenthesisDataToken.CLOSE_ID)){
							continue;
						}
					}
					ContainerToken container = (ContainerToken)tokenBefore;
					if (container.getTokenList().get(0).identifier.contains(OperatorToken.BINARY_OPERATOR_ID)){
						BinaryOperator bin = (BinaryOperator) container.getTokenList().get(0);
						
						if (bin.binaryOperatorType == BinaryOperator.SUBTRACTION){
							NumberToken nbr = new NumberToken(inputExpression, "-1");
							ContainerToken opCon = new ContainerToken(inputExpression, ContainerToken.MULT_DIV_MOD_CONTAINER_TYPE);
							opCon.getTokenList().add(new BinaryOperator(inputExpression, BinaryOperator.MULTIPLCATION));
							inputExpression.getTokenList().add(i,opCon);
							inputExpression.getTokenList().add(i,nbr);
							i+=2;
						}
						inputExpression.getTokenList().remove(i - 3);
						i--;
					}
				}
			}
			if (token.identifier.contains(ContainerToken.FUNCTION_CONTAINER_TYPE_ID)){
				ContainerToken functionContainer = (ContainerToken)token;
				for (DataToken tempToken:functionContainer.getTokenList()){
					if (tempToken.type == DataToken.FUNCTION_TOKEN){
						FunctionToken functionToken = (FunctionToken) tempToken;
						for (int j = 0; j < functionToken.getNumberExpression(); j++){
							Expression ex = functionToken.getExpression(j);
							checkSignOfFunctions(ex);
						}
					}
				}
			}
		}
	}
	
	public void unpackContainers(Expression inputExpression){
		for (int i = 0; i < inputExpression.getTokenList().size(); i++){
			DataToken token = inputExpression.getTokenList().get(i);
			if (!token.identifier.contains(DataToken.CONTAINER_TOKEN_ID)) continue;
			
			ContainerToken container = (ContainerToken)token;
			for (int j = 0; j < container.getTokenList().size(); j++){
				DataToken tokenInside = container.getTokenList().get(j);
				inputExpression.getTokenList().add(i, tokenInside);
				i++;
			}
			
			inputExpression.getTokenList().remove(i);
			i--;
			
			if (container.identifier.contains(ContainerToken.FUNCTION_CONTAINER_TYPE_ID) ){
				for (int j = 0; j < container.getTokenList().size(); j++){
					DataToken tokenInside =  container.getTokenList().get(j);
					if (tokenInside.identifier.contains(DataToken.FUNCTION_TOKEN_ID)){
						FunctionToken functionToken = (FunctionToken) container.getTokenList().get(j);
						for (int k = 0; k < functionToken.getNumberExpression(); k++){
							unpackContainers(functionToken.getExpression(k));
						}
					}
				}
			}
		}
	}
	
	public void handleParenthesis(Expression inputExpression){
		for (int i = 0; i < inputExpression.getTokenList().size(); i++){
			DataToken token = inputExpression.getTokenList().get(i);
			if (token.identifier.contains(OperatorToken.PARENTHES_ID)){
				ParenthesisDataToken paren = (ParenthesisDataToken)token;
				ArrayList<String> filter = new ArrayList<String>();
				switch (paren.parenthesisType) {
				case ParenthesisDataToken.OPEN:
					filter.add(ContainerToken.MULT_DIV_MOD_CONTAINER_TYPE_ID);
					filter.add(ContainerToken.POWER_TO_CONTAINER_TYPE_ID);
					filter.add(ContainerToken.POST_UNARY_CONTAINER_TYPE_ID);
					filter.add(ParenthesisDataToken.CLOSE_ID);
					filter.add("Nothing");
					break;
				case ParenthesisDataToken.CLOSE:
					filter.add(DataToken.NUMBER_TOKEN_ID);
					break;
				default:
					break;
				}
			}
		}
	}
	
	public void autoAddPowerToOperator(Expression inputExpression){
		for (int i = 0; i < inputExpression.getTokenList().size(); i++){
			DataToken token = inputExpression.getTokenList().get(i);
			if (token.identifier.contains(ContainerToken.FUNCTION_CONTAINER_TYPE_ID)){
				ContainerToken container = (ContainerToken)token;
				if (container.getTokenList().isEmpty()) continue;
				FunctionToken firstToken = (FunctionToken)container.getTokenList().get(0);
				if (firstToken.identifier.contains(FunctionToken.POWER_TO_DATA_TOKEN_ID)){
					if (i > 0){
						DataToken tokenBefore = inputExpression.getTokenList().get(i - 1);
						if (tokenBefore.identifier.contains(DataToken.NUMBER_TOKEN_ID) 
								|| tokenBefore.identifier.contains(OperatorToken.PARENTHES_ID)){
							BinaryOperator powerTo = new BinaryOperator(inputExpression, BinaryOperator.POWER_TO);
							inputExpression.getTokenList().add(i, powerTo);
							i++;
						}
					}
				}
				for (int k = 0; k < firstToken.getNumberExpression(); k++){
					Expression ex = firstToken.getExpression(k);
					autoAddPowerToOperator(ex);
				}
				for (int j = 1; j < container.getTokenList().size(); j++){
					DataToken function = container.getTokenList().get(j);
					DataToken functionBefore = container.getTokenList().get(j - 1);
					if (function.identifier.contains(FunctionToken.POWER_TO_DATA_TOKEN_ID) 
							&& functionBefore.identifier.contains(DataToken.FUNCTION_TOKEN_ID)){
						BinaryOperator powerTo = new BinaryOperator(inputExpression, BinaryOperator.POWER_TO);
						inputExpression.getTokenList().add(j, powerTo);
						j++;
						
						for (int k = 0; k < ((FunctionToken)function).getNumberExpression(); k++){
							Expression ex = ((FunctionToken)function).getExpression(k);
							autoAddPowerToOperator(ex);
						}
					}
				}
			}
		}
	}

	public void controlParenthesis(Expression inputExpression){
		Stack<DataToken> stack = new Stack<DataToken>();
		for (int i = 0; i < inputExpression.getTokenList().size(); i++){
			DataToken token = inputExpression.getTokenList().get(i);
			if (token.identifier.contains(ParenthesisDataToken.OPEN_ID)){
				if (i < inputExpression.getTokenList().size() - 1){
					DataToken tokenAfter = inputExpression.getTokenList().get(i + 1);
					if (tokenAfter.identifier.contains(ContainerToken.MULT_DIV_MOD_CONTAINER_TYPE_ID) || 
							tokenAfter.identifier.contains(ParenthesisDataToken.CLOSE_ID)){
						inputExpression.getTokenList().add(i + 1, new FlagToken(inputExpression, FlagToken.SYNTAX_ERROR));
						i++;
					}
				}
				stack.push(token);
			}else if (token.identifier.contains(ParenthesisDataToken.CLOSE_ID)){
				if (!stack.isEmpty()){
					stack.pop();
				}else if (i == inputExpression.getTokenList().size() - 1){
					inputExpression.getTokenList().add(new FlagToken(inputExpression, FlagToken.SYNTAX_ERROR));
				}else{
					inputExpression.getTokenList().add(i + 1, new FlagToken(inputExpression, FlagToken.SYNTAX_ERROR));
					i++;
				}
			}else if (token.identifier.contains(ContainerToken.FUNCTION_CONTAINER_TYPE_ID)){
				ContainerToken container = (ContainerToken)token;
				for (int j = 0; j < container.getTokenList().size(); j++){
					FunctionToken ft = (FunctionToken)container.getTokenList().get(j);
					for (int k = 0; k < ft.getNumberExpression(); k++){
						Expression ex = ft.getExpression(k);
						controlParenthesis(ex);
					}
				}
			}
		}
		if (!stack.isEmpty()){
			if (inputExpression.isRoot()){
				for (int i = 0; i < stack.size(); i++){
					ParenthesisDataToken closeParen = new ParenthesisDataToken(inputExpression, ParenthesisDataToken.CLOSE);
					inputExpression.getTokenList().add(closeParen);
				}
			}else{
				DataToken temp = stack.peek();
				int index = inputExpression.getTokenList().indexOf(temp);
				if (index == inputExpression.getTokenList().size() - 1){
					inputExpression.getTokenList().add(new FlagToken(inputExpression, FlagToken.SYNTAX_ERROR));
				}else{
					inputExpression.getTokenList().add(index + 1, new FlagToken(inputExpression, FlagToken.SYNTAX_ERROR));
				}
			}
		}
	}
	
	public void checkEmptyExpressions(Expression inputExpression){
		if (inputExpression.getTokenList().isEmpty()){
			if (!inputExpression.isRoot()){
				FlagToken syntaxError = new FlagToken(inputExpression, FlagToken.SYNTAX_ERROR);
				inputExpression.getTokenList().add(syntaxError);
			}else{
				NumberToken nbr = new NumberToken(inputExpression, "0");
				inputExpression.getTokenList().add(nbr);
			}
		}else{
			for (int i = 0; i < inputExpression.getTokenList().size(); i++){
				DataToken token = inputExpression.getTokenList().get(i);
				if (token.identifier.contains(ContainerToken.FUNCTION_CONTAINER_TYPE_ID)){
					ContainerToken container = (ContainerToken)token;
					for (int j = 0; j < container.getTokenList().size(); j++){
						DataToken temp = container.getTokenList().get(j);
						if (temp.identifier.contains(DataToken.FUNCTION_TOKEN_ID)){
							FunctionToken ft = (FunctionToken)temp;
							for (int k = 0; k < ft.getNumberExpression(); k++){
								Expression ex = ft.getExpression(k);
								checkEmptyExpressions(ex);
							}
						}
					}
				}
			}
		}
	}
	
	private boolean trackError(Expression inputExpression){
		for (DataToken token:inputExpression.getTokenList()){
			if (token.type == DataToken.FLAG_TOKEN) return true;
			if (token.type == DataToken.NUMBER_TOKEN){
				NumberToken numberToken = (NumberToken)token;
				if (numberToken.getIndexOfFlagToken() >= 0) return true;
			}
			if (token.identifier.contains(ContainerToken.FUNCTION_CONTAINER_TYPE_ID)){
				ContainerToken container = (ContainerToken)token;
				for (int i = 0; i < container.getTokenList().size(); i++){
					DataToken tempToken = container.getTokenList().get(i);
					if (tempToken.identifier.contains(DataToken.FUNCTION_TOKEN_ID)){
						FunctionToken functionToken = (FunctionToken)tempToken;
						for (int j = 0; j < functionToken.getNumberExpression(); j++){
							if (trackError(functionToken.getExpression(j))) return true;
						}
					}
				}
			}
		}
		
		return false;
	}
	
	public boolean controlSyntax(Expression inputExpression){
		
		long startTime = System.currentTimeMillis();
		combineDigitToken(inputExpression);
		putTokensInContainers(inputExpression);
		checkEmptyExpressions(inputExpression);
		controlParenthesis(inputExpression);
		
		controlSyntaxErrorsOfContainers(inputExpression);
		controlSyntaxErrorInsideContainers(inputExpression);
		
		combineSignIntoEXPNumber(inputExpression);
		
		controlSyntaxErrorsNumbers(inputExpression);
		
		checkSignOfNumbers(inputExpression);
		checkSignOfFunctions(inputExpression);
		
		if (trackError(inputExpression)) return true;
		autoAddPowerToOperator(inputExpression);
		autoAddTokenBetweenTokens(inputExpression);
		autoAddTokenInsideContainers(inputExpression);
		Log.d("","###### controlSyntax: " + inputExpression.getReportString(null));
		
		unpackContainers(inputExpression);
		long endTime = System.currentTimeMillis();
		Log.d("","In controlSyntax time = " + (endTime - startTime));
		return false;
	}

	public Expression infixToPostfix(Expression inputExpression){
		Expression result = new Expression(inputExpression.getParent(), inputExpression.isRoot());
		Stack<OperatorToken> stack = new Stack<OperatorToken>();
		for (DataToken token:inputExpression.getTokenList()){
			if (token.identifier.contains(DataToken.NUMBER_TOKEN_ID)){
				result.getTokenList().add(token);
			}else if (token.identifier.contains(DataToken.FUNCTION_TOKEN_ID)){
				FunctionToken ft = (FunctionToken) token;
				for (int i = 0; i < ft.getNumberExpression(); i++){
					Expression ex = ft.getExpression(i);
					ft.setExpression(i, infixToPostfix(ex));
				}
				result.getTokenList().add(token);
			}else if (token.identifier.contains(DataToken.OPERATOR_TOKEN_ID) && !token.identifier.contains(OperatorToken.PARENTHES_ID)){
				OperatorToken o1 = (OperatorToken)token;
				while (!stack.isEmpty()){
					OperatorToken o2 = stack.peek();
					boolean temp = o2.getAssociation() == OperatorToken.LEFT_ASSOCIATION && o1.getPrecedence() == o2.getPrecedence();
					if (temp || o1.getPrecedence() < o2.getPrecedence()){
						stack.pop();
						result.getTokenList().add(o2);
					}else{
						break;
					}
				}
				stack.push(o1);
			}else if (token.identifier.contains(ParenthesisDataToken.OPEN_ID)){
				stack.push((OperatorToken)token);
			}else if (token.identifier.contains(ParenthesisDataToken.CLOSE_ID)){
				boolean foundOpen = false;
				while (!stack.isEmpty()){
					OperatorToken op = stack.pop();
					if (op.identifier.contains(ParenthesisDataToken.OPEN_ID)){
						foundOpen = true;
						break;
					}else{
						result.getTokenList().add(op);
					}
				}
				
				if (!foundOpen){
					//not necessary because we already check for parentheses in the syntax control process.
				}
			}
		}
		
		while (!stack.isEmpty()){
			result.getTokenList().add(stack.pop());
		}
		
		return result;
	}

	public DataToken evaluatePostfix(Expression inputExpression){
		Stack<DataToken> stack = new Stack<DataToken>();
		for (DataToken token:inputExpression.getTokenList()){
			if (token.identifier.contains(DataToken.NUMBER_TOKEN_ID)){
				NumberToken numberToken = (NumberToken)token;
				ResultToken resultToken = createResultToken(numberToken);
				if (resultToken.isRRE()){
					resultToken.simplifyRRE();
				}
				stack.push(resultToken);
			}else if (token.identifier.contains(DataToken.FUNCTION_TOKEN_ID)){
				FunctionToken ft = (FunctionToken) token;
				DataToken temp = null;
				try {
					temp = ft.calculate1(this);
				} catch (EmptyExpressionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (temp.identifier.contains(DataToken.FLAG_TOKEN_ID)){
					return temp;
				}else{
					stack.push(temp);
				}
			}else if (token.identifier.contains(OperatorToken.BINARY_OPERATOR_ID)){
				BinaryOperator bOp = (BinaryOperator)token;
				if (stack.size() < 2){
					return new FlagToken(inputExpression, FlagToken.SYNTAX_ERROR);
				}
				DataToken temp = null;
				try {
					temp = bOp.calculate1((ResultToken)stack.pop(), (ResultToken)stack.pop());
				} catch (EmptyExpressionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (temp.identifier.contains(DataToken.FLAG_TOKEN_ID)){
					return temp;
				}else{
					stack.push(temp);
				}
			}else if (token.identifier.contains(OperatorToken.UNARY_OPERATOR_ID)){
				UnaryOperator uOp = (UnaryOperator)token;
				if (stack.size() < 1){
					return new FlagToken(inputExpression, FlagToken.SYNTAX_ERROR); 
				}
				DataToken temp = uOp.calculate1((ResultToken) stack.pop());
				if (temp.identifier.contains(DataToken.FLAG_TOKEN_ID)){
					return temp;
				}else{
					stack.push(temp);
				}
			}
		}
		
		if (stack.size() != 1){
			return new FlagToken(inputExpression, FlagToken.SYNTAX_ERROR);
		}else{
			return stack.pop();
		}
	}
	
	private ResultToken createResultToken(NumberToken n){
		String s = n.getNumber();
		String a[] = s.split("[.]");
		ResultToken resultToken = null;
		if (a.length == 1 || (a.length == 2 && a[1].length() <= MAX_SHORT_DECIMAL_DIGIT)){
			RationalNumber r = RationalNumber.convertDecimalToRational_withoutPatternRecognition(new BigDecimal(s));
			RationalRootExpression rre = new RationalRootExpression();
			SquareRootNumber sr = new SquareRootNumber(RationalNumber.ONE, r);
			rre.add(sr, RationalRootExpression.NUMERATOR);
			rre.add(SquareRootNumber.ONE, RationalRootExpression.DENOMINATOR);
			resultToken = new ResultToken(n.getParent(), ResultToken.RRE);
			resultToken.setRrExpression(rre);
			resultToken.computeDecimalResult();
		}else{
			BigDecimal decimal = new BigDecimal(s);
			resultToken = new ResultToken(n.getParent(), ResultToken.DECIMAL);
			resultToken.setDecimal(decimal);
		}
		return resultToken;
	}
}
