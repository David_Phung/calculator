package computing;

public class PrimeLimitReachedException extends Exception {

	public PrimeLimitReachedException() {
		// TODO Auto-generated constructor stub
	}

	public PrimeLimitReachedException(String detailMessage) {
		super(detailMessage);
		// TODO Auto-generated constructor stub
	}

	public PrimeLimitReachedException(Throwable throwable) {
		super(throwable);
		// TODO Auto-generated constructor stub
	}

	public PrimeLimitReachedException(String detailMessage, Throwable throwable) {
		super(detailMessage, throwable);
		// TODO Auto-generated constructor stub
	}
	

}
