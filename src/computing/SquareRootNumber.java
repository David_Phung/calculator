package computing;

import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Scanner;
import visualToken.Square;
import visualToken.SquareRoot;
import visualToken.StringToken;
import visualToken.TextArea;
import visualToken.VisualToken;

public class SquareRootNumber implements Comparable<SquareRootNumber>{
	
	public static final SquareRootNumber ZERO = new SquareRootNumber(RationalNumber.ZERO,
			RationalNumber.ZERO);
	public static final SquareRootNumber ONE = new SquareRootNumber(RationalNumber.ONE,
			RationalNumber.ONE);
	
	private RationalNumber radicand;
	private RationalNumber coefficient;

	public SquareRootNumber(RationalNumber radicand, RationalNumber coeff) {
		// TODO Auto-generated constructor stub
		if (radicand.signum() < 0) throw new IllegalArgumentException("radicand < 0");
		this.radicand = radicand;
		this.coefficient = coeff;
	}
	
	public SquareRootNumber(String radicand, String coeff){
		String temp[] = radicand.split("[/]");
		RationalNumber rad = new RationalNumber(temp[0], "1");
		if (temp.length == 2) rad = new RationalNumber(temp[0], temp[1]);
		
		temp = coeff.split("[/]");
		RationalNumber co = new RationalNumber(temp[0], "1");
		if (temp.length == 2) co = new RationalNumber(temp[0], temp[1]);
		
		if (rad.signum() < 0) throw new IllegalArgumentException("radicand < 0");
		this.radicand = rad;
		this.coefficient = co;
	}
	
	public boolean isRational(){
		if (this.compareTo(ZERO) != 0
				&& radicand.compareTo(RationalNumber.ONE) != 0){
			return false;
		}
		return true;
	}
	
	public SquareRootNumber simplify() throws PrimeLimitReachedException{
		if (radicand.signum() == 0) return ZERO;
		RationalNumber newRadicand = radicand.simplify();
		RationalNumber newCoefficient = coefficient;
		BigInteger radicandDen = newRadicand.getDenominator();
		BigInteger radicandNum = newRadicand.getNumerator();
		radicandNum = radicandNum.multiply(radicandDen);
		int[][] primeFactors = PrimeMath.primeFactorize(radicandNum);
		
		BigInteger squareFactor = BigInteger.ONE;
		BigInteger remainingFactor = BigInteger.ONE;
		for (int i = 0; i < primeFactors[0].length; i++){
			int evenPartOfExponent = primeFactors[1][i] / 2;
			int remainingPartOfExponent = primeFactors[1][i] % 2;
			BigInteger temp = new BigInteger(primeFactors[0][i] + "").pow(evenPartOfExponent);
			squareFactor = squareFactor.multiply(temp);
			temp = new BigInteger(primeFactors[0][i] + "").pow(remainingPartOfExponent);
			remainingFactor = remainingFactor.multiply(temp);
		}
		
		RationalNumber newFactor = new RationalNumber(squareFactor, radicandDen);
		newCoefficient = newCoefficient.multiply(newFactor).simplify();
		newRadicand = new RationalNumber(remainingFactor, BigInteger.ONE);
		
		SquareRootNumber res = new SquareRootNumber(newRadicand, newCoefficient);
		return res;
	}

	public SquareRootNumber multiply(SquareRootNumber val){
		if (signum() == 0 || val.signum() == 0) return ZERO;
		
		RationalNumber newCoeff = coefficient.multiply(val.coefficient);
		RationalNumber newRadicand = radicand.multiply(val.radicand);
		SquareRootNumber res = new SquareRootNumber(newRadicand, newCoeff);
		return res;
	}
	
	public SquareRootNumber divide(SquareRootNumber val){
		if (val.signum() == 0) throw new ArithmeticException("Divided by 0");
		if (signum() == 0) return ZERO;
		
		RationalNumber newCoeff = coefficient.divide(val.coefficient);
		RationalNumber newRadicand = radicand.divide(val.radicand);
		SquareRootNumber res = new SquareRootNumber(newRadicand, newCoeff);
		return res;
	}
	
	public SquareRootNumber negate(){
		SquareRootNumber res = new SquareRootNumber(radicand, coefficient.negate());
		return res;
	}
	
	public int compareTo(SquareRootNumber o) {
		// TODO Auto-generated method stub
		if (signum() * o.signum() < 0) return (signum() - o.signum()) / Math.abs(signum() - o.signum());
		RationalNumber temp1 = coefficient.multiply(coefficient).multiply(radicand);
		RationalNumber temp2 = o.coefficient.multiply(o.coefficient).multiply(o.radicand);
		RationalNumber diff = temp1.subtract(temp2);
		if (signum() > 0) return diff.signum();
		return -diff.signum();
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		String s = new String();
		if (this.compareTo(ONE) == 0) return " 1";
		if (this.compareTo(ZERO) == 0) return " 0";
		if (coefficient.compareTo(new RationalNumber("-1", "1")) == 0) s += "-";
		else if (coefficient.compareTo(RationalNumber.ONE) != 0)	s += coefficient.toString(); 
		if (radicand.compareTo(RationalNumber.ONE) != 0){
			s += "sqrt(";
			s += radicand.toString();
			s += ")";
		}
		return s;
	}
	
	public ArrayList<VisualToken> toVisualTokens(TextArea parent){
		ArrayList<VisualToken> list = new ArrayList<VisualToken>();
		if (this.compareTo(ZERO) == 0){
			list.add(new StringToken(parent, "0"));
		}else if (radicand.compareTo(RationalNumber.ONE) == 0){
			list.addAll(coefficient.toVisualTokens(parent));
		}else if(coefficient.compareTo(RationalNumber.ONE) == 0){
			SquareRoot sqrtToken = new SquareRoot(parent);
			sqrtToken.getRadicand().getTokenList().addAll(radicand.toVisualTokens(sqrtToken.getRadicand()));
			list.add(sqrtToken);
		}else if (coefficient.compareTo(new RationalNumber("-1", "1")) == 0){
			SquareRoot sqrtToken = new SquareRoot(parent);
			sqrtToken.getRadicand().getTokenList().addAll(radicand.toVisualTokens(sqrtToken.getRadicand()));
			list.add(new StringToken(parent, "-"));
			list.add(sqrtToken);
		}else{
			SquareRoot sqrtToken = new SquareRoot(parent);
			sqrtToken.getRadicand().getTokenList().addAll(radicand.toVisualTokens(sqrtToken.getRadicand()));
			list.addAll(coefficient.toVisualTokens(parent));
			list.add(sqrtToken);
		}
		return list;
	}
	
	public int signum(){
		return radicand.signum() * coefficient.signum();
	}
	
	public BigDecimal toBigDecimal(){
		BigDecimal res = BigDecimalFunctions.sqrt(radicand.toBigDecimal(), DataProcessor.BIG_DECIMAL_SCALE);
		res = res.multiply(coefficient.toBigDecimal());
		return res;
	}
	
	public RationalNumber getRadicand() {
		return radicand;
	}


	public RationalNumber getCoefficient() {
		return coefficient;
	}


	public static void main(String[] args) throws PrimeLimitReachedException, FileNotFoundException {
		PrimeMath.loadData();
		
		SquareRootNumber sr1 = new SquareRootNumber("2", "1");
		SquareRootNumber sr2 = new SquareRootNumber("3", "1");
		SquareRootNumber sr3 = new SquareRootNumber("3", "-2");
		SquareRootNumber sr4 = new SquareRootNumber("4", "-1");
		SquareRootNumber sr5 = new SquareRootNumber("0", "1");
		SquareRootNumber sr6 = new SquareRootNumber("0", "1");
		
		System.out.println(sr1.compareTo(sr2)); //-1
		System.out.println(sr1.compareTo(sr3)); // 1
		System.out.println(sr3.compareTo(sr4)); // -1
		System.out.println(sr1.compareTo(sr5)); // 1
		System.out.println(sr3.compareTo(sr5)); // -1
		System.out.println(sr5.compareTo(sr6)); // 0
		
	}
	
}
