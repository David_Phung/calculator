package computing;

import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;

import android.util.Log;
import visualToken.StringToken;
import visualToken.TextArea;
import visualToken.VisualToken;

public class RootExpression {
	
	private ArrayList<SquareRootNumber> termList;

	public RootExpression() {
		// TODO Auto-generated constructor stub
		termList = new ArrayList<SquareRootNumber>();
	}
	
	public RootExpression(RootExpression ex){
		termList = new ArrayList<SquareRootNumber>();
		for (SquareRootNumber term:ex.termList){
			this.termList.add(term);
		}
	}
	public void add(SquareRootNumber term){
		termList.add(term);
	}
	
	public void remove(int index){
		termList.remove(index);
	}
	
	public int size(){
		return termList.size();
	}

	
	public SquareRootNumber get(int index){
		return termList.get(index);
	}
	
	public RootExpression simplify() throws PrimeLimitReachedException, EmptyExpressionException{
		if (termList.isEmpty()) throw new EmptyExpressionException("expression is empty");
		ArrayList<SquareRootNumber> newTermList = new ArrayList<SquareRootNumber>(); 
		for (SquareRootNumber term:termList){
			term = term.simplify();
			RationalNumber r = term.getRadicand();
			boolean added = false;
			for (int j = 0; j < newTermList.size(); j++){
				SquareRootNumber temp = newTermList.get(j);
				int comp = temp.getRadicand().compareTo(r);
				if (comp > 0){
					newTermList.add(j, term);
					added = true;
					break;
				}else if (comp == 0){
					SquareRootNumber newNumber = new SquareRootNumber(temp.getRadicand(), temp.getCoefficient().add(term.getCoefficient()).simplify());
					newTermList.set(j, newNumber);
					added = true;
					break;
				}
			}
			if (!added){
				newTermList.add(term);
			}
		}
		RootExpression res = new RootExpression();
		for (SquareRootNumber sr:newTermList){
			if (sr.compareTo(SquareRootNumber.ZERO) != 0){
				res.add(sr);
			}
		}
		if (res.termList.isEmpty()){
			res.add(SquareRootNumber.ZERO);
		}
		
		return res;
	}

	public RootExpression add(RootExpression ex){
		RootExpression res = new RootExpression();
		for (SquareRootNumber sr:termList){
			res.add(sr);
		}
		for (SquareRootNumber sr:ex.termList){
			res.add(sr);
		}
		return res;
	}
	
	public RootExpression subtract(RootExpression ex){
		RootExpression res = new RootExpression();
		for (SquareRootNumber sr:termList){
			res.add(sr);
		}
		for (SquareRootNumber sr:ex.termList){
			sr = sr.negate();
			res.add(sr);
		}
		return res;
	}
	
	public RootExpression multiply(RootExpression ex){
		RootExpression res = new RootExpression();
		for (SquareRootNumber sr:termList){
			for (SquareRootNumber sr1:ex.termList){
				res.add(sr.multiply(sr1));
			}
		}
		return res;
	}
	
	public RootExpression multiply(SquareRootNumber val){
		RootExpression res = new RootExpression();
		for (SquareRootNumber term:termList){
			res.add(term.multiply(val));
		}
		return res;
	}
	
	public RootExpression divide(SquareRootNumber val){
		RootExpression res = new RootExpression();
		for (SquareRootNumber term:termList){
			res.add(term.divide(val));
		}
		return res;
	}
	
	public RootExpression negate(){
		RootExpression res = new RootExpression();
		for (SquareRootNumber term:termList){
			res.add(term.negate());
		}
		return res;
	}
	private RationalNumber gcdSelf(){
		if (termList.isEmpty()) return RationalNumber.ONE;
		RationalNumber gcd = termList.get(0).getCoefficient();
		for (SquareRootNumber term:termList){
			gcd = gcd.gcd(term.getCoefficient());
		}
		return gcd;
	}
	
	public RationalNumber gcd(RootExpression ex){
		return gcdSelf().gcd(ex.gcdSelf());
	}
	
	public RootExpression convertToSameDenominators() throws EmptyExpressionException{
		if (termList.isEmpty()) throw new EmptyExpressionException("Expression empty");
		BigInteger bcd = BigInteger.ONE;
		BigInteger gcd = termList.get(0).getCoefficient().getDenominator();
		for (SquareRootNumber term:termList){
			bcd = bcd.multiply(term.getCoefficient().getDenominator());
			gcd = gcd.gcd(term.getCoefficient().getDenominator());
		}
		bcd = bcd.divide(gcd);
		RootExpression resEx = new RootExpression();
		for (SquareRootNumber term:termList){
			RationalNumber radicand = term.getRadicand();
			RationalNumber coeff = term.getCoefficient();
			BigInteger temp = bcd.divide(coeff.getDenominator());
			coeff = coeff.multiply(new RationalNumber(temp, temp));
			SquareRootNumber newTerm = new SquareRootNumber(radicand, coeff);
			resEx.add(newTerm);
		}
		return resEx;
	}
	
	public RootExpression getFactorToRationalize(){
		RootExpression factor = new RootExpression();
		if (termList.size() == 2){
			factor.add(termList.get(0));
			factor.add(termList.get(1).negate());
			return factor;
		}else if (termList.size() == 1){
			SquareRootNumber sr = new SquareRootNumber(termList.get(0).getRadicand(), RationalNumber.ONE);
			factor.add(sr);
			return factor;
		}
		return null;
	}
	
	public boolean allTermsNegative(){
		for (SquareRootNumber term:termList){
			if (term.signum() < 0) return false;
		}
		return true;
	}
	
	@Override
		public String toString() {
		// TODO Auto-generated method stub
		String s = new String();
		for (SquareRootNumber term:termList){
			if (term.signum() > 0){
				s+= "+";
			}
			s += term.toString();
		}
		return s;
	}

	public ArrayList<VisualToken> toVisualTokens(TextArea parent){
		ArrayList<VisualToken> list = new ArrayList<VisualToken>();
		int zeroCount = 0;
		for (int i = 0; i < termList.size(); i++){
			SquareRootNumber term = termList.get(i);
			if (term.compareTo(SquareRootNumber.ZERO) == 0){
				zeroCount ++;
				continue;
			}
			if (term.signum() >= 0 && i > 0){
				list.add(new StringToken(parent, "+"));
			}
			list.addAll(term.toVisualTokens(parent));
		}
		if (zeroCount > 0 && zeroCount == termList.size()){
			list.add(new StringToken(parent, "0"));
		}
		return list;
	}
	
	public BigDecimal toBigDecimal(){
		BigDecimal sum = BigDecimal.ZERO;
		for (SquareRootNumber term:termList){
			sum = sum.add(term.toBigDecimal());
		}
		return sum;
	}
	
	public static void main(String[] args) throws PrimeLimitReachedException, FileNotFoundException, EmptyExpressionException {
		PrimeMath.loadData();
		SquareRootNumber sr1 = SquareRootNumber.ONE;
		SquareRootNumber sr2 = new SquareRootNumber("2", "3");
		SquareRootNumber sr3 = new SquareRootNumber("18", "-4");
		SquareRootNumber sr4 = new SquareRootNumber("5", "1");
		
		RootExpression re = new RootExpression();
		RootExpression re1 = new RootExpression();
		re.add(sr1);
		re.add(sr2);
		re1.add(sr3);
		re1.add(sr4);
		
		System.out.println(re);
		System.out.println(re1);
		
		System.out.println(re.multiply(re1));
		RootExpression temp = re.multiply(re1);
		System.out.println(temp.simplify());
	}

}
