package displayLayoutOld;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.RectF;
import android.util.Log;

public abstract class LayoutBox {
	
	public static final int MATCH_PARENT = 0;
	public static final int WRAP_CONTENT = 1;
	
	protected float WIDTH, HEIGHT;
	protected PointF location;
	protected int verticalLayoutParam;
	protected int horizontalLayoutParam;
	protected Margin margin;
	protected float verticalWeight, horizontalWeight;
	protected float minimumWidth, minimumHeight;
	protected boolean isContainer;
	protected Bitmap bitmap;
	protected boolean isDisabled;

	public LayoutBox() {
		// TODO Auto-generated constructor stub
		location = new PointF();
		margin = new Margin();
		isContainer = false;
	}
	
	public void recreateBitmap(boolean bCallChildren){
		if (WIDTH <= 0 || HEIGHT <= 0){
			bitmap = null;
			return;
		}
		Bitmap tempBitmap = Bitmap.createBitmap((int)WIDTH, (int)HEIGHT, Bitmap.Config.ARGB_8888);
		if (bitmap != null){
			Canvas canvas = new Canvas(tempBitmap);
			canvas.drawBitmap(bitmap, 0, 0, new Paint());
		}
		bitmap = tempBitmap;
		
		if (isContainer && bCallChildren){
			Container container = (Container)this;
			for (LayoutBox box:container.boxList){
				box.recreateBitmap(true);
			}
		}
	}

	public abstract void draw();

	public RectF getBounds(){
		RectF r = new RectF(location.x, location.y, location.x + WIDTH, location.y + HEIGHT);
		return r;
	}
	
	public float getWidth() {
		return WIDTH;
	}

	public void setWidth(float width) {
		this.WIDTH = width;
	}

	public float getHeight() {
		return HEIGHT;
	}

	public void setHeight(float height) {
		this.HEIGHT = height;
	}

	public PointF getLocation() {
		return location;
	}

	public void setLocation(PointF location) {
		this.location = location;
	}

	public Margin getMargin() {
		return margin;
	}

	public void setMargin(Margin margin) {
		this.margin = margin;
	}

	public float getVerticalWeight() {
		return verticalWeight;
	}

	public void setVerticalWeight(float verticalWeight) {
		this.verticalWeight = verticalWeight;
	}

	public float getHorizontalWeight() {
		return horizontalWeight;
	}

	public void setHorizontalWeight(float horizontalWeight) {
		this.horizontalWeight = horizontalWeight;
	}

	public float getMinimumWidth() {
		return minimumWidth;
	}

	public void setMinimumWidth(float minimumWidth) {
		this.minimumWidth = minimumWidth;
	}

	public float getMinimumHeight() {
		return minimumHeight;
	}

	public void setMinimumHeight(float minimumHeight) {
		this.minimumHeight = minimumHeight;
	}

	public boolean isContainer() {
		return isContainer;
	}

	public void setContainer(boolean isContainer) {
		this.isContainer = isContainer;
	}

	public Bitmap getBitmap() {
		return bitmap;
	}

	public void setBitmap(Bitmap bitmap) {
		this.bitmap = bitmap;
	}

	public boolean isDisabled() {
		return isDisabled;
	}

	public void setDisabled(boolean isDisabled) {
		this.isDisabled = isDisabled;
	}
	
	

}
