package displayLayoutOld;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.PointF;
import android.graphics.RectF;

public class DisplayLayoutGenerator {
	
	private LinearLayoutOld test;

	public DisplayLayoutGenerator() {
		// TODO Auto-generated constructor stub
	}
	
	public void generateTestLayout(){
		LabelPart p1 = new LabelPart();
		p1.setText("p1");
		LabelPart p2 = new LabelPart();
		p2.setText("p2");
		LabelPart p3 = new LabelPart();
		p3.setText("p3");
		
		p1.horizontalWeight = 1;
		p2.horizontalWeight = 5;
		p3.horizontalWeight = 2;
		
		p1.verticalWeight = 1;
		p2.verticalWeight = 8;
		p3.verticalWeight = 4;
		
		p1.minimumWidth = 40;
		p2.minimumWidth = 100;
		p3.minimumWidth = 50;
		
		p1.minimumHeight = 10;
		p2.minimumHeight = 20;
		p3.minimumHeight = 15;
		
		p1.margin = new Margin(10, 10, 10, 10);
		p2.margin = new Margin(10, 10, 10, 10);
		p3.margin = new Margin(10, 10, 10, 10);
		
		test = new LinearLayoutOld(true, LinearLayoutOld.VERTICAL);
		test.addBox(p1);
		test.addBox(p2);
		test.addBox(p3);
		test.verticalLayoutDistributionMethod = Container.LAYOUT_PARAM_METHOD;
		p1.verticalLayoutParam = LayoutBox.MATCH_PARENT;
		p2.verticalLayoutParam = LayoutBox.MATCH_PARENT;
		p3.verticalLayoutParam = LayoutBox.MATCH_PARENT;
		p1.horizontalLayoutParam = LayoutBox.WRAP_CONTENT;
		p2.horizontalLayoutParam = LayoutBox.MATCH_PARENT;
		p3.horizontalLayoutParam = LayoutBox.MATCH_PARENT;
		test.horizontalLayoutDistributionMethod = Container.LAYOUT_PARAM_METHOD;
	}
	
	public void placeTestLayout(RectF bounds){
		test.assignWidthAndHeight(bounds);
		test.assignLocations();
		test.setWidth(bounds.width());
		test.setHeight(bounds.height());
		test.recreateBitmap(true);
	}
	
	public RectF computeMinimumsize(){
		test.computeMinimumWidth();
		test.computeMinimumHeight();
		RectF minimumRect = new RectF(0,0, test.minimumWidth, test.minimumHeight);
		return minimumRect;
	}
	
	public Bitmap getTestBitmap(){
		test.draw();
		//test.setWidth(350);
		//test.setHeight(100);
		//test.recreateBitmap(false);
		return test.bitmap;
	}

	public Container generateBasicModeLayout(){
		LinearLayoutOld ll = new LinearLayoutOld(true, LinearLayoutOld.VERTICAL);
		InputPart input = new InputPart();
		OutputPart output = new OutputPart();
		
		input.verticalLayoutParam = LayoutBox.MATCH_PARENT;
		output.verticalLayoutParam = LayoutBox.MATCH_PARENT;
		
		input.horizontalLayoutParam = LayoutBox.MATCH_PARENT;
		output.horizontalLayoutParam = LayoutBox.MATCH_PARENT;
		
		ll.horizontalLayoutDistributionMethod = ll.verticalLayoutDistributionMethod = Container.LAYOUT_PARAM_METHOD;
		
		input.margin = new Margin(5,5,5,5);
		output.margin = new Margin(5,5,5,5);
		
		input.textAreaAlignment = Part.TOP_LEFT;
		output.textAreaAlignment = Part.BOTTOM_RIGHT;
		
		ll.addBox(input);
		ll.addBox(output);
		ll.refreshBoxList();
		
		return ll;
	}

	public Container generateHypLayout(){
		LinearLayoutOld mainLL = new LinearLayoutOld(true, LinearLayoutOld.VERTICAL);
		LinearLayoutOld line1 = new LinearLayoutOld(false, LinearLayoutOld.HORIZONTAL);
		LinearLayoutOld line2 = new LinearLayoutOld(false, LinearLayoutOld.HORIZONTAL);
		LinearLayoutOld line3= new LinearLayoutOld(false, LinearLayoutOld.HORIZONTAL);
		
		mainLL.horizontalLayoutDistributionMethod = mainLL.verticalLayoutDistributionMethod = Container.LAYOUT_PARAM_METHOD;
		line1.horizontalLayoutDistributionMethod = line1.verticalLayoutDistributionMethod = Container.LAYOUT_PARAM_METHOD;
		line2.horizontalLayoutDistributionMethod = line2.verticalLayoutDistributionMethod = Container.LAYOUT_PARAM_METHOD;
		line3.horizontalLayoutDistributionMethod = line3.verticalLayoutDistributionMethod = Container.LAYOUT_PARAM_METHOD;
		
		mainLL.margin = new Margin(5, 5, 5, 5);
		
		LabelPart sinh = new LabelPart();
		LabelPart cosh = new LabelPart();
		LabelPart tanh = new LabelPart();
		LabelPart sinh_1 = new LabelPart();
		LabelPart cosh_1 = new LabelPart();
		LabelPart tanh_1 = new LabelPart();
		
		sinh.setText("sinh");
		cosh.setText("cosh");
		tanh.setText("tanh");
		sinh_1.setText("sinh_1");
		cosh_1.setText("cosh_1");
		tanh_1.setText("tanh_1");
		
		line1.horizontalLayoutParam = LayoutBox.MATCH_PARENT;
		line2.horizontalLayoutParam = LayoutBox.MATCH_PARENT;
		line3.horizontalLayoutParam = LayoutBox.MATCH_PARENT;
		
		line1.verticalLayoutParam = LayoutBox.MATCH_PARENT;
		line2.verticalLayoutParam = LayoutBox.MATCH_PARENT;
		line3.verticalLayoutParam = LayoutBox.MATCH_PARENT;
		
		sinh.horizontalLayoutParam = LayoutBox.MATCH_PARENT;
		cosh.horizontalLayoutParam = LayoutBox.MATCH_PARENT;
		tanh.horizontalLayoutParam = LayoutBox.MATCH_PARENT;
		sinh_1.horizontalLayoutParam = LayoutBox.MATCH_PARENT;
		cosh_1.horizontalLayoutParam = LayoutBox.MATCH_PARENT;
		tanh_1.horizontalLayoutParam = LayoutBox.MATCH_PARENT;
		
		sinh.verticalLayoutParam = LayoutBox.MATCH_PARENT;
		cosh.verticalLayoutParam = LayoutBox.MATCH_PARENT;
		tanh.verticalLayoutParam = LayoutBox.MATCH_PARENT;
		sinh_1.verticalLayoutParam = LayoutBox.MATCH_PARENT;
		cosh_1.verticalLayoutParam = LayoutBox.MATCH_PARENT;
		tanh_1.verticalLayoutParam = LayoutBox.MATCH_PARENT;
				
		line1.addBox(sinh);
		line1.addBox(sinh_1);
		line2.addBox(cosh);
		line2.addBox(cosh_1);
		line3.addBox(tanh);
		line3.addBox(tanh_1);
		
		mainLL.addBox(line1);
		mainLL.addBox(line2);
		mainLL.addBox(line3);
		
		line1.refreshBoxList();
		line2.refreshBoxList();
		line3.refreshBoxList();
		mainLL.refreshBoxList();
		return mainLL; 
	}
}
