package displayLayoutOld;

import java.util.ArrayList;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.PointF;
import android.graphics.RectF;
import android.util.Log;

public abstract class Container extends LayoutBox {
	
	public static final int WEIGHT_METHOD = 0;
	public static final int LAYOUT_PARAM_METHOD = 1;
	
	public static final int HORIZONTAL = 0;
	public static final int VERTICAL = 1;
	
	protected ArrayList<LayoutBox> boxList;
	protected ArrayList<LayoutBox> fullBoxList;
	protected int verticalLayoutDistributionMethod;
	protected int horizontalLayoutDistributionMethod;
	protected boolean isRoot;

	public Container(boolean isRoot) {
		// TODO Auto-generated constructor stub
		super();
		boxList = new ArrayList<LayoutBox>();
		isContainer = true;
		this.isRoot = isRoot;
		fullBoxList = new ArrayList<LayoutBox>();
	}
	
	
	public abstract void computeMinimumWidth();
	
	public abstract void computeMinimumHeight();
	
	public abstract void assignWidthAndHeight(RectF bounds);
	
	public abstract void assignLocations();
	
	public void addBox(LayoutBox box){
		fullBoxList.add(box);
	}
	
	public void refreshBoxList(){
		boxList.clear();
		for (LayoutBox box:fullBoxList){
			if (!box.isDisabled){
				boxList.add(box);
			}
		}
	}
	
	//public abstract void draw(Canvas canvas);
	
/*	
	public void computeMinimumWidth(){
		
		if (horizontalLayoutDistributionMethod == WEIGHT_METHOD){
			float maxHorizontalUnitSize = 0;
			
			for (LayoutBox box:boxList){
				if (box.isContainer){
					Container container = (Container)box;
					container.updateLayout_pass_1(HORIZONTAL);
				}
				float horizontalUnitSize = box.minimumWidth / box.horizontalWeight;
				maxHorizontalUnitSize = Math.max(maxHorizontalUnitSize, horizontalUnitSize);
			}
			
			minimumWidth = 0;
			
			for (LayoutBox box:boxList){
				if (box.horizontalWeight > 0){
					minimumWidth += maxHorizontalUnitSize * box.horizontalWeight;
				}else{
					minimumWidth += box.minimumWidth;
				}
			}
		}else if (horizontalLayoutDistributionMethod == LAYOUT_PARAM_METHOD){
			minimumWidth = 0;
			
			for (LayoutBox box:boxList){
				if (box.isContainer){
					Container container = (Container)box;
					container.updateLayout_pass_1(HORIZONTAL);
				}
				minimumWidth += box.minimumWidth;
			}
		}
	}
	
	public void computeMinimumHeight(){
		if (verticalLayoutDistributionMethod == WEIGHT_METHOD){
			float maxVerticalUnitSize = 0;
			
			for (LayoutBox box:boxList){
				if (box.isContainer){
					Container container = (Container)box;
					container.updateLayout_pass_1(VERTICAL);
				}
				float verticalUnitSize = box.minimumHeight / box.verticalWeight;
				maxVerticalUnitSize = Math.max(maxVerticalUnitSize, verticalUnitSize);
			}
			
			minimumWidth = minimumHeight = 0;
			
			for (LayoutBox box:boxList){			
				if (box.verticalWeight > 0){
					minimumHeight += maxVerticalUnitSize * box.verticalWeight;
				}else{
					minimumHeight += box.minimumHeight;
				}
			}
		}else if (horizontalLayoutDistributionMethod == LAYOUT_PARAM_METHOD){
			minimumHeight = 0;
			
			for (LayoutBox box:boxList){
				if (box.isContainer){
					Container container = (Container)box;
					container.updateLayout_pass_1(VERTICAL);
				}
				minimumHeight += box.minimumHeight;
			}		
		}
	}
*/	

	public RectF computeMinimumSize(){
		computeMinimumWidth();
		computeMinimumHeight();
		RectF minimumRect = new RectF(0,0, minimumWidth, minimumHeight);
		return minimumRect;
	}
	
	public void placeTestLayout(RectF bounds){
		assignWidthAndHeight(bounds);
		assignLocations();
		WIDTH = bounds.width();
		HEIGHT = bounds.height();
		recreateBitmap(true);
	}
	
	public Bitmap getDrawnBitmap(){
		draw();
		return bitmap;
	}
	
	public void updateLayout_pass_1(int dimension){
		if (dimension == HORIZONTAL) computeMinimumWidth();
		else if (dimension == VERTICAL)	computeMinimumHeight();
	}

	public ArrayList<LayoutBox> getBoxList() {
		return boxList;
	}

	public void setBoxList(ArrayList<LayoutBox> boxList) {
		this.boxList = boxList;
	}


	public ArrayList<LayoutBox> getFullBoxList() {
		return fullBoxList;
	}


	public void setFullBoxList(ArrayList<LayoutBox> fullBoxList) {
		this.fullBoxList = fullBoxList;
	}
	
	

}
