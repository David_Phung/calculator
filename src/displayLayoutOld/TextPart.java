package displayLayoutOld;

import com.example.calculator03.PaintManager;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Paint.Style;
import android.util.Log;
import visualToken.TextArea;
import visualToken.VisualCursorManager;

public class TextPart extends Part {
	
	private VisualCursorManager VCM;

	public TextPart() {
		// TODO Auto-generated constructor stub
		super();
		PaintManager.textPaint.setColor(Color.BLUE);
		textArea = new TextArea(null, false, PaintManager.textPaint);
		VCM = new VisualCursorManager();
		VCM.getTAList().add(textArea);
		VCM.placeCursorAt(textArea, -1);
		TADisabled = false;
		bTouchResponsive = true;
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		textArea.getTokenList().clear();
		VCM.clear();
		textArea.update_pass_1(true);
	}

	@Override
	public void draw() {
		// TODO Auto-generated method stub
		if (bitmap == null) return;
		
		Canvas bitmapCanvas = new Canvas(bitmap);
		Paint paint = new Paint();
		paint.setStyle(Style.STROKE);
		paint.setColor(Color.parseColor("#188962"));
		
		//bitmapCanvas.drawPaint(paint);
		//bitmapCanvas.drawColor(Color.TRANSPARENT);
		
		bitmap.eraseColor(PaintManager.getColor(1));
		
		paint.setColor(Color.RED);
		//bitmapCanvas.drawRect(new Rect(0,0, bitmap.getWidth() - 1, bitmap.getHeight() -1), paint);
		
		textArea.drawSelf(bitmapCanvas);
		VCM.draw(bitmapCanvas);
	}

	@Override
	public void onTouch() {
		// TODO Auto-generated method stub

	}
	
	public void alignTextArea(){
		if (textArea == null) return;
		switch (textAreaAlignment) {
		case TOP_LEFT:		
			textArea.setLocation(new Point(0,0));
			textArea.update_pass_2();
			break;
		case BOTTOM_RIGHT:
			int x = 0;
			if (textArea == VCM.getFocusedTextArea() && VCM.getCursor().getElementIndex() == textArea.getTokenList().size() - 1 && !textArea.getTokenList().isEmpty()){
				x = (int) Math.max(WIDTH - textArea.getWIDTH() - VCM.getCursor().getThickness(), 0);
			}else{
				x = (int) Math.max(WIDTH - textArea.getWIDTH(), 0);
			}
			int y = (int) Math.max(HEIGHT - textArea.getHEIGHT(), 0);
			textArea.setLocation(new Point(x,y));
			textArea.update_pass_2();
			break;
		default:
			break;
		}
	}
	//---------------------- GETTERS SETTERS -------------------

	public VisualCursorManager getVCM() {
		return VCM;
	}

	public void setVCM(VisualCursorManager vCM) {
		VCM = vCM;
	}


	
	

}
