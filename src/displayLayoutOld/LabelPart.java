package displayLayoutOld;

import visualToken.StringToken;

public class LabelPart extends TextPart {

	public LabelPart() {
		// TODO Auto-generated constructor stub
		TADisabled = true;
		bTouchResponsive = false;
	}
	
	public void setText(String text){
		StringToken s = new StringToken(textArea, text);
		textArea.getTokenList().add(s);
		s.update_pass_1(true);
	}

}
