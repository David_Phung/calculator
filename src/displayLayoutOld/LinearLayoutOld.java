package displayLayoutOld;

import java.util.ArrayList;

import com.example.calculator03.PaintManager;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.RectF;
import android.util.Log;

public class LinearLayoutOld extends Container {
	
	public static final int VERTICAL = 0;
	public static final int HORIZONTAL = 1;
	
	private int layoutAlign;

	public LinearLayoutOld(boolean isRoot, int align) {
		// TODO Auto-generated constructor stub
		super(isRoot);
		this.layoutAlign = align;
	}
	
	@Override
	public void computeMinimumWidth() {
		// TODO Auto-generated method stub
		if (layoutAlign == HORIZONTAL){
			float maxHorizontalUnitSize = 0;
			for (LayoutBox box:boxList){
				if (box.isContainer){
					Container container = (Container)box;
					container.computeMinimumWidth();
				}
				
				if (horizontalLayoutDistributionMethod == WEIGHT_METHOD){
					float horizontalUnitSize = box.minimumWidth / box.horizontalWeight;
					maxHorizontalUnitSize = Math.max(maxHorizontalUnitSize, horizontalUnitSize);
				}else if (horizontalLayoutDistributionMethod == LAYOUT_PARAM_METHOD){
					minimumWidth += box.minimumWidth;
					minimumWidth += box.margin.getWidth();
				}
			}
			
			if (horizontalLayoutDistributionMethod == WEIGHT_METHOD){
				minimumWidth = 0;
				Log.d("","In LinearLayout, computeMinimumWidth(): maxHorizontalUnitSize =  " + maxHorizontalUnitSize);
				for (LayoutBox box:boxList){
					if (box.horizontalWeight > 0){
						minimumWidth += maxHorizontalUnitSize * box.horizontalWeight;
					}else{
						minimumWidth += box.minimumWidth;
					}
					minimumWidth += box.margin.getWidth();
					
					//Log.d("","In LinearLayout, computeMinimumWidth(): box.minimumWidth = " + box.minimumWidth);
					
				}
			}
		}else if (layoutAlign == VERTICAL){
			float maxHorizontalUnitSize = 0;
			float maxHorizontalWeight = 0;
			for (LayoutBox box:boxList){
				if (box.isContainer){
					Container container = (Container)box;
					container.computeMinimumWidth();
				}
				if (horizontalLayoutDistributionMethod == WEIGHT_METHOD){
					float horizontalUnitSize = box.minimumWidth / box.horizontalWeight;
					maxHorizontalUnitSize = Math.max(maxHorizontalUnitSize, horizontalUnitSize);
					maxHorizontalWeight = Math.max(maxHorizontalWeight, box.horizontalWeight);
				}else if (horizontalLayoutDistributionMethod == LAYOUT_PARAM_METHOD){
					minimumWidth = Math.max(minimumWidth, box.minimumWidth + box.margin.getWidth());
				}
			}
			
			if (horizontalLayoutDistributionMethod == WEIGHT_METHOD){
				for (LayoutBox box:boxList){
					float temp = maxHorizontalUnitSize * box.horizontalWeight + box.margin.getWidth();
					minimumWidth = Math.max(minimumWidth, temp);
				}
			}
		}
		Log.d("","In LinearLayout, computeMinimumWidth(): container minimumWidth = " + minimumWidth);
	}

	@Override
	public void computeMinimumHeight() {
		// TODO Auto-generated method stub
		if (layoutAlign == HORIZONTAL){
			float maxVerticalUnitSize = 0;
			for (LayoutBox box:boxList){
				if (box.isContainer){
					Container container = (Container)box;
					container.computeMinimumHeight();
				}
				
				if (verticalLayoutDistributionMethod == WEIGHT_METHOD){
					float verticalUnitSize = box.minimumHeight / box.verticalWeight;
					maxVerticalUnitSize = Math.max(maxVerticalUnitSize, verticalUnitSize);
				}else if (verticalLayoutDistributionMethod == LAYOUT_PARAM_METHOD){
					minimumHeight = Math.max(minimumHeight, box.minimumHeight + box.margin.getHeight());
				}
			}
			
			if (verticalLayoutDistributionMethod == WEIGHT_METHOD){
				for (LayoutBox box:boxList){
					float temp = maxVerticalUnitSize * box.verticalWeight + box.margin.getHeight();
					minimumHeight = Math.max(minimumHeight, temp);
				}
			}
		}else if (layoutAlign == VERTICAL){
			float maxVerticalUnitSize = 0;
			for (LayoutBox box:boxList){
				if (box.isContainer){
					Container container = (Container)box;
					container.computeMinimumHeight();
				}
				
				if (verticalLayoutDistributionMethod == WEIGHT_METHOD){
					float verticalUnitSize = box.minimumHeight / box.verticalWeight;
					maxVerticalUnitSize = Math.max(maxVerticalUnitSize, verticalUnitSize);
				}else if (verticalLayoutDistributionMethod == LAYOUT_PARAM_METHOD){
					minimumHeight += box.minimumHeight;
					minimumHeight += box.margin.getHeight();
				}
			}
			
			if (verticalLayoutDistributionMethod == WEIGHT_METHOD){
				minimumHeight = 0;
				
				for (LayoutBox box:boxList){
					if (box.verticalWeight > 0){
						minimumHeight += maxVerticalUnitSize * box.verticalWeight;
					}else{
						minimumHeight += box.minimumHeight;
					}
					minimumHeight += box.margin.getHeight();
				}
			}	
		}
		Log.d("","In LinearLayout, computeMinimumHeight(): container minimumWidth = " + minimumWidth);
	}

	public void assignWidthAndHeight(RectF bounds){
		RectF tempBounds = new RectF(bounds);
		for (LayoutBox box:boxList){
			tempBounds.left += box.margin.left;
			tempBounds.right -= box.margin.right;
			tempBounds.top += box.margin.top;
			tempBounds.bottom -= box.margin.bottom;
		}
		if (layoutAlign == HORIZONTAL){
			if (horizontalLayoutDistributionMethod == WEIGHT_METHOD){
				float weightSum = 0;
				for (LayoutBox box:boxList){
					weightSum += box.horizontalWeight;
				}
				for (LayoutBox box:boxList){
					if (box.horizontalWeight > 0){
						box.WIDTH = Math.max(tempBounds.width() / weightSum * box.horizontalWeight, box.minimumWidth);
					}else{
						box.WIDTH = box.minimumWidth;
					}
					Log.d("","In LinearLayout assignWidthAndHeight(): box.WIDTH = " + box.WIDTH );
				}
			}else if (horizontalLayoutDistributionMethod == LAYOUT_PARAM_METHOD){
				float spaceLeft = tempBounds.width();
				ArrayList<LayoutBox> matchParentList = new ArrayList<LayoutBox>();
				for (LayoutBox box:boxList){
					if(box.horizontalLayoutParam == WRAP_CONTENT){
						box.WIDTH = box.minimumWidth;
						spaceLeft -= box.WIDTH;
					}else if (box.horizontalLayoutParam == MATCH_PARENT){
						matchParentList.add(box);
					}
				}
				boolean done = false;
				while (!matchParentList.isEmpty() && done == false){
					done = true;
					float temp = spaceLeft / matchParentList.size();
					for (int i = 0 ; i < matchParentList.size(); i++){ 
						LayoutBox box = matchParentList.get(i);
						if (box.minimumWidth > temp){
							box.WIDTH = box.minimumWidth;
							done = false;
							spaceLeft -= box.WIDTH;
							matchParentList.remove(box);
							i--;
						}
					}
				}
				for (LayoutBox box:matchParentList){
					box.WIDTH = spaceLeft / matchParentList.size();
				}
			}
			
			if (verticalLayoutDistributionMethod == WEIGHT_METHOD){
				float smallestUnitSize = bounds.height();
				for (LayoutBox box:boxList){
					if (box.verticalWeight < 0) continue;
					float temp = (bounds.height() - box.margin.getHeight()) / box.verticalWeight;
					smallestUnitSize = Math.min(smallestUnitSize, temp);
				}
				for (LayoutBox box:boxList){
					if (box.verticalWeight > 0){
						box.HEIGHT = Math.max(smallestUnitSize * box.verticalWeight, box.minimumHeight);
					}else{
						box.HEIGHT = box.minimumHeight;
					}
				}
			}else if (verticalLayoutDistributionMethod == LAYOUT_PARAM_METHOD){
				for (LayoutBox box:boxList){
					if (box.verticalLayoutParam == WRAP_CONTENT){
						box.HEIGHT = box.minimumHeight;
					}else if (box.verticalLayoutParam == MATCH_PARENT){
						box.HEIGHT = bounds.height() - box.margin.getHeight();
					}
				}
			}
		}else if (layoutAlign == VERTICAL){
			if (horizontalLayoutDistributionMethod == WEIGHT_METHOD){
				float smallestUnitSize = bounds.width();
				for (LayoutBox box:boxList){
					if (box.horizontalWeight < 0) continue;
					float temp = (bounds.width() - box.margin.getWidth()) / box.horizontalWeight;
					smallestUnitSize = Math.min(smallestUnitSize, temp);
				}
				for (LayoutBox box:boxList){
					if (box.horizontalWeight > 0){
						box.WIDTH = Math.max(smallestUnitSize * box.horizontalWeight, box.minimumWidth);
					}else{
						box.WIDTH = box.minimumWidth;
					}
				}
			}else if (horizontalLayoutDistributionMethod == LAYOUT_PARAM_METHOD){
				for (LayoutBox box:boxList){
					if (box.horizontalLayoutParam == WRAP_CONTENT){
						box.WIDTH = box.minimumWidth;
					}else if (box.horizontalLayoutParam == MATCH_PARENT){
						box.WIDTH = bounds.width() - box.margin.getWidth();
					}
				}
			}
			
			if (verticalLayoutDistributionMethod == WEIGHT_METHOD){
				float weightSum = 0;
				for (LayoutBox box:boxList){
					weightSum += box.verticalWeight;
				}
				for (LayoutBox box:boxList){
					if (box.verticalWeight > 0){
						box.HEIGHT = Math.max(tempBounds.height() / weightSum * box.verticalWeight, box.minimumHeight);
					}else{
						box.HEIGHT = box.minimumHeight;
					}
				}
			}else if (verticalLayoutDistributionMethod == LAYOUT_PARAM_METHOD){
				float spaceLeft = tempBounds.height();
				ArrayList<LayoutBox> matchParentList = new ArrayList<LayoutBox>();
				for (LayoutBox box:boxList){
					if(box.verticalLayoutParam == WRAP_CONTENT){
						box.HEIGHT = box.minimumHeight;
						spaceLeft -= box.HEIGHT;
					}else if (box.verticalLayoutParam == MATCH_PARENT){
						matchParentList.add(box);
					}
				}
				boolean done = false;
				while (!matchParentList.isEmpty() && done == false){
					done = true;
					float temp = spaceLeft / matchParentList.size();
					for (int i = 0 ; i < matchParentList.size(); i++){ 
						LayoutBox box = matchParentList.get(i);
						if (box.minimumHeight > temp){
							box.HEIGHT = box.minimumHeight;
							done = false;
							spaceLeft -= box.HEIGHT;
							matchParentList.remove(box);
							i--;
						}
					}
				}
				for (LayoutBox box:matchParentList){
					box.HEIGHT = spaceLeft / matchParentList.size();
				}
			}
		}
		
		for (LayoutBox box:boxList){
			if (box.isContainer){
				RectF boxtempBounds = new RectF(0, 0, box.WIDTH, box.HEIGHT);
				((Container)box).assignWidthAndHeight(boxtempBounds);
			}
		}
	}
	
	public void assignLocations(){
		if (layoutAlign == HORIZONTAL){
			float x = 0;
			float y = 0;
			for (LayoutBox box:boxList){
				y = box.margin.top;
				x += box.margin.left;
				box.setLocation(new PointF(x,y));
				x += box.WIDTH + box.margin.right;
			}
		}else if (layoutAlign == VERTICAL){
			float x = 0;
			float y = 0;
			for (LayoutBox box:boxList){
				x = box.margin.left;
				y += box.margin.top;
				box.setLocation(new PointF(x,y));
				y += box.HEIGHT + box.margin.bottom;
			}
		}
		
		for (LayoutBox box:boxList){
			if (box.isContainer){
				PointF loc = new PointF(box.getLocation().x, box.getLocation().y);
				((Container)box).assignLocations();
			}
		}
	}

	public void draw(){
		if (bitmap == null) return;
		Canvas tempCanvas = new Canvas(bitmap);
		bitmap.eraseColor(PaintManager.getColor(1));
		int count = 0;
		for (LayoutBox box:boxList){
			if (box.bitmap == null) continue;
			box.draw();
			tempCanvas.drawBitmap(box.bitmap, box.location.x, box.location.y, new Paint());
			box.margin.drawSelf(tempCanvas, box.getBounds());
		}
	}
}
