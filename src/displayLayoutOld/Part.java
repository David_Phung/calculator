package displayLayoutOld;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import visualToken.TextArea;

public abstract class Part extends LayoutBox {
	
	public static final int TOP_LEFT = 0;
	public static final int BOTTOM_RIGHT = 1;
	
	protected TextArea textArea;
	protected boolean TADisabled, bTouchResponsive;
	protected int textAreaAlignment;

	public Part() {
		// TODO Auto-generated constructor stub
		super();
	}
	
	public abstract void clear();
	
	public abstract void draw();
	
	public abstract void onTouch();

	public TextArea getTextArea() {
		return textArea;
	}

	public void setTextArea(TextArea textArea) {
		this.textArea = textArea;
	}

	public void alignTextArea(){
		if (textArea == null) return;
		switch (textAreaAlignment) {
		case TOP_LEFT:		
			textArea.setLocation(new Point(0,0));
			textArea.update_pass_2();
			break;
		case BOTTOM_RIGHT:
			int x = (int) Math.max(WIDTH - textArea.getWIDTH(), 0);
			int y = (int) Math.max(HEIGHT - textArea.getHEIGHT(), 0);
			textArea.setLocation(new Point(x,y));
			textArea.update_pass_2();
			break;
		default:
			break;
		}
	}

	public boolean isTADisabled() {
		return TADisabled;
	}

	public void setTADisabled(boolean tADisabled) {
		TADisabled = tADisabled;
	}

	public boolean isbTouchResponsive() {
		return bTouchResponsive;
	}

	public void setbTouchResponsive(boolean bTouchResponsive) {
		this.bTouchResponsive = bTouchResponsive;
	}

	public int getTextAreaAlignment() {
		return textAreaAlignment;
	}

	public void setTextAreaAlignment(int textAreaAlignment) {
		this.textAreaAlignment = textAreaAlignment;
	}
	
	
	
}
