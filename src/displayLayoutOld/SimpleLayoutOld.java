package displayLayoutOld;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.RectF;

public class SimpleLayoutOld extends Container {

	public SimpleLayoutOld(boolean isRoot) {
		// TODO Auto-generated constructor stub
		super(isRoot);
	}

	@Override
	public void computeMinimumWidth() {
		// TODO Auto-generated method stub
		if (boxList.isEmpty()){
			
		}else{
			LayoutBox box = boxList.get(0);
			if (box.isContainer){
				Container container = (Container)box;
				container.computeMinimumWidth();
			}
			minimumWidth += box.minimumWidth;
			minimumWidth += box.margin.getWidth();
		}
	}

	@Override
	public void computeMinimumHeight() {
		// TODO Auto-generated method stub
		if (boxList.isEmpty()){
			
		}else{
			LayoutBox box = boxList.get(0);
			if (box.isContainer){
				Container container = (Container)box;
				container.computeMinimumWidth();
			}
			minimumHeight += box.minimumHeight;
			minimumHeight += box.margin.getHeight();
		}
	}

	@Override
	public void assignWidthAndHeight(RectF bounds) {
		// TODO Auto-generated method stub
		if (boxList.isEmpty()){
			
		}else{
			LayoutBox box = boxList.get(0);
			RectF tempBounds = new RectF(bounds);
			
			tempBounds.left += box.margin.left;
			tempBounds.right -= box.margin.right;
			tempBounds.top += box.margin.top;
			tempBounds.bottom -= box.margin.bottom;
			
			box.WIDTH = Math.max(tempBounds.width(), box.minimumWidth);
			box.HEIGHT = Math.max(tempBounds.height(), box.minimumHeight);
			
			if (box.isContainer){
				RectF boxtempBounds = new RectF(0, 0, box.WIDTH, box.HEIGHT);
				((Container)box).assignWidthAndHeight(boxtempBounds);
			}
		}
	}

	@Override
	public void assignLocations() {
		// TODO Auto-generated method stub
		if (!boxList.isEmpty()){
			float x = 0;
			float y = 0;
			LayoutBox box = boxList.get(0);
			y = box.margin.top;
			x += box.margin.left;
			box.setLocation(new PointF(x,y));
		}
	}

	@Override
	public void draw() {
		// TODO Auto-generated method stub
		if (bitmap == null) return;
		Canvas tempCanvas = new Canvas(bitmap);
		bitmap.eraseColor(Color.RED);
		int count = 0;
		if (!boxList.isEmpty()){
			LayoutBox box = boxList.get(0);
			box.draw();
			tempCanvas.drawBitmap(box.bitmap, box.location.x, box.location.y, new Paint());
			box.margin.drawSelf(tempCanvas, box.getBounds());
		}
	}


}
