package displayLayoutOld;

import com.example.calculator03.PaintManager;

import android.graphics.Canvas;
import android.graphics.PointF;
import android.graphics.RectF;

public class Margin {
	
	public float top, left, right ,bottom;
	
	public Margin() {
		// TODO Auto-generated constructor stub
	}
	
	public Margin(float left, float top, float right, float bottom) {
		super();
		this.top = top;
		this.left = left;
		this.right = right;
		this.bottom = bottom;
	}

	public float getWidth(){
		return left + right;
	}
	
	public float getHeight(){
		return top + bottom;
	}
	
	public void drawSelf(Canvas canvas, RectF ownerBounds){
		RectF topR = new RectF(ownerBounds.left - left, ownerBounds.top - top, ownerBounds.right + right, ownerBounds.top);
		RectF bottomR = new RectF(ownerBounds.left - left, ownerBounds.bottom, ownerBounds.right + right, ownerBounds.bottom + bottom);
		RectF leftR = new RectF(ownerBounds.left - left, ownerBounds.top, ownerBounds.left, ownerBounds.bottom);
		RectF rightR = new RectF(ownerBounds.right, ownerBounds.top, ownerBounds.right + right, ownerBounds.bottom);
		
		canvas.drawRect(topR, PaintManager.paddingPaint);
		canvas.drawRect(bottomR, PaintManager.paddingPaint);
		canvas.drawRect(leftR, PaintManager.paddingPaint);
		canvas.drawRect(rightR, PaintManager.paddingPaint);
	}

}
