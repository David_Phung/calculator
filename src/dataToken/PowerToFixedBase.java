package dataToken;

public class PowerToFixedBase extends PowerToDataToken {

	private String base;
	
	public PowerToFixedBase(Expression parent, String base) {
		super(parent);
		// TODO Auto-generated constructor stub
		this.base = new String(base);
		tempBase.setPowerToTempBase(false);
		NumberToken nbr = new NumberToken(tempBase, base);
		tempBase.getTokenList().add(nbr);
		bTempBaseEnabled = true;
		bFixedBase = true;
	}
	
	@Override
	public void updateTempBase() {
		// TODO Auto-generated method stub
		bTempBaseEnabled = true;
	}
	
	@Override
	public DataToken clone(Expression parent) {
		// TODO Auto-generated method stub
		PowerToFixedBase token = new PowerToFixedBase(parent, base);
		token.setExponent(exponent.clone(token));
		return token;
	}
	
	@Override
	public int getNumberExpression() {
		// TODO Auto-generated method stub
		return 1;
	}
	
	@Override
	public Expression getExpression(int index) {
		// TODO Auto-generated method stub
		if (index == 0) return exponent;
		return null;
	}
	
	@Override
	public int getIndexOfExpression(Expression expression) {
		// TODO Auto-generated method stub
		if (expression == exponent)  return 0;
		return -1;
	}
}
