package dataToken;

import java.math.BigDecimal;

import computing.DataProcessor;

public class PowerToDataToken extends FunctionToken {
	
	protected Expression exponent;
	protected Expression tempBase;
	protected boolean bTempBaseEnabled;
	protected boolean bFixedBase;

	public PowerToDataToken(Expression parent) {
		super(parent, FRACTION_DATA_TOKEN);
		// TODO Auto-generated constructor stub
		exponent = new Expression(this, false);
		tempBase = new Expression(this, false);
		tempBase.setPowerToTempBase(true);
		bTempBaseEnabled = true;
		identifier += " " + POWER_TO_DATA_TOKEN_ID;
		bFixedBase = false;
	}
	
	public void updateTempBase(){
		int index = parent.getTokenList().indexOf(this);
		if (index <= 0){
			bTempBaseEnabled = true;
		}else{
			bTempBaseEnabled = false;
		}
	}

	@Override
	public void infixToPostfix(DataProcessor DP) {
		// TODO Auto-generated method stub

	}

	@Override
	public DataToken calculate(DataProcessor DP) {
		// TODO Auto-generated method stub
		DataToken exponentValue = DP.evaluatePostfix(exponent);
		if (exponentValue.identifier.contains(DataToken.FLAG_TOKEN_ID)) return exponentValue;
		
		NumberToken exponentNumber = (NumberToken) exponentValue;
		Double exponentD = Double.parseDouble(exponentNumber.getNumber());
		
		BigDecimal exponentBD = new BigDecimal(exponentNumber.getNumber());
		
		return new NumberToken(parent, exponentBD.toPlainString(), exponentBD);
	}
	
	public DataToken calculate1(DataProcessor DP){
		return DP.evaluatePostfix(exponent);
	}
	
	@Override
	public DataToken clone(Expression parent) {
		// TODO Auto-generated method stub
		PowerToDataToken token = new PowerToDataToken(parent);
		token.bTempBaseEnabled = bTempBaseEnabled;
		token.setExponent(exponent.clone(token));
		return token;
	}

	@Override
	public String getReportString(DataCursorManager DCM) {
		// TODO Auto-generated method stub
		if (bTempBaseEnabled){
			return "Pow{" + tempBase.getReportString(DCM) + "," + exponent.getReportString(DCM) + "}";
		}
		return "Pow{" + exponent.getReportString(DCM) + "}";
	}
	
	@Override
	public int getNumberExpression() {
		// TODO Auto-generated method stub
		if (bTempBaseEnabled) return 2;
		return 1;
	}

	@Override
	public Expression getExpression(int index) {
		// TODO Auto-generated method stub
		if (bTempBaseEnabled){
			if (index == 0) return tempBase;
			if (index == 1) return exponent;
		}
		if (index == 0) return exponent;
		return null;
	}

	@Override
	public int getIndexOfExpression(Expression expression) {
		// TODO Auto-generated method stub
		if (bTempBaseEnabled){
			if (expression == tempBase) return 0;
			if (expression == exponent) return 1;
		}
		if (expression == exponent) return 0;
		return -1;
	}

	@Override
	public void setExpression(int index, Expression expression) {
		// TODO Auto-generated method stub
		if (bTempBaseEnabled ){
			if (index == 0){
				tempBase = expression;
			}else if (index == 1){
				exponent = expression;
			}
		}else if (index == 0){
			exponent = expression;
		}
	}
	
	public Expression getExponent() {
		return exponent;
	}

	public void setExponent(Expression exponent) {
		this.exponent = exponent;
	}

	public Expression getTempBase() {
		return tempBase;
	}

	public void setTempBase(Expression tempBase) {
		this.tempBase = tempBase;
	}

	public boolean isbTempBaseEnabled() {
		return bTempBaseEnabled;
	}

	public void setbTempBaseEnabled(boolean bTempBaseEnabled) {
		this.bTempBaseEnabled = bTempBaseEnabled;
	}

	public boolean isbFixedBase() {
		return bFixedBase;
	}

	public void setbFixedBase(boolean bFixedBase) {
		this.bFixedBase = bFixedBase;
	}

	
	

}
