package dataToken;

import computing.DataProcessor;

public class CombinatoricAndPolRectDataToken extends FunctionToken {
	
	public static final int PERMUTATION = 0;
	public static final int COMBNATION = 1;
	public static final int POL = 2;
	public static final int RECT = 3;
	
	public static final String PERMUTATION_ID = "Permutation";
	public static final String COMBINTAITON_ID = "Combination";
	public static final String POL_ID = "Pol";
	public static final String RECT_ID = "Rect";
	
	private Expression n,r;
	private int combinatoricType;
	public CombinatoricAndPolRectDataToken(Expression parent, int combinatoricType) {
		super(parent, COMBINATORIC_TOKEN);
		// TODO Auto-generated constructor stub
		n = new Expression(this, false);
		r = new Expression(this, false);
		this.combinatoricType = combinatoricType;
		switch (combinatoricType) {
		case PERMUTATION:		
			identifier += " " + PERMUTATION_ID;
			break;
		case COMBNATION:
			identifier += " " + COMBINATORIC_POL_RECT_ID;
			break;
		default:
			break;
		}
	}

	@Override
	public void infixToPostfix(DataProcessor DP) {
		// TODO Auto-generated method stub

	}

	@Override
	public DataToken calculate(DataProcessor DP) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public DataToken calculate1(DataProcessor DP) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getNumberExpression() {
		// TODO Auto-generated method stub
		return 2;
	}

	@Override
	public Expression getExpression(int index) {
		// TODO Auto-generated method stub
		if (index == 0) return n;
		if (index == 1) return r;
		return null;
	}

	@Override
	public int getIndexOfExpression(Expression expression) {
		// TODO Auto-generated method stub
		if (expression == n) return 0;
		if (expression == r) return 1;
		return -1;
	}

	@Override
	public void setExpression(int index, Expression expression) {
		// TODO Auto-generated method stub
		if (index == 0) n = expression;
		if (index == 1) r = expression;
	}

	@Override
	public DataToken clone(Expression parent) {
		// TODO Auto-generated method stub
		CombinatoricAndPolRectDataToken token = new CombinatoricAndPolRectDataToken(parent, combinatoricType);
		token.setN(n.clone(token));
		token.setR(r.clone(token));
		return token;
	}

	@Override
	public String getReportString(DataCursorManager DCM) {
		// TODO Auto-generated method stub
		String s = new String();
		switch (combinatoricType) {
		case PERMUTATION:
			s = "P";
			break;
		case COMBNATION:
			s = "C";
			break;
		case POL:
			s = "Pol";
			break;
		case RECT:
			s = "Rect";
			break;
		default:
			break;
		}
		
		s += "{" + n.getReportString(DCM) + "," + r.getReportString(DCM) + "}";
		return s;
	}

	public Expression getN() {
		return n;
	}

	public void setN(Expression n) {
		this.n = n;
	}

	public Expression getR() {
		return r;
	}

	public void setR(Expression r) {
		this.r = r;
	}
	
	

}
