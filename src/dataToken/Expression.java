package dataToken;

import java.util.ArrayList;

import computing.DataProcessor;

public class Expression {
	
	private DataToken parent;
	private ArrayList<DataToken> tokenList;
	private boolean isRoot;
	private boolean isPowerToTempBase;
	
	public Expression(DataToken parent, boolean isRoot){
		this.parent = parent;
		this.isRoot = isRoot;
		tokenList = new ArrayList<DataToken>();
		isPowerToTempBase = false;
	}
	
	public Expression infixToPostfix(DataProcessor DP){
		
		return null;
	}
	
	public DataToken calculate(DataProcessor DP){
		return null;
	}
	
	public String getReportString(DataCursorManager DCM){
		String report = new String();
		if (DCM != null && DCM.getCurrFocusedExpression() == this && (tokenList.size() == 0 || DCM.getCursor_elementIndex() == -1)){
			report += "�";
		}
		int i = 0;
		for (DataToken token:tokenList){
			report += token.getReportString(DCM) + " ";
			if (DCM != null && DCM.getCurrFocusedExpression() == this && i == DCM.getCursor_elementIndex()){
				report += "�";
			}
			i++;
		}
		return report;
	}
	
	public Expression clone(DataToken parent){
		Expression expression = new Expression(parent, isRoot);
		for (DataToken token:tokenList){
			expression.getTokenList().add(token.clone(expression));
		}
		return expression;
	}

	public DataToken getParent() {
		return parent;
	}

	public void setParent(DataToken parent) {
		this.parent = parent;
	}

	public ArrayList<DataToken> getTokenList() {
		return tokenList;
	}

	public void setTokenList(ArrayList<DataToken> tokenList) {
		this.tokenList = tokenList;
	}

	public boolean isRoot() {
		return isRoot;
	}

	public void setRoot(boolean isRoot) {
		this.isRoot = isRoot;
	}

	public boolean isPowerToTempBase() {
		return isPowerToTempBase;
	}

	public void setPowerToTempBase(boolean isPowerToTempBase) {
		this.isPowerToTempBase = isPowerToTempBase;
	}
	
	

}
