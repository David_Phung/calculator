package dataToken;

import java.util.ArrayList;

public class ContainerToken extends DataToken {
	
	public static final int ADD_SUB_CONTAINER_TYPE = 0;
	public static final int MULT_DIV_MOD_CONTAINER_TYPE = 1;
	public static final int POST_UNARY_CONTAINER_TYPE = 2;
	public static final int PRE_UNARY_CONTAINER_TYPE = 3;
	public static final int PRE_UNARY_NEGATIVE_SIGN_CONTAINER_TYPE = 4;
	public static final int FUNCTION_CONTAINER_TYPE = 5;
	public static final int POWER_TO_CONTAINER_TYPE = 6;
	
	public static final String ADD_SUB_CONTAINER_TYPE_ID = "AddSubContainerType";
	public static final String MULT_DIV_MOD_CONTAINER_TYPE_ID = "MultDivModContainerType";
	public static final String POST_UNARY_CONTAINER_TYPE_ID = "PostUnaryContainerType";
	public static final String PRE_UNARY_CONTAINER_TYPE_ID = "PreUnaryContainerType";
	public static final String PRE_UNARY_NEGATIVE_SIGN_CONTAINER_TYPE_ID = "NegativeSignContainerType";
	public static final String FUNCTION_CONTAINER_TYPE_ID = "FunctionContainerType";
	public static final String POWER_TO_CONTAINER_TYPE_ID = "PowerToContainerType";
	
	private ArrayList<DataToken> tokenList;
	public int containerType;
	
	public static int getContainerType(DataToken token){
		if (token.identifier.contains(BinaryOperator.ADDITION_ID) || token.identifier.contains(BinaryOperator.SUBTRACTION_ID)){
			return ADD_SUB_CONTAINER_TYPE;
		}else if (token.identifier.contains(BinaryOperator.MULTIPLCATION_ID) || token.identifier.contains(BinaryOperator.DIVISION_ID) 
				|| token.identifier.contains(BinaryOperator.MODULUS_ID)){
			return MULT_DIV_MOD_CONTAINER_TYPE;
		}else if (token.identifier.contains(UnaryOperator.POST_UNARY_ID)){
			return POST_UNARY_CONTAINER_TYPE;
		}else if (token.identifier.contains(UnaryOperator.NEGATIVE_SIGN_ID)){
			return PRE_UNARY_NEGATIVE_SIGN_CONTAINER_TYPE;
		}else if (token.identifier.contains(UnaryOperator.PRE_UNARY_ID)){
			return PRE_UNARY_CONTAINER_TYPE;
		}else if (token.identifier.contains(FUNCTION_TOKEN_ID)){
			return FUNCTION_CONTAINER_TYPE;
		}else if (token.identifier.contains(BinaryOperator.POWER_TO_ID)){
			return POWER_TO_CONTAINER_TYPE;
		}
		
		return -1;
	}

	public ContainerToken(Expression parent, int containerType) {
		super(CONTAINER_TOKEN, parent);
		// TODO Auto-generated constructor stub
		this.containerType = containerType;
		identifier += " " + CONTAINER_TOKEN_ID;
		tokenList = new ArrayList<DataToken>();
		
		switch (containerType) {
		case ADD_SUB_CONTAINER_TYPE:
			identifier += " " + ADD_SUB_CONTAINER_TYPE_ID;
			break;
		case MULT_DIV_MOD_CONTAINER_TYPE:
			identifier += " " + MULT_DIV_MOD_CONTAINER_TYPE_ID;
			break;
		case POST_UNARY_CONTAINER_TYPE:
			identifier += " " + POST_UNARY_CONTAINER_TYPE_ID;
			break;
		case PRE_UNARY_NEGATIVE_SIGN_CONTAINER_TYPE:
			identifier += " " + PRE_UNARY_NEGATIVE_SIGN_CONTAINER_TYPE_ID;
			break;
		case PRE_UNARY_CONTAINER_TYPE:
			identifier += " " + PRE_UNARY_CONTAINER_TYPE_ID;
			break;
		case FUNCTION_CONTAINER_TYPE:
			identifier += " " + FUNCTION_CONTAINER_TYPE_ID;
			break;
		case POWER_TO_CONTAINER_TYPE:
			identifier += " " + POWER_TO_CONTAINER_TYPE_ID;
			break;
		default:
			break;
		}
	}

	@Override
	public DataToken clone(Expression parent) {
		// TODO Auto-generated method stub
		ContainerToken container = new ContainerToken(parent, containerType);
		for (DataToken token:tokenList){
			container.tokenList.add(token.clone(parent));
		}
		return container;
	}
	
	@Override
	public String getReportString(DataCursorManager DCM) {
		// TODO Auto-generated method stub
		String s = new String();
		for (DataToken t:tokenList){
			s += t.getReportString(DCM) + ",";
		}
		if (s.length() > 0) s = s.substring(0, s.length() - 1);
		
		String code = "";
		switch (containerType) {
		case ADD_SUB_CONTAINER_TYPE:
			code = "AddSub";
			break;
		case MULT_DIV_MOD_CONTAINER_TYPE:
			code = "MultDiv";
			break;
		case POST_UNARY_CONTAINER_TYPE:
			code = "PostU";
			break;
		case PRE_UNARY_CONTAINER_TYPE:
			code = "PreU";
			break;
		case PRE_UNARY_NEGATIVE_SIGN_CONTAINER_TYPE:
			code = "Neg";
			break;
		case FUNCTION_CONTAINER_TYPE:
			code = "Func";
			break;
		case POWER_TO_CONTAINER_TYPE:
			code = "Pow";
			break;
		default:
			break;
		}
		
		return "Con" + "(" + code + "){" + s + "}";
	}

	public ArrayList<DataToken> getTokenList() {
		return tokenList;
	}

	public void setTokenList(ArrayList<DataToken> tokenList) {
		this.tokenList = tokenList;
	}
	
	

}
