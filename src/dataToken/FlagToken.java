package dataToken;

public class FlagToken extends DataToken {
	
	public static final int SYNTAX_ERROR = 0;
	public static final int MATH_ERROR = 1;
	
	public static final String SYNTAX_ERROR_ID = "SyntaxError";
	public static final String MATH_ERROR_ID = "MathError";

	public int flag;
	
	public FlagToken(Expression parent, int flag) {
		super(FLAG_TOKEN, parent);
		// TODO Auto-generated constructor stub
		this.flag = flag;
		identifier += FLAG_TOKEN_ID;
		
		if (flag == SYNTAX_ERROR){
			identifier += " " + SYNTAX_ERROR_ID;
		}else if (flag == MATH_ERROR){
			identifier += " " + MATH_ERROR_ID;
		}
	}
	
	public String getFlagString(){
		if (flag == SYNTAX_ERROR) return "Syntax Error";
		if (flag == MATH_ERROR) return "Math Error";
		
		return "";
	}
	
	@Override
	public DataToken clone(Expression parent) {
		// TODO Auto-generated method stub
		return new FlagToken(parent, flag);
	}
	
	@Override
	public String getReportString(DataCursorManager DCM) {
		// TODO Auto-generated method stub
		String s = new String();
		switch (flag) {
		case SYNTAX_ERROR:
			s = "SyntaxError";
			break;
		case MATH_ERROR:
			s = "MathError";
			break;
		default:
			break;
		}
		return "Flag{" + s + "}";
	}
	
	public String getErrorString(){
		switch (flag) {
		case SYNTAX_ERROR:
			return "Syntax Error";
		case MATH_ERROR:
			return "Math Error";
		default:
			break;
		}
		return null;
	}

}
