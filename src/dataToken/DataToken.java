package dataToken;

import android.R.id;

public abstract class DataToken {
	
	public static final int FLAG_TOKEN = 0;
	public static final int DIGIT_TOKEN = 1;
	public static final int NUMBER_TOKEN = 2;
	public static final int OPERATOR_TOKEN = 3;
	public static final int FUNCTION_TOKEN = 4;
	public static final int CONTAINER_TOKEN = 5;
	
	public static final int RESULT_TOKEN = 6;
	
	public static final String FLAG_TOKEN_ID = "Flag";
	public static final String DIGIT_TOKEN_ID = "Digit";
	public static final String NUMBER_TOKEN_ID = "Number";
	public static final String OPERATOR_TOKEN_ID = "Operator";
	public static final String FUNCTION_TOKEN_ID = "Function";
	public static final String CONTAINER_TOKEN_ID = "Container";
	
	public static final String RESULT_TOKEN_ID = "Result";
	
	public int type;
	public String identifier;
	
	protected Expression parent;

	public DataToken(int type, Expression parent) {
		// TODO Auto-generated constructor stub
		this.parent = parent;
		this.type = type;
		identifier = new String();
	}
	
	public abstract DataToken clone(Expression parent);

	public abstract String getReportString(DataCursorManager DCM);
	
	public Expression getParent() {
		return parent;
	}

	public void setParent(Expression parent) {
		this.parent = parent;
	}
	
	

}
