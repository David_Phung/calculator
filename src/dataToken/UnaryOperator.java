package dataToken;

import java.math.BigDecimal;

import computing.BigDecimalFunctions;
import computing.DataProcessor;
import computing.RationalRootExpression;
import computing.TrigTable;

public class UnaryOperator extends OperatorToken {
	
	public static final int POST_UNARY = 0;
	public static final int PRE_UNARY = 1;
	
	public static final int NEGATIVE_SIGN = 0;
	public static final int SINE_TRIG_FUNCTION = 1;
	public static final int COSINE_TRIG_FUNCTION = 2;
	public static final int TAN_TRIG_FUNCTION = 3;
	public static final int INVERSE_SIN_TRIG_FUNCTION = 4;
	public static final int INVERSE_COS_TRIG_FUNCTION = 5;
	public static final int INVERSE_TAN_TRIG_FUNCTION = 6;
	
	public static final int SINH_TRIG_FUNCTION = 7;
	public static final int COSH_TRIG_FUNCTION = 8;
	public static final int TANH_TRIG_FUNCTION = 9;
	public static final int INVERSE_SINH_TRIG_FUNCTION = 10;
	public static final int INVERSE_COSH_TRIG_FUNCTION = 11;
	public static final int INVERSE_TANH_TRIG_FUNCTION = 12;
	
	public static final String POST_UNARY_ID = "PostUnary";
	public static final String PRE_UNARY_ID = "PreUnary";
	
	public static final String NEGATIVE_SIGN_ID = "NegativeSign";
	public static final String SINE_TRIG_FUNCTION_ID = "SineTrig";
	public static final String COSINE_TRIG_FUNCTION_ID = "CosineTrig";
	public static final String TAN_TRIG_FUNCTION_ID = "TanTrig";
	public static final String INVERSE_SIN_TRIG_FUNCTION_ID = "InverseSin";
	public static final String INVERSE_COS_TRIG_FUNCTION_ID = "InverseCos";
	public static final String INVERSE_TAN_TRIG_FUNCTION_ID = "InverseTan";
	
	public static final String SINH_TRIG_FUNCTION_ID = "SinhTrig";
	public static final String COSH_TRIG_FUNCTION_ID = "CoshTrig";
	public static final String TANH_TRIG_FUNCTION_ID = "TanhTrig";
	public static final String INVERSE_SINH_TRIG_FUNCTION_ID = "InverseSinh";
	public static final String INVERSE_COSH_TRIG_FUNCTION_ID = "InverseCosh";
	public static final String INVERSE_TANH_TRIG_FUNCTION_ID = "InverseTanh";
	
	public static final String IS_TRIG_ID = "IsTrig";
	
	public int unaryOperatorType;
	public int position;
	
	public UnaryOperator(Expression parent, int unaryOperatorType, int position) {
		super(parent, UNARY_OPERATOR);
		// TODO Auto-generated constructor stub
		this.position = position;
		this.unaryOperatorType = unaryOperatorType;
		identifier += " " + UNARY_OPERATOR_ID;
		
		switch (position) {
		case PRE_UNARY:
			identifier += " " + PRE_UNARY_ID;
			association = RIGHT_ASSOCIATION;
			precedence = 5;
			break;
		case POST_UNARY:
			identifier += " " + POST_UNARY_ID;
			association = LEFT_ASSOCIATION;
			precedence = 6;
		default:
			break;
		}
		
		switch (unaryOperatorType) {
		case SINE_TRIG_FUNCTION:
			identifier += " " + SINE_TRIG_FUNCTION_ID;
			break;
		case COSINE_TRIG_FUNCTION:
			identifier += " " + COSINE_TRIG_FUNCTION_ID;
			break;
		case TAN_TRIG_FUNCTION:
			identifier += " " + TAN_TRIG_FUNCTION_ID;
			break;
		case NEGATIVE_SIGN:
			identifier += " " + NEGATIVE_SIGN_ID;
			break;
		case INVERSE_SIN_TRIG_FUNCTION:
			identifier += " " + INVERSE_SIN_TRIG_FUNCTION_ID;
			break;
		case INVERSE_COS_TRIG_FUNCTION:
			identifier += " " + INVERSE_COS_TRIG_FUNCTION_ID;
			break;
		case INVERSE_TAN_TRIG_FUNCTION:
			identifier += " " + INVERSE_TAN_TRIG_FUNCTION_ID;
			break;
		case SINH_TRIG_FUNCTION:
			identifier += " " + SINH_TRIG_FUNCTION_ID;
			break;
		case COSH_TRIG_FUNCTION:
			identifier += " " + COSH_TRIG_FUNCTION_ID;
			break;
		case TANH_TRIG_FUNCTION:
			identifier += " " + TANH_TRIG_FUNCTION_ID;
			break;
		case INVERSE_SINH_TRIG_FUNCTION:
			identifier += " " + INVERSE_SINH_TRIG_FUNCTION_ID;
			break;
		case INVERSE_COSH_TRIG_FUNCTION:
			identifier += " " + INVERSE_COSH_TRIG_FUNCTION_ID;
			break;
		case INVERSE_TANH_TRIG_FUNCTION:
			identifier += " " + INVERSE_TANH_TRIG_FUNCTION_ID;
			break;
		default:
			break;
		}
		
		if (operatorType != NEGATIVE_SIGN){
			identifier += " " + IS_TRIG_ID;
		}
	}
	
	public DataToken calculate(NumberToken n){
		double d = Double.parseDouble(n.getNumber());
		double resultD = 0;
		switch (unaryOperatorType) {
		case SINE_TRIG_FUNCTION:
			resultD = Math.sin(d);
			break;
		case COSINE_TRIG_FUNCTION:
			resultD = Math.cos(d);
			break;
		case TAN_TRIG_FUNCTION:
			resultD = Math.tan(d);
			break;
		case INVERSE_SIN_TRIG_FUNCTION:
			resultD = Math.asin(d);
			break;
		case INVERSE_COS_TRIG_FUNCTION:
			resultD = Math.acos(d);
			break;
		case INVERSE_TAN_TRIG_FUNCTION:
			resultD = Math.atan(d);
			break;
		case NEGATIVE_SIGN:
			resultD = -resultD;
			break;
		case SINH_TRIG_FUNCTION:
			resultD = Math.sinh(d);
			break;
		case COSH_TRIG_FUNCTION:
			resultD = Math.cosh(d);
			break;
		case TANH_TRIG_FUNCTION:
			resultD = Math.tanh(d);
			break;
		case INVERSE_SINH_TRIG_FUNCTION:
			
			break;
		case INVERSE_COSH_TRIG_FUNCTION:

			break;
		case INVERSE_TANH_TRIG_FUNCTION:

			break;
		default:
			break;
		}
		NumberToken nbr = new NumberToken(n.getParent(), Double.toString(resultD));
		return nbr;
	}
	
	public DataToken calculate1(ResultToken r){
		ResultToken resultToken = null;
		RationalRootExpression tableResult = checkTable(r);
		if (tableResult != null){
			resultToken = new ResultToken(r.parent, ResultToken.RRE);
			resultToken.setRrExpression(tableResult);
			resultToken.computeDecimalResult();
		}else{
			BigDecimal bd = r.getDecimal();
			BigDecimal resBD = BigDecimal.ZERO;
			switch (unaryOperatorType) {
			case SINE_TRIG_FUNCTION:
				resBD = BigDecimalFunctions.sin(bd, DataProcessor.BIG_DECIMAL_SCALE);
				break;
			case COSINE_TRIG_FUNCTION:
				resBD = BigDecimalFunctions.cos(bd, DataProcessor.BIG_DECIMAL_SCALE);
				break;
			case TAN_TRIG_FUNCTION:
				resBD = BigDecimalFunctions.tan(bd, DataProcessor.BIG_DECIMAL_SCALE);
				break;
			case INVERSE_SIN_TRIG_FUNCTION:
				resBD = BigDecimalFunctions.arcsin(bd, DataProcessor.BIG_DECIMAL_SCALE);
				break;
			case INVERSE_COS_TRIG_FUNCTION:
				resBD = BigDecimalFunctions.arccos(bd, DataProcessor.BIG_DECIMAL_SCALE);
				break;
			case INVERSE_TAN_TRIG_FUNCTION:
				resBD = BigDecimalFunctions.arctan(bd, DataProcessor.BIG_DECIMAL_SCALE);
				break;
			case NEGATIVE_SIGN:
				resBD = bd.negate();
				break;
			case SINH_TRIG_FUNCTION:
				resBD = BigDecimalFunctions.sinh(bd, DataProcessor.BIG_DECIMAL_SCALE);
				break;
			case COSH_TRIG_FUNCTION:
				resBD = BigDecimalFunctions.cosh(bd, DataProcessor.BIG_DECIMAL_SCALE);
				break;
			case TANH_TRIG_FUNCTION:
				resBD = BigDecimalFunctions.tanh(bd, DataProcessor.BIG_DECIMAL_SCALE);
				break;
			case INVERSE_SINH_TRIG_FUNCTION:
				resBD = BigDecimalFunctions.arcsinh(bd, DataProcessor.BIG_DECIMAL_SCALE);
				break;
			case INVERSE_COSH_TRIG_FUNCTION:
				resBD = BigDecimalFunctions.arccosh(bd, DataProcessor.BIG_DECIMAL_SCALE);
				break;
			case INVERSE_TANH_TRIG_FUNCTION:
				resBD = BigDecimalFunctions.arctanh(bd, DataProcessor.BIG_DECIMAL_SCALE);
				break;
			default:
				break;
			}
			
			resultToken = new ResultToken(r.parent, ResultToken.DECIMAL);
			resultToken.setDecimal(resBD);
		}
		return resultToken;
	}

	private RationalRootExpression checkTable(ResultToken r){
		if (r.isTrig()){
			switch (unaryOperatorType) {
			case SINE_TRIG_FUNCTION:
				return TrigTable.checkSinTable(r.getTrigExpression());
			case COSINE_TRIG_FUNCTION:
				return TrigTable.checkCosTable(r.getTrigExpression());
			case TAN_TRIG_FUNCTION:
				return TrigTable.checkTanTable(r.getTrigExpression());
			default:
				return null;
			}
		}
		return null;
	}
	
	@Override
	public DataToken clone(Expression parent) {
		// TODO Auto-generated method stub
		return new UnaryOperator(parent, unaryOperatorType, position);
	}

	@Override
	public String getReportString(DataCursorManager DCM) {
		// TODO Auto-generated method stub
		String s = new String();
		switch (unaryOperatorType) {
		case NEGATIVE_SIGN:
			s = "(-)";
			break;
		case SINE_TRIG_FUNCTION:
			s = "sin";
			break;
		case COSINE_TRIG_FUNCTION:
			s = "cos";
			break;
		case TAN_TRIG_FUNCTION:
			s = "tan";
			break;
		case INVERSE_SIN_TRIG_FUNCTION:
			s = "sin-1";
			break;
		case INVERSE_COS_TRIG_FUNCTION:
			s = "cos-1";
			break;
		case INVERSE_TAN_TRIG_FUNCTION:
			s = "tan-1";
			break;
		case SINH_TRIG_FUNCTION:
			s = "sinh";
			break;
		case COSH_TRIG_FUNCTION:
			s = "cosh";
			break;
		case TANH_TRIG_FUNCTION:
			s = "tanh";
			break;
		case INVERSE_SINH_TRIG_FUNCTION:
			s = "sinh-1";
			break;
		case INVERSE_COSH_TRIG_FUNCTION:
			s = "cosh-1";
			break;
		case INVERSE_TANH_TRIG_FUNCTION:
			s = "tanh-1";
			break;
		default:
			break;
		}
		return s;
	}

}
