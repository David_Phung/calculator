package dataToken;

import android.R.id;

public class ParenthesisDataToken extends OperatorToken {
	
	public static final int OPEN = 0;
	public static final int CLOSE = 1;
	
	public static final String OPEN_ID = "OpenParen";
	public static final String CLOSE_ID = "CloseParen";

	public int parenthesisType;
	public boolean bBelongToATrigonometryToken;

	public ParenthesisDataToken(Expression parent, int parenthesisType) {
		super(parent, PARENTHES);
		// TODO Auto-generated constructor stub
		this.parenthesisType = parenthesisType;
		identifier += " " + PARENTHES_ID;
		switch (parenthesisType) {
		case OPEN:
			identifier += OPEN_ID;
			break;
		case CLOSE:
			identifier += CLOSE_ID;
		default:
			break;
		}
		precedence = 1;
		bBelongToATrigonometryToken = false;
	}
	
	@Override
	public DataToken clone(Expression parent) {
		// TODO Auto-generated method stub
		return new ParenthesisDataToken(parent, parenthesisType);
	}
	
	@Override
	public String getReportString(DataCursorManager DCM) {
		// TODO Auto-generated method stub
		String s = new String();
		switch (parenthesisType) {
		case OPEN:
			s = "(";
			break;
		case CLOSE:
			s = ")";
			break;
		default:
			break;
		}
		return s;
	}
	
	

}
