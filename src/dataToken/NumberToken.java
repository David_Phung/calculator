package dataToken;

import java.math.BigDecimal;

public class NumberToken extends DataToken {
	
	private String number;
	private int indexOfFlagToken;
	private int flagType;
	private BigDecimal bigDecimalNumber;

	public NumberToken(Expression parent, String number) {
		super(NUMBER_TOKEN, parent);
		// TODO Auto-generated constructor stub
		this.number = number;
		identifier += " " + NUMBER_TOKEN_ID;
		flagType = -1;
		indexOfFlagToken = -1;
		bigDecimalNumber = null;
	}
	
	public NumberToken(Expression parent, String number, BigDecimal bigDecimalNumber) {
		super(NUMBER_TOKEN, parent);
		// TODO Auto-generated constructor stub
		this.number = number;
		identifier += " " + NUMBER_TOKEN_ID;
		flagType = -1;
		indexOfFlagToken = -1;
		this.bigDecimalNumber = bigDecimalNumber;
	}
	
	public void addFlagToken(int indexOfFlagToken, int flagType){
		this.indexOfFlagToken = indexOfFlagToken;
		this.flagType = flagType;
	}
	
	@Override
	public DataToken clone(Expression parent) {
		// TODO Auto-generated method stub
		return new NumberToken(parent, new String(number));
	}
	
	@Override
	public String getReportString(DataCursorManager DCM) {
		// TODO Auto-generated method stub
		String s = new String();
		String errorType = new String();
		if (flagType == FlagToken.SYNTAX_ERROR) errorType = "SyntaxError";
		if (flagType == FlagToken.MATH_ERROR) errorType = "MathError";
		if (indexOfFlagToken >= 0) s+= "," + errorType + "(" + indexOfFlagToken +")";
		return "Nr{" + number + s + "}";
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public int getIndexOfFlagToken() {
		return indexOfFlagToken;
	}

	public void setIndexOfFlagToken(int indexOfFlagToken) {
		this.indexOfFlagToken = indexOfFlagToken;
	}

	public int getFlagType() {
		return flagType;
	}

	public void setFlagType(int flagType) {
		this.flagType = flagType;
	}

	public BigDecimal getBigDecimalNumber() {
		return bigDecimalNumber;
	}

	public void setBigDecimalNumber(BigDecimal bigDecimalNumber) {
		this.bigDecimalNumber = bigDecimalNumber;
	}
	
	

}
