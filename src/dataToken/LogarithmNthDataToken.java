package dataToken;

import java.math.BigDecimal;

import computing.BigDecimalFunctions;
import computing.DataProcessor;
import android.view.ViewDebug.ExportedProperty;

public class LogarithmNthDataToken extends FunctionToken {
	
	protected Expression base, logExpression;

	public LogarithmNthDataToken(Expression parent) {
		super(parent, LOGARITHM_CUSTOM_BASE);
		// TODO Auto-generated constructor stub
		base = new Expression(this, false);
		logExpression = new Expression(this, false);
		identifier += " " + LOGARITHM_CUSTOM_BASE_ID;
	}

	@Override
	public void infixToPostfix(DataProcessor DP) {
		// TODO Auto-generated method stub

	}

	@Override
	public DataToken calculate(DataProcessor DP) {
		// TODO Auto-generated method stub
		DataToken baseValue = DP.evaluatePostfix(base);
		if (baseValue.identifier.contains(DataToken.FLAG_TOKEN_ID)) return baseValue;
		
		DataToken logExpressionValue = DP.evaluatePostfix(logExpression);
		if (logExpressionValue.identifier.contains(DataToken.FLAG_TOKEN_ID)) return logExpressionValue;
		
		NumberToken baseNumber = (NumberToken)baseValue;
		NumberToken logExpressionNumber  = (NumberToken)logExpressionValue;
		double baseDouble = Double.parseDouble(baseNumber.getNumber());
		double logExpressionDouble = Double.parseDouble(logExpressionNumber.getNumber());
		if (baseDouble <= 0 || logExpressionDouble <= 0) return new FlagToken(parent, FlagToken.MATH_ERROR);
		
		BigDecimal baseBD = new BigDecimal(baseNumber.getNumber());
		BigDecimal logValueBD = new BigDecimal(logExpressionNumber.getNumber());
		
		if (baseBD.compareTo(BigDecimal.valueOf(0)) <= 0 || logValueBD.compareTo(BigDecimal.valueOf(0)) <= 0){
			return new FlagToken(parent, FlagToken.MATH_ERROR);
		}
		
		BigDecimal resBD = BigDecimalFunctions.log(baseBD, logValueBD, DataProcessor.BIG_DECIMAL_SCALE);
		
		double result = Math.log(logExpressionDouble) / Math.log(baseDouble);
		return new NumberToken (parent, resBD.toPlainString(), resBD);
	}
	
	@Override
	public DataToken calculate1(DataProcessor DP) {
		// TODO Auto-generated method stub
		DataToken baseValue = DP.evaluatePostfix(base);
		if (baseValue.identifier.contains(DataToken.FLAG_TOKEN_ID)) return baseValue;
		
		DataToken logExpressionValue = DP.evaluatePostfix(logExpression);
		if (logExpressionValue.identifier.contains(DataToken.FLAG_TOKEN_ID)) return logExpressionValue;
		
		ResultToken baseRes = (ResultToken)baseValue;
		ResultToken logExpressionRes  = (ResultToken)logExpressionValue;
		
		BigDecimal baseBD = baseRes.getDecimal();
		BigDecimal logValueBD = logExpressionRes.getDecimal();
		
		if (baseBD.compareTo(BigDecimal.valueOf(0)) <= 0 || logValueBD.compareTo(BigDecimal.valueOf(0)) <= 0){
			return new FlagToken(parent, FlagToken.MATH_ERROR);
		}
		
		BigDecimal resBD = BigDecimalFunctions.log(baseBD, logValueBD, DataProcessor.BIG_DECIMAL_SCALE);
		
		ResultToken resultToken = new ResultToken(parent, ResultToken.DECIMAL);
		resultToken.setDecimal(resBD);
		return resultToken;
	}
	
	@Override
	public DataToken clone(Expression parent) {
		// TODO Auto-generated method stub
		LogarithmNthDataToken token = new LogarithmNthDataToken(parent);
		token.setBase(base.clone(token));
		token.setLogExpression(logExpression.clone(token));
		return token;
	}

	@Override
	public int getNumberExpression() {
		// TODO Auto-generated method stub
		return 2;
	}

	@Override
	public Expression getExpression(int index) {
		// TODO Auto-generated method stub
		if (index == 0) return base;
		if (index == 1) return logExpression;
		return null;
	}

	@Override
	public int getIndexOfExpression(Expression expression) {
		// TODO Auto-generated method stub
		if (expression == base) return 0;
		if (expression == logExpression) return 1;
		return -1;
	}
	
	@Override
	public void setExpression(int index, Expression expression) {
		// TODO Auto-generated method stub
		if (index == 0) base = expression;
		if (index == 1) logExpression = expression;
	}
	
	@Override
	public String getReportString(DataCursorManager DCM) {
		// TODO Auto-generated method stub
		return "LogN{" + base.getReportString(DCM) + "," + logExpression.getReportString(DCM) + "}";
	}

	public Expression getBase() {
		return base;
	}

	public void setBase(Expression base) {
		this.base = base;
	}

	public Expression getLogExpression() {
		return logExpression;
	}

	public void setLogExpression(Expression logExpression) {
		this.logExpression = logExpression;
	}
	
	

}
