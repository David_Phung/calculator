package dataToken;

public class LogarithmFixedBaseDataToken extends LogarithmNthDataToken {

	public LogarithmFixedBaseDataToken(Expression parent, String baseValue) {
		super(parent);
		// TODO Auto-generated constructor stub
		NumberToken nbr = new NumberToken(base, baseValue);
		base.getTokenList().add(nbr);
	}
	
	@Override
	public int getNumberExpression() {
		// TODO Auto-generated method stub
		return 1;
	}
	
	@Override
	public Expression getExpression(int index) {
		// TODO Auto-generated method stub
		if (index == 0) return logExpression;
		return null;
	}
	
	@Override
	public int getIndexOfExpression(Expression expression) {
		// TODO Auto-generated method stub
		if (expression == logExpression) return 0;
		return -1;
	}
	
	@Override
	public String getReportString(DataCursorManager DCM) {
		// TODO Auto-generated method stub
		return "LogF{" + base.getReportString(DCM) + "," + logExpression.getReportString(DCM) + "}";
	}

}
