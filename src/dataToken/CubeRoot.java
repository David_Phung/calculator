package dataToken;

public class CubeRoot extends RootNthDataToken {

	public CubeRoot(Expression parent) {
		super(parent);
		// TODO Auto-generated constructor stub
		degree.getTokenList().add(new NumberToken(degree, "3"));
	}

	@Override
	public int getNumberExpression() {
		// TODO Auto-generated method stub
		return 1;
	}
	
	@Override
	public Expression getExpression(int index) {
		// TODO Auto-generated method stub
		if (index == 0) return radicand;
		return null;
	}
	
	@Override
	public int getIndexOfExpression(Expression expression) {
		// TODO Auto-generated method stub
		if (expression == radicand) return 0;
		return -1;
	}
}
