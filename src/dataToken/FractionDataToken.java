package dataToken;

import java.math.BigDecimal;

import computing.DataProcessor;
import computing.EmptyExpressionException;
import computing.PrimeLimitReachedException;
import computing.RationalNumber;
import computing.RationalRootExpression;
import computing.TrigExpression;

public class FractionDataToken extends FunctionToken {
	
	private Expression numerator, denominator;

	public FractionDataToken(Expression parent) {
		super(parent, FRACTION_DATA_TOKEN);
		// TODO Auto-generated constructor stub
		numerator = new Expression(this, false);
		denominator = new Expression(this, false);
		identifier += " " + FRACTION_TOKEN_ID;
	}

	@Override
	public void infixToPostfix(DataProcessor DP) {
		// TODO Auto-generated method stub

	}

	@Override
	public DataToken calculate(DataProcessor DP) {
		// TODO Auto-generated method stub
		DataToken num = DP.evaluatePostfix(numerator);
		if (num.identifier.contains(DataToken.FLAG_TOKEN_ID)) return num;
		DataToken denom = DP.evaluatePostfix(denominator);
		if (denom.identifier.contains(DataToken.FLAG_TOKEN_ID)) return denom;
		
		NumberToken denomNumber = (NumberToken)denom;
		if (Double.parseDouble(denomNumber.getNumber()) == 0) return new FlagToken(parent, FlagToken.MATH_ERROR);
		
		NumberToken numNumber = (NumberToken)num;
		double d = Double.parseDouble(numNumber.getNumber()) / Double.parseDouble(denomNumber.getNumber());
		
		BigDecimal numBD = new BigDecimal(numNumber.getNumber());
		BigDecimal denomBD = new BigDecimal(denomNumber.getNumber());
		
		if (denomBD.compareTo(BigDecimal.valueOf(0)) == 0){
			return new FlagToken(parent, FlagToken.MATH_ERROR);
		}

		BigDecimal resBD = numBD.divide(denomBD, DataProcessor.BIG_DECIMAL_SCALE, DataProcessor.BIG_DECIMAL_DEFAULT_ROUNDING);
		
		
		NumberToken result = new NumberToken(parent, d + "");
		return new NumberToken(parent, resBD.toPlainString(), resBD);
	}
	
	public DataToken calculate1(DataProcessor DP) throws EmptyExpressionException{
		DataToken num = DP.evaluatePostfix(numerator);
		if (num.identifier.contains(DataToken.FLAG_TOKEN_ID)) return num;
		DataToken denom = DP.evaluatePostfix(denominator);
		if (denom.identifier.contains(DataToken.FLAG_TOKEN_ID)) return denom;
		
		ResultToken numResult = (ResultToken)num;
		ResultToken denomResult = (ResultToken)denom;
		boolean isRRE = false;
		boolean isTrig = false;
		boolean primeLimitReached = false;
		ResultToken resultToken = null;
		
		if (numResult.isTrig() && denomResult.isRRE() && denomResult.getRrExpression().onlyContainsRationalNumbers()){
			isTrig = true;
			TrigExpression te = numResult.getTrigExpression();
			RationalNumber rn = denomResult.getRrExpression().toRational();
			TrigExpression resTrig = te.divide(rn).simplify();
			resultToken = new ResultToken(parent, ResultToken.TRIG);
			resultToken.setTrigExpression(resTrig);
			resultToken.computeDecimalResult();
		}
		
		if (numResult.isRRE() && denomResult.isRRE()){
			isRRE = true;
			RationalRootExpression rNum = numResult.getRrExpression();
			RationalRootExpression rDen = denomResult.getRrExpression();
			RationalRootExpression result = null;
			try {
				result = rNum.divide(rDen).simplify();
				resultToken = new ResultToken(parent, ResultToken.RRE);
				resultToken.setRrExpression(result);
				resultToken.computeDecimalResult();
			} catch (PrimeLimitReachedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				primeLimitReached = true;
			}
		}
		if ((!isRRE && !isTrig) || primeLimitReached){
			BigDecimal dNum = numResult.getDecimal();
			BigDecimal dDen = denomResult.getDecimal();
			BigDecimal result = dNum.divide(dDen, DataProcessor.BIG_DECIMAL_SCALE, DataProcessor.BIG_DECIMAL_DEFAULT_ROUNDING);
			resultToken = new ResultToken(parent, ResultToken.DECIMAL);
			resultToken.setDecimal(result);
		}
		return resultToken;
	}
	
	@Override
	public DataToken clone(Expression parent) {
		// TODO Auto-generated method stub
		FractionDataToken token = new FractionDataToken(parent);
		token.setNumerator(numerator.clone(token));
		token.setDenominator(denominator.clone(token));
		return token;
	}

	@Override
	public int getNumberExpression() {
		// TODO Auto-generated method stub
		return 2;
	}

	@Override
	public Expression getExpression(int index) {
		// TODO Auto-generated method stub
		if (index == 0) return numerator;
		if (index == 1) return denominator;
		return null;
	}

	@Override
	public int getIndexOfExpression(Expression expression) {
		// TODO Auto-generated method stub
		if (expression == numerator) return 0;
		if (expression == denominator) return 1;
		return -1;
	}
	
	@Override
	public void setExpression(int index, Expression expression) {
		// TODO Auto-generated method stub
		if (index == 0) numerator = expression;
		if (index == 1) denominator = expression;
	}
	
	@Override
	public String getReportString(DataCursorManager DCM) {
		// TODO Auto-generated method stub
		return "Fr{" + numerator.getReportString(DCM) + "," + denominator.getReportString(DCM) + "}";
	}
	
	public Expression getNumerator() {
		return numerator;
	}

	public void setNumerator(Expression numerator) {
		this.numerator = numerator;
	}

	public Expression getDenominator() {
		return denominator;
	}

	public void setDenominator(Expression denominator) {
		this.denominator = denominator;
	}

}
