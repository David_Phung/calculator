package dataToken;

import java.math.BigDecimal;

import computing.EmptyExpressionException;
import computing.PrimeLimitReachedException;
import computing.RationalRootExpression;
import computing.TrigExpression;

public class ResultToken extends DataToken {
	
	public static final int RRE = 0;
	public static final int DECIMAL = 1;
	public static final int TRIG = 2;
	
	private int resultType;
	private RationalRootExpression rrExpression;
	private BigDecimal decimal;
	private TrigExpression trigExpression;
	private int exp_of_10;
	private boolean expEnabled;

	public ResultToken(Expression parent, int resultType) {
		super(RESULT_TOKEN, parent);
		// TODO Auto-generated constructor stub
		this.identifier = RESULT_TOKEN_ID;
		this.resultType = resultType;
		rrExpression = new RationalRootExpression();
		decimal = BigDecimal.ZERO;
		exp_of_10 = 0;
		expEnabled = false;
	}

	@Override
	public DataToken clone(Expression parent) {
		// TODO Auto-generated method stub
		ResultToken cloneToken = new ResultToken(parent, resultType);
		cloneToken.rrExpression = new RationalRootExpression(rrExpression);
		cloneToken.decimal = decimal;
		if (trigExpression != null){
			cloneToken.trigExpression = new TrigExpression(trigExpression);
		}
		cloneToken.exp_of_10 = exp_of_10;
		return cloneToken;
	}

	@Override
	public String getReportString(DataCursorManager DCM) {
		// TODO Auto-generated method stub
		String s = new String();
		switch (resultType) {
		case RRE:
			s = rrExpression.toString();
			break;
		case DECIMAL:
			s = decimal.toPlainString();
			break;
		case TRIG:
			
			break;
		default:
			break;
		}
		return s;
	}

	public void computeDecimalResult(){
		switch (resultType) {
		case RRE:
			decimal = rrExpression.toBigDecimal();
			break;
		case TRIG:
			decimal = trigExpression.toBigDecimal();
			break;
		default:
			break;
		}
	}
	
	public void simplifyRRE(){
		if (rrExpression != null){
			try {
				rrExpression = rrExpression.simplify();
			} catch (PrimeLimitReachedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (EmptyExpressionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public boolean isRRE(){
		return resultType == RRE;
	}
	
	public boolean isTrig(){
		return resultType == TRIG;
	}
	
	public boolean isDecimal(){
		return resultType == DECIMAL;
	}

	public int getResultType() {
		return resultType;
	}

	public void setResultType(int resultType) {
		this.resultType = resultType;
	}


	public BigDecimal getDecimal() {
		return decimal;
	}

	public void setDecimal(BigDecimal decimal) {
		this.decimal = decimal;
	}

	public TrigExpression getTrigExpression() {
		return trigExpression;
	}

	public void setTrigExpression(TrigExpression trigExpression) {
		this.trigExpression = trigExpression;
	}

	public RationalRootExpression getRrExpression() {
		return rrExpression;
	}

	public void setRrExpression(RationalRootExpression rrExpression) {
		this.rrExpression = rrExpression;
	}

	public int getExp_of_10() {
		return exp_of_10;
	}

	public void setExp_of_10(int exp_of_10) {
		this.exp_of_10 = exp_of_10;
	}

	public boolean isExpEnabled() {
		return expEnabled;
	}

	public void setExpEnabled(boolean expEnabled) {
		this.expEnabled = expEnabled;
	}
	
	
}
