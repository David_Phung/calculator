package dataToken;

public class Cube extends PowerToDataToken {

	public Cube(Expression parent) {
		super(parent);
		// TODO Auto-generated constructor stub
		exponent.getTokenList().add(new NumberToken(exponent, "3"));
	}
	
	@Override
	public int getNumberExpression() {
		// TODO Auto-generated method stub
		if (bTempBaseEnabled) return 1;
		return 0;
	}

	@Override
	public Expression getExpression(int index) {
		// TODO Auto-generated method stub
		if (bTempBaseEnabled){
			if (index == 0) return tempBase;
		}
		return null;
	}

	@Override
	public int getIndexOfExpression(Expression expression) {
		// TODO Auto-generated method stub
		if (bTempBaseEnabled){
			if (expression == tempBase) return 0;
		}
		return -1;
	}

}
