package dataToken;

import java.math.BigDecimal;

import android.util.Log;
import computing.BigDecimalFunctions;
import computing.DataProcessor;
import computing.EmptyExpressionException;
import computing.PrimeLimitReachedException;
import computing.RationalNumber;
import computing.RationalRootExpression;
import computing.SquareRootNumber;
import visualToken.VisualToken;

public class SquareRoot extends RootNthDataToken {

	public SquareRoot(Expression parent) {
		super(parent);
		// TODO Auto-generated constructor stub
		degree.getTokenList().add(new NumberToken(degree, "2"));
	}
	
	@Override
	public DataToken clone(Expression parent) {
		// TODO Auto-generated method stub
		SquareRoot token = new SquareRoot(parent);
		token.setRadicand(radicand.clone(token));
		return token;
	}
	
	@Override
	public DataToken calculate1(DataProcessor DP) throws EmptyExpressionException {
		// TODO Auto-generated method stub
		boolean primeLimitReached = false; 
		DataToken result = null;
		
		DataToken radicandValue = DP.evaluatePostfix(radicand);
		if (radicandValue.identifier.contains(DataToken.FLAG_TOKEN_ID)) return radicandValue;
		
		ResultToken radicandRes = (ResultToken)radicandValue;
		if (radicandRes.isRRE() && radicandRes.getRrExpression().onlyContainsRationalNumbers()){
			RationalRootExpression rre = radicandRes.getRrExpression();
			SquareRootNumber sr = new SquareRootNumber(rre.toRational(), RationalNumber.ONE);
			RationalRootExpression res = new RationalRootExpression();
			res.add(sr, RationalRootExpression.NUMERATOR);
			res.add(SquareRootNumber.ONE, RationalRootExpression.DENOMINATOR);
			try {
				res = res.simplify();
			} catch (PrimeLimitReachedException e) {
				// TODO Auto-generated catch block
				primeLimitReached = true;
			}
			ResultToken resultToken = new ResultToken(parent, ResultToken.RRE);
			resultToken.setRrExpression(res);
			resultToken.computeDecimalResult();
			result = resultToken;
		}else{
			 result= super.calculate1(DP);
		}
		
		if (primeLimitReached) result = super.calculate1(DP);
		return result;
	}

	@Override
	public int getNumberExpression() {
		// TODO Auto-generated method stub
		return 1;
	}
	
	@Override
	public Expression getExpression(int index) {
		// TODO Auto-generated method stub
		if (index == 0) return radicand;
		return null;
	}
	
	@Override
	public int getIndexOfExpression(Expression expression) {
		// TODO Auto-generated method stub
		if (expression == radicand) return 0;
		return -1;
	}
	
	@Override
	public String getReportString(DataCursorManager DCM) {
		// TODO Auto-generated method stub
		return "Sqrt{" + radicand.getReportString(DCM) + "}";
	}

}
