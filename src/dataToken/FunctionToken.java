package dataToken;

import computing.DataProcessor;
import computing.EmptyExpressionException;
import computing.PrimeLimitReachedException;

public abstract class FunctionToken extends DataToken {
	
	public static final int FRACTION_DATA_TOKEN = 0;
	public static final int POWER_TO_DATA_TOKEN = 1;
	public static final int ROOTH_Nth_DATA_TOKEN = 2;
	public static final int LOGARITHM_CUSTOM_BASE = 3;
	public static final int ANS_FUNCTION_TOKEN = 4;
	public static final int COMBINATORIC_TOKEN = 5;
	
	public static final String FRACTION_TOKEN_ID = "Fraction";
	public static final String POWER_TO_DATA_TOKEN_ID = "PowerToFunction";
	public static final String ROOTH_Nth_TOKEN_ID = "RootNth";
	public static final String LOGARITHM_CUSTOM_BASE_ID = "LogarithmCustomBase";
	public static final String ANS_FUNCTION_TOKEN_ID = "AnsFunctionToken";
	
	public static final String SQUARE_ROOT_ID = "Sqrt";
	public static final String CUBE_ROOT_ID = "Cbr";
	public static final String SQUARE_ID = "Square";
	public static final String CUBE_ID = "Cube";
	
	public static final String COMBINATORIC_POL_RECT_ID = "COMBINATORIC_ID";
	
	public static final String POWER_TO_FIXED_BASE_ID = "PtFixedBase";
	public static final String LOGARITHM_FIXED_BASE_ID = "LogFixedBase";
	
	public int functionType;

	public FunctionToken(Expression parent, int functionType) {
		super(FUNCTION_TOKEN, parent);
		// TODO Auto-generated constructor stub
		this.functionType = functionType;
		identifier += " " + FUNCTION_TOKEN_ID;
	}

	public abstract void infixToPostfix(DataProcessor DP);
	
	public abstract DataToken calculate(DataProcessor DP);
	
	public abstract DataToken calculate1(DataProcessor DP) throws EmptyExpressionException;
	//Expressions order
	
	public abstract int getNumberExpression();
	
	public abstract Expression getExpression(int index);
	
	public abstract int getIndexOfExpression(Expression expression);
	
	public abstract void setExpression(int index, Expression expression);
}
