package dataToken;

import java.util.ArrayList;

import visualToken.VisualToken;
import android.util.Log;

public class DataCursorManager {
	
	private ArrayList<Expression> expressionList;
	private int index_of_currFocusExpression;
	private int cursor_elementIndex;
	
	public DataCursorManager() {
		// TODO Auto-generated constructor stub
		expressionList = new ArrayList<Expression>();
		index_of_currFocusExpression = -1;
		cursor_elementIndex = -1;
	}
	
	public void placeCursorAt(Expression expression, int index){
		int index_of_expression = expressionList.indexOf(expression);
		
		if (index_of_expression < 0){
			Log.d("","in place Cursor (DataCursorManager): cant find textArea in expressionList");
		}else{
			index_of_currFocusExpression = index_of_expression;
		}
		
		cursor_elementIndex = index;
		
	}
	
	public void addRootExpression(Expression rootExpression){
		expressionList.add(0,rootExpression);
		index_of_currFocusExpression = 0;
	}
	
	public void clear(){
		Expression temp = expressionList.get(0);
		expressionList.clear();
		expressionList.add(temp);
		placeCursorAt(temp, -1);
	}
	
	public void moveCursorLeft(){
		Expression focusedExpression = getCurrFocusedExpression();
		if (cursor_elementIndex == -1){
			//if in root expression
			if (focusedExpression.isRoot()){
				return;
			}else{ //if in function token
				FunctionToken parentToken = (FunctionToken) focusedExpression.getParent();
				//if in the first expression of the function token -> move out of the token
				if (parentToken.getIndexOfExpression(focusedExpression) == 0){
					Expression parentExpression = parentToken.getParent();
					int index_of_parentToken = parentExpression.getTokenList().indexOf(parentToken);
					placeCursorAt(parentExpression, index_of_parentToken - 1);
					
					//if the expression we were in is a power token temp base and it's at the beginning of the expression, move cursor left once more
					if ( focusedExpression.isPowerToTempBase() && index_of_parentToken == 0){
						moveCursorLeft();
					}
				}else if (parentToken.getIndexOfExpression(focusedExpression) > 0){ //if not, move to the previous expression
					Expression previousExpression = parentToken.getExpression(parentToken.getIndexOfExpression(focusedExpression) - 1);
					placeCursorAt(previousExpression, previousExpression.getTokenList().size() - 1);
				}
			}
		}else if (cursor_elementIndex >= 0 && cursor_elementIndex < focusedExpression.getTokenList().size()){
			//if not at the beginning of the expression, aka normal case
			//we check the token to the left,
			//if it's a simple token, move normally,
			//if it's a function token, move to the last expression of that token
			DataToken leftToken = focusedExpression.getTokenList().get(cursor_elementIndex);
			if (leftToken.type == DataToken.FUNCTION_TOKEN && ((FunctionToken)leftToken).getExpression(0) != null){
				//place cursor at the end of last expression
				FunctionToken tempFT = (FunctionToken)leftToken;
				Expression lastExpression_of_leftToken = tempFT.getExpression(tempFT.getNumberExpression() - 1);
				placeCursorAt(lastExpression_of_leftToken, lastExpression_of_leftToken.getTokenList().size() - 1);
			}else{
				//place normally
				placeCursorAt(focusedExpression, cursor_elementIndex - 1);
				
				//we perform check 1 more time to see if the cursor we moved past 
				//was a open parenthesis that belongs to a trigonometry token. 
				//If yes then we move the cursor 1 more step to the left
				
				/*if (leftToken.identifier.contains(ParenthesisDataToken.OPEN_ID)){
					ParenthesisDataToken parenToken = (ParenthesisDataToken) leftToken;
					if (parenToken.bBelongToATrigonometryToken == true){
						//For DEBUG purpose, we perform check 1 more time to see if the token that is to 
						//the left NOW is a trig token.
						DataToken leftTokenAFTER = getCurrFocusedToken();
						if (leftTokenAFTER != null){
							if (leftTokenAFTER.identifier.contains(UnaryOperator.TRIGONOMETRY_FUNCTION_OPERATOR_ID) == false){
								Log.d("","ERROR: >> Inside DataCursorManager -> moveCursorLeft -> token to the left is not trig token");
							}
							placeCursorAt(focusedExpression, cursor_elementIndex - 1);
						}
					}
				}*/
			}
		}
	}
	
	public void moveCursorRight(){
		Expression focusedExpression = getCurrFocusedExpression();
		//if cursor is at the end of expression
		if (cursor_elementIndex == focusedExpression.getTokenList().size() - 1){
			//if focusedExpression is root
			if (focusedExpression.isRoot()){
				return;
			}
			//If it is NOT root, then the focusedExpression is in a function token
			//we move the cursor to the next expression or out of the token
			FunctionToken parentToken = (FunctionToken) focusedExpression.getParent();
			//if in the LAST expression of the function token -> move out of the token
			if (parentToken.getIndexOfExpression(focusedExpression) == parentToken.getNumberExpression() - 1){
				Expression parentExpression_of_parentToken = parentToken.getParent();
				int index_of_parentToken = parentExpression_of_parentToken.getTokenList().indexOf(parentToken);
				placeCursorAt(parentExpression_of_parentToken, index_of_parentToken);
			}else if (parentToken.getIndexOfExpression(focusedExpression) < parentToken.getNumberExpression() - 1){
				//if not, move to the NEXT expression
				Expression nextExpression = parentToken.getExpression(parentToken.getIndexOfExpression(focusedExpression) + 1);
				placeCursorAt(nextExpression, -1);
			}
		}else if (cursor_elementIndex >= -1 && cursor_elementIndex < focusedExpression.getTokenList().size()){
			//if not at the end of the expression, aka normal case
			//we check the token to the right,
			//if it's a simple token, move normally,
			//if it's a function token, move to the first expression of that token
			DataToken rightToken = focusedExpression.getTokenList().get(cursor_elementIndex + 1);
			if (rightToken.type == DataToken.FUNCTION_TOKEN && ((FunctionToken)rightToken).getExpression(0) != null){
				//place cursor at the beginning of first expression
				FunctionToken tempFT = ((FunctionToken)rightToken);
				Expression firstExpression = tempFT.getExpression(0);
				placeCursorAt(firstExpression, -1);
					
				//if the first expression contains a power to token at the beginning of it
				//and if that token is tempBaseEnabled -> move right once more
				if (firstExpression.getTokenList().size() > 0 
						&& firstExpression.getTokenList().get(0).identifier.contains(FunctionToken.POWER_TO_DATA_TOKEN_ID)){
					PowerToDataToken powerToken = (PowerToDataToken)firstExpression.getTokenList().get(0);
					if (powerToken.isbTempBaseEnabled() && !powerToken.isbFixedBase()) moveCursorRight();
				}
			}else{
				//place normally
				placeCursorAt(focusedExpression, cursor_elementIndex + 1);
				
				//we perform check 1 more time to see if the curr token is a trig token
				//if then we move the cursor to the right 1 more time
				
			/*	if (tokenToTheRight.identifier.contains(UnaryOperator.TRIGONOMETRY_FUNCTION_OPERATOR_ID)){
					//For DEBUG purpose we perform check 1 more time to see if that is 
					//a open paren that has bBelongToATrigToken = true
					if (cursor_elementIndex < focusedExpression.getTokenList().size()){
					DataToken tokenToTheRightNOW = focusedExpression.getTokenList().get(cursor_elementIndex + 1);
						if (tokenToTheRightNOW.identifier.contains(ParenthesisDataToken.OPEN_ID) == false){
							Log.d("","BUG: >> Inside DataCursorManager -> moveCursorRight -> token to the right is not open paren");
						}else{
							ParenthesisDataToken parenToken = (ParenthesisDataToken)tokenToTheRightNOW;
							if (parenToken.bBelongToATrigonometryToken == false){
								Log.d("","BUG: >> Inside DataCursorManager -> moveCursorRight -> paren token to the right is not belonged to a trig token");
							}
							placeCursorAt(focusedExpression, cursor_elementIndex + 1);
						}	
					}
				}*/
			}
		}
	}
	
	public Expression getCurrFocusedExpression(){
		return expressionList.get(index_of_currFocusExpression);
	}
	
	public DataToken getCurrFocusedToken(){
		if (cursor_elementIndex >= 0){
			return expressionList.get(index_of_currFocusExpression).getTokenList().get(cursor_elementIndex);
		}
		return null;
	}

	public ArrayList<Expression> getExpressionList() {
		return expressionList;
	}

	public void setExpressionList(ArrayList<Expression> expressionList) {
		this.expressionList = expressionList;
	}

	public int getIndex_of_currFocusExpression() {
		return index_of_currFocusExpression;
	}

	public void setIndex_of_currFocusExpression(int index_of_currFocusExpression) {
		this.index_of_currFocusExpression = index_of_currFocusExpression;
	}

	public int getCursor_elementIndex() {
		return cursor_elementIndex;
	}

	public void setCursor_elementIndex(int cursor_elementIndex) {
		this.cursor_elementIndex = cursor_elementIndex;
	}

	
	
}
