package dataToken;

import java.math.BigDecimal;

import computing.BigDecimalFunctions;
import computing.BigDecimalUtils;
import computing.DataProcessor;
import computing.DividedByZeroException;
import computing.EmptyExpressionException;
import computing.PrimeLimitReachedException;
import computing.RationalNumber;
import computing.RationalRootExpression;
import computing.TrigExpression;
import android.util.Log;

public class BinaryOperator extends OperatorToken {
	
	public static final int ADDITION = 0;
	public static final int SUBTRACTION = 1;
	public static final int MULTIPLCATION = 2;
	public static final int DIVISION = 3;
	public static final int POWER_TO = 4;
	public static final int MODULUS = 5;
	
	public static final String ADDITION_ID = "Addition";
	public static final String SUBTRACTION_ID = "Substraction";
	public static final String MULTIPLCATION_ID = "Multiplication";
	public static final String DIVISION_ID = "Division";
	public static final String POWER_TO_ID = "PowerTo";
	public static final String MODULUS_ID = "Modulus";
	
	public int binaryOperatorType;

	public BinaryOperator(Expression parent, int binaryOperatorType) {
		super(parent, BINARY_OPERATOR);
		// TODO Auto-generated constructor stub
		this.binaryOperatorType = binaryOperatorType;
		identifier += " " + BINARY_OPERATOR_ID;
		
		switch (binaryOperatorType) {
		case ADDITION:
			precedence = 2;
			identifier += " " + ADDITION_ID;
			association = LEFT_ASSOCIATION;
			break;
		case SUBTRACTION:
			precedence = 2;
			identifier += " " + SUBTRACTION_ID;
			association = LEFT_ASSOCIATION;
			break;
		case MULTIPLCATION:
			precedence = 3;
			identifier += " " + MULTIPLCATION_ID;
			association = LEFT_ASSOCIATION;
			break;
		case DIVISION:
			precedence = 3;
			identifier += " " + DIVISION_ID;
			association = LEFT_ASSOCIATION;
			break;
		case POWER_TO:
			precedence = 4;
			identifier += " " + POWER_TO_ID;
			association = RIGHT_ASSOCIATION;
			break;
		case MODULUS:
			precedence = 3;
			identifier += " " + MODULUS_ID;
			association = LEFT_ASSOCIATION;
			break;
		}
	}
	
	@Override
	public DataToken clone(Expression parent) {
		// TODO Auto-generated method stub
		return new BinaryOperator(parent, binaryOperatorType);
	}
	
	public DataToken calculate(NumberToken n1, NumberToken n2){
		double resultD = 0;
		double d1 = Double.parseDouble(n1.getNumber());
		double d2 = Double.parseDouble(n2.getNumber());
		DataToken resultToken = null;
		
		BigDecimal bd1 = new BigDecimal(n1.getNumber());
		BigDecimal bd2 = new BigDecimal(n2.getNumber());
		BigDecimal bdRes = null;
		
		if (bd1.compareTo(new BigDecimal(0)) == 0){
			resultToken = new FlagToken(n1.getParent(), FlagToken.MATH_ERROR);
		}else{
			switch (binaryOperatorType) {
			case ADDITION:
				resultD = d1 + d2;
				bdRes = bd1.add(bd2);
				break;
			case SUBTRACTION:
				resultD = d2 - d1;
				bdRes = bd2.subtract(bd1);
				break;
			case MULTIPLCATION:
				resultD = d1 * d2;
				bdRes = bd1.multiply(bd2);
				break;
			case DIVISION:
				resultD = d2 / d1;
				bdRes = bd2.divide(bd1, DataProcessor.BIG_DECIMAL_SCALE, DataProcessor.BIG_DECIMAL_DEFAULT_ROUNDING);
				break;
			case POWER_TO:
				bdRes = BigDecimalFunctions.pow(bd2, bd1, DataProcessor.BIG_DECIMAL_SCALE); 
				break;
			case MODULUS:
				resultD = d2 % d1;
				bdRes = bd2.divideAndRemainder(bd1)[1];
				break;
			default:
				break;
			}
			resultToken = new NumberToken(n1.getParent(), bdRes.toPlainString(), bdRes);
		}
		return resultToken;
	}
	
	public DataToken calculate1(ResultToken t1, ResultToken t2) throws EmptyExpressionException{
		ResultToken resultToken = null;
		boolean primeLimitReached = false;
		boolean operationNotSupported = false;
		
		boolean isRRE = false;
		
		boolean isTrig = false;
		TrigExpression trigRes = null;
		if (t1.isTrig() && t2.isTrig()){
			isTrig = true;
			TrigExpression te1 = t1.getTrigExpression();
			TrigExpression te2 = t2.getTrigExpression();
			try{
				trigRes = calculateTrig(te1, te2).simplify();
			}catch (UnsupportedOperationException e){
				operationNotSupported = true;
			}
		}
		if (t1.isTrig() && t2.isRRE() && t2.getRrExpression().onlyContainsRationalNumbers()){
			isTrig = true;
			TrigExpression te1 = t1.getTrigExpression();
			RationalNumber rn2 = t2.getRrExpression().toRational();
			try{
				trigRes = calculateTrig(te1, rn2);
			}catch (UnsupportedOperationException e){
				operationNotSupported = true;
			}
		}
		if (t2.isTrig() && t1.isRRE() && t1.getRrExpression().onlyContainsRationalNumbers()){
			isTrig = true;
			TrigExpression te2 = t2.getTrigExpression();
			RationalNumber rn1 = t1.getRrExpression().toRational();
			try{
				trigRes = calculateTrig(rn1, te2);
			}catch (UnsupportedOperationException e){
				operationNotSupported = true;
			}
		}
		if (trigRes != null){
			resultToken = new ResultToken(t1.getParent(), ResultToken.TRIG);
			resultToken.setTrigExpression(trigRes);
			resultToken.computeDecimalResult();
		}
	
		
		if (t1.isRRE() && t2.isRRE()){
			isRRE = true;
			RationalRootExpression r1 = t1.getRrExpression();
			RationalRootExpression r2 = t2.getRrExpression();
			RationalRootExpression result = null;
			try {
				result = calculateRRE(r1, r2);
				resultToken = new ResultToken(t1.getParent(), ResultToken.RRE);
				resultToken.setRrExpression(result);
				resultToken.computeDecimalResult();
			} catch (PrimeLimitReachedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				primeLimitReached = true;
			} catch (UnsupportedOperationException e){
				operationNotSupported = true;
			}
		}
		if ((!isRRE && !isTrig)|| primeLimitReached || operationNotSupported){
			BigDecimal d1 = t1.getDecimal();
			BigDecimal d2 = t2.getDecimal();
			BigDecimal result = calculateDecimal(d1, d2);
			resultToken = new ResultToken(t1.getParent(), ResultToken.DECIMAL);
			resultToken.setDecimal(result);
		}
		return resultToken;
	}
	
	private BigDecimal calculateDecimal(BigDecimal d1, BigDecimal d2){
		BigDecimal result = BigDecimal.ZERO;
		switch (binaryOperatorType) {
		case ADDITION:
			result = d1.add(d2);
			break;
		case SUBTRACTION:
			result = d2.subtract(d1);
			break;
		case MULTIPLCATION:
			result = d1.multiply(d2);
			break;
		case DIVISION:
			if (d1.compareTo(BigDecimal.ZERO) == 0) throw new DividedByZeroException();
			result = d2.divide(d1, DataProcessor.BIG_DECIMAL_SCALE, DataProcessor.BIG_DECIMAL_DEFAULT_ROUNDING);
			break;
		case POWER_TO:
			result = BigDecimalFunctions.pow(d2, d1, DataProcessor.BIG_DECIMAL_SCALE);
			break;
		default:
			break;
		}
		return result;
	}
	
	private RationalRootExpression calculateRRE(RationalRootExpression r1, RationalRootExpression r2) throws PrimeLimitReachedException, EmptyExpressionException{
		RationalRootExpression result = new RationalRootExpression();
		switch (binaryOperatorType) {
		case ADDITION:
			result = r1.add(r2);
			break;
		case SUBTRACTION:
			result = r2.substract(r1);
			break;
		case MULTIPLCATION:
			result = r1.multiply(r2);
			break;
		case DIVISION:
			result = r2.divide(r1);
			break;
		case POWER_TO:
			throw new UnsupportedOperationException("not yet supported");
		default:
			break;
		}
		return result.simplify();
	}

	private TrigExpression calculateTrig(TrigExpression te1, TrigExpression te2){
		TrigExpression res = null;
		switch (binaryOperatorType) {
		case ADDITION:
			res = te1.add(te2);
			break;
		case SUBTRACTION:
			res = te2.subtract(te1);
			break;
		case MULTIPLCATION:
			throw new UnsupportedOperationException("not yet supported");
		case DIVISION:
			throw new UnsupportedOperationException("not yet supported");
		case POWER_TO:
			throw new UnsupportedOperationException("not yet supported");
		default:
			break;
		}
		return res;
		
	}
	
	private TrigExpression calculateTrig(RationalNumber rn1, TrigExpression te2){
		TrigExpression res = null;
		switch (binaryOperatorType) {
		case ADDITION:
			res = te2.add(rn1);
			break;
		case SUBTRACTION:
			res = te2.subtract(rn1);
			break;
		case MULTIPLCATION:
			res = te2.multiply(rn1);
		case DIVISION:
			res = te2.divide(rn1);
		case POWER_TO:
			throw new UnsupportedOperationException("not yet supported");
		default:
			break;
		}
		return res;
	}
	
	private TrigExpression calculateTrig(TrigExpression te1, RationalNumber rn2){
		TrigExpression res = null;
		switch (binaryOperatorType) {
		case ADDITION:
			res = te1.add(rn2);
			break;
		case SUBTRACTION:
			TrigExpression temp = te1.negate();
			res = temp.add(rn2);
			break;
		case MULTIPLCATION:
			res = te1.multiply(rn2);
		case DIVISION:
			throw new UnsupportedOperationException("not yet supported");
		case POWER_TO:
			throw new UnsupportedOperationException("not yet supported");
		default:
			break;
		}
		return res;
	}
	
	
	@Override
	public String getReportString(DataCursorManager DCM) {
		// TODO Auto-generated method stub
		String s = new String();
		switch (binaryOperatorType) {
		case ADDITION:
			s = "+";
			break;
		case SUBTRACTION:
			s = "-";
			break;
		case MULTIPLCATION:
			s = "*";
			break;
		case DIVISION:
			s = "/";
			break;
		case POWER_TO:
			s = "^";
			break;
		case MODULUS:
			s = "%";
			break;
		default:
			break;
		}
		return s;
	}
}
