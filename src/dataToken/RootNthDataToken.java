package dataToken;

import java.math.BigDecimal;

import android.util.Log;
import computing.BigDecimalFunctions;
import computing.DataProcessor;
import computing.EmptyExpressionException;

public class RootNthDataToken extends FunctionToken {
	
	protected Expression degree, radicand;

	public RootNthDataToken(Expression parent) {
		super(parent, ROOTH_Nth_DATA_TOKEN);
		// TODO Auto-generated constructor stub
		degree = new Expression(this, false);
		radicand = new Expression(this, false);
		identifier += " " + ROOTH_Nth_TOKEN_ID;
	}

	@Override
	public void infixToPostfix(DataProcessor DP) {
		// TODO Auto-generated method stub

	}

	@Override
	public DataToken calculate(DataProcessor DP) {
		// TODO Auto-generated method stub
		DataToken degreeValue = DP.evaluatePostfix(degree);
		if (degreeValue.identifier.contains(DataToken.FLAG_TOKEN_ID)) return degreeValue;
		
		DataToken radicandValue = DP.evaluatePostfix(radicand);
		if (radicandValue.identifier.contains(DataToken.FLAG_TOKEN_ID)) return radicandValue;
		
		NumberToken degreeNumber = (NumberToken)degreeValue;
		NumberToken radicandNumber = (NumberToken)radicandValue;
		
		double degreeD = Double.parseDouble(degreeNumber.getNumber());
		double radicandD = Double.parseDouble(radicandNumber.getNumber());
		
		BigDecimal degreeBD = new BigDecimal(degreeNumber.getNumber());
		BigDecimal radicandBD = new BigDecimal(radicandNumber.getNumber());
		
		if (degreeBD.compareTo(BigDecimal.valueOf(0)) == 0){
			return new FlagToken(parent, FlagToken.MATH_ERROR);
		}
		
		BigDecimal temp = new BigDecimal("1").divide(degreeBD, DataProcessor.BIG_DECIMAL_SCALE, DataProcessor.BIG_DECIMAL_DEFAULT_ROUNDING);
		BigDecimal resBD = BigDecimalFunctions.pow(radicandBD, temp, DataProcessor.BIG_DECIMAL_SCALE);
		double result = Math.pow(radicandD, 1 / degreeD);
		
		return new NumberToken(parent, resBD.toPlainString(), resBD);
	}
	
	public DataToken calculate1(DataProcessor DP) throws EmptyExpressionException{
		DataToken degreeValue = DP.evaluatePostfix(degree);
		if (degreeValue.identifier.contains(DataToken.FLAG_TOKEN_ID)) return degreeValue;
		
		DataToken radicandValue = DP.evaluatePostfix(radicand);
		if (radicandValue.identifier.contains(DataToken.FLAG_TOKEN_ID)) return radicandValue;
		
		ResultToken degreeNumber = (ResultToken)degreeValue;
		ResultToken radicandNumber = (ResultToken)radicandValue;
		
		BigDecimal degreeBD = degreeNumber.getDecimal();
		BigDecimal radicandBD = radicandNumber.getDecimal();
		
		if (degreeBD.compareTo(BigDecimal.valueOf(0)) == 0){
			return new FlagToken(parent, FlagToken.MATH_ERROR);
		}
		
		BigDecimal temp = new BigDecimal("1").divide(degreeBD, DataProcessor.BIG_DECIMAL_SCALE, DataProcessor.BIG_DECIMAL_DEFAULT_ROUNDING);
		BigDecimal resBD = BigDecimalFunctions.pow(radicandBD, temp, DataProcessor.BIG_DECIMAL_SCALE);
		
		ResultToken resultToken = new ResultToken(parent, ResultToken.DECIMAL);
		resultToken.setDecimal(resBD);
		
		return resultToken;
	}
	
	@Override
	public DataToken clone(Expression parent) {
		// TODO Auto-generated method stub
		RootNthDataToken token = new RootNthDataToken(parent);
		token.setDegree(degree.clone(token));
		token.setRadicand(radicand.clone(token));
		return token;
	}

	@Override
	public String getReportString(DataCursorManager DCM) {
		// TODO Auto-generated method stub
		return "RootN{" + degree.getReportString(DCM) + "," + radicand.getReportString(DCM) + "}";
	}
	
	@Override
	public int getNumberExpression() {
		// TODO Auto-generated method stub
		return 2;
	}

	@Override
	public Expression getExpression(int index) {
		// TODO Auto-generated method stub
		if (index == 0) return degree;
		if (index == 1) return radicand;
		return null;
	}

	@Override
	public int getIndexOfExpression(Expression expression) {
		// TODO Auto-generated method stub
		if (expression == degree) return 0;
		if (expression == radicand) return 1;
		return -1;
	}
	
	@Override
	public void setExpression(int index, Expression expression) {
		// TODO Auto-generated method stub
		if (index == 0) degree = expression;
		if (index == 1) radicand = expression;
	}

	public Expression getDegree() {
		return degree;
	}

	public void setDegree(Expression degree) {
		this.degree = degree;
	}

	public Expression getRadicand() {
		return radicand;
	}

	public void setRadicand(Expression radicand) {
		this.radicand = radicand;
	}
	
	

}
