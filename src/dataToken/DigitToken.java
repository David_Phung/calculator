package dataToken;

public class DigitToken extends DataToken {
	
	public static final char POINT_CHAR = '.';
	public static final char EXP = 'e';
	
	public static final String EXP_ID = "Exp";
	
	protected String digit;

	public DigitToken(Expression parent, String digit) {
		super(DIGIT_TOKEN, parent);
		// TODO Auto-generated constructor stub
		this.digit = digit;
		identifier += " " + DIGIT_TOKEN_ID;
		if (digit.equals(EXP)){
			identifier += " " + EXP_ID;
		}
	}
	
	@Override
	public DataToken clone(Expression parent) {
		// TODO Auto-generated method stub
		return new DigitToken(parent, new String(digit));
	}
	
	@Override
	public String getReportString(DataCursorManager DCM) {
		// TODO Auto-generated method stub
		return digit;
	}

	public String getDigit() {
		return digit;
	}

	public void setDigit(String digit) {
		this.digit = digit;
	}
	
	

}
