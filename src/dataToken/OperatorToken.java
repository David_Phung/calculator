package dataToken;

public abstract class OperatorToken extends DataToken {
	
	public static final int PARENTHESIS = 0;
	public static final int BINARY_OPERATOR = 1;
	public static final int PARENTHES = 2;
	public static final int UNARY_OPERATOR = 3;
	
	public static final String BINARY_OPERATOR_ID = "BinaryOperator";
	public static final String PARENTHES_ID = "Parenthesis";
	public static final String UNARY_OPERATOR_ID = "UnaryOperator";
	
	public static final int LEFT_ASSOCIATION = 0;
	public static final int RIGHT_ASSOCIATION = 1;
	
	public int operatorType;
	protected int precedence;
	protected int association;
	
	public OperatorToken(Expression parent, int operatorType) {
		super(OPERATOR_TOKEN, parent);
		// TODO Auto-generated constructor stub
		this.operatorType = operatorType;
		identifier += " " + OPERATOR_TOKEN_ID;
		precedence = 0;
		association = 0;
	}

	public int getPrecedence() {
		return precedence;
	}

	public void setPrecedence(int precedence) {
		this.precedence = precedence;
	}

	public int getAssociation() {
		return association;
	}

	public void setAssociation(int association) {
		this.association = association;
	}

	
	
}
