package displayLayout;

import com.example.calculator03.PaintManager;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;

public class Padding {
	
	public int top, left, right ,bottom;
	
	public Padding() {
		// TODO Auto-generated constructor stub
	}
	
	public Padding(int left, int top, int right, int bottom) {
		super();
		this.top = top;
		this.left = left;
		this.right = right;
		this.bottom = bottom;
	}

	public int getWidth(){
		return left + right;
	}
	
	public int getHeight(){
		return top + bottom;
	}
	
	public void drawSelf(Canvas canvas, Rect ownerBounds, boolean bUseMainPaddingPaint){
		Rect topR = new Rect(ownerBounds.left + left, ownerBounds.top, ownerBounds.right, ownerBounds.top - top);
		Rect bottomR = new Rect(ownerBounds.left, ownerBounds.bottom - bottom, ownerBounds.right - right, ownerBounds.bottom);
		Rect leftR = new Rect(ownerBounds.left, ownerBounds.top, ownerBounds.left + left, ownerBounds.bottom - bottom);
		Rect rightR = new Rect(ownerBounds.right - right, ownerBounds.top + top, ownerBounds.right, ownerBounds.bottom);
		
		Paint tempPaint = null;
		if (bUseMainPaddingPaint){
			tempPaint = PaintManager.mainPaddingPaint;
		}else{
			tempPaint = PaintManager.paddingPaint;
		}
		canvas.drawRect(topR, tempPaint);
		canvas.drawRect(bottomR, tempPaint);
		canvas.drawRect(leftR, tempPaint);
		canvas.drawRect(rightR, tempPaint);
	}

}
