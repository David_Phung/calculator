package displayLayout;

import android.graphics.AvoidXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.Log;

public class LinearLayout extends Container {
	
	public static final int HORIZONTAL = 0;
	public static final int VERTICAL = 1;
	
	private int align;

	public LinearLayout(boolean isRoot, int align) {
		super(isRoot);
		// TODO Auto-generated constructor stub
		this.align = align;
	}

	
	/**
	 * Compute the size of this container if it was a WRAP_CONTENT box, padding included.
	 */
	@Override
	public void preCompute() {
		// TODO Auto-generated method stub
		WIDTH = HEIGHT = 0;
		WIDTH += padding.left + padding.right;
		HEIGHT += padding.top + padding.bottom;
		if (horizontalLayoutDistributionMethod == LAYOUT_PARAM_METHOD){
			for (LayoutBox box:activeBoxList){
				if (box.horizontalLayoutParam == WRAP_CONTENT && box.isContainer){
					((Container)box).preCompute();
					WIDTH += box.WIDTH;
				}else if (box.horizontalLayoutParam == WRAP_CONTENT){
					WIDTH += box.WIDTH;
				}
			}
		}
		
		if (verticalLayoutDistributionMethod == LAYOUT_PARAM_METHOD){
			for (LayoutBox box:activeBoxList){
				if (box.verticalLayoutParam == WRAP_CONTENT && box.isContainer){
					((Container)box).preCompute();
					HEIGHT += box.WIDTH;
				}else if (box.horizontalLayoutParam == WRAP_CONTENT){
					HEIGHT += box.WIDTH;
				}
			}
		}
	}

	@Override
	public void placeLayout(Rect bounds) {
		// TODO Auto-generated method stub
		
		if (isRoot){
			WIDTH = bounds.width();
			HEIGHT = bounds.height();
			location.x = bounds.left;
			location.y = bounds.top;
		}
		switch (horizontalLayoutDistributionMethod) {
		case LAYOUT_PARAM_METHOD:
			assignWidthLayoutParamMethod(bounds);
			break;
		case WEIGHT_METHOD:
			assignWidthWeightMethod(bounds);
			break;
		default:
			break;
		}
		switch (verticalLayoutDistributionMethod) {
		case LAYOUT_PARAM_METHOD:
			assignHeightLayoutParamMethod(bounds);
			break;
		case WEIGHT_METHOD:
			assignHeightWeightMethod(bounds);
			break;
		default:
			break;
		}
		
		assignLocations(bounds);
	}

	@Override
	public void assignWidthLayoutParamMethod(Rect bounds) {
		// TODO Auto-generated method stub
		Rect boundsExcludingPadding = new Rect(bounds.left + padding.left, bounds.top + padding.top, bounds.right - padding.right, bounds.bottom - padding.bottom);
		switch (align) {
		case HORIZONTAL:
			int totalWidthWrapContent = 0;
			int matchParentCount = 0;
			for (LayoutBox box:activeBoxList){
				if (box.horizontalLayoutParam == WRAP_CONTENT){
					totalWidthWrapContent += box.WIDTH;
				}else if (box.horizontalLayoutParam == MATCH_PARENT){
					matchParentCount++;
				}
			}
			if (totalWidthWrapContent >= boundsExcludingPadding.width()){
				int widthLeft = boundsExcludingPadding.width();
				for (LayoutBox box:activeBoxList){
					if (box.horizontalLayoutParam == MATCH_PARENT){
						box.WIDTH = 0;
					}else if (box.horizontalLayoutParam == WRAP_CONTENT){
						if (box.WIDTH < widthLeft){
							widthLeft -= box.WIDTH;
						}else if (widthLeft >= 0){
							box.WIDTH = widthLeft;
							widthLeft -= box.WIDTH;
						}else{
							box.WIDTH = 0;
						}
					}
				}
			}else{
				int totalWidthMatchParent = boundsExcludingPadding.width() - totalWidthWrapContent;
				for (LayoutBox box:activeBoxList){
					if (box.horizontalLayoutParam == MATCH_PARENT){
						box.WIDTH = Math.round((float)totalWidthMatchParent / matchParentCount);
					}else if (box.horizontalLayoutParam == WRAP_CONTENT){
						
					}
				}
			}
			break;
		case VERTICAL:
			for (LayoutBox box:activeBoxList){
				if (box.horizontalLayoutParam == MATCH_PARENT){
					box.WIDTH = boundsExcludingPadding.width();
				}else if (box.horizontalLayoutParam == WRAP_CONTENT){
					if (box.WIDTH > boundsExcludingPadding.width()){
						box.WIDTH = boundsExcludingPadding.width();
					}
				}
			}
			break;
		default:
			break;
		}
	}

	@Override
	public void assignHeightLayoutParamMethod(Rect bounds) {
		// TODO Auto-generated method stub
		Rect boundsExcludingPadding = new Rect(bounds.left + padding.left, bounds.top + padding.top, bounds.right - padding.right, bounds.bottom - padding.bottom);
		switch (align) {
		case HORIZONTAL:
			for (LayoutBox box:activeBoxList){
				if (box.verticalLayoutParam == MATCH_PARENT){
					box.HEIGHT = boundsExcludingPadding.height();
				}else if (box.verticalLayoutParam == WRAP_CONTENT){
					if (box.HEIGHT > boundsExcludingPadding.height()){
						box.HEIGHT = boundsExcludingPadding.height();
					}
				}
			}
			break;
		case VERTICAL:
			int totalHeightWrapContent = 0;
			int matchParentCount = 0;
			for (LayoutBox box:activeBoxList){
				if (box.verticalLayoutParam == WRAP_CONTENT){
					totalHeightWrapContent += box.HEIGHT;
				}else if (box.verticalLayoutParam == MATCH_PARENT){
					matchParentCount++;
				}
			}
			if (totalHeightWrapContent >= boundsExcludingPadding.height()){
				int heightLeft = boundsExcludingPadding.height();
				for (LayoutBox box:activeBoxList){
					if (box.verticalLayoutParam == MATCH_PARENT){
						box.HEIGHT = 0;
					}else if (box.verticalLayoutParam == WRAP_CONTENT){
						if (box.HEIGHT < heightLeft){
							heightLeft -= box.HEIGHT;
						}else if (heightLeft >= 0){
							box.HEIGHT = heightLeft;
							heightLeft -= box.HEIGHT;
						}else{
							box.HEIGHT = 0;
						}
					}
				}
			}else{
				int totalHeightMatchParent = boundsExcludingPadding.height() - totalHeightWrapContent;
				for (LayoutBox box:activeBoxList){
					if (box.verticalLayoutParam == MATCH_PARENT){
						box.HEIGHT = Math.round((float)totalHeightMatchParent / matchParentCount);
					}else if (box.verticalLayoutParam == WRAP_CONTENT){
						
					}
				}
			}
			break;
		default:
			break;
		}
	}

	@Override
	public void assignWidthWeightMethod(Rect bounds) {
		// TODO Auto-generated method stub
		Rect boundsExcludingPadding = new Rect(bounds.left + padding.left, bounds.top + padding.top, bounds.right - padding.right, bounds.bottom - padding.bottom);
		switch (align) {
		case HORIZONTAL:
			float totalWeight = 0;
			for (LayoutBox box:activeBoxList){
				if (box.horizontalWeight > 0){
					totalWeight += box.horizontalWeight;
				}
			}
			for (LayoutBox box:activeBoxList){
				if (box.horizontalWeight > 0){
					box.WIDTH = Math.round(box.horizontalWeight / totalWeight * boundsExcludingPadding.width());
				}else{
					box.WIDTH = 0;
				}
			}
			break;
		case VERTICAL:
			float maxWeight = 0;
			for (LayoutBox box:activeBoxList){
				maxWeight = Math.max(maxWeight, box.horizontalWeight);
			}
			for (LayoutBox box:activeBoxList){
				if (box.horizontalWeight > 0){
					box.WIDTH = Math.round(box.horizontalWeight / maxWeight * boundsExcludingPadding.width());
				}else{
					box.WIDTH = 0;
				}
			}
			break;
		default:
			break;
		}
	}

	@Override
	public void assignHeightWeightMethod(Rect bounds) {
		// TODO Auto-generated method stub
		Rect boundsExcludingPadding = new Rect(bounds.left + padding.left, bounds.top + padding.top, bounds.right - padding.right, bounds.bottom - padding.bottom);
		switch (align) {
		case HORIZONTAL:
			float maxWeight = 0;
			for (LayoutBox box:activeBoxList){
				maxWeight = Math.max(maxWeight, box.verticalWeight);
			}
			for (LayoutBox box:activeBoxList){
				if (box.verticalWeight > 0){
					box.HEIGHT = Math.round(box.verticalWeight / maxWeight * boundsExcludingPadding.height());
				}else{
					box.HEIGHT = 0;
				}
			}
			break;
		case VERTICAL:
			float totalWeight = 0;
			for (LayoutBox box:activeBoxList){
				if (box.verticalWeight > 0){
					totalWeight += box.verticalWeight;
				}
			}
			for (LayoutBox box:activeBoxList){
				if (box.verticalWeight > 0){
					box.HEIGHT = Math.round(box.verticalWeight / totalWeight * boundsExcludingPadding.height());
				}else{
					box.HEIGHT = 0;
				}
			}
			break;
		default:
			break;
		}
	}

	@Override
	public void assignLocations(Rect bounds) {
		// TODO Auto-generated method stub
		Rect boundsExcludingPadding = new Rect(bounds.left + padding.left, bounds.top + padding.top, bounds.right - padding.right, bounds.bottom - padding.bottom);
		switch (align) {
		case HORIZONTAL:
			int x = padding.left;
			for (LayoutBox box:activeBoxList){
				box.location.y = padding.top;
				box.location.x = x;
				x += box.WIDTH;
			}
			break;
		case VERTICAL:
			int y = padding.top;
			for (LayoutBox box:activeBoxList){
				box.location.x = padding.left;
				box.location.y = y;
				y += box.HEIGHT;
			}
			break;

		default:
			break;
		}
	}

}
