package displayLayout;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.Log;

public abstract class LayoutBox {
	
	public static final int MATCH_PARENT = 0;
	public static final int WRAP_CONTENT = 1;
	
	protected int WIDTH, HEIGHT;
	protected Point location;
	protected int verticalLayoutParam;
	protected int horizontalLayoutParam;
	protected Padding padding;
	protected float verticalWeight, horizontalWeight;
	protected boolean isContainer;
	protected Bitmap bitmap;
	protected boolean isDisabled;
	
	protected String nameTag;

	public LayoutBox() {
		// TODO Auto-generated constructor stub
		location = new Point();
		padding = new Padding();
		isContainer = false;
		nameTag = new String();
	}
	
	public void recreateBitmap(boolean bCallChildren){
		//Log.d("","In layoutbox recreateBitmap(): WIDTH " + WIDTH + " HEIGHT " + HEIGHT);
		if (WIDTH <= 0 || HEIGHT <= 0){
			//Log.d("","In layoutbox recreateBitmap(): 1" );
			bitmap = null;
			return;
		}
		Bitmap tempBitmap = Bitmap.createBitmap((int)WIDTH, (int)HEIGHT, Bitmap.Config.ARGB_8888);
		if (bitmap != null){
			Canvas canvas = new Canvas(tempBitmap);
			canvas.drawBitmap(bitmap, 0, 0, new Paint());
		}
		bitmap = tempBitmap;
		//Log.d("","In layoutbox recreateBitmap(): 2 " + bitmap );
		if (isContainer && bCallChildren){
			Container container = (Container)this;
			for (LayoutBox box:container.boxList){
				box.recreateBitmap(true);
			}
		}
	}

	public abstract void draw();

	public Bitmap getDrawnBitmap(){
		draw();
		return bitmap;
	}
	
	//----------------------------------------------------------------
	public Rect getBounds(){
		Rect r = new Rect(location.x, location.y, location.x + WIDTH, location.y + HEIGHT);
		return r;
	}
	
	public int getWidth() {
		return WIDTH;
	}

	public void setWidth(int width) {
		this.WIDTH = width;
	}

	public int getHeight() {
		return HEIGHT;
	}

	public void setHeight(int height) {
		this.HEIGHT = height;
	}

	public Point getLocation() {
		return location;
	}

	public void setLocation(Point location) {
		this.location = location;
	}

	public Padding getPadding() {
		return padding;
	}

	public void setPadding(Padding padding) {
		this.padding = padding;
	}

	public float getVerticalWeight() {
		return verticalWeight;
	}

	public void setVerticalWeight(float verticalWeight) {
		this.verticalWeight = verticalWeight;
	}

	public float getHorizontalWeight() {
		return horizontalWeight;
	}

	public void setHorizontalWeight(float horizontalWeight) {
		this.horizontalWeight = horizontalWeight;
	}

	public boolean isContainer() {
		return isContainer;
	}

	public void setContainer(boolean isContainer) {
		this.isContainer = isContainer;
	}

	public Bitmap getBitmap() {
		return bitmap;
	}

	public void setBitmap(Bitmap bitmap) {
		this.bitmap = bitmap;
	}

	public boolean isDisabled() {
		return isDisabled;
	}

	public void setDisabled(boolean isDisabled) {
		this.isDisabled = isDisabled;
	}
	
	

}
