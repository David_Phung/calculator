package displayLayout;

import visualToken.InverseTrigToken;
import visualToken.StringToken;
import visualToken.VisualToken;

public class DisplayLayoutGenerator {

	public DisplayLayoutGenerator() {
		// TODO Auto-generated constructor stub
	}
	
	public Container generateBasicModeLayout(){
		LinearLayout ll = new LinearLayout(true, LinearLayout.VERTICAL);
		TextComponent input = new TextComponent();
		TextComponent output = new TextComponent();
		
		output.setbDrawCursor(false);
		
		input.verticalLayoutParam = LayoutBox.MATCH_PARENT;
		output.verticalLayoutParam = LayoutBox.MATCH_PARENT;
		
		input.horizontalLayoutParam = LayoutBox.MATCH_PARENT;
		output.horizontalLayoutParam = LayoutBox.MATCH_PARENT;
		
		ll.horizontalLayoutDistributionMethod = ll.verticalLayoutDistributionMethod = Container.LAYOUT_PARAM_METHOD;
		
		ll.padding = new Padding(12, 8, 12, 8);
		
		input.setTextAreaAlignment(TextComponent.TOP_LEFT);
		output.setTextAreaAlignment(TextComponent.BOTTOM_RIGHT);
		
		ll.addBox(input);
		ll.addBox(output);
		ll.updateBoxList();
		
		return ll;
	}

	public Container generateHypLayout(){
		LinearLayout mainLL = new LinearLayout(true, LinearLayout.VERTICAL);
		mainLL.horizontalLayoutDistributionMethod = Container.LAYOUT_PARAM_METHOD;
		mainLL.verticalLayoutDistributionMethod = Container.LAYOUT_PARAM_METHOD;
		mainLL.nameTag = "MainLL";
		
		LinearLayout[] llArray = new LinearLayout[4];
		for (int i = 0; i < 4; i++){
			llArray[i] = new LinearLayout(false, LinearLayout.HORIZONTAL);
			llArray[i].horizontalLayoutParam = Container.MATCH_PARENT;
			llArray[i].verticalLayoutParam = Container.MATCH_PARENT;
			llArray[i].horizontalLayoutDistributionMethod = Container.LAYOUT_PARAM_METHOD;
			llArray[i].verticalLayoutDistributionMethod = Container.LAYOUT_PARAM_METHOD;
			
			llArray[i].nameTag = "ll" + i;
		}
		
		TextComponent[] tcArray = new TextComponent[6];
		
		String[] stringArray = {"sinh", "cosh", "tanh"};
		
		for (int i = 0; i < 6; i++){
			tcArray[i] = new TextComponent();
			VisualToken token = null;
			if (i < 3){
				token = new StringToken(tcArray[i].getRootTextArea(), (i + 1) + ": " + stringArray[i]);
			}else{
				token = new InverseTrigToken(tcArray[i].getRootTextArea(), (i + 1) + ": " + stringArray[i - 3]);
			}
			tcArray[i].getRootTextArea().getTokenList().add(token);
			token.update_pass_1(true);
			tcArray[i].horizontalLayoutParam = Container.MATCH_PARENT;
			tcArray[i].verticalLayoutParam = Container.MATCH_PARENT;
			tcArray[i].setbDrawCursor(false);
			
			tcArray[i].nameTag = "tc" + i;
		}
		
		mainLL.padding = new Padding(12, 8, 12, 8);
		
		int j = 0;
		for (int i = 0; i < 3; i++){
			llArray[i].addBox(tcArray[j++]);
			llArray[i].addBox(tcArray[j++]);
		}
		
		for (int i = 0; i < 3; i++){
			mainLL.addBox(llArray[i]);
		}
		mainLL.updateBoxList();
		return mainLL;
	}
}
