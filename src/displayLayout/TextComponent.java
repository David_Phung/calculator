package displayLayout;

import com.example.calculator03.PaintManager;

import visualToken.TextArea;
import visualToken.VisualCursorManager;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Paint.Style;
import android.util.Log;

public class TextComponent extends ElementComponent {
	
	public static final int TOP_LEFT = 0;
	public static final int BOTTOM_RIGHT = 1;
	public static final int BOTTOM_LEFT = 2;
	
	private TextArea rootTextArea;
	private VisualCursorManager VCM;
	private int textAreaAlignment;
	private boolean bCursorEnabled;
	
	public TextComponent() {
		// TODO Auto-generated constructor stub
		super();
		rootTextArea = new TextArea(null, false, PaintManager.textPaint);
		VCM = new VisualCursorManager();
		VCM.getTAList().add(rootTextArea);
		VCM.placeCursorAt(rootTextArea, -1);
		bCursorEnabled = true;
	}
	
	public void clear() {
		// TODO Auto-generated method stub
		rootTextArea.getTokenList().clear();
		VCM.clear();
		rootTextArea.update_pass_1(true);
	}

	@Override
	public void draw() {
		// TODO Auto-generated method stub
		if (bitmap == null) return;
		
		Canvas bitmapCanvas = new Canvas(bitmap);
		Paint paint = new Paint();
		paint.setStyle(Style.STROKE);
		paint.setColor(Color.parseColor("#188962"));
		
		//bitmapCanvas.drawPaint(paint);
		//bitmapCanvas.drawColor(Color.TRANSPARENT);
		
		bitmap.eraseColor(PaintManager.backgroundPaint.getColor());
		
		paint.setColor(Color.RED);
		//bitmapCanvas.drawRect(new Rect(0,0, bitmap.getWidth() - 1, bitmap.getHeight() -1), paint);
		
		rootTextArea.drawSelf(bitmapCanvas);
		if (bCursorEnabled){
			VCM.draw(bitmapCanvas);
		}
	}

	public void onTouch() {
		// TODO Auto-generated method stub

	}
	
	public void alignTextArea(){
		if (rootTextArea == null) return;
		switch (textAreaAlignment) {
		case TOP_LEFT:		
			rootTextArea.setLocation(new Point(0,0));
			rootTextArea.update_pass_2();
			break;
		case BOTTOM_RIGHT:
			int x = 0;
			if (rootTextArea == VCM.getFocusedTextArea() && VCM.getCursor().getElementIndex() == rootTextArea.getTokenList().size() - 1 && !rootTextArea.getTokenList().isEmpty()){
				x = (int) Math.max(WIDTH - rootTextArea.getWIDTH() - VCM.getCursor().getThickness(), 0);
			}else{
				x = (int) Math.max(WIDTH - rootTextArea.getWIDTH(), 0);
			}
			int y = (int) Math.max(HEIGHT - rootTextArea.getHEIGHT(), 0);
			rootTextArea.setLocation(new Point(x,y));
			rootTextArea.update_pass_2();
			break;
		case BOTTOM_LEFT:
			y = (int) Math.min(HEIGHT - rootTextArea.getHEIGHT(), 0);
			rootTextArea.setLocation(new Point(0,y));
			rootTextArea.update_pass_2();
			break;
		default:
			break;
		}
	}

	public void toggleCursor(boolean bEnabled){
		bCursorEnabled = bEnabled;
	}
	//-----------------------------------------------------

	public TextArea getRootTextArea() {
		return rootTextArea;
	}

	public void setRootTextArea(TextArea rootTextArea) {
		this.rootTextArea = rootTextArea;
	}

	public VisualCursorManager getVCM() {
		return VCM;
	}

	public void setVCM(VisualCursorManager vCM) {
		VCM = vCM;
	}

	public int getTextAreaAlignment() {
		return textAreaAlignment;
	}

	public void setTextAreaAlignment(int textAreaAlignment) {
		this.textAreaAlignment = textAreaAlignment;
	}

	public boolean isbDrawCursor() {
		return bCursorEnabled;
	}

	public void setbDrawCursor(boolean bDrawCursor) {
		this.bCursorEnabled = bDrawCursor;
	}
	
}
