package displayLayout;

import java.util.ArrayList;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.Log;
import displayLayout.LayoutBox;

public abstract class Container extends LayoutBox {
	
	public static final int WEIGHT_METHOD = 0;
	public static final int LAYOUT_PARAM_METHOD = 1;
	
	protected ArrayList<LayoutBox> boxList;
	protected ArrayList<LayoutBox> activeBoxList;
	protected int verticalLayoutDistributionMethod;
	protected int horizontalLayoutDistributionMethod;
	protected boolean isRoot;

	public Container(boolean isRoot) {
		// TODO Auto-generated constructor stub
		super();
		isContainer = true;
		boxList = new ArrayList<LayoutBox>();
		activeBoxList = new ArrayList<LayoutBox>();
		verticalLayoutDistributionMethod = horizontalLayoutDistributionMethod = LAYOUT_PARAM_METHOD;
		this.isRoot = isRoot;
	}
	
	public void addBox(LayoutBox box){
		this.boxList.add(box);
	}
	
	public void updateLayout(Rect bounds){
		updateBoxList();
		preCompute();
		for (int i = 0; i < activeBoxList.size(); i++){
			LayoutBox box = activeBoxList.get(i);
			if (box.isContainer){
				((Container)box).preCompute();
			}
		}
		placeLayout(bounds);
		for (int i = 0; i < activeBoxList.size(); i++){
			LayoutBox box = activeBoxList.get(i);
			if (box.isContainer){
				((Container)box).placeLayout(box.getBounds());
			}
		}
		recreateBitmap(true);
	}
	
	public void updateBoxList(){
		activeBoxList.clear();
		for (LayoutBox box:boxList){
			if (!box.isDisabled){
				activeBoxList.add(box);
			}
		}
		for (LayoutBox box:boxList){
			if (box.isContainer){
				((Container)box).updateBoxList();
			}
		}
	}
	
	public abstract void  assignWidthLayoutParamMethod(Rect bounds);
	
	public abstract void assignHeightLayoutParamMethod(Rect bounds);
	
	public abstract void assignWidthWeightMethod(Rect bounds);
	
	public abstract void assignHeightWeightMethod(Rect bounds);
	
	public abstract void assignLocations(Rect bounds);
	
	public abstract void preCompute();
	
	public abstract void placeLayout(Rect bounds);

	@Override
	public void draw() {
		// TODO Auto-generated method stub
		if (bitmap == null) return;
		Canvas canvas = new Canvas(bitmap);
		padding.drawSelf(canvas, new Rect(0, 0, WIDTH, HEIGHT), isRoot);
		for (int i = 0; i < activeBoxList.size(); i++){
			LayoutBox box = activeBoxList.get(i);
			Bitmap tempBitmap = box.getDrawnBitmap();
			if (tempBitmap != null){
				canvas.drawBitmap(tempBitmap, box.location.x, box.location.y, new Paint());
			}
		}
	}

	public ArrayList<LayoutBox> getBoxList() {
		return boxList;
	}

	public void setBoxList(ArrayList<LayoutBox> boxList) {
		this.boxList = boxList;
	}

	public ArrayList<LayoutBox> getActiveBoxList() {
		return activeBoxList;
	}

	public void setActiveBoxList(ArrayList<LayoutBox> activeBoxList) {
		this.activeBoxList = activeBoxList;
	}

	public int getVerticalLayoutDistributionMethod() {
		return verticalLayoutDistributionMethod;
	}

	public void setVerticalLayoutDistributionMethod(
			int verticalLayoutDistributionMethod) {
		this.verticalLayoutDistributionMethod = verticalLayoutDistributionMethod;
	}

	public int getHorizontalLayoutDistributionMethod() {
		return horizontalLayoutDistributionMethod;
	}

	public void setHorizontalLayoutDistributionMethod(
			int horizontalLayoutDistributionMethod) {
		this.horizontalLayoutDistributionMethod = horizontalLayoutDistributionMethod;
	}

	public boolean isRoot() {
		return isRoot;
	}

	public void setRoot(boolean isRoot) {
		this.isRoot = isRoot;
	}
	
	

}
