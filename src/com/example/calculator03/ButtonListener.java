package com.example.calculator03;

import java.util.ArrayList;

import android.util.Log;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class ButtonListener implements OnClickListener, Runnable{

	private Calculator03 mainActivity;
	private DisplayManager displayManager;
	private DataManager dataManager;
	private MySurfaceView mySurfaceView;
	private ArrayList<Button> buttonsPressed;
	private Thread thread;
	boolean running;
	
	
	public ButtonListener() {
		// TODO Auto-generated constructor stub
		buttonsPressed = new ArrayList<Button>();
	}
	
	public void setReferences(Calculator03 mainActivity){
		this.mainActivity = mainActivity;
		this.displayManager = mainActivity.getDisplayManager();
		this.mySurfaceView = mainActivity.getMySurfaceView();
		dataManager = mainActivity.getDataManager();
	}

	public void onClick(View v) {
		// TODO Auto-generated method stub
		Button b = (Button)v;
		buttonsPressed.add(b);
	}

	public void run() {
		// TODO Auto-generated method stub
		int displayOnClickStatus = 1;
		while(running){
			while (!buttonsPressed.isEmpty()){
				Button b = buttonsPressed.get(buttonsPressed.size() - 1);
				buttonsPressed.remove(buttonsPressed.size() - 1);
				displayOnClickStatus = displayManager.onClick(b);
				dataManager.onClick(b);
				dataManager.report();
			}
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void onDestroy() {
		// TODO Auto-generated method stub
		boolean retry = true;
		running = false;
		while (retry) {
			try {
				thread.join();
				retry = false;
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void onPause(){
		boolean retry = true;
		running = false;
		while (retry) {
			try {
				thread.join();
				retry = false;
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void onResume(){
		running = true;
		thread = new Thread(this);
		thread.start();
	}
}
