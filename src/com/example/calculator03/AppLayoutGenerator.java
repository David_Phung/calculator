package com.example.calculator03;

import java.util.ArrayList;

import android.R.dimen;
import android.content.Context;
import android.graphics.Color;
import android.text.Html;
import android.text.Layout;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewParent;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class AppLayoutGenerator {

	private String smallButtonsTexts[][] = {
			{ "SHIFT", "MOVE", "LEFT", "$&#8678|LEFT", "$&#8679|UP", "$&#8680|RIGHT" },
			{ "$x/y|Fr", "sin", "cos", "tan", "$&#8681|DOWN", "hyp" },
			{ "$x<sup>2</sup>|square", "$\u221ay|sqrt", "(", ")", "ln", "$x<sup>y</sup>|Pt" } };

	private String smallTVsTexts[][] = {
			{ "", "", " ", "", "", "" },
			{ "", "$sin<sup>-1</sup>|sin-1", "$cos<sup>-1</sup>|cos-1", "$tan<sup>-1</sup>|tan-1", "", "(-)" },
			{ "$x<sup>3</sup>|cube", "$<sup>3</sup>\u221ax|crt", "", "$log<sub>x</sub>y|logN", "log", "$<sup>x</sup>\u221ay|root" } };

	private String bigButtonsTexts[][] = { 
			{ "7", "8", "9", "DEL", "AC" },
			{ "4", "5", "6", "�", "�" }, { "1", "2", "3", "+", "-" },
			{ "0", ".", "(-)", "$�10<sup>x</sup>|exp", "=" } };

	private String bigTVsTexts[][] = { 
			{ "STO", "", "", "INS", "" },
			{ "D", "E", "F", "nPr", "nCr" }, { "A", "B", "C", "Pol", "Rec" },
			{ "", "", "", "Mod", "Ans" } };

	private Context context;
	private ArrayList<MyButton> bigButtonList;
	private ArrayList<MyButton> smallButtonList;
	private ArrayList<MyTextView> smallTextViewList;
	private ArrayList<MyTextView> bigTextViewList;
	private MySurfaceView displayView;
	private Calculator03 mainActivity;
	private Button mButton;
	private FrameLayout upLayout, downLayout;
	
	public boolean upDownLayoutOn;
	public boolean mButtonPressed;
	public boolean nonDirectionButtonPressed;

	public AppLayoutGenerator(Context context) {
		// TODO Auto-generated constructor stub
		this.context = context;
		//this.mainActivity = mainActivity;
		bigButtonList = new ArrayList<MyButton>();
		smallButtonList = new ArrayList<MyButton>();
		smallTextViewList = new ArrayList<MyTextView>();
		bigTextViewList = new ArrayList<MyTextView>();
		
		upDownLayoutOn = false;
		mButtonPressed = false;
		nonDirectionButtonPressed = false;
	}
	
	public RelativeLayout generateLayout_usingTableLayout(){
		//create the main Layout:
		RelativeLayout mainLayout = new RelativeLayout(context);
		mainLayout.setLayoutParams(new LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
				RelativeLayout.LayoutParams.MATCH_PARENT));
		mainLayout.setBackgroundColor(Color.parseColor("#1d242c"));
		
		LinearLayout mainLL = new LinearLayout(context);
		mainLL.setOrientation(LinearLayout.VERTICAL);
		mainLL.setLayoutParams(new LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
				RelativeLayout.LayoutParams.MATCH_PARENT));
		//add surface view
		displayView = new MySurfaceView(context);
		displayView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
				0, 1f));
		
		//add small section
		TableLayout smallSectionLayout = generateSmallSectionLayout_usingTableLayout();
		smallSectionLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
				0, 18f/17));
		
		//add big section
		TableLayout bigSectionLayout = generateBigSectionLayout_usingTableLayout();
		bigSectionLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
				0, 33/17f));
		
		mainLL.addView(displayView);
		mainLL.addView(smallSectionLayout);
		mainLL.addView(bigSectionLayout);
		
		//add up down layouts
		upLayout = generateUpButtonLayout();
		downLayout = generateDownButtonLayout();
		
		mainLayout.addView(mainLL);
		mainLayout.addView(upLayout);
		mainLayout.addView(downLayout);
		
		return mainLayout;
	}
	
	public void setListener(ButtonListener listener){
		for (int i = 0; i < bigButtonList.size(); i++){
			Button b = bigButtonList.get(i);
			b.setOnClickListener(listener);
		}
		
		for (MyButton b:smallButtonList){
			b.setOnClickListener(listener);
		}
	}

	public TableLayout generateSmallSectionLayout_usingTableLayout() {

		// create the main layout
		TableLayout tableLayout = new TableLayout(context);
		tableLayout.setStretchAllColumns(true);
		tableLayout.setShrinkAllColumns(true);
		tableLayout.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT));

		// create the rows
		for (int i = 0; i < smallButtonsTexts.length; i++) {
			// add TVs
			if (i > 0) {
				LinearLayout rowTV = new LinearLayout(context);
				rowTV.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, 0, 2));
				for (int j = 0; j < smallTVsTexts[i].length; j++) {
					MyTextView tv = new MyTextView(context);
					tv.setLayoutParams(new LinearLayout.LayoutParams(0,
							LinearLayout.LayoutParams.MATCH_PARENT, 1));
					tv.setText(smallTVsTexts[i][j]);
					tv.setNameTag(smallTVsTexts[i][j]);
					tv.setDisplayString(smallTVsTexts[i][j]);
					tv.setGravity(Gravity.CENTER);
					
					tv.setTextSize(12);
					tv.setTextColor(Color.WHITE);
					
					setBackgroundBasedOnNameTag(tv);
					rowTV.addView(tv);
					
					smallTextViewList.add(tv);
				}
				tableLayout.addView(rowTV);
			}

			// add buttons
			LinearLayout row = new LinearLayout(context);
			row.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, 0, 3));
			for (int j = 0; j < smallButtonsTexts[i].length; j++) {
				MyButton button = new MyButton(context);
				button.setLayoutParams(new LinearLayout.LayoutParams(0,
						LinearLayout.LayoutParams.MATCH_PARENT, 1));
			    button.setText(smallButtonsTexts[i][j]);
			//	button.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_launcher, 0, 0);
				button.setGravity(Gravity.CENTER);
				button.setNameTag(smallButtonsTexts[i][j]);
				button.setDisplayString(smallButtonsTexts[i][j]);
				
				button.setTextSize(12);
				button.setTextColor(Color.WHITE);
				
				if (button.getNameTag().equals("$&#8681|DOWN")){
					button.setBackgroundResource(R.drawable.button_special);
				}else if ( i == 0 && j < smallButtonsTexts[i].length){
					button.setBackgroundResource(R.drawable.button_special);
				}else{
					button.setBackgroundResource(R.drawable.button_small);
				}
				
				setBackgroundBasedOnNameTag(button);
				
				row.addView(button);
				smallButtonList.add(button);
				
				if (smallButtonsTexts[i][j].equals("m")){
					mButton = button;
				}
			}
			tableLayout.addView(row);
		}
		return tableLayout;
	}

	public TableLayout generateBigSectionLayout_usingTableLayout() {
		// create the main layout
		TableLayout tableLayout = new TableLayout(context);
		tableLayout.setStretchAllColumns(true);
		tableLayout.setShrinkAllColumns(true);
		tableLayout.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT));

		// create the rows
		for (int i = 0; i < bigButtonsTexts.length; i++) {
			// add TVs
			LinearLayout rowTV = new LinearLayout(context);
			rowTV.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, 0, 1));
			for (int j = 0; j < bigTVsTexts[i].length; j++) {
				MyTextView tv = new MyTextView(context);
				tv.setLayoutParams(new TableRow.LayoutParams(0,
						TableRow.LayoutParams.MATCH_PARENT, 1));
				tv.setText(bigTVsTexts[i][j]);
				tv.setNameTag(bigTVsTexts[i][j]);
				tv.setDisplayString(bigTVsTexts[i][j]);
				
				tv.setGravity(Gravity.CENTER);
				
				tv.setTextSize(16);
				tv.setTextColor(Color.WHITE);
				
				setBackgroundBasedOnNameTag(tv);
				
				rowTV.addView(tv);
				
				bigTextViewList.add(tv);
			}
			tableLayout.addView(rowTV);

			// add buttons
			LinearLayout row = new LinearLayout(context);
			row.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, 0, 2));
			for (int j = 0; j < bigButtonsTexts[i].length; j++) {
				MyButton button = new MyButton(context);
				LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(0,
						TableRow.LayoutParams.MATCH_PARENT, 1);
			//	param.setMargins(1, 1, 1, 1);
				button.setLayoutParams(param);
				button.setText(bigButtonsTexts[i][j]);
				button.setNameTag(bigButtonsTexts[i][j]);
				button.setDisplayString(bigButtonsTexts[i][j]);
				
				button.setTextSize(20);
				
				if (button.getText().equals("AC") || button.getText().equals("DEL")){
					button.setBackgroundResource(R.drawable.button_big_red);
				}else{
					button.setBackgroundResource(R.drawable.button_big);
				}
				button.setTextColor(Color.WHITE);
				
				setBackgroundBasedOnNameTag(button);
				
				row.addView(button);
				bigButtonList.add(button);
			}
			tableLayout.addView(row);
		}
		return tableLayout;
	}

	private void setBackgroundBasedOnNameTag(MyButton b){
		String displayString = b.getDisplayString();
		if (displayString.length() == 0){
			return;
		}
		if (displayString.charAt(0) == '#'){
			String temp[] = displayString.split("[|]");
			displayString = temp[0].substring(1);
			int id = context.getResources().getIdentifier(displayString, "drawable", "com.example.calculator03");
			b.setBackgroundResource(id);
			b.setText("");
			b.setNameTag(temp[1]);
		}
		if (displayString.charAt(0) == '$'){
			String temp[] = displayString.split("[|]");
			displayString = temp[0].substring(1);
			b.setText(Html.fromHtml(displayString));
			b.setNameTag(temp[1]);
		}
	}
	
	private void setBackgroundBasedOnNameTag(MyTextView tv){
		String displayString = tv.getDisplayString();
		if (displayString.length() == 0){
			return;
		}
		if (displayString.charAt(0) == '#'){
			String temp[] = displayString.split("[|]");
			displayString = temp[0].substring(1);
			int id = context.getResources().getIdentifier(displayString, "drawable", "com.example.calculator03");
			tv.setBackgroundResource(id);
			tv.setText("");
			tv.setNameTag(temp[1]);
		}
		if (displayString.charAt(0) == '$'){
			String temp[] = displayString.split("[|]");
			displayString = temp[0].substring(1);
			tv.setText(Html.fromHtml(displayString));
			tv.setNameTag(temp[1]);
		}
	}
	
	public FrameLayout generateUpButtonLayout(){
		FrameLayout frameLayout = new FrameLayout(context);
		//frameLayout.setLayoutParams(new FrameLayout.LayoutParams(LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT));
		Button up = new Button(context);
		up.setLayoutParams(new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));
		up.setText("UP");
		up.setTextColor(Color.WHITE);
		up.setBackgroundResource(R.drawable.button_special);
		frameLayout.addView(up);
		frameLayout.setVisibility(FrameLayout.GONE);
		return frameLayout;
	}
	
	public FrameLayout generateDownButtonLayout(){
		FrameLayout frameLayout = new FrameLayout(context);
		//frameLayout.setLayoutParams(new FrameLayout.LayoutParams(LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT));
		Button up = new Button(context);
		up.setText("DOWN");
		up.setTextColor(Color.WHITE);
		up.setBackgroundResource(R.drawable.button_special);
		up.setLayoutParams(new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));
		frameLayout.addView(up);
		frameLayout.setVisibility(FrameLayout.GONE);
		return frameLayout;
	}

	public void toggleUpDownLayouts(){
		
		if (upLayout.getVisibility() == FrameLayout.GONE || downLayout.getVisibility() == FrameLayout.GONE){
			Button b = bigButtonList.get(0);
			RelativeLayout.LayoutParams upParams = (RelativeLayout.LayoutParams) upLayout.getLayoutParams();
			RelativeLayout.LayoutParams downParams = (RelativeLayout.LayoutParams) downLayout.getLayoutParams();
			
			ViewParent mButtonRow = mButton.getParent();
			TableLayout mButtonTable = (TableLayout) mButtonRow.getParent();
			int yTop = mButtonTable.getTop();
			
			upParams.width = downParams.width = b.getHeight();
			upParams.height = downParams.height = b.getWidth();
			
			upParams.leftMargin = downParams.leftMargin = mButton.getLeft() + (mButton.getWidth() - upParams.width) / 2;
			upParams.topMargin = yTop - upParams.height;
			downParams.topMargin = yTop + mButton.getHeight();
			
			upLayout.setLayoutParams(upParams);
			downLayout.setLayoutParams(downParams);
			
			upLayout.setVisibility(RelativeLayout.VISIBLE);
			downLayout.setVisibility(RelativeLayout.VISIBLE);
			
			upDownLayoutOn = true;
		}else{
			upLayout.setVisibility(FrameLayout.GONE);
			downLayout.setVisibility(FrameLayout.GONE);
			upDownLayoutOn = false;
		}
	}
	
	public void disableUpDownLayouts(){
		upLayout.setVisibility(FrameLayout.GONE);
		downLayout.setVisibility(FrameLayout.GONE);
		upDownLayoutOn = false;
	}
	
	public void printSizes(){
		TextView smallTv = null;
		Button smallButton = null;
		TextView bigTv = null;
		Button bigButton = null;
		for (int i = 0; i < bigButtonList.size(); i++){
			Button b = bigButtonList.get(i);
			if (b.getText().toString().equals(smallButtonsTexts[0][0])){
				smallButton = b;
			}else if (b.getText().toString().equals(bigButtonsTexts[0][0])){
				bigButton = b;
			}
		}
		smallTv = smallTextViewList.get(0);
		bigTv = bigTextViewList.get(0);
		
		Log.d("","In AppLayoutGenerator getSizes(): displayView: " + displayView.getWidth() + "," + displayView.getHeight()
				+ " smallButton: " + smallButton.getWidth() + "," + smallButton.getHeight()
				+ " smallTv: " + smallTv.getWidth() + "," + smallTv.getHeight()
				+ " bigButton: " + bigButton.getWidth() + "," + bigButton.getHeight()
				+ " bigTv: " + bigTv.getWidth() + "," + bigTv.getHeight());
	} 
	
	//------------------------------------------------------------
	public MySurfaceView getDisplayView() {
		return displayView;
	}


	public void setButtonList(ArrayList<MyButton> buttonList) {
		this.bigButtonList = buttonList;
	}

	public ArrayList<MyTextView> getSmallTextViewList() {
		return smallTextViewList;
	}

	public void setSmallTextViewList(ArrayList<MyTextView> smallTextViewList) {
		this.smallTextViewList = smallTextViewList;
	}

	public ArrayList<MyTextView> getBigTextViewList() {
		return bigTextViewList;
	}

	public void setBigTextViewList(ArrayList<MyTextView> bigTextViewList) {
		this.bigTextViewList = bigTextViewList;
	}

	public void setDisplayView(MySurfaceView displayView) {
		this.displayView = displayView;
	}

	public Button getmButton() {
		return mButton;
	}

	public ArrayList<MyButton> getBigButtonList() {
		return bigButtonList;
	}

	public void setBigButtonList(ArrayList<MyButton> bigButtonList) {
		this.bigButtonList = bigButtonList;
	}

	public ArrayList<MyButton> getSmallButtonList() {
		return smallButtonList;
	}

	public void setSmallButtonList(ArrayList<MyButton> smallButtonList) {
		this.smallButtonList = smallButtonList;
	}

	
	
}
