package com.example.calculator03;

import displayLayout.TextComponent;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import visualToken.FractionToken;
import visualToken.StringToken;
import visualToken.TextArea;
import visualToken.VisualToken;

public class CommonButtonCommands_Visual {
	
	private DisplayManager DM;
	
	private TextAreaEditor TAEditor;
	
	private ButtonListener buttonListener;
	
	private boolean bShiftOn;
	
	private AppLayoutGenerator appLayoutGenerator;

	public CommonButtonCommands_Visual() {
		// TODO Auto-generated constructor stub
		bShiftOn = false;
	}
	
	public void setReferences(Calculator03 mainActivity){
		DM = mainActivity.getDisplayManager();
		buttonListener = mainActivity.getButtonListener();
		TAEditor = mainActivity.getTAEditor();
		appLayoutGenerator = mainActivity.getAppLayoutGenerator();
	}
	
	public void onClick(Button b){
		TAEditor.setTargetPart(DM.getInputTextComponentToWorkWith());
		
		if (DM.dataUpdateStatus != DisplayManager.WAITING_FOR_INPUT){
			return;
		}
		
		boolean shiftPressed = false;
		boolean directionButtonPressed = false;
		
		MyButton tempB = (MyButton)b;
		String bText = tempB.getNameTag();
		// ------------------ Special button section ------------------------
		if (bText.equals("SHIFT")){
			bShiftOn = !bShiftOn;
			shiftPressed = true;
		}else if (bText.equals("MOVE")){
			//DM.test();
			test();
		}else if (bText.equals("LEFT")){
			TAEditor.moveCursorLeft();
			directionButtonPressed = true;
		}else if (bText.equals("m")){
			appLayoutGenerator.mButtonPressed = true;
			directionButtonPressed = true;
		}else if (bText.equals("RIGHT")){
			TAEditor.moveCursorRight();
			directionButtonPressed = true;
		}else if (bText.equals("MODE")){

		}else if (bText.equals("M+")){
			
		}
		
		//------------------------ Big buttons section ---------------------------------
		else if (bText.equals("1")){
			TAEditor.addToken(VisualToken.STRING_TOKEN, "1");
		}else if (bText.equals("2")){
			TAEditor.addToken(VisualToken.STRING_TOKEN, "2");
		}else if (bText.equals("3")){
			TAEditor.addToken(VisualToken.STRING_TOKEN, "3");
		}else if (bText.equals("4")){
			TAEditor.addToken(VisualToken.STRING_TOKEN, "4");
		}else if (bText.equals("5")){
			TAEditor.addToken(VisualToken.STRING_TOKEN, "5");
		}else if (bText.equals("6")){
			TAEditor.addToken(VisualToken.STRING_TOKEN, "6");
		}else if (bText.equals("7")){
			TAEditor.addToken(VisualToken.STRING_TOKEN, "7");
		}else if (bText.equals("8")){
			TAEditor.addToken(VisualToken.STRING_TOKEN, "8");
		}else if (bText.equals("9")){
			TAEditor.addToken(VisualToken.STRING_TOKEN, "9");
		}else if (bText.equals("0")){
			TAEditor.addToken(VisualToken.STRING_TOKEN, "0");
		}else if (bText.equals(".")){
			TAEditor.addToken(VisualToken.STRING_TOKEN, ".");
		}else if (bText.equals("(-)")){
			TAEditor.addToken(VisualToken.STRING_TOKEN, "_");
		}else if (bText.equals("exp")){
			if (bShiftOn){
				TAEditor.addToken(VisualToken.STRING_TOKEN, "mod");
			}else{
				TAEditor.addToken(VisualToken.EXP_TOKEN, "x10");
			}
		}else if (bText.equals("+")){
			if (bShiftOn){
				TAEditor.addToken(VisualToken.COMBINATORIC_POL_RECT_TOKEN, "Pol");
			}else{
				TAEditor.addToken(VisualToken.STRING_TOKEN, "+");
			}
		}else if (bText.equals("-")){
			if (bShiftOn){
				TAEditor.addToken(VisualToken.COMBINATORIC_POL_RECT_TOKEN, "Rec");
			}else{
				TAEditor.addToken(VisualToken.STRING_TOKEN, "-");
			}
		}else if (bText.equals("�")){
			if (bShiftOn){
				TAEditor.addToken(VisualToken.COMBINATORIC_POL_RECT_TOKEN, "P");
			}else{
				TAEditor.addToken(VisualToken.STRING_TOKEN, "*");
			}
		}else if (bText.equals("�")){
			if (bShiftOn){
				TAEditor.addToken(VisualToken.COMBINATORIC_POL_RECT_TOKEN, "C");
			}else{
				TAEditor.addToken(VisualToken.STRING_TOKEN, "/");
			}
		}else if (bText.equals("=")){
			if (bShiftOn){
				TAEditor.addToken(VisualToken.STRING_TOKEN, "Ans");
			}else{
				DM.startComputing();
			}
		}else if (bText.equals("AC")){
			TAEditor.clear();
		}else if (bText.equals("DEL")){
			TAEditor.delete(true);
		}
		
		//-------------------------- Small button section --------------------------------
		else if (bText.equals("Fr")){
			TAEditor.addToken(VisualToken.FRACTION_TOKEN, null);
		}else if (bText.equals("Pt")){
			if (bShiftOn){
				TAEditor.addToken(VisualToken.ROOT_CUSTOM_DEGREE_TOKEN, null);
			}else{
				TAEditor.addToken(VisualToken.POWER_TO_TOKEN, null);
			}
		}else if (bText.equals("hyp")){
			if (bShiftOn){
				TAEditor.addToken(VisualToken.STRING_TOKEN, "-");
			}else {
				if (DM.mode != DisplayManager.HYP_LAYOUT){
					DM.switchMode(DisplayManager.HYP_LAYOUT);
				}else{
					DM.mode = DisplayManager.BASIC_MODE;
				}
			}
		}else if (bText.equals("sin")){
			if (bShiftOn){
				TAEditor.addToken(VisualToken.INVERSE_TRIG, "sin");
			}else{
				TAEditor.addToken(VisualToken.STRING_TOKEN, "sin");
			}
		}else if (bText.equals("cos")){
			if (bShiftOn){
				TAEditor.addToken(VisualToken.INVERSE_TRIG, "cos");
			}else{
				TAEditor.addToken(VisualToken.STRING_TOKEN, "cos");
			}
		}else if (bText.equals("tan")){
			if (bShiftOn){
				TAEditor.addToken(VisualToken.INVERSE_TRIG, "tan");
			}else{
				TAEditor.addToken(VisualToken.STRING_TOKEN, "tan");
			}
		}else if (bText.equals("sqrt")){
			if (bShiftOn){
				TAEditor.addToken(VisualToken.CUBE_ROOT, null);
			}else{
				TAEditor.addToken(VisualToken.SQUARE_ROOT, null);
			}
		}else if (bText.equals("square")){
			if (bShiftOn){
				TAEditor.addToken(VisualToken.CUBE, null);
			}else{
				TAEditor.addToken(VisualToken.SQUARE, null);
			}
		}else if (bText.equals("(")){
			if (bShiftOn){
				
			}else{
				TAEditor.addToken(VisualToken.STRING_TOKEN, "(");
			}
		}else if (bText.equals(")")){
			if (bShiftOn){
				TAEditor.addToken(VisualToken.LOGARITHM_CUSTOM_BASE, "log");
			}else{
				TAEditor.addToken(VisualToken.STRING_TOKEN, ")");
			}
		}else if (bText.equals("ln")){
			if (bShiftOn){
				TAEditor.addToken(VisualToken.LOGARITHM_FIXED_BASE, "log");
			}else{
				TAEditor.addToken(VisualToken.LOGARITHM_FIXED_BASE, "ln");
			}
		}

		if (!shiftPressed)	bShiftOn = false;
		if (!directionButtonPressed){
			appLayoutGenerator.nonDirectionButtonPressed = true;
		}
	}

	public void hypOnClick(String bText){
		TAEditor.setTargetPart(DM.getInputTextComponentToWorkWith());
		
		if (DM.dataUpdateStatus != DisplayManager.WAITING_FOR_INPUT){
			return;
		}
		
		if (bText.equals("1")){
			TAEditor.addToken(VisualToken.STRING_TOKEN, "sinh");
			DM.mode = DisplayManager.BASIC_MODE;
		}else if (bText.equals("2")){
			TAEditor.addToken(VisualToken.STRING_TOKEN, "cosh");
			DM.mode = DisplayManager.BASIC_MODE;
		}else if (bText.equals("3")){
			TAEditor.addToken(VisualToken.STRING_TOKEN, "tanh");
			DM.mode = DisplayManager.BASIC_MODE;
		}else if (bText.equals("4")){
			TAEditor.addToken(VisualToken.INVERSE_TRIG, "sinh");
			DM.mode = DisplayManager.BASIC_MODE;
		}else if (bText.equals("5")){
			TAEditor.addToken(VisualToken.INVERSE_TRIG, "cosh");
			DM.mode = DisplayManager.BASIC_MODE;
		}else if (bText.equals("6")){
			TAEditor.addToken(VisualToken.INVERSE_TRIG, "tanh");
			DM.mode = DisplayManager.BASIC_MODE;
		}else if (bText.equals("hyp")){
			DM.mode = DisplayManager.BASIC_MODE;
		}else if (bText.equals("AC")){
			DM.mode = DisplayManager.BASIC_MODE;
		}
	}
	
	private void test(){
		TextComponent tc = DM.getInputTextComponentToWorkWith();
		TextArea ta = tc.getRootTextArea();
		FractionToken ft = new FractionToken(ta);
		TextArea num = ft.getNumerator();
		TextArea den = ft.getDenominator();
		
		StringToken s1 = new StringToken(num , "1");
		StringToken s2 = new StringToken(den , "2");
		
		ft.getNumerator().getTokenList().add(s1);
		ft.getDenominator().getTokenList().add(s2);
		
		ta.getTokenList().add(ft);
		ta.update_pass_1_chain_down();
		ta.update_pass_2();
	}
}
