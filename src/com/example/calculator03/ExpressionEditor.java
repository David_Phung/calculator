package com.example.calculator03;

import android.util.Log;
import dataMode.DataInputComponent;
import dataToken.AnsDataToken;
import dataToken.BinaryOperator;
import dataToken.CombinatoricAndPolRectDataToken;
import dataToken.ContainerToken;
import dataToken.Cube;
import dataToken.CubeRoot;
import dataToken.DataCursorManager;
import dataToken.DataToken;
import dataToken.DigitToken;
import dataToken.Expression;
import dataToken.FlagToken;
import dataToken.FractionDataToken;
import dataToken.FunctionToken;
import dataToken.LogarithmFixedBaseDataToken;
import dataToken.LogarithmNthDataToken;
import dataToken.NumberToken;
import dataToken.ParenthesisDataToken;
import dataToken.PowerToDataToken;
import dataToken.PowerToFixedBase;
import dataToken.RootNthDataToken;
import dataToken.Square;
import dataToken.SquareRoot;
import dataToken.UnaryOperator;

public class ExpressionEditor {
	
	private DataManager dataM;
	private DataInputComponent targetInputComponent;

	public ExpressionEditor() {
		// TODO Auto-generated constructor stub
	}
	
	public void setReferences(Calculator03 mainActivity) {
		
	}
	
	public void clear(){
		DataCursorManager dataCursorManager = targetInputComponent.getDataCursorManager();
		Expression rootExpression = targetInputComponent.getExpression();
		dataCursorManager.clear();
		rootExpression.getTokenList().clear();
		dataCursorManager.addRootExpression(rootExpression);
	}
	
	public void delete(boolean bCheckTokenToTheRightRequested){
		DataCursorManager DCM = targetInputComponent.getDataCursorManager();
		//get the focused expression
		Expression focusedExpression = DCM.getCurrFocusedExpression();
		
		boolean bNeedCheck_IfTokenToTheRight_IsPowerToToken = false;
		
		//if index == -1
		if (DCM.getCursor_elementIndex() == -1){
			//if cursor is inside rootExpression
			if (focusedExpression.isRoot()){
				return;
			}else{ //if cursor is inside some function token
				FunctionToken parent_functionToken = (FunctionToken) focusedExpression.getParent();
				Expression parentExpression_of_parentFunctionToken = parent_functionToken.getParent();
				//if cursor is inside the FIRST expression of that token, delete it
				if (parent_functionToken.getIndexOfExpression(focusedExpression) == 0){
					int index_of_parentFunctionToken = parentExpression_of_parentFunctionToken.getTokenList().indexOf(parent_functionToken);
					parentExpression_of_parentFunctionToken.getTokenList().remove(parent_functionToken);
					DCM.placeCursorAt(parentExpression_of_parentFunctionToken, index_of_parentFunctionToken - 1);
					
					bNeedCheck_IfTokenToTheRight_IsPowerToToken = true;
				}else{//if cursor is NOT inside the FIRST expression of that token
					DCM.moveCursorLeft();
				}
			}
		}else{ //if index is not -1
			DataToken focusedToken = DCM.getCurrFocusedToken();
			//if cursor point at a functionToken
			if (focusedToken.type == DataToken.FUNCTION_TOKEN){
				DCM.moveCursorLeft();
			}else{//normal case
				int index_of_focusedToken = focusedExpression.getTokenList().indexOf(focusedToken);
				focusedExpression.getTokenList().remove(focusedToken);
				DCM.placeCursorAt(focusedExpression, index_of_focusedToken - 1);
				
				//we perform check to see if we just deleted a 
				//open paren that belong to a trig token
				//If yes then we delete the token on the left 1 more time
				/*if (focusedToken.identifier.contains(ParenthesisDataToken.OPEN_ID)){
					ParenthesisDataToken parenToken = (ParenthesisDataToken)focusedToken;
					if (parenToken.bBelongToATrigonometryToken == true){
						//For debug purpose we perform check 1 more time to see if the 
						//token to the left is a trig token
						
						DataToken tokenToTheLeftNOW = DCM.getCurrFocusedToken();
						if (tokenToTheLeftNOW != null){
							if (tokenToTheLeftNOW.identifier.contains(UnaryOperator.TRIGONOMETRY_FUNCTION_OPERATOR_ID) == false){
								Log.d("","BUG: >> Inside ExpressionEditor -> delete -> token to the left is not trig token");
							}
							int index_of_focusedTokenNOW = focusedExpression.getTokenList().indexOf(tokenToTheLeftNOW);
							focusedExpression.getTokenList().remove(tokenToTheLeftNOW);
							DCM.placeCursorAt(focusedExpression, index_of_focusedTokenNOW - 1);
						}
					}
				}*/
				
				bNeedCheck_IfTokenToTheRight_IsPowerToToken = true;
			}
		}
		
		if (bCheckTokenToTheRightRequested || bNeedCheck_IfTokenToTheRight_IsPowerToToken){
			focusedExpression = DCM.getCurrFocusedExpression();
			int currentIndex = DCM.getCursor_elementIndex();
			if (currentIndex < focusedExpression.getTokenList().size() - 1){
				DataToken tokenToTheRight = focusedExpression.getTokenList().get(currentIndex + 1);
				if (tokenToTheRight.identifier.contains(FunctionToken.POWER_TO_DATA_TOKEN_ID)){
					PowerToDataToken powerToToken = (PowerToDataToken)tokenToTheRight;
					powerToToken.updateTempBase();
				}
			}
		}
		
	}
	
	private DataToken createToken(String id, String argument, int typeArgument){
		DataCursorManager dataCursorManager = targetInputComponent.getDataCursorManager();
		Expression expression = dataCursorManager.getCurrFocusedExpression();
		DataToken token = null;
		if (id.equals(DataToken.DIGIT_TOKEN_ID)){
			token = new DigitToken(expression, argument);
		}else if (id.equals(DataToken.NUMBER_TOKEN_ID)){
			token = new NumberToken(expression, argument);
		}else if (id.equals(ParenthesisDataToken.OPEN_ID)){
			token = new ParenthesisDataToken(expression, ParenthesisDataToken.OPEN);
		}else if (id.equals(ParenthesisDataToken.CLOSE_ID)){
			token = new ParenthesisDataToken(expression, ParenthesisDataToken.CLOSE);
		}else if (id.equals(BinaryOperator.ADDITION_ID)){
			token = new BinaryOperator(expression, BinaryOperator.ADDITION);
		}else if (id.equals(BinaryOperator.SUBTRACTION_ID)){
			token = new BinaryOperator(expression, BinaryOperator.SUBTRACTION);
		}else if (id.equals(BinaryOperator.MULTIPLCATION_ID)){
			token = new BinaryOperator(expression, BinaryOperator.MULTIPLCATION);
		}else if (id.equals(BinaryOperator.DIVISION_ID)){
			token = new BinaryOperator(expression, BinaryOperator.DIVISION);
		}else if (id.equals(BinaryOperator.POWER_TO_ID)){
			token = new BinaryOperator(expression, BinaryOperator.POWER_TO);
		}else if (id.equals(BinaryOperator.MODULUS_ID)){
			token = new BinaryOperator(expression, BinaryOperator.MODULUS);
		}else if (id.equals(UnaryOperator.NEGATIVE_SIGN_ID)){
			token = new UnaryOperator(expression, UnaryOperator.NEGATIVE_SIGN, UnaryOperator.PRE_UNARY);
		}else if (id.equals(UnaryOperator.SINE_TRIG_FUNCTION_ID)){
			token = new UnaryOperator(expression, UnaryOperator.SINE_TRIG_FUNCTION, UnaryOperator.PRE_UNARY);
		}else if (id.equals(UnaryOperator.COSINE_TRIG_FUNCTION_ID)){
			token = new UnaryOperator(expression, UnaryOperator.COSINE_TRIG_FUNCTION, UnaryOperator.PRE_UNARY);
		}else if (id.equals(UnaryOperator.TAN_TRIG_FUNCTION_ID)){
			token = new UnaryOperator(expression, UnaryOperator.TAN_TRIG_FUNCTION, UnaryOperator.PRE_UNARY);
		}else if (id.equals(UnaryOperator.INVERSE_SIN_TRIG_FUNCTION_ID)){
			token = new UnaryOperator(expression, UnaryOperator.INVERSE_SIN_TRIG_FUNCTION, UnaryOperator.PRE_UNARY);
		}else if (id.equals(UnaryOperator.INVERSE_COS_TRIG_FUNCTION_ID)){
			token = new UnaryOperator(expression, UnaryOperator.INVERSE_COS_TRIG_FUNCTION, UnaryOperator.PRE_UNARY);
		}else if (id.equals(UnaryOperator.INVERSE_TAN_TRIG_FUNCTION_ID)){
			token = new UnaryOperator(expression, UnaryOperator.INVERSE_TAN_TRIG_FUNCTION, UnaryOperator.PRE_UNARY);
		}else if (id.equals(UnaryOperator.SINH_TRIG_FUNCTION_ID)){
			token = new UnaryOperator(expression, UnaryOperator.SINH_TRIG_FUNCTION, UnaryOperator.PRE_UNARY);
		}else if (id.equals(UnaryOperator.COSH_TRIG_FUNCTION_ID)){
			token = new UnaryOperator(expression, UnaryOperator.COSH_TRIG_FUNCTION, UnaryOperator.PRE_UNARY);
		}else if (id.equals(UnaryOperator.TANH_TRIG_FUNCTION_ID)){
			token = new UnaryOperator(expression, UnaryOperator.TANH_TRIG_FUNCTION, UnaryOperator.PRE_UNARY);
		}else if (id.equals(UnaryOperator.INVERSE_SINH_TRIG_FUNCTION_ID)){
			token = new UnaryOperator(expression, UnaryOperator.INVERSE_SINH_TRIG_FUNCTION, UnaryOperator.PRE_UNARY);
		}else if (id.equals(UnaryOperator.INVERSE_COSH_TRIG_FUNCTION_ID)){
			token = new UnaryOperator(expression, UnaryOperator.INVERSE_COSH_TRIG_FUNCTION, UnaryOperator.PRE_UNARY);
		}else if (id.equals(UnaryOperator.INVERSE_TANH_TRIG_FUNCTION_ID)){
			token = new UnaryOperator(expression, UnaryOperator.INVERSE_TANH_TRIG_FUNCTION, UnaryOperator.PRE_UNARY);
		}else if (id.equals(FunctionToken.FRACTION_TOKEN_ID)){
			token = new FractionDataToken(expression);
		}else if (id.equals(FunctionToken.POWER_TO_DATA_TOKEN_ID)){
			token = new PowerToDataToken(expression);
		}else if (id.equals(FunctionToken.POWER_TO_FIXED_BASE_ID)){
			token = new PowerToFixedBase(expression, argument);
		}else if (id.equals(FunctionToken.SQUARE_ID)){
			token = new Square(expression);
		}else if (id.equals(FunctionToken.CUBE_ID)){
			token = new Cube(expression);
		}else if (id.equals(FunctionToken.ROOTH_Nth_TOKEN_ID)){
			token = new RootNthDataToken(expression);
		}else if(id.equals(FunctionToken.SQUARE_ROOT_ID)){
			token = new SquareRoot(expression);
		}else if (id.equals(FunctionToken.CUBE_ROOT_ID)){
			token = new CubeRoot(expression);
		}else if (id.equals(FunctionToken.LOGARITHM_CUSTOM_BASE_ID)){
			token = new LogarithmNthDataToken(expression);
		}else if(id.equals(FunctionToken.LOGARITHM_FIXED_BASE_ID)){
			token = new LogarithmFixedBaseDataToken(expression, argument);
		}else if (id.equals(FunctionToken.ANS_FUNCTION_TOKEN_ID)){
			token = new AnsDataToken(expression);
		}else if (id.equals(FunctionToken.COMBINATORIC_POL_RECT_ID)){
			token = new CombinatoricAndPolRectDataToken(expression, typeArgument);
		}else if (id.equals(ContainerToken.ADD_SUB_CONTAINER_TYPE_ID)){
			token = new ContainerToken(expression, ContainerToken.ADD_SUB_CONTAINER_TYPE);
		}else if (id.equals(ContainerToken.MULT_DIV_MOD_CONTAINER_TYPE_ID)){
			token = new ContainerToken(expression, ContainerToken.MULT_DIV_MOD_CONTAINER_TYPE);
		}else if (id.equals(ContainerToken.POST_UNARY_CONTAINER_TYPE_ID)){
			token = new ContainerToken(expression, ContainerToken.POST_UNARY_CONTAINER_TYPE);
		}else if (id.equals(ContainerToken.PRE_UNARY_NEGATIVE_SIGN_CONTAINER_TYPE_ID)){
			token = new ContainerToken(expression, ContainerToken.PRE_UNARY_NEGATIVE_SIGN_CONTAINER_TYPE);
		}else if (id.equals(ContainerToken.PRE_UNARY_CONTAINER_TYPE_ID)){
			token = new ContainerToken(expression, ContainerToken.PRE_UNARY_CONTAINER_TYPE);
		}else if (id.equals(ContainerToken.POWER_TO_CONTAINER_TYPE_ID)){
			token = new ContainerToken(expression, ContainerToken.POWER_TO_CONTAINER_TYPE);
		}else if (id.equals(ContainerToken.FUNCTION_CONTAINER_TYPE_ID)){
			token = new ContainerToken(expression, ContainerToken.FUNCTION_CONTAINER_TYPE);
		}else if (id.equals(FlagToken.SYNTAX_ERROR_ID)){
			token = new FlagToken(expression, FlagToken.SYNTAX_ERROR);
		}else if (id.equals(FlagToken.MATH_ERROR_ID)){
			token = new FlagToken(expression, FlagToken.MATH_ERROR);
		}
		
		return token;
	}
	
	public void addToken(String id, String argument, int typeArgument){
		DataCursorManager dataCursorManager = targetInputComponent.getDataCursorManager();
		Expression focusedExpression = dataCursorManager.getCurrFocusedExpression();
		
		boolean bNeedCheckPowerToToken = false;
		
		PowerToDataToken powerToToken = null;
		
		if (focusedExpression.isPowerToTempBase()){
			bNeedCheckPowerToToken = true;
			powerToToken = (PowerToDataToken) focusedExpression.getParent();
			Expression parentExpression = powerToToken.getParent();
			int indexOfPowerToToken = parentExpression.getTokenList().indexOf(powerToToken);
			dataCursorManager.placeCursorAt(parentExpression, indexOfPowerToToken - 1);
			
			focusedExpression = dataCursorManager.getCurrFocusedExpression();
		}
		
		DataToken token = createToken(id, argument, typeArgument);
		
		focusedExpression.getTokenList().add(dataCursorManager.getCursor_elementIndex() + 1, token);
		
		if (bNeedCheckPowerToToken){
			powerToToken.updateTempBase();
		}
		
		if (token.identifier.contains(FunctionToken.POWER_TO_DATA_TOKEN_ID)){
			PowerToDataToken pt = (PowerToDataToken)token;
			pt.updateTempBase();
		}
		
		int indexOfToken = focusedExpression.getTokenList().indexOf(token);
		if ( indexOfToken < focusedExpression.getTokenList().size() - 1){
			DataToken rightToken = focusedExpression.getTokenList().get(indexOfToken + 1);
			if (rightToken.identifier.contains(FunctionToken.POWER_TO_DATA_TOKEN_ID)){
				((PowerToDataToken)rightToken).updateTempBase();
			}
		}
		
		if (token.type == DataToken.FUNCTION_TOKEN){
			FunctionToken functionToken = (FunctionToken)token;
			
			for (int i = 0; i < functionToken.getNumberExpression(); i++){
				Expression expression = functionToken.getExpression(i);
				dataCursorManager.getExpressionList().add(expression);
			}
			if (functionToken.getExpression(0) != null){
				dataCursorManager.placeCursorAt(functionToken.getExpression(0), -1);
			}else{
				dataCursorManager.placeCursorAt(focusedExpression, dataCursorManager.getCursor_elementIndex() + 1);
			}
		}else{
			dataCursorManager.placeCursorAt(focusedExpression, dataCursorManager.getCursor_elementIndex() + 1);
		}
		
	}
	
	public void moveCursorLeft(){
		DataCursorManager dataCursorManager = targetInputComponent.getDataCursorManager();
		dataCursorManager.moveCursorLeft();
	}
	
	public void moveCursorRight(){
		DataCursorManager dataCursorManager = targetInputComponent.getDataCursorManager();
		dataCursorManager.moveCursorRight();
	}

	public DataInputComponent getTargetInputComponent() {
		return targetInputComponent;
	}

	public void setTargetInputComponent(DataInputComponent targetInputComponent) {
		this.targetInputComponent = targetInputComponent;
	}

	
}
