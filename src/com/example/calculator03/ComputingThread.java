package com.example.calculator03;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;

import computing.DataProcessor;
import computing.EmptyExpressionException;
import computing.RationalRootExpression;
import visualToken.ExpVisualToken;
import visualToken.StringToken;
import visualToken.TextArea;
import visualToken.VisualToken;
import android.util.Log;
import dataToken.DataToken;
import dataToken.Expression;
import dataToken.FlagToken;
import dataToken.NumberToken;
import dataToken.ResultToken;
import dataToken.UnaryOperator;

public class ComputingThread implements Runnable{
	
	private DataManager dataM;
	private DisplayManager DM;
	private DataProcessor DP;
	private Thread thread;
	private boolean running;

	public ComputingThread() {
		// TODO Auto-generated constructor stub
	}
	
	public void setReferences(DisplayManager DM, DataManager dataM){
		this.DM = DM;
		this.dataM = dataM;
		this.DP = dataM.getDataProcesser();
	}

	public void run() {
		// TODO Auto-generated method stub
		while (running){
			if (DM.dataUpdateStatus == DisplayManager.WAITING_FOR_RESULT && dataM.status == DataManager.WAITING_FOR_RESULT){
				
				DataToken result = null;
				
				Expression inputExpression = dataM.getInputComponentToWorkWith().getExpression();
				Expression clonedExpression = inputExpression.clone(null);
				Log.d("","In Computing Thread -----------------------------");
				boolean syntax = DP.controlSyntax(clonedExpression);
				Log.d("","After syntax checking " + clonedExpression.getReportString(dataM.getInputComponentToWorkWith().getDataCursorManager()));
				if (syntax){
					Log.d("","SYNTAX ERROR after controlSyntax");
					result = new FlagToken(null, FlagToken.SYNTAX_ERROR);
				}
				else{
					Expression postfix = DP.infixToPostfix(clonedExpression);
					Log.d("","After postfix " + postfix.getReportString(null));
					DataToken evaluatedToken = null;
					long startTime = System.currentTimeMillis();
					evaluatedToken = DP.evaluatePostfix(postfix);
					long endTime = System.currentTimeMillis();
					Log.d("","TIME TOOK TO EVALUATE: " + (endTime-startTime));
					result = evaluatedToken;
					if (evaluatedToken.identifier.contains(DataToken.FLAG_TOKEN_ID)){
						Log.d("","ERROR while compute: " + evaluatedToken.getReportString(null));
					}
					else {
						Log.d("","PROCESS WENT WELL!: " + evaluatedToken.getReportString(null));
					}
				}
				TextArea ta = makeDisplayResult(prepairDisplayResult(result));
				Expression ex = makeDataResult(result);
				DM.onResultReceived(ta, result);
				dataM.onResultReiceived(ex);
				Log.d("","END Computing in ComputingThread -----------------------------");
			}
		}
	}
	
	public static DataToken prepairDisplayResult(DataToken result){
		if (result.type == DataToken.FLAG_TOKEN) return result.clone(null);
		
		ResultToken resultToken = (ResultToken)result;
		DataToken finalToken = null;
		String resultString = resultToken.getDecimal().toPlainString();
		String[] temp = resultString.split("[.]");
		
		if (temp[0].length() > DataProcessor.MAX_INT_LENGTH){
			finalToken = new FlagToken(null, FlagToken.MATH_ERROR);
		}else if (resultToken.isRRE()){
			finalToken = makeRREResult(resultToken, temp[0]);
		}else if (resultToken.isDecimal() && temp[0].length() > DataProcessor.DEFAULT_RESULT_LENGTH){
			finalToken = makeExpResult(resultToken, temp[0]);
		}else if (resultToken.isDecimal() && temp[0].length() <= DataProcessor.DEFAULT_RESULT_LENGTH){
			finalToken = resultToken.clone(null);
		}else if (resultToken.isDecimal() && temp.length == 2){
			String s = new String();
			BigDecimal tempBD = resultToken.getDecimal().round(new MathContext(DataProcessor.DEFAULT_RESULT_LENGTH, RoundingMode.HALF_UP));
			s = tempBD.toPlainString();
			for (int i = s.length() - 1; i > s.indexOf("."); i--){
				if (s.charAt(i) == '0'){
					s = s.substring(0, i);
				}else{
					break;
				}
			}
			if (s.charAt(s.length() - 1) == '.'){
				s = s.substring(0, s.length() - 1);
			}
			
			ResultToken tempRes = new ResultToken(null, ResultToken.DECIMAL);
			tempRes.setDecimal(new BigDecimal(s));
			finalToken = tempRes;
		}
		if (finalToken.type == DataToken.RESULT_TOKEN){
			ResultToken finalRes = (ResultToken)finalToken;
			String finalResString = finalRes.getDecimal().toPlainString();
			//remove 0 at the end
			if (finalResString.contains(".")){
				String s = new String(finalResString);
				for (int i = s.length() - 1; i > s.indexOf("."); i--){
					if (s.charAt(i) == '0'){
						s = s.substring(0, i);
					}else{
						break;
					}
				}
				finalRes.setDecimal(new BigDecimal(s));
			}
		}
		return finalToken;
	}
	
	private static ResultToken makeExpResult(ResultToken resultToken, String integerString){
		if (resultToken.getDecimal().abs().compareTo(BigDecimal.ONE) >= 0){
			String power_of_10 = new String("1");
			for (int i = 0; i < integerString.length() - 1; i++){
				power_of_10 += "0";
			}
			BigDecimal tempBD = resultToken.getDecimal().divide(new BigDecimal(power_of_10));
			BigDecimal roundedBD = tempBD.round(new MathContext(DataProcessor.DEFAULT_RESULT_LENGTH, RoundingMode.HALF_UP));
			
			String s = roundedBD.toPlainString();
			for (int i = s.length() - 1; i > s.indexOf("."); i--){
				if (s.charAt(i) == '0'){
					s = s.substring(0, i);
				}else{
					break;
				}
			}
			int exp = 0;
			if (integerString.charAt(0) == '-'){
				exp = integerString.length() - 2;
			}else{
				exp = integerString.length() - 1;
			}
			ResultToken res = new ResultToken(null, ResultToken.DECIMAL);
			res.setDecimal(new BigDecimal(s));
			res.setExp_of_10(exp);
			res.setExpEnabled(true);
			return res;
		}else{
			String resultString = resultToken.getDecimal().toPlainString();
			Log.d("","#####################33" + resultString.length());
			String[] temp = resultString.split("[.]");
			if (temp.length == 2){
				//remove 0 at the end
				String s = new String(temp[1]);
				Log.d("",">>>>>>>>>>>>>>>>>>>>>< " + s);
				for (int i = s.length() - 1; i >= 0; i--){
					if (s.charAt(i) == '0'){
						s = s.substring(0, i);
					}else{
						break;
					}
				}
				//count nbr 0 in front
				int count = 0;
				int index = 0;
				String postZero = new String(); //digits after zero
				for (int i = 0; i < s.length(); i++){
					if (s.charAt(i) == '0') count++;
					else{
						index = i;
						break;
					}
				}
				int nbrPostZero = s.length() - count; //nbr digits after zeros.
				for (int i = index; i < s.length(); i++){
					postZero += s.charAt(i);
				}
				if (postZero.length() > DataProcessor.DEFAULT_RESULT_LENGTH){
					BigDecimal tempBD = new BigDecimal(postZero);
					tempBD = tempBD.round(new MathContext(DataProcessor.DEFAULT_RESULT_LENGTH, RoundingMode.HALF_UP));
					postZero = tempBD.toPlainString();
					postZero = postZero.substring(0, DataProcessor.DEFAULT_RESULT_LENGTH);
				}
				int exp = -(count + 1);
				Log.d("","###############################" + postZero);
				Log.d("","" + postZero.charAt(0));
				postZero = postZero.charAt(0) + "." + postZero.substring(1, postZero.length());
				ResultToken res = new ResultToken(null, ResultToken.DECIMAL);
				res.setDecimal(new BigDecimal(postZero));
				res.setExp_of_10(exp);
				res.setExpEnabled(true);
				return res;
			}else{
				ResultToken res = new ResultToken(null, ResultToken.DECIMAL);
				res.setDecimal(BigDecimal.ZERO);
				return res;
			}
		}
	}
	
	private static ResultToken makeRREResult(ResultToken resultToken, String integerPart){
		if (!resultToken.isRRE()) return null;
		if (!resultToken.getRrExpression().isTooLong()) return (ResultToken) resultToken.clone(null);
		
		return makeExpResult(resultToken, integerPart);
	}
	
	public static TextArea makeDisplayResult(DataToken result){
		TextArea textArea = new TextArea(null, false, PaintManager.textPaint);
		if (result.identifier.contains(DataToken.FLAG_TOKEN_ID)){
			FlagToken flag = (FlagToken)result;
			StringToken s = new StringToken(textArea, flag.getErrorString());
			textArea.getTokenList().add(s);
			//s.update_pass_1(true);
		}else{
			ResultToken resultToken = (ResultToken) result;
			if (resultToken.isRRE()){
				RationalRootExpression rre = resultToken.getRrExpression();
				if (rre.denominatorIsOne()){
					textArea.getTokenList().addAll(rre.toVisualTokens(textArea, RationalRootExpression.NUMERATOR));
				}else{
					textArea.getTokenList().add(rre.toFractionToken(textArea));
				}
			}else{
				BigDecimal resBD = resultToken.getDecimal();
				StringToken s = new StringToken(textArea, resBD.toPlainString());
				textArea.getTokenList().add(s);
				if (resultToken.isExpEnabled()){
					ExpVisualToken exp = new ExpVisualToken(textArea, "x10");
					textArea.getTokenList().add(exp);
					StringToken expVal = new StringToken(textArea, resultToken.getExp_of_10() + "");
					textArea.getTokenList().add(expVal);
				}
			
				//s.update_pass_1(true);
			}
		}
		
		return textArea;
	}
	
	public Expression makeDataResult(DataToken result){
		Expression ex = new Expression(null,  false);
		ex.getTokenList().add(result.clone(ex));
		return ex;
	}

	public void onPause(){
		boolean retry = true;
		running = false;
		while (retry) {
			try {
				thread.join();
				retry = false;
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void onResume(){
		running = true;
		thread = new Thread(this);
		thread.start();
	}
}
