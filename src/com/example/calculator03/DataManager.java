package com.example.calculator03;

import computing.DataProcessor;
import android.util.Log;
import android.widget.Button;
import dataMode.BasicDataMode;
import dataMode.DataInputComponent;
import dataToken.DataCursorManager;
import dataToken.DataToken;
import dataToken.Expression;

public class DataManager {
	
	public static final int WAITING_FOR_INPUT = 0;
	public static final int INITIATING_COMPUTING = 1;
	public static final int WAITING_FOR_RESULT = 2;
	
	public static final int BASIC_MODE = 0;
	public static final int HYP_LAYOUT = 1;
	
	public int status;
	public int mode;
	
	private BasicDataMode basicDataMode;
	private DataProcessor dataProcesser;
	private CommonButtonCommands_Data commonButtonCommands_Data;

	public DataManager() {
		// TODO Auto-generated constructor stub
		basicDataMode = new BasicDataMode();
		dataProcesser = new DataProcessor();
		status = WAITING_FOR_INPUT;
		mode = BASIC_MODE;
		commonButtonCommands_Data = new CommonButtonCommands_Data();
	}
	
	public void setReferences(Calculator03 mainActivity){
		basicDataMode.setReferences(mainActivity);
		commonButtonCommands_Data.setReferences(mainActivity);
		dataProcesser.setReferences(mainActivity);
	}
	
	public void onClick(Button b){
		switch (mode) {
		case BASIC_MODE:
			basicDataMode.onClick(b);
			break;
		case HYP_LAYOUT:
			commonButtonCommands_Data.hypOnClick(b.getText().toString());
			break;
		default:
			break;
		}
	}
	
	public DataInputComponent getInputComponentToWorkWith(){
		return basicDataMode.getInputComponentToWorkWith();
	}
	
	public void startComputing(){
		status = WAITING_FOR_RESULT;
	}
	
	public void onResultReiceived(Expression expression){
		basicDataMode.onResultRececived(expression);
		status = WAITING_FOR_INPUT;
	}
	
	public void report(){
		DataInputComponent inputComponent = basicDataMode.getInputComponentToWorkWith();
		DataCursorManager DCM = inputComponent.getDataCursorManager();
		Expression rootEx = inputComponent.getExpression();
		Log.d("",rootEx.getReportString(DCM));
	}

	public DataProcessor getDataProcesser() {
		return dataProcesser;
	}
	
	

}
