package com.example.calculator03;

import android.util.Log;
import dataMode.DataInputComponent;
import dataToken.BinaryOperator;
import dataToken.CombinatoricAndPolRectDataToken;
import dataToken.DataCursorManager;
import dataToken.DataToken;
import dataToken.DigitToken;
import dataToken.FunctionToken;
import dataToken.ParenthesisDataToken;
import dataToken.UnaryOperator;
import visualToken.FractionToken;
import visualToken.StringToken;
import visualToken.TextArea;
import visualToken.VisualToken;

public class CommonButtonCommands_Data {

	private DataManager dataM;
	private ButtonListener buttonListener;
	private ExpressionEditor expressionEditor;

	private boolean bShiftOn;
	public CommonButtonCommands_Data() {
		// TODO Auto-generated constructor stub
		bShiftOn = false;
	}

	public void setReferences(Calculator03 mainActivity) {
		dataM = mainActivity.getDataManager();
		buttonListener = mainActivity.getButtonListener();
		expressionEditor = mainActivity.getExpressionEditor();
	}

	public void onClick(String bText) {
		expressionEditor.setTargetInputComponent(dataM.getInputComponentToWorkWith());
		
		if (dataM.status != DataManager.WAITING_FOR_INPUT) {
			return;
		}
		
		boolean shiftPressed = false;
		// ------------------ Special button section ------------------------
		if (bText.equals("SHIFT")) {
			bShiftOn = !bShiftOn;
			shiftPressed = true;
		} else if (bText.equals("MOVE")) {
			
		} else if (bText.equals("LEFT")) {
			expressionEditor.moveCursorLeft();
		} else if (bText.equals("RIGHT")) {
			expressionEditor.moveCursorRight();
		} else if (bText.equals("MODE")) {

		} else if (bText.equals("M+")) {

		}

		// ------------------------ Big buttons section ---------------------------------
		else if (bText.equals("1")) {
			expressionEditor.addToken(DataToken.DIGIT_TOKEN_ID, "1", -1);
		} else if (bText.equals("2")) {
			expressionEditor.addToken(DataToken.DIGIT_TOKEN_ID, "2", -1);
		} else if (bText.equals("3")) {
			expressionEditor.addToken(DataToken.DIGIT_TOKEN_ID, "3", -1);
		} else if (bText.equals("4")) {
			expressionEditor.addToken(DataToken.DIGIT_TOKEN_ID, "4", -1);
		} else if (bText.equals("5")) {
			expressionEditor.addToken(DataToken.DIGIT_TOKEN_ID, "5", -1);
		} else if (bText.equals("6")) {
			expressionEditor.addToken(DataToken.DIGIT_TOKEN_ID, "6", -1);
		} else if (bText.equals("7")) {
			expressionEditor.addToken(DataToken.DIGIT_TOKEN_ID, "7", -1);
		} else if (bText.equals("8")) {
			expressionEditor.addToken(DataToken.DIGIT_TOKEN_ID, "8", -1);
		} else if (bText.equals("9")) {
			expressionEditor.addToken(DataToken.DIGIT_TOKEN_ID, "9", -1);
		} else if (bText.equals("0")) {
			expressionEditor.addToken(DataToken.DIGIT_TOKEN_ID, "0", -1);
		} else if (bText.equals(".")) {
			expressionEditor.addToken(DataToken.DIGIT_TOKEN_ID, "" + DigitToken.POINT_CHAR, -1);
		} else if (bText.equals("(-)")) {
			expressionEditor.addToken(UnaryOperator.NEGATIVE_SIGN_ID, null, -1);
		} else if (bText.equals("exp")) {
			if (bShiftOn){
				expressionEditor.addToken(BinaryOperator.MODULUS_ID, null, -1);
			}else{
				expressionEditor.addToken(DataToken.DIGIT_TOKEN_ID, DigitToken.EXP + "", -1);
			}
		} else if (bText.equals("+")) {
			if (bShiftOn){
				expressionEditor.addToken(FunctionToken.COMBINATORIC_POL_RECT_ID, null, CombinatoricAndPolRectDataToken.POL);
			}else{
				expressionEditor.addToken(BinaryOperator.ADDITION_ID, null, -1);
			}
		} else if (bText.equals("-")) {
			if (bShiftOn){
				expressionEditor.addToken(FunctionToken.COMBINATORIC_POL_RECT_ID, null, CombinatoricAndPolRectDataToken.RECT);
			}else{
				expressionEditor.addToken(BinaryOperator.SUBTRACTION_ID, null, -1);
			}
		} else if (bText.equals("�")) {
			if (bShiftOn){
				expressionEditor.addToken(FunctionToken.COMBINATORIC_POL_RECT_ID, null, CombinatoricAndPolRectDataToken.PERMUTATION);
			}else{
				expressionEditor.addToken(BinaryOperator.MULTIPLCATION_ID, null, -1);
			}
		} else if (bText.equals("�")) {
			if (bShiftOn){
				expressionEditor.addToken(FunctionToken.COMBINATORIC_POL_RECT_ID, null, CombinatoricAndPolRectDataToken.COMBNATION);
			}else{
				expressionEditor.addToken(BinaryOperator.DIVISION_ID, null, -1);
			}
		} else if (bText.equals("=")) {
			if (bShiftOn){
				expressionEditor.addToken(FunctionToken.ANS_FUNCTION_TOKEN_ID, null, -1);
			}else{
				dataM.startComputing();
			}
		} else if (bText.equals("AC")) {
			expressionEditor.clear();
		} else if (bText.equals("DEL")) {
			expressionEditor.delete(true);
		}

		// -------------------------- Small button section --------------------------------
		else if (bText.equals("Fr")) {
			expressionEditor.addToken(FunctionToken.FRACTION_TOKEN_ID, null, -1);
		} else if (bText.equals("Pt")) {
			if (bShiftOn){
				expressionEditor.addToken(FunctionToken.ROOTH_Nth_TOKEN_ID, null, -1);
			}else{
				expressionEditor.addToken(FunctionToken.POWER_TO_DATA_TOKEN_ID, null, -1);
			}
		} else if (bText.equals("hyp")) {
			if (bShiftOn){
				expressionEditor.addToken(UnaryOperator.NEGATIVE_SIGN_ID, null, -1);
			}else{	
				if (dataM.mode != DataManager.HYP_LAYOUT){
					dataM.mode = DataManager.HYP_LAYOUT;
				}else{
					dataM.mode = DataManager.BASIC_MODE;
				}
			}
		} else if (bText.equals("sin")) {
			if (bShiftOn){
				expressionEditor.addToken(UnaryOperator.INVERSE_SIN_TRIG_FUNCTION_ID, null, -1);
			}else{
				expressionEditor.addToken(UnaryOperator.SINE_TRIG_FUNCTION_ID, null, -1);
			}
		} else if (bText.equals("cos")) {
			if (bShiftOn){
				expressionEditor.addToken(UnaryOperator.INVERSE_COS_TRIG_FUNCTION_ID, null, -1);
			}else{
				expressionEditor.addToken(UnaryOperator.COSINE_TRIG_FUNCTION_ID, null, -1);
			}
		} else if (bText.equals("tan")) {
			if (bShiftOn){
				expressionEditor.addToken(UnaryOperator.INVERSE_TAN_TRIG_FUNCTION_ID, null, -1);
			}else{
				expressionEditor.addToken(UnaryOperator.TAN_TRIG_FUNCTION_ID, null, -1);
			}
		} else if (bText.equals("sqrt")) {
			if (bShiftOn){
				expressionEditor.addToken(FunctionToken.CUBE_ROOT_ID, null, -1);
			}else{
				expressionEditor.addToken(FunctionToken.SQUARE_ROOT_ID, null, -1);
			}
		} else if (bText.equals("square")) {
			if (bShiftOn){
				expressionEditor.addToken(FunctionToken.CUBE_ID, null, -1);
			}else{
				expressionEditor.addToken(FunctionToken.SQUARE_ID, null, -1);
			}
		} else if (bText.equals("(")) {
			expressionEditor.addToken(ParenthesisDataToken.OPEN_ID, null, -1);
		} else if (bText.equals(")")) {
			if (bShiftOn){
				expressionEditor.addToken(FunctionToken.LOGARITHM_CUSTOM_BASE_ID, null, -1);
			}else{
				expressionEditor.addToken(ParenthesisDataToken.CLOSE_ID, null, -1);
			}
		} else if (bText.equals("ln")) {
			if (bShiftOn){
				expressionEditor.addToken(FunctionToken.POWER_TO_FIXED_BASE_ID, "10", -1);
			}else{
				expressionEditor.addToken(FunctionToken.LOGARITHM_FIXED_BASE_ID, "2.718", -1);
			}
		}
		
		if (!shiftPressed) bShiftOn = false;
	}

	public void hypOnClick(String bText){
		expressionEditor.setTargetInputComponent(dataM.getInputComponentToWorkWith());
		
		if (dataM.status != DataManager.WAITING_FOR_INPUT) {
			return;
		}
		
		if (bText.equals("1")){
			expressionEditor.addToken(UnaryOperator.SINH_TRIG_FUNCTION_ID, null, -1);
			dataM.mode = DataManager.BASIC_MODE;
		}else if (bText.equals("2")){
			expressionEditor.addToken(UnaryOperator.COSH_TRIG_FUNCTION_ID, null, -1);
			dataM.mode = DataManager.BASIC_MODE;
		}else if (bText.equals("3")){
			expressionEditor.addToken(UnaryOperator.TANH_TRIG_FUNCTION_ID, null, -1);
			dataM.mode = DataManager.BASIC_MODE;
		}else if (bText.equals("4")){
			expressionEditor.addToken(UnaryOperator.INVERSE_SINH_TRIG_FUNCTION_ID, null, -1);
			dataM.mode = DataManager.BASIC_MODE;
		}else if (bText.equals("5")){
			expressionEditor.addToken(UnaryOperator.INVERSE_COSH_TRIG_FUNCTION_ID, null, -1);
			dataM.mode = DataManager.BASIC_MODE;
		}else if (bText.equals("6")){
			expressionEditor.addToken(UnaryOperator.INVERSE_TANH_TRIG_FUNCTION_ID, null, -1);
			dataM.mode = DataManager.BASIC_MODE;
		}else if (bText.equals("hyp")){
			dataM.mode = DataManager.BASIC_MODE;
		}else if (bText.equals("AC")){
			dataM.mode = DataManager.BASIC_MODE;
		}
	}
}
