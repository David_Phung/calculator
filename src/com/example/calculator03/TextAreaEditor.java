package com.example.calculator03;

import android.util.Log;
import android.view.Display;
import visualToken.CombinatoricAndPolRectToken;
import visualToken.Cube;
import visualToken.CubeRoot;
import visualToken.ExpVisualToken;
import visualToken.FractionToken;
import visualToken.InverseTrigToken;
import visualToken.LogarithmFixedBase;
import visualToken.LogarithmVisualToken;
import visualToken.PowerToFixedBase;
import visualToken.PowerToTempBaseToken;
import visualToken.PowerToVisualToken;
import visualToken.RootToken;
import visualToken.Square;
import visualToken.SquareRoot;
import visualToken.StringToken;
import visualToken.TextArea;
import visualToken.VisualCursorManager;
import visualToken.VisualToken;
import displayLayout.TextComponent;
import displayLayoutOld.TextPart;

public class TextAreaEditor {

	private DisplayManager displayManager;
	private TextComponent targetTextComponent;

	public TextAreaEditor() {
		// TODO Auto-generated constructor stub
	}

	public void setReferences(Calculator03 mainActivity) {
		displayManager = mainActivity.getDisplayManager();
	}

	public void clear() {
		if (targetTextComponent == null)
			return;
		displayManager.dataUpdateStatus = DisplayManager.UPDATING_DATA;
		targetTextComponent.clear();
		displayManager.dataUpdateStatus = DisplayManager.WAITING_FOR_INPUT;
		displayManager.performPostTextAreaEdittingTasks();
	}

	public void delete(boolean bCheckTokenToTheRightRequested) {
		if (targetTextComponent == null)
			return;
		displayManager.dataUpdateStatus = DisplayManager.UPDATING_DATA;
		VisualCursorManager VCM = targetTextComponent.getVCM();
		TextArea focusedTA = VCM.getFocusedTextArea();

		boolean bNeedCheck_IfTokenToTheRight_IsPowerToToken = false;
		TextArea TAtoCallUpdate = null;
		
		// -----------------
		// if index == -1, at beginning of a TA
		if (VCM.getCursor().getElementIndex() == -1) {
			// if the TA is root
			if (focusedTA.isRoot()) {
				return;
			} else { // if inside some token
				VisualToken parentToken = focusedTA.getParent();
				TextArea TA_of_parentToken = parentToken.getParent();
				// if cursor is inside the FIRST TA of that token
				if (parentToken.getTextArea(0) == focusedTA) {
					int index_of_parentToken = TA_of_parentToken.getTokenList()
							.indexOf(parentToken);
					TA_of_parentToken.getTokenList().remove(parentToken);
					TA_of_parentToken.update_pass_1(true);
					VCM.placeCursorAt(TA_of_parentToken,
							index_of_parentToken - 1);
					for (int i = 0; i < parentToken.getNumberTextArea(); i++){
						VCM.getTAList().remove(parentToken.getTextArea(i));
					}
					
					bNeedCheck_IfTokenToTheRight_IsPowerToToken = true;
					TAtoCallUpdate = TA_of_parentToken;
				} else { // if cursor is NOT inside the FIRST TA of that token
					VCM.moveCursorLeft();
				}
			}
		} else { // if index not -1
			VisualToken focusedToken = VCM.getFocusedToken();

			// if cursor point at token that contains TAs, move cursor into the token
			if (focusedToken.getTextArea(0) != null) {
				VCM.moveCursorLeft();
			} else { // if cursor point at a token that does NOT contain TAs, ex
						// StringToken, simply remove it
						// then we check whether the token to the right is power to token and then update_pass_1
				int index_of_focusedToken = focusedTA.getTokenList().indexOf(
						focusedToken);
				focusedTA.getTokenList().remove(focusedToken);
				VCM.placeCursorAt(focusedTA, index_of_focusedToken - 1);

				bNeedCheck_IfTokenToTheRight_IsPowerToToken = true;
				TAtoCallUpdate = focusedTA;
			}
		}
		
		if (bNeedCheck_IfTokenToTheRight_IsPowerToToken || bCheckTokenToTheRightRequested){
			focusedTA = VCM.getFocusedTextArea();
			int currentIndex = VCM.getCursor().getElementIndex();
			if ( currentIndex < focusedTA.getTokenList().size() - 1){
				VisualToken tokenToTheRight = focusedTA.getTokenList().get(currentIndex + 1);
				if (tokenToTheRight.type == VisualToken.POWER_TO_TOKEN){
					tokenToTheRight.update_pass_1(true);
				}else if (TAtoCallUpdate != null){
					TAtoCallUpdate.update_pass_1(true);
				}
			}else if (TAtoCallUpdate != null){
				TAtoCallUpdate.update_pass_1(true);
			}
		}else if (TAtoCallUpdate != null){
			TAtoCallUpdate.update_pass_1(true);
		}
		
		displayManager.dataUpdateStatus = DisplayManager.WAITING_FOR_INPUT;
		displayManager.performPostTextAreaEdittingTasks();
	}

	public void addToken(int type, String argument) {
		if (targetTextComponent == null)
			return;
		
		displayManager.dataUpdateStatus = DisplayManager.UPDATING_DATA;
		
		VisualCursorManager VCM = targetTextComponent.getVCM();
		TextArea focusedTA = VCM.getFocusedTextArea();
		VisualToken token = null;
		
		if (focusedTA.isPowerToTokenTempBase()){
			focusedTA.getTokenList().clear();
			VCM.placeCursorAt(focusedTA, -1);
			
			VisualToken tempTAs_parent = focusedTA.getParent();
			TextArea parentOf_tempTAs_parent = tempTAs_parent.getParent();
			int indexOf_tempTAs_parent = parentOf_tempTAs_parent.getTokenList().indexOf(tempTAs_parent);
			VCM.placeCursorAt(parentOf_tempTAs_parent, indexOf_tempTAs_parent - 1);
			
			focusedTA = VCM.getFocusedTextArea();
		}
		
		// create new token
		switch (type) {
		case VisualToken.STRING_TOKEN:
			token = new StringToken(focusedTA, argument);
			break;
		case VisualToken.FRACTION_TOKEN:
			token = new FractionToken(focusedTA);
			break;
		case VisualToken.ROOT_CUSTOM_DEGREE_TOKEN:
			token = new RootToken(focusedTA);
			break;
		case VisualToken.LOGARITHM_CUSTOM_BASE:
			token = new LogarithmVisualToken(focusedTA, argument);
			break;
		case VisualToken.POWER_TO_TOKEN:
			token = new PowerToVisualToken(focusedTA);
			break;
		case VisualToken.TEMP_BASE_FOR_POWER_TO_TOKEN:
			token = new PowerToTempBaseToken(focusedTA);
			break;
		case VisualToken.SQUARE_ROOT:
			token = new SquareRoot(focusedTA);
			break;
		case VisualToken.CUBE_ROOT:
			token = new CubeRoot(focusedTA);
			break;
		case VisualToken.SQUARE:
			token = new Square(focusedTA);
			break;
		case VisualToken.CUBE:
			token = new Cube(focusedTA);
			break;
		case VisualToken.INVERSE_TRIG:
			token = new InverseTrigToken(focusedTA, argument);
			break;
		case VisualToken.COMBINATORIC_POL_RECT_TOKEN:
			token = new CombinatoricAndPolRectToken(focusedTA, argument);
			break;
		case VisualToken.POWERTO_FIXEDBASE:
			token = new PowerToFixedBase(focusedTA, argument);
			break;
		case VisualToken.LOGARITHM_FIXED_BASE:
			token = new LogarithmFixedBase(focusedTA, argument);
			break;
		case VisualToken.EXP_TOKEN:
			token = new ExpVisualToken(focusedTA, argument);
			break;
		default:

			break;
		}
		if (token == null)
			return;
		
		/*if (!focusedTA.isRoot() && focusedTA.getParent().type == VisualToken.TEMP_BASE_FOR_POWER_TO_TOKEN){
			delete(false);
			focusedTA.getTokenList().add(VCM.getCursor().getElementIndex() + 1,
					token);
			//check token the right
			focusedTA = VCM.getFocusedTextArea();
			int currentIndex = VCM.getCursor().getElementIndex();
			if ( currentIndex < focusedTA.getTokenList().size() - 1){
				VisualToken tokenToTheRight = focusedTA.getTokenList().get(currentIndex + 1);
				if (tokenToTheRight.type == VisualToken.POWER_TO_TOKEN){
					tokenToTheRight.update_pass_1(false);
				}
			}
			token.update_pass_1(true);
		}*/

		focusedTA.getTokenList().add(VCM.getCursor().getElementIndex() + 1,
				token);
		token.update_pass_1(true);
		
		for (int i = 0; i < token.getNumberTextArea(); i++) {
			VCM.getTAList().add(token.getTextArea(i));
		}

		if (token.getNumberTextArea() == 0) {
			VCM.placeCursorAt(focusedTA, VCM.getCursor().getElementIndex() + 1);
		} else {
			VCM.placeCursorAt(token.getTextArea(0), -1);
		}
		
		displayManager.dataUpdateStatus = DisplayManager.WAITING_FOR_INPUT;
		displayManager.performPostTextAreaEdittingTasks();
	}

	public void moveCursorLeft() {
		if (targetTextComponent == null)
			return;
		displayManager.dataUpdateStatus = DisplayManager.UPDATING_DATA;
		targetTextComponent.getVCM().moveCursorLeft();
		displayManager.dataUpdateStatus = DisplayManager.WAITING_FOR_INPUT;
	}

	public void moveCursorRight() {
		if (targetTextComponent == null)
			return;
		displayManager.dataUpdateStatus = DisplayManager.UPDATING_DATA;
		targetTextComponent.getVCM().moveCursorRight();
		displayManager.dataUpdateStatus = DisplayManager.WAITING_FOR_INPUT;
	}

	public TextComponent getTargetPart() {
		return targetTextComponent;
	}

	public void setTargetPart(TextComponent targetPart) {
		this.targetTextComponent = targetPart;
	}

}
