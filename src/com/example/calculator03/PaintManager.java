package com.example.calculator03;

import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;

public class PaintManager {
	
	public static Paint textPaint = new Paint();
	
	public static Paint mainPaddingPaint = new Paint();
	
	public static Paint paddingPaint = new Paint();
	
	public static Paint backgroundPaint = new Paint();
	
	public static Paint cursorPaint = new Paint();
	
	public static int colorArray[] = {Color.CYAN, Color.DKGRAY, Color.GRAY, Color.LTGRAY, Color.MAGENTA};
	public static int colorCount = 0;

	public PaintManager() {
		// TODO Auto-generated constructor stub
	}
	
	public static void initPaints(){
		mainPaddingPaint.setStyle(Style.FILL_AND_STROKE);
		mainPaddingPaint.setColor(Color.TRANSPARENT);
		paddingPaint.setStyle(Style.FILL_AND_STROKE);
		paddingPaint.setColor(Color.BLACK);
		backgroundPaint.setStyle(Style.FILL);
		backgroundPaint.setColor(Color.parseColor("#188962"));
		//backgroundPaint.setColor(Color.TRANSPARENT);
		textPaint.setStyle(Style.FILL);
		textPaint.setColor(Color.parseColor("#03053f"));
		textPaint.setAntiAlias(true);
		cursorPaint.setStyle(Style.FILL);
		cursorPaint.setColor(Color.parseColor("#03053f"));
		cursorPaint.setAntiAlias(true);
	}
	
	public static int getColor(int nbrColors){
		int n = Math.min(nbrColors, colorArray.length);
		colorCount %= n;
		return colorArray[colorCount++];
	}
	
	

}
