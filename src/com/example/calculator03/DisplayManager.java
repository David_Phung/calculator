package com.example.calculator03;

import android.graphics.Bitmap;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.widget.Button;
import dataToken.DataToken;
import displayLayout.Container;
import displayLayout.DisplayLayoutGenerator;
import displayLayout.TextComponent;
import displayLayoutOld.Part;
import displayLayoutOld.TextPart;
import visualMode.BasicVisualMode;
import visualToken.TextArea;

public class DisplayManager {
	public static final int WAITING_FOR_INPUT = 0;
	public static final int INITIATING_COMPUTING = 1;
	public static final int WAITING_FOR_RESULT = 2;
	public static final int UPDATING_DATA = 3;
	
	public static final int BASIC_MODE = 0;
	public static final int HYP_LAYOUT = 1;
	
	public int dataUpdateStatus;
	public int mode;
	
	private DisplayLayoutGenerator DLG;
	private BasicVisualMode basicMode;
	private Container hypLayout;
	
	private Rect displayArea;
	private CommonButtonCommands_Visual commonButtonCommands_Visual;
	public DisplayManager() {
		// TODO Auto-generated constructor stub
		DLG = new DisplayLayoutGenerator();
		basicMode = new BasicVisualMode(DLG.generateBasicModeLayout());
		dataUpdateStatus = WAITING_FOR_INPUT;
		
		hypLayout = DLG.generateHypLayout();
		
		mode = BASIC_MODE;
		
		displayArea = new Rect();
		commonButtonCommands_Visual = new CommonButtonCommands_Visual();
	}
	
	public void setReferences(Calculator03 mainActivity){
		basicMode.setReferences(mainActivity);
		commonButtonCommands_Visual.setReferences(mainActivity);
	}
	
	public void updateDisplayArea(Rect displayArea){
		this.displayArea = new Rect(displayArea);
		switch (mode) {
		case BASIC_MODE:
			basicMode.updateDisplayArea(displayArea);
			break;
		case HYP_LAYOUT:
			if (hypLayout.getWidth() != displayArea.width() && hypLayout.getHeight() != displayArea.height()){
				hypLayout.updateLayout(displayArea);
			}
			break;
		default:
			break;
		}
	}
	
	public Bitmap getDrawnBitmap(){
		Bitmap bitmap = null;
		if (dataUpdateStatus != WAITING_FOR_INPUT) return null;
		switch (mode) {
		case BASIC_MODE:
			bitmap = basicMode.getDrawnBitmap();
			break;
		case HYP_LAYOUT:
			bitmap = hypLayout.getDrawnBitmap();
			break;

		default:
			break;
		}
		return bitmap;
	}
	
	public void switchMode(int modeToSwitchTo){
		switch (modeToSwitchTo) {
		case HYP_LAYOUT:
			hypLayout.updateLayout(displayArea);
			mode = HYP_LAYOUT;
			break;

		default:
			break;
		};
	}
	
	/**
	 * 
	 * <p><b><i>   </i></b></p>
	 * return 1 if the command was executed and consumed, 0 if it was not executed but was consumed.
	 * -1 if it was not executed and not consumed. -1 means that the thread that calls this command should continue calling this command 
	 * until 0 or 1 is returned.
	 * @param b
	 * @return
	 */
	public int onClick(Button b){
		int i = 0;
		switch (mode) {
		case BASIC_MODE:
			i = basicMode.onClick(b);
			break;
		case HYP_LAYOUT:
			commonButtonCommands_Visual.hypOnClick(b.getText().toString());
			break;
		default:
			break;
		};
		return i;
	}
	
	public int onTouch(PointF p){
		if (displayArea.contains((int)p.x, (int)p.y)){
			return basicMode.onTouch(p);
		}
		return -1;
	}
	
/*	public void test(){
		basicMode.test();
	}*/
	
	public void performPostTextAreaEdittingTasks(){
		basicMode.alignTextAreas();
	}
	
	public void startComputing(){
		dataUpdateStatus = WAITING_FOR_RESULT;
	}
	
	public void onResultReceived(TextArea result, DataToken resultDataToken){
		basicMode.onResultReceived(result, resultDataToken);
		dataUpdateStatus = WAITING_FOR_INPUT;
	}
	
	public TextComponent getInputTextComponentToWorkWith(){
		return basicMode.getInputTextComponentToWorkWith();
	}

}
