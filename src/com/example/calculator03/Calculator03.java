package com.example.calculator03;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.RoundingMode;

import computing.BigDecimalFunctions;
import computing.DataProcessor;
import computing.RationalNumber;
import android.os.Bundle;
import android.app.Activity;
import android.text.Layout;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.ViewTreeObserver;

public class Calculator03 extends Activity {
	
	private MySurfaceView mySurfaceView;
	private DisplayManager displayManager;
	private DataManager dataManager;
	private AppLayoutGenerator appLayoutGenerator;
	private ButtonListener buttonListener;
	private TextAreaEditor TAEditor;
	private ExpressionEditor expressionEditor;
	private ComputingThread computingThread;

	private BufferedReader br_primes_100_000;
	
	private View mainLayout;
	private boolean textSizeInited;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        displayManager = new DisplayManager();
        dataManager = new DataManager();
        appLayoutGenerator = new AppLayoutGenerator(this);
        buttonListener = new ButtonListener();
        TAEditor = new TextAreaEditor();
        expressionEditor = new ExpressionEditor();
        computingThread = new ComputingThread();
        PaintManager.initPaints();
        
        mainLayout = appLayoutGenerator.generateLayout_usingTableLayout();
        appLayoutGenerator.setListener(buttonListener);
        setContentView(mainLayout);
        
        textSizeInited = false;
        mainLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
			
			public void onGlobalLayout() {
				// TODO Auto-generated method stub
				if (textSizeInited == false){
					setTextSizesForLayout();
					textSizeInited = true;
				}
			}
		});
        
        loadFiles();
        
        
        displayManager.setReferences(this);
        dataManager.setReferences(this);
        mySurfaceView = appLayoutGenerator.getDisplayView();
        mySurfaceView.setReferences(this);
        buttonListener.setReferences(this);
        TAEditor.setReferences(this);
        computingThread.setReferences(displayManager, dataManager);
        
       // runThread();
    }
    
    private void setTextSizesForLayout(){
    	for (MyButton b:appLayoutGenerator.getBigButtonList()){
    		int h = b.getHeight();
    		b.setTextSize(0.5f * h);
    	}
    	
    	for (MyButton b:appLayoutGenerator.getSmallButtonList()){
    		int h = b.getHeight();
    		b.setTextSize(0.35f * h);
    	}
    	
    	for (MyTextView tv:appLayoutGenerator.getBigTextViewList()){
    		int h = tv.getHeight();
    		tv.setTextSize(0.5f * h);
    	}
    	
    	for (MyTextView tv:appLayoutGenerator.getSmallTextViewList()){
    		int h = tv.getHeight();
    		tv.setTextSize(0.5f * h);
    	}
    }
    
    private void loadFiles(){
    	InputStreamReader is = null;
        try {
			is = new InputStreamReader(getAssets().open("primes_100_000.txt"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        br_primes_100_000 = new BufferedReader(is);
    }

    private void runThread() {

        new Thread() {
            public void run() {
                while (true) {
                    try {
                        runOnUiThread(new Runnable() {
							public void run() {
								// TODO Auto-generated method stub
								if (appLayoutGenerator.mButtonPressed){
									appLayoutGenerator.toggleUpDownLayouts();
									appLayoutGenerator.mButtonPressed = false;
								}else if (appLayoutGenerator.upDownLayoutOn && appLayoutGenerator.nonDirectionButtonPressed){
									appLayoutGenerator.disableUpDownLayouts();
									appLayoutGenerator.nonDirectionButtonPressed = false;
								}
							}
                           
                        });
                        Thread.sleep(300);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.start();
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        appLayoutGenerator.printSizes();
        return true;
    }
    
    @Override
    protected void onPause() {
    	// TODO Auto-generated method stub
    	super.onPause();
    	Log.d("","ON PAUSE!!");
    	buttonListener.onPause();
    	computingThread.onPause();
    }
    
    @Override
    protected void onResume() {
    	// TODO Auto-generated method stub
    	super.onResume();
    	Log.d("","ON RESUME!!");
    	buttonListener.onResume();
    	computingThread.onResume();
    }
    
	public MySurfaceView getMySurfaceView() {
		return mySurfaceView;
	}

	public DisplayManager getDisplayManager() {
		return displayManager;
	}

	public AppLayoutGenerator getAppLayoutGenerator() {
		return appLayoutGenerator;
	}

	public ButtonListener getButtonListener() {
		return buttonListener;
	}

	public TextAreaEditor getTAEditor() {
		return TAEditor;
	}

	public DataManager getDataManager() {
		return dataManager;
	}

	public void setDataManager(DataManager dataManager) {
		this.dataManager = dataManager;
	}

	public ExpressionEditor getExpressionEditor() {
		return expressionEditor;
	}

	public void setExpressionEditor(ExpressionEditor expressionEditor) {
		this.expressionEditor = expressionEditor;
	}

	public void setAppLayoutGenerator(AppLayoutGenerator appLayoutGenerator) {
		this.appLayoutGenerator = appLayoutGenerator;
	}

	public BufferedReader getBr_primes_100_000() {
		return br_primes_100_000;
	}
    
    
    
    
}
