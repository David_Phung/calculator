package com.example.calculator03;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.util.AttributeSet;
import android.widget.Button;

public class ArrowButton extends Button {

	public ArrowButton(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		setBackgroundResource(R.drawable.round_arrow_button);
	}

	public ArrowButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		setBackgroundResource(R.drawable.round_arrow_button);
	}

	public ArrowButton(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
		setBackgroundResource(R.drawable.round_arrow_button);
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		super.onDraw(canvas);
	}

}
