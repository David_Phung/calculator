package com.example.calculator03;

import displayLayoutOld.DisplayLayoutGenerator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class MySurfaceView extends SurfaceView implements Runnable, SurfaceHolder.Callback{
	
	public int viewWidth = -1;
	public int viewHeight = -1;

	Thread thread ;
	SurfaceHolder surfaceHolder;
	volatile boolean running ;
	
	private PointF lastMouseLocation;
	
	private Bitmap scaledBackgroundBitmap;
	private Bitmap background;

	private int count = 0;
	
	private DisplayManager displayManager;
	private Calculator03 mainActivity;
	
	DisplayLayoutGenerator DLG = new DisplayLayoutGenerator();
	
	public MySurfaceView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		thread = null;
		running = false;
		surfaceHolder = getHolder();
		surfaceHolder.addCallback(this);
		
		lastMouseLocation = new PointF();
	}

	public MySurfaceView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		thread = null;
		running = false;
		surfaceHolder = getHolder();
		surfaceHolder.addCallback(this);
		
		lastMouseLocation = new PointF();

	}
	
	public void setReferences(Calculator03 mainActivity){
		this.mainActivity = mainActivity;
	}
	
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		// TODO Auto-generated method stub
		
	}

	public void surfaceCreated(SurfaceHolder holder) {
		// TODO Auto-generated method stub   
		running = true;
		thread = new Thread(this);
		thread.start();
	}

	public void surfaceDestroyed(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		boolean retry = true;
		running = false;
		while (retry) {
			try {
				thread.join();
				retry = false;
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		// TODO Auto-generated method stub
		if (displayManager == null) return true;
		if (event.getAction() == MotionEvent.ACTION_DOWN) {

		}
		if (event.getAction() == MotionEvent.ACTION_UP) {
			displayManager.onTouch(new PointF(event.getX(), event.getY()));
		}
		if (event.getAction() == MotionEvent.ACTION_MOVE) {
			

		}
		lastMouseLocation = new PointF(event.getX(), event.getY());
		this.invalidate();
		return true;
	}
	
	public void run() {
		// TODO Auto-generated method stub
		while (running) {
			if (surfaceHolder.getSurface().isValid()) {
				Canvas canvas = surfaceHolder.lockCanvas();

				// Get size of the view
				int currentViewWidth = surfaceHolder.getSurfaceFrame().width();
				int currentViewHeight = surfaceHolder.getSurfaceFrame().height();


				
				if (background == null){
					background = BitmapFactory.decodeResource(getResources(), R.drawable.screen_test_02);
				}
				
				if (scaledBackgroundBitmap == null || currentViewWidth != viewWidth || currentViewHeight != viewHeight){
				    float scaleHeight = (float)background.getHeight()/(float)currentViewHeight;
				    float scaleWidth = (float)background.getWidth()/(float)currentViewWidth;
				    int newWidth = Math.round(background.getWidth()/scaleWidth);
				    int newHeight = Math.round(background.getHeight()/scaleHeight);
				    scaledBackgroundBitmap = Bitmap.createScaledBitmap(background, newWidth, newHeight, true);
				}
				canvas.drawBitmap(scaledBackgroundBitmap, 0, 0, null); // draw the background
				//canvas.drawBitmap(background, 0, 0, null);
				
			/*	if (count == 0){
				DLG.generateTestLayout();
				DLG.computeMinimumsize();
				DLG.placeTestLayout(new RectF(50,50,450,650));
				Log.d("","In MysurfaceView, run() " + DLG.getTestBitmap() +"");
				count++;
				}
				canvas.drawBitmap(DLG.getTestBitmap(), 50,50, new Paint()); */		
				viewWidth = currentViewWidth;
				viewHeight = currentViewHeight;
				
				if (displayManager == null){
					displayManager = mainActivity.getDisplayManager();
				}else{
					displayManager.updateDisplayArea(new Rect(0,0,viewWidth,viewHeight));
					Bitmap bitmapToDraw = displayManager.getDrawnBitmap();
					if (bitmapToDraw != null){
						canvas.drawBitmap(bitmapToDraw, 0, 0 , new Paint());
					}
				}
				
				try {
					Thread.sleep(5);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				surfaceHolder.unlockCanvasAndPost(canvas);
			}
		}
	}

}
