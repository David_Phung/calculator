package dataMode;

import dataToken.DataCursorManager;
import dataToken.Expression;

public class DataInputComponent {
	
	private Expression expression;
	private DataCursorManager dataCursorManager;

	public DataInputComponent() {
		// TODO Auto-generated constructor stub
		expression = new Expression(null, true);
		dataCursorManager = new DataCursorManager();
		dataCursorManager.addRootExpression(expression);
	}

	public Expression getExpression() {
		return expression;
	}

	public void setExpression(Expression expression) {
		this.expression = expression;
	}

	public DataCursorManager getDataCursorManager() {
		return dataCursorManager;
	}

	public void setDataCursorManager(DataCursorManager dataCursorManager) {
		this.dataCursorManager = dataCursorManager;
	}
	
	

}
