package dataMode;

import com.example.calculator03.Calculator03;
import com.example.calculator03.MyButton;

import dataToken.DataToken;
import dataToken.Expression;
import android.widget.Button;

public class BasicDataMode extends DataMode {
	

	public BasicDataMode() {
		// TODO Auto-generated constructor stub
		super();
	}
	
	public void setReferences(Calculator03 mainActivity){
		super.setReferences(mainActivity);
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onFocused() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onClick(Button b) {
		// TODO Auto-generated method stub
		MyButton mb = (MyButton)b;
		commonButtonCommands_Data.onClick((String) mb.getNameTag());
	}

	@Override
	public void onResultRececived(Expression expression) {
		// TODO Auto-generated method stub

	}

	@Override
	public DataInputComponent getInputComponentToWorkWith() {
		// TODO Auto-generated method stub
		return inputComponent;
	}

}
