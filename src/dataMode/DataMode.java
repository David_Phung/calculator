package dataMode;

import android.widget.Button;

import com.example.calculator03.Calculator03;
import com.example.calculator03.CommonButtonCommands_Data;

import dataToken.Expression;

public abstract class DataMode {

	protected CommonButtonCommands_Data commonButtonCommands_Data;
	protected DataInputComponent inputComponent;
	
	public DataMode() {
		// TODO Auto-generated constructor stub
		commonButtonCommands_Data = new CommonButtonCommands_Data();
		inputComponent = new DataInputComponent();
	}
	
	public void setReferences(Calculator03 mainActivity){
		commonButtonCommands_Data.setReferences(mainActivity);
	}
	
	public abstract void onDestroy();
	
	public abstract void onFocused();
	
	public abstract void onClick(Button b);
	
	public abstract void onResultRececived(Expression expression);
	
	public abstract DataInputComponent getInputComponentToWorkWith();

	
}
