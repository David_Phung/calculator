package visualToken;

public class Cube extends PowerToVisualToken{

	public Cube(TextArea parent) {
		super(parent);
		StringToken token = new StringToken(exponent, "3");
		exponent.getTokenList().add(token);
		token.update_pass_1(false);
		exponent.update_pass_1(false);
	}

	@Override
	public int getNumberTextArea() {
		// TODO Auto-generated method stub
		if (tokenBefore == null) return 1;
		return 0;
	}

	@Override
	public TextArea getTextArea(int index) {
		// TODO Auto-generated method stub
		if (tokenBefore == null && index == 0) return tempBase;
		return null;
	}

	@Override
	public int getIndexOfTextArea(TextArea textArea) {
		// TODO Auto-generated method stub
		if (tokenBefore == null && textArea == tempBase) return 0;
		return -1;
	}
}
