package visualToken;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;

public class StringToken extends VisualToken {
	
	public static final float upper_lower_ratio = 3f;
	
	private String string;
	
	protected boolean bForceTextSizeSmaller;

	public StringToken(TextArea parent, String string) {
		super(STRING_TOKEN, parent);
		// TODO Auto-generated constructor stub
		this.string = string;
		WIDTH = calculateWidth();
		HEIGHT = calculateHeight();

		upperHeight = (int) Math.floor(upper_lower_ratio
				/ (upper_lower_ratio + 1.0f) * HEIGHT);
		lowerHeight = HEIGHT - upperHeight;
		
		bForceTextSizeSmaller = false;
	}
	
	// Update helpers
	private int calculateHeight() {
		// TODO Auto-generated method stub
		//The code in the comment is used if u want to return the real height
		//of the string token, but this might cause the base line to jump up and down
		//when adding new string tokens.
		
		/*Paint paint = new Paint(parent.getPaint());
		paint.setTextSize(parent.getTextSize());
		Rect bounds = new Rect();
		paint.getTextBounds(content, 0, content.length(), bounds);
		return bounds.height(); */
		if (bForceTextSizeSmaller) return (int) (TextArea.TEXT_SIZE_SMALLER_RATIO * parent.getTextSize());
		return parent.getTextSize();
	}

	private int calculateWidth() {
		// TODO Auto-generated method stub
		Paint paint = new Paint(parent.getPaint());
		if (bForceTextSizeSmaller){
			paint.setTextSize(TextArea.TEXT_SIZE_SMALLER_RATIO * parent.getTextSize());
		}
		else{
			paint.setTextSize(parent.getTextSize());
		}
		Rect bounds = new Rect();
		paint.getTextBounds(string, 0, string.length(), bounds);
		return bounds.width();
	}

	@Override
	public void updateWidthHeight() {
		// TODO Auto-generated method stub
		WIDTH = calculateWidth();
		HEIGHT = calculateHeight();
	}

	@Override
	public void updateUpperLowerHeight() {
		// TODO Auto-generated method stub
		upperHeight = (int) Math.floor(upper_lower_ratio
				/ (upper_lower_ratio + 1.0f) * HEIGHT);
		lowerHeight = HEIGHT - upperHeight;
	}

	@Override
	public void update_pass_1(boolean bCallParent) {
		// TODO Auto-generated method stub
		updateWidthHeight();
		updateUpperLowerHeight();
		checkTokenToTheRight_ForPowerToToken();
		if (bCallParent){
			parent.update_pass_1(bCallParent);
		}
	}

	@Override
	public void update_pass_2() {
		// TODO Auto-generated method stub

	}

	@Override
	public void update_pass_1_chain_down() {
		// TODO Auto-generated method stub
		update_pass_1(false);
	}
	
	@Override
	public void updateOnParentTextSizeChanged() {
		// TODO Auto-generated method stub
		updateWidthHeight();
		updateUpperLowerHeight();
	}

	@Override
	public void updateOnParentPaintChanged() {
		// TODO Auto-generated method stub
		WIDTH = calculateWidth();
		HEIGHT = calculateHeight();
	}

	@Override
	public int getNumberTextArea() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public TextArea getTextArea(int index) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getIndexOfTextArea(TextArea textArea) {
		// TODO Auto-generated method stub
		return -1;
	}

	@Override
	public void drawSelf(Canvas canvas) {
		// TODO Auto-generated method stub
		if (isVisible == false) {
			return;
		}
		
		Paint paint = new Paint(parent.getPaint());
		if (bForceTextSizeSmaller){
			paint.setTextSize(TextArea.TEXT_SIZE_SMALLER_RATIO * parent.getTextSize());
		}
		else{
			paint.setTextSize(parent.getTextSize());
		}

		Rect bounds = new Rect();
		paint.getTextBounds(string, 0, string.length(), bounds);
		int width = bounds.width();

		String string2 = string + string;
		Rect bounds2 = new Rect();
		paint.getTextBounds(string2, 0, string2.length(), bounds2);
		int width2 = bounds2.width();

		int shift_distance = (width2 - 2 * width) / 2;
		canvas.drawText(string, location.x - shift_distance,
				location.y + HEIGHT, paint);
	}

	public String getString() {
		return string;
	}

	public void setString(String string) {
		this.string = string;
	}
	
	

}
