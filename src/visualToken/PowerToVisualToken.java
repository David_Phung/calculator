package visualToken;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PointF;

public class PowerToVisualToken extends VisualToken {
	
	//The ratio between the height which lie under the top of the base expression and 
	//the base's text size
	public static final float HEIGHT_UNDER_BASE_TOP_OVER_TEXT_SIZE_RATIO = 1/3f;
	public static final float HEIGHT_UNDER_BASE_TOP_OVER_TEXT_SIZE_RATIO_STRING = 1/2f;
	
	protected TextArea exponent, tempBase;
	private int heightUnderBaseTop;
	protected VisualToken tokenBefore;
	private int defaultBaseUpperHeight;

	public PowerToVisualToken(TextArea parent) {
		super(POWER_TO_TOKEN, parent);
		// TODO Auto-generated constructor stub
		exponent = new TextArea(this, true, parent.getPaint());
		
		if (parent.getTextSize() == TextArea.TEXT_SIZE_NORMAL){
			tempBase = new TextArea(this, false, parent.getPaint());
		}else{
			tempBase = new TextArea(this, true, parent.getPaint());
		}
		tempBase.setPowerToTokenTempBase(true);
		
		heightUnderBaseTop = (int) (HEIGHT_UNDER_BASE_TOP_OVER_TEXT_SIZE_RATIO * parent.getTextSize());
		defaultBaseUpperHeight =  (int) Math.floor(StringToken.upper_lower_ratio
				/ (StringToken.upper_lower_ratio + 1.0f) * parent.getTextSize());
		
		//initialize inherited data		
		upperHeight = defaultBaseUpperHeight + exponent.getHEIGHT() - heightUnderBaseTop;
		lowerHeight = 0;
		
		WIDTH = exponent.getWIDTH();
		HEIGHT = upperHeight + lowerHeight;
	}
	
	/**
	 * 
	 * <p><b><i> public void findBase()  </i></b></p>
	 * find the token that stands before this token. That token will be considered the base and will be used to compute dimensions and locations.
	 * This must be called after this token has been added to the parent text area.
	 */
	public VisualToken findBase(){
		int index = parent.getTokenList().indexOf(this);
		if (index <= 0){
			return null;
		}
		return parent.getTokenList().get(index - 1);
	}

	@Override
	public void updateWidthHeight() {
		// TODO Auto-generated method stub
		if (tokenBefore != null){
			WIDTH = exponent.getWIDTH();
		}else {
			WIDTH = tempBase.getWIDTH() + parent.getSpace() + exponent.getWIDTH();
		}
		HEIGHT = upperHeight + lowerHeight;
	}

	@Override
	public void updateUpperLowerHeight() {
		// TODO Auto-generated method stub
		heightUnderBaseTop = (int) (HEIGHT_UNDER_BASE_TOP_OVER_TEXT_SIZE_RATIO * parent.getTextSize());
		if (tokenBefore != null){
			if (tokenBefore.type == VisualToken.STRING_TOKEN){
				heightUnderBaseTop = (int) (HEIGHT_UNDER_BASE_TOP_OVER_TEXT_SIZE_RATIO_STRING * parent.getTextSize());
			}
			upperHeight = tokenBefore.getUpperHeight() + exponent.getHEIGHT() - heightUnderBaseTop;
			lowerHeight = 0;
		}else{
			upperHeight = tempBase.getUpperHeight() + exponent.getHEIGHT() - heightUnderBaseTop;
			lowerHeight = tempBase.getLowerHeight();
		}
	}

	@Override
	public void update_pass_1(boolean bCallParent) {
		// TODO Auto-generated method stub
		tokenBefore = findBase();
		updateUpperLowerHeight();
		updateWidthHeight();
		checkTokenToTheRight_ForPowerToToken();
		if (bCallParent) parent.update_pass_1(bCallParent);
	}

	@Override
	public void update_pass_2() {
		// TODO Auto-generated method stub
		if (tokenBefore != null){
			exponent.setLocation(new Point(location.x, location.y));
			exponent.update_pass_2();
		}else{
			int baseLineY = parent.getBaseLineY();
			tempBase.setLocation(new Point(location.x, baseLineY - tempBase.getUpperHeight()));
			tempBase.update_pass_2();
			exponent.setLocation(new Point(location.x + tempBase.getWIDTH() + parent.getSpace(), location.y));
			exponent.update_pass_2();
		}
	}

	@Override
	public void update_pass_1_chain_down() {
		// TODO Auto-generated method stub
		exponent.update_pass_1_chain_down();
		if (tokenBefore == null){
			tempBase.update_pass_1_chain_down();
		}
		update_pass_1(false);
	}
	
	@Override
	public void updateOnParentTextSizeChanged() {
		// TODO Auto-generated method stub
		if (tokenBefore != null){
			exponent.setTextSize(TextArea.TEXT_SIZE_SMALL);
			exponent.updateOnTextSizeChanged();
		}else{
			tempBase.setTextSize(parent.getTextSize());
			tempBase.updateOnTextSizeChanged();
			
			exponent.setTextSize(TextArea.TEXT_SIZE_SMALL);
			exponent.updateOnTextSizeChanged();
		}
		
		updateUpperLowerHeight();
		updateWidthHeight();
	}

	@Override
	public void updateOnParentPaintChanged() {
		// TODO Auto-generated method stub

		
		if (tokenBefore != null){
			exponent.setPaint(new Paint(parent.getPaint()));
			exponent.updateOnPaintChanged();
		}else{
			tempBase.setPaint(new Paint(parent.getPaint()));
			tempBase.updateOnPaintChanged();
			
			exponent.setPaint(new Paint(parent.getPaint()));
			exponent.updateOnPaintChanged();
		}
		
		updateUpperLowerHeight();
		updateWidthHeight();
	}

	@Override
	public int getNumberTextArea() {
		// TODO Auto-generated method stub
		if (tokenBefore == null) return 2;
		return 1;
	}

	@Override
	public TextArea getTextArea(int index) {
		// TODO Auto-generated method stub
		if (tokenBefore != null && index == 0) return exponent;
		if (tokenBefore == null && index == 0) return tempBase;
		if (tokenBefore == null && index == 1) return exponent;
		return null;
	}

	@Override
	public int getIndexOfTextArea(TextArea textArea) {
		// TODO Auto-generated method stub
		if (tokenBefore != null && textArea == exponent) return 0;
		if (tokenBefore == null && textArea == tempBase) return 0;
		if (tokenBefore == null && textArea == exponent) return 1;
		return -1;
	}

	@Override
	public void drawSelf(Canvas canvas) {
		// TODO Auto-generated method stub
		if (isVisible == false) return;
		if (tokenBefore == null) tempBase.drawSelf(canvas);
		exponent.drawSelf(canvas);
	}
	
	//-------------------------- GETTERS SETTERS ----------------------
	public TextArea getExponent() {
		return exponent;
	}

	public void setExponent(TextArea exponent) {
		this.exponent = exponent;
	}

	public int getHeightUnderBaseTop() {
		return heightUnderBaseTop;
	}

	public void setHeightUnderBaseTop(int heightUnderBaseTop) {
		this.heightUnderBaseTop = heightUnderBaseTop;
	}

	public VisualToken getTokenBefore() {
		return tokenBefore;
	}

	public void setTokenBefore(VisualToken tokenBefore) {
		this.tokenBefore = tokenBefore;
	}

	public int getDefaultBaseUpperHeight() {
		return defaultBaseUpperHeight;
	}

	public void setDefaultBaseUpperHeight(int defaultBaseUpperHeight) {
		this.defaultBaseUpperHeight = defaultBaseUpperHeight;
	}

	public TextArea getTempBase() {
		return tempBase;
	}

	public void setTempBase(TextArea tempBase) {
		this.tempBase = tempBase;
	}
	
	

}
