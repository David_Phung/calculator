package visualToken;

public class InverseTrigToken extends PowerToVisualToken {
	

	public InverseTrigToken(TextArea parent, String inverseType) {
		super(parent);
		// TODO Auto-generated constructor stub
		tempBase.setPowerToTokenTempBase(false);
		
		StringToken st1 = new StringToken(tempBase, inverseType);
		tempBase.getTokenList().add(st1);
		st1.update_pass_1(false);
		tempBase.update_pass_1(false);
		
		StringToken st2 = new StringToken(exponent, "-1");
		exponent.getTokenList().add(st2);
		st2.update_pass_1(false);
		exponent.update_pass_1(false);
	}
	
	@Override
	public VisualToken findBase() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public int getNumberTextArea() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	@Override
	public TextArea getTextArea(int index) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public int getIndexOfTextArea(TextArea textArea) {
		// TODO Auto-generated method stub
		return -1;
	}

}
