package visualToken;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.RectF;
import android.graphics.Paint.Style;

public class RootToken extends VisualToken {
	
	public static final int defaultSpaceToTAs = 3;
	public static final int defaultExtraHeight = 5;
	public static final int defaultExtraLength = 5;
	public static final int defaultVerticalDistanceToDegreeTA = 5;
	public static final int defaultBCLength = 5;
	public static final int defaultHorizontalDistance_EndOfDegree_A = 5;
	public static final float defaultVerticalRatio = 1/4f;
	
	protected TextArea degree, radicand;
	private Point A,B,C,D;

	public RootToken(TextArea parent) {
		super(ROOT_CUSTOM_DEGREE_TOKEN, parent);
		// TODO Auto-generated constructor stub
		degree = new TextArea(this, true, parent.getPaint());
		radicand = new TextArea(this, !parent.isRoot(), parent.getPaint());
		
		A = new Point();
		B = new Point();
		C = new Point();
		D = new Point();
		
		//init WIDTH and HEIGHT
		WIDTH = Math.max(defaultHorizontalDistance_EndOfDegree_A, degree.getWIDTH()) + defaultSpaceToTAs * 2 + defaultExtraLength;
		HEIGHT = Math.max(degree.getHEIGHT() - defaultVerticalDistanceToDegreeTA, 0) + radicand.getHEIGHT() + defaultExtraHeight;
		
		//init upperHeight and lowerHeight
		lowerHeight = (int) ((1 / (StringToken.upper_lower_ratio + 1.0f)) * radicand.getHEIGHT());
		upperHeight = HEIGHT - lowerHeight;
	}
	
	public void calculateRadicalSign(){
		C.x = location.x + degree.getWIDTH() + defaultSpaceToTAs;
		if (degree.getHEIGHT() < defaultVerticalDistanceToDegreeTA){
			C.y = location.y;
		}else{
			C.y = location.y + degree.getHEIGHT() - defaultVerticalDistanceToDegreeTA;
		}
		
		B.x = C.x - defaultBCLength;
		B.y = C.y + defaultExtraHeight + radicand.getHEIGHT();
		
		A.x = C.x - defaultSpaceToTAs - defaultHorizontalDistance_EndOfDegree_A;
		A.y = (int) (B.y + defaultVerticalRatio * (C.y - B.y));
		
		D.x = C.x + defaultSpaceToTAs + radicand.getWIDTH() + defaultExtraLength;
		D.y = C.y;
	}

	@Override
	public void updateWidthHeight() {
		// TODO Auto-generated method stub
		WIDTH = Math.max(defaultHorizontalDistance_EndOfDegree_A, degree.getWIDTH()) + defaultSpaceToTAs * 2 + radicand.getWIDTH() + defaultExtraLength;
		HEIGHT = Math.max(degree.getHEIGHT() - defaultVerticalDistanceToDegreeTA, 0) + radicand.getHEIGHT() + defaultExtraHeight;
	}

	@Override
	public void updateUpperLowerHeight() {
		// TODO Auto-generated method stub
		lowerHeight = (int) ((1 / (StringToken.upper_lower_ratio + 1.0f)) * radicand.getHEIGHT());
		upperHeight = HEIGHT - lowerHeight;
	}

	@Override
	public void update_pass_1(boolean bCallParent) {
		// TODO Auto-generated method stub
		updateWidthHeight();
		updateUpperLowerHeight();
		checkTokenToTheRight_ForPowerToToken();
		if (bCallParent) parent.update_pass_1(bCallParent);
	}

	@Override
	public void update_pass_2() {
		// TODO Auto-generated method stub
		calculateRadicalSign();
		
		degree.setLocation(new Point(location.x, location.y));
		radicand.setLocation(new Point(C.x + defaultSpaceToTAs, C.y + defaultExtraHeight));
		
		degree.update_pass_2();
		radicand.update_pass_2();
	}

	@Override
	public void update_pass_1_chain_down() {
		// TODO Auto-generated method stub
		degree.update_pass_1_chain_down();
		radicand.update_pass_1_chain_down();
		update_pass_1(false);
	}
	
	@Override
	public void updateOnParentTextSizeChanged() {
		// TODO Auto-generated method stub
		degree.setTextSize(TextArea.TEXT_SIZE_SMALL);
		degree.updateOnTextSizeChanged();
		
		if (parent.isRoot()){
			radicand.setTextSize(TextArea.TEXT_SIZE_NORMAL);
		}else{
			radicand.setTextSize(TextArea.TEXT_SIZE_NORMAL);
		}
		radicand.updateOnTextSizeChanged();
		
		updateWidthHeight();
		updateUpperLowerHeight();
	}

	@Override
	public void updateOnParentPaintChanged() {
		// TODO Auto-generated method stub
		degree.setPaint(new Paint(parent.getPaint()));
		degree.updateOnPaintChanged();
		
		radicand.setPaint(new Paint(parent.getPaint()));
		radicand.updateOnPaintChanged();
	}

	@Override
	public int getNumberTextArea() {
		// TODO Auto-generated method stub
		return 2;
	}

	@Override
	public TextArea getTextArea(int index) {
		// TODO Auto-generated method stub
		if (index == 0) return degree;
		if (index == 1) return radicand;
		return null;
	}

	@Override
	public int getIndexOfTextArea(TextArea textArea) {
		// TODO Auto-generated method stub
		if (textArea == degree) return 0;
		if (textArea == radicand) return 1;
		return -1;
	}

	@Override
	public void drawSelf(Canvas canvas) {
		// TODO Auto-generated method stub
		Paint tempPaint = new Paint(parent.getPaint());
		tempPaint.setAntiAlias(true);
		canvas.drawLine(A.x, A.y, B.x, B.y, tempPaint);
		canvas.drawLine(B.x, B.y, C.x, C.y, tempPaint);
		canvas.drawLine(C.x, C.y, D.x, D.y, tempPaint);
		degree.drawSelf(canvas);
		radicand.drawSelf(canvas);
		
		/*RectF r = new RectF(location.x, location.y, location.x + WIDTH, location.y + HEIGHT);
		tempPaint.setColor(Color.RED);
		tempPaint.setStyle(Style.STROKE);
		canvas.drawRect(r, tempPaint);*/
	}
	
	//----------------------- GETTERS SETTERS ------------------------

	public TextArea getDegree() {
		return degree;
	}

	public void setDegree(TextArea degree) {
		this.degree = degree;
	}

	public TextArea getRadicand() {
		return radicand;
	}

	public void setRadicand(TextArea radicand) {
		this.radicand = radicand;
	}

	public Point getA() {
		return A;
	}

	public void setA(Point a) {
		A = a;
	}

	public Point getB() {
		return B;
	}

	public void setB(Point b) {
		B = b;
	}

	public Point getC() {
		return C;
	}

	public void setC(Point c) {
		C = c;
	}

	public Point getD() {
		return D;
	}

	public void setD(Point d) {
		D = d;
	}

	
}
