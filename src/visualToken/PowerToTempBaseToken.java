package visualToken;

import android.graphics.Canvas;
import android.graphics.Point;

public class PowerToTempBaseToken extends VisualToken {
	
	private TextArea base;

	public PowerToTempBaseToken(TextArea parent) {
		super(VisualToken.TEMP_BASE_FOR_POWER_TO_TOKEN, parent);
		// TODO Auto-generated constructor stub
		if (parent.getTextSize() == TextArea.TEXT_SIZE_NORMAL){
			base = new TextArea(this, false, parent.getPaint());
		}else{
			base = new TextArea(this, true, parent.getPaint());
		}
		WIDTH = HEIGHT = parent.getTextSize();
		upperHeight = (int) (StringToken.upper_lower_ratio	/ (StringToken.upper_lower_ratio + 1.0f) * parent.getTextSize());
		lowerHeight = parent.getTextSize() - this.upperHeight;
	}

	@Override
	public void updateWidthHeight() {
		// TODO Auto-generated method stub
		WIDTH = HEIGHT = parent.getTextSize();
	}

	@Override
	public void updateUpperLowerHeight() {
		// TODO Auto-generated method stub
		upperHeight = (int) (StringToken.upper_lower_ratio	/ (StringToken.upper_lower_ratio + 1.0f) * parent.getTextSize());
		lowerHeight = parent.getTextSize() - this.upperHeight;
	}

	@Override
	public void update_pass_1(boolean bCallParent) {
		// TODO Auto-generated method stub
		updateWidthHeight();
		updateUpperLowerHeight();
		if (bCallParent){
			parent.update_pass_1(bCallParent);
		}
	}

	@Override
	public void update_pass_2() {
		// TODO Auto-generated method stub
		base.setLocation(new Point(location.x, location.y));
	}

	@Override
	public void update_pass_1_chain_down() {
		// TODO Auto-generated method stub
		base.update_pass_1_chain_down();
		update_pass_1(false);
	}
	
	@Override
	public void updateOnParentTextSizeChanged() {
		// TODO Auto-generated method stub
		updateWidthHeight();
		updateUpperLowerHeight();
	}

	@Override
	public void updateOnParentPaintChanged() {
		// TODO Auto-generated method stub
		updateWidthHeight();
		updateUpperLowerHeight();
	}

	@Override
	public int getNumberTextArea() {
		// TODO Auto-generated method stub
		return 1;
	}

	@Override
	public TextArea getTextArea(int index) {
		// TODO Auto-generated method stub
		if (index == 0) return base;
		return null;
	}

	@Override
	public int getIndexOfTextArea(TextArea textArea) {
		// TODO Auto-generated method stub
		if (textArea == base) return 0;
		return -1;
	}

	@Override
	public void drawSelf(Canvas canvas) {
		// TODO Auto-generated method stub
		base.drawSelf(canvas);
	}

}
