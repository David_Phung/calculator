package visualToken;

public class SquareRoot extends RootToken {

	public SquareRoot(TextArea parent) {
		super(parent);
		// TODO Auto-generated constructor stub
		degree.setWIDTH(degree.getWIDTH() / 2);
		degree.setHEIGHT(degree.getHEIGHT() / 2);
		degree.setbDrawBoundsWhenEmpty(false);
	}
	
	@Override
	public void update_pass_1_chain_down() {
		// TODO Auto-generated method stub
		radicand.update_pass_1_chain_down();
		update_pass_1(false);
	}
	
	@Override
	public int getNumberTextArea() {
		// TODO Auto-generated method stub
		return 1;
	}
	
	@Override
	public TextArea getTextArea(int index) {
		// TODO Auto-generated method stub
		if (index == 0) return radicand;
		return null;
	}
	
	@Override
	public int getIndexOfTextArea(TextArea textArea) {
		// TODO Auto-generated method stub
		if (textArea == radicand) return 0;
		return -1;
	}

}
