package visualToken;

import android.graphics.Point;
import android.graphics.PointF;

/**
 * 
 * <p><b><i> public class VisualCursor  </i></b></p>
 * The cursor that will be displayed on the screen. Most data fields are self-explained.
 * Element index is the index of the token that this cursor points to. This index does not 
 * correspond with the element index in Data Cursor. 
 * @author PTP
 *
 */
public class VisualCursor {
	
public static final float defaultThickness = 4;
	
	private PointF location;
	private float thickness;
	private float length;
	private int elementIndex;
	
	/**
	 * 
	 * <p><b><i> public VisualCursor()  </i></b></p>
	 * Thickness is set to defaultThickness and elementIndex is set to -1
	 */
	public VisualCursor(){
		location = new PointF();
		length = 0;
		thickness = defaultThickness;
		elementIndex = -1;
	}
	//*****************GETTERS && SETTERS ***********************

	public PointF getLocation() {
		return location;
	}

	public void setLocation(PointF location) {
		this.location = location;
	}

	public float getThickness() {
		return thickness;
	}

	public void setThickness(float thickness) {
		this.thickness = thickness;
	}

	public float getLength() {
		return length;
	}

	public void setLength(float length) {
		this.length = length;
	}

	public int getElementIndex() {
		return elementIndex;
	}

	public void setElementIndex(int elementIndex) {
		this.elementIndex = elementIndex;
	}
	
	
	
}
