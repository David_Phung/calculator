package visualToken;

import java.util.ArrayList;

import com.example.calculator03.PaintManager;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.RectF;
import android.graphics.Paint.Style;
import android.util.Log;

public class VisualCursorManager{
	
	public static final long blinkTime = 750;
	
	private VisualCursor cursor;
	private ArrayList<TextArea> TAList;
	private int indexOfFocusedTA;
	private Paint cursorPaint;
	private boolean bBlurring;
	private int currentPaintOpacity;
	private long lastTime;

	public VisualCursorManager(){
		cursor = new VisualCursor();
		TAList = new ArrayList<TextArea>();
		indexOfFocusedTA = -1;
		cursorPaint = PaintManager.cursorPaint;
		bBlurring = true;
		currentPaintOpacity = 255;
		lastTime = -1;
	}
	
	public void refreshCursor(){
		placeCursorAt(getFocusedTextArea(), cursor.getElementIndex());
	}
	
	public void clear(){
		TextArea tempTA = TAList.get(0);
		TAList.clear();
		TAList.add(tempTA);
		placeCursorAt(tempTA, -1);
	}
	
	/**
	 * 
	 * <p><b><i> public void placeCursorAt(TextArea textArea, int index)  </i></b></p>
	 * Place the cursor inside the text area and point to the element at the
	 * position of index. The index of focused text area is updated here, programmers do not
	 * need to it manually.
	 * <p> <b>**</b> The method is very important to move cursor and is used frequently both inside 
	 * cursor manager and outside. Therefore it is written carefully.
	 * All error that might occur will be printed out to log cat to help debugging. Although later these debugging codes
	 * should be handled properly</p>
	 * 
	 * @param textArea
	 * @param index
	 */
	public void placeCursorAt(TextArea textArea, int index){
		cursor.setLength(textArea.getTextSize());
		float textAreaX = textArea.getLocation().x;
		float textAreaY = textArea.getLocation().y;
		
		//In case index is -1, we place the cursor at the beginning of the text area
		if (index == -1){
			//TokenList empty
			if (textArea.getTokenList().isEmpty()){
				cursor.setLocation(new PointF(textAreaX, textAreaY));
			}else{
				float cursorY = textArea.getBaseLineY() - StringToken.upper_lower_ratio
						/ (StringToken.upper_lower_ratio + 1.0f) * textArea.getTextSize();
				cursor.setLocation(new PointF(textAreaX, cursorY));
			}
		}else if (index >= 0 && index < textArea.getTokenList().size()){
			//TokenList empty
			if (textArea.getTokenList().isEmpty()){
				Log.d("","In placeCursorAt(...): index >= 0 while tokenList is empty");
			}else{
				float marker = textAreaX;
				for (int i = 0; i < index + 1; i++) {
					marker += textArea.getTokenList().get(i).getWIDTH();
					marker += textArea.getSpace();
				}
				marker -= textArea.getSpace();
				float cursorY = textArea.getBaseLineY() - StringToken.upper_lower_ratio
						/ (StringToken.upper_lower_ratio + 1.0f) * textArea.getTextSize();
				cursor.setLocation(new PointF(marker, cursorY));
			}
		}else{
			Log.d("","In placeCursorAt(...): index is greater than tokenList size");
		}
		
		cursor.setElementIndex(index);
		
		int tempIndex = TAList.indexOf(textArea);
		if (tempIndex == -1) {
			Log.d("", "In placeCursorAt(...): cant find textArea in TAList");
		} else {
			indexOfFocusedTA = tempIndex;
		}
	}

	/**
	 * 
	 * <p><b><i> public void moveCursorLeft()  </i></b></p>
	 * Move cursor to the left 1 step. The method automatically detects when to move
	 * the cursor to another text area.
	 * 
	 * moveCursorLeft() is also used in the Del() function in DisplayManager.
	 * Before modify this function, one should take that into account.
	 */
	public void moveCursorLeft(){
		TextArea tempTA = TAList.get(indexOfFocusedTA);
		//normal case
		if (cursor.getElementIndex() >= 0 && cursor.getElementIndex() < tempTA.getTokenList().size()){
			VisualToken tokenToTheLeft = tempTA.getTokenList().get(cursor.getElementIndex());
			//tokenToTheLefts_lastTA is the last text area of the token, if it's null
			//then the token is a simple token like string token, we just need to handle normally
			TextArea tokenToTheLefts_lastTA = tokenToTheLeft.getTextArea(tokenToTheLeft.getNumberTextArea() - 1);
			if (tokenToTheLefts_lastTA == null){
				placeCursorAt(TAList.get(indexOfFocusedTA), cursor.getElementIndex() - 1);
			}else{
				//place at the end of the last text area
				placeCursorAt(tokenToTheLefts_lastTA, tokenToTheLefts_lastTA.getTokenList().size() - 1);
			}
		}
		//if the cursor is at the beginning of the text area, we move the cursor to the previous text area
		// or outside of the token
		else if (cursor.getElementIndex() == -1){
			//If tempTA is root text area
			if (tempTA.isRoot()){
				return;
			}
			//If tempTA is not root, we get the token contains tempTA
			//and ask it to return the previous text area, if the returned value
			//is null, we move the cursor out of the token.
			VisualToken tempTAs_parent = tempTA.getParent();
			TextArea previousTA = tempTAs_parent.getTextArea(tempTAs_parent.getIndexOfTextArea(tempTA) - 1);
			if (previousTA == null){
				TextArea parentOf_tempTAs_parent = tempTAs_parent.getParent();
				int indexOf_tempTAs_parent = parentOf_tempTAs_parent.getTokenList().indexOf(tempTAs_parent);
				placeCursorAt(parentOf_tempTAs_parent, indexOf_tempTAs_parent - 1);
				//if the text area we were in is a power token temp base and it's at the beginning of the text area, move cursor left once more
				if ( tempTA.isPowerToTokenTempBase() && indexOf_tempTAs_parent == 0){
					moveCursorLeft();
				}
			}else{
				placeCursorAt(previousTA, previousTA.getTokenList().size() - 1);
			}
		}
	}

	/**
	 * 
	 * <p><b><i>  public void moveCursorRight() </i></b></p>
	 * Move cursor to the right 1 step. The method automatically detects when to move
	 * the cursor to another text area.
	 */
	public void moveCursorRight(){
		TextArea tempTA = TAList.get(indexOfFocusedTA);
		//normal case
		if (cursor.getElementIndex() < tempTA.getTokenList().size() - 1 && cursor.getElementIndex() >= -1){
			VisualToken tokenToTheRight = tempTA.getTokenList().get(cursor.getElementIndex() + 1);
			//tokenToTheRights_firstTA is the first text area of the token, if it's null
			//then the token is a simple token like string token, we just need to handle normally
			TextArea firstTA = tokenToTheRight.getTextArea(0);
			if (firstTA == null){
				placeCursorAt(TAList.get(indexOfFocusedTA), cursor.getElementIndex() + 1);
			}else{
				placeCursorAt(firstTA, -1);
				
				//if the first expression contains a power to token at the beginning of it
				//and if that token is tempBaseEnabled -> move right once more
				if (!firstTA.getTokenList().isEmpty() && firstTA.getTokenList().get(0).type == VisualToken.POWER_TO_TOKEN){
					PowerToVisualToken powerToken = (PowerToVisualToken)firstTA.getTokenList().get(0);
					if (powerToken.getTextArea(0) != null && powerToken.getTextArea(0).isPowerToTokenTempBase()) moveCursorRight();
				}
			}
		}
		//if the cursor is at the end of the text area, we move the cursor to the next text area
		// or outside of the token
		else if (cursor.getElementIndex() == tempTA.getTokenList().size() - 1){
			//If tempTA is root text area
			if (tempTA.isRoot()){
				return;
			}
			//If tempTA is not root, we get the token contains tempTA
			//and ask it to return the next text area, if the returned value
			//is null, we move the cursor out of the token.
			VisualToken tempTAs_parent = tempTA.getParent();
			TextArea nextTA = tempTAs_parent.getTextArea(tempTAs_parent.getIndexOfTextArea(tempTA) + 1);
			if (nextTA == null){
				TextArea parentOf_tempTAs_parent = tempTAs_parent.getParent();
				int indexOf_tempTAs_parent = parentOf_tempTAs_parent.getTokenList().indexOf(tempTAs_parent);
				placeCursorAt(parentOf_tempTAs_parent, indexOf_tempTAs_parent);
			}else{
				placeCursorAt(nextTA, -1);
			}
		}
	}
	
	public void draw(Canvas canvas) {
		/*if (lastTime == -1){
			lastTime = System.currentTimeMillis();
		}else{
			long currentTime = System.currentTimeMillis();
			long passedTime = currentTime - lastTime;
			int x = (int) Math.round((double)passedTime / blinkTime * 255);
			if (bBlurring){
				currentPaintOpacity -= x;
			}else{
				currentPaintOpacity += x;
			}
			if (currentPaintOpacity <= 0){
				bBlurring = false;
			}else if (currentPaintOpacity >= 255){
				bBlurring = true;
			}
			lastTime = System.currentTimeMillis();
		}
		Paint tempPaint = new Paint(cursorPaint);
		if (currentPaintOpacity > 150){
			tempPaint.setAlpha(255);
		}else if (currentPaintOpacity < 50){
			tempPaint.setAlpha(0);
		}else{
			tempPaint.setAlpha(currentPaintOpacity);
		}*/
		Paint tempPaint = new Paint(cursorPaint);
		RectF cursorRect = new RectF(cursor.getLocation().x,
				cursor.getLocation().y, cursor.getLocation().x
						+ cursor.getThickness(), cursor.getLocation().y
						+ cursor.getLength());
		canvas.drawRect(cursorRect, tempPaint);
	}
	
	//Special getters

	public TextArea getFocusedTextArea(){
		return TAList.get(indexOfFocusedTA);
	}
	
	public VisualToken getFocusedToken(){
		if (cursor.getElementIndex() >= 0){
			return TAList.get(indexOfFocusedTA).getTokenList().get(cursor.getElementIndex());
		}
		return null;
	}
	
	//------------------------- GETTERS && SETTERS -----------------------

	public VisualCursor getCursor() {
		return cursor;
	}

	public void setCursor(VisualCursor cursor) {
		this.cursor = cursor;
	}

	public ArrayList<TextArea> getTAList() {
		return TAList;
	}

	public void setTAList(ArrayList<TextArea> tAList) {
		TAList = tAList;
	}

	public int getIndexOfFocusedTA() {
		return indexOfFocusedTA;
	}

	public void setIndexOfFocusedTA(int indexOfFocusedTA) {
		this.indexOfFocusedTA = indexOfFocusedTA;
	}

	public Paint getCursorPaint() {
		return cursorPaint;
	}

	public void setCursorPaint(Paint cursorPaint) {
		this.cursorPaint = cursorPaint;
	}
	
}
