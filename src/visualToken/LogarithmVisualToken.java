package visualToken;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;

public class LogarithmVisualToken extends VisualToken {
	
	public static final String openParen = "(";
	public static final String closeParen = ")";
	
	public static final int defaultHorizontalSpace_text_base = 5;
	public static final int defaultHorizontalSpace_TAs_Parens = 5;
	public static final int defaultHeightAboveFoot = 3;
	public static final int defaultHeightBelowBaseLineY = 1;
	
	protected String identifierText;
	protected TextArea base, logExpression;
	private Point identifierTextLocation, openParenLocation, closeParenLocation;
	private boolean identifierTextVisibility;

	public LogarithmVisualToken(TextArea parent, String identifierText) {
		super(LOGARITHM_CUSTOM_BASE, parent);
		// TODO Auto-generated constructor stub
		this.identifierText = new String(identifierText);
		base = new TextArea(this, true, parent.getPaint());
		if (parent.getTextSize() == TextArea.TEXT_SIZE_NORMAL){
			logExpression = new TextArea(this, false, parent.getPaint());
		}else{
			logExpression = new TextArea(this, true, parent.getPaint());
		}
		
		identifierTextLocation = new Point();
		openParenLocation = new Point();
		closeParenLocation = new Point();
		identifierTextVisibility = true;
		
		
		Paint paint = new Paint(parent.getPaint());
		paint.setTextSize(parent.getTextSize());
		Rect logTextBounds = new Rect();
		paint.getTextBounds(identifierText, 0, identifierText.length(), logTextBounds);
		
		Rect openParenBounds = new Rect();
		Rect closeParenBounds = new Rect();
		
		paint.getTextBounds(openParen, 0, openParen.length(), openParenBounds);
		paint.getTextBounds(closeParen, 0, closeParen.length(), closeParenBounds);
		
		int logText_upperHeight = (int) (StringToken.upper_lower_ratio	/ (StringToken.upper_lower_ratio + 1.0f) * logTextBounds.height());
		int openParen_upperHeight = (int) (StringToken.upper_lower_ratio	/ (StringToken.upper_lower_ratio + 1.0f) * openParenBounds.height());
		int closeParen_upperHeight = (int) (StringToken.upper_lower_ratio	/ (StringToken.upper_lower_ratio + 1.0f) * closeParenBounds.height());
		int logExpression_upperHeight =  logExpression.getUpperHeight();
		
		upperHeight = Math.max(logText_upperHeight, openParen_upperHeight);
		upperHeight = Math.max(upperHeight, closeParen_upperHeight);
		upperHeight = Math.max(upperHeight, logExpression_upperHeight);
		upperHeight = Math.max(upperHeight, - defaultHeightBelowBaseLineY);

		int logText_lowerHeight = logTextBounds.height() - logText_upperHeight;
		int openParen_lowerHeight = openParenBounds.height() - openParen_upperHeight;
		int closeParen_lowerHeight = closeParenBounds.height() - closeParen_upperHeight;
		int logExpression_lowerHeight = logExpression.getLowerHeight();
		
		lowerHeight = Math.max(logText_lowerHeight, openParen_lowerHeight);
		lowerHeight = Math.max(lowerHeight, closeParen_lowerHeight);
		lowerHeight = Math.max(lowerHeight, logExpression_lowerHeight);
		lowerHeight = Math.max(lowerHeight, base.getHEIGHT() + defaultHeightBelowBaseLineY);
		
		//init WIDTH && HEIGHT
		WIDTH = logTextBounds.width() + openParenBounds.width() + closeParenBounds.width()
				+ base.getWIDTH() + logExpression.getWIDTH() + defaultHorizontalSpace_text_base + 3 * defaultHorizontalSpace_TAs_Parens;
		HEIGHT = upperHeight + lowerHeight;
		
	}

	@Override
	public void updateWidthHeight() {
		// TODO Auto-generated method stub
		Paint paint = new Paint(parent.getPaint());
		paint.setTextSize(parent.getTextSize());
		
		Rect logTextBounds = new Rect();
		paint.getTextBounds(identifierText, 0, identifierText.length(), logTextBounds);
		
		Rect openParenBounds = new Rect();
		Rect closeParenBounds = new Rect();
		
		paint.getTextBounds(openParen, 0, openParen.length(), openParenBounds);
		paint.getTextBounds(closeParen, 0, closeParen.length(), closeParenBounds);
		
		WIDTH = logTextBounds.width() + openParenBounds.width() + closeParenBounds.width()
				+ base.getWIDTH() + logExpression.getWIDTH() + defaultHorizontalSpace_text_base + 3 * defaultHorizontalSpace_TAs_Parens;
		HEIGHT = upperHeight + lowerHeight;
	}

	@Override
	public void updateUpperLowerHeight() {
		// TODO Auto-generated method stub
		Paint paint = new Paint(parent.getPaint());
		paint.setTextSize(parent.getTextSize());
		
		Rect logTextBounds = new Rect();
		paint.getTextBounds(identifierText, 0, identifierText.length(), logTextBounds);
		
		Rect openParenBounds = new Rect();
		Rect closeParenBounds = new Rect();
		
		paint.getTextBounds(openParen, 0, openParen.length(), openParenBounds);
		paint.getTextBounds(closeParen, 0, closeParen.length(), closeParenBounds);
		
		int logText_upperHeight = (int) (StringToken.upper_lower_ratio	/ (StringToken.upper_lower_ratio + 1.0f) * logTextBounds.height());
		int openParen_upperHeight = (int) (StringToken.upper_lower_ratio	/ (StringToken.upper_lower_ratio + 1.0f) * openParenBounds.height());
		int closeParen_upperHeight = (int) (StringToken.upper_lower_ratio	/ (StringToken.upper_lower_ratio + 1.0f) * closeParenBounds.height());
		int logExpression_upperHeight =  logExpression.getUpperHeight();
		
		upperHeight = Math.max(logText_upperHeight, openParen_upperHeight);
		upperHeight = Math.max(upperHeight, closeParen_upperHeight);
		upperHeight = Math.max(upperHeight, logExpression_upperHeight);
		upperHeight = Math.max(upperHeight, - defaultHeightBelowBaseLineY);

		int logText_lowerHeight = logTextBounds.height() - logText_upperHeight;
		int openParen_lowerHeight = openParenBounds.height() - openParen_upperHeight;
		int closeParen_lowerHeight = closeParenBounds.height() - closeParen_upperHeight;
		int logExpression_lowerHeight = logExpression.getLowerHeight();
		
		lowerHeight = Math.max(logText_lowerHeight, openParen_lowerHeight);
		lowerHeight = Math.max(lowerHeight, closeParen_lowerHeight);
		lowerHeight = Math.max(lowerHeight, logExpression_lowerHeight);
		lowerHeight = Math.max(lowerHeight, base.getHEIGHT() + defaultHeightBelowBaseLineY);
	}

	@Override
	public void update_pass_1(boolean bCallParent) {
		// TODO Auto-generated method stub
		updateUpperLowerHeight();
		updateWidthHeight();
		checkTokenToTheRight_ForPowerToToken();
		if (bCallParent) parent.update_pass_1(bCallParent);
	}

	@Override
	public void update_pass_2() {
		// TODO Auto-generated method stub
		Paint paint = new Paint(parent.getPaint());
		paint.setTextSize(parent.getTextSize());
		
		Rect logTextBounds = new Rect();
		paint.getTextBounds(identifierText, 0, identifierText.length(), logTextBounds);
		
		Rect openParenBounds = new Rect();
		Rect closeParenBounds = new Rect();
		
		paint.getTextBounds(openParen, 0, openParen.length(), openParenBounds);
		paint.getTextBounds(closeParen, 0, closeParen.length(), closeParenBounds);
		
		int logText_upperHeight = (int) (StringToken.upper_lower_ratio	/ (StringToken.upper_lower_ratio + 1.0f) * logTextBounds.height());
		int openParen_upperHeight = (int) (StringToken.upper_lower_ratio	/ (StringToken.upper_lower_ratio + 1.0f) * openParenBounds.height());
		int closeParen_upperHeight = (int) (StringToken.upper_lower_ratio	/ (StringToken.upper_lower_ratio + 1.0f) * closeParenBounds.height());
		
		//Identifier text
		
		identifierTextLocation.x = location.x;
		identifierTextLocation.y = parent.getBaseLineY() - logText_upperHeight;
		
		//Base
		Point baseLocation = new Point();
		baseLocation.x = location.x + logTextBounds.width() + defaultHorizontalSpace_text_base;
		//baseLocation.y = identifierTextLocation.y + logTextBounds.height() - defaultHeightAboveFoot;
		baseLocation.y = parent.getBaseLineY() + defaultHeightBelowBaseLineY;
		
		base.setLocation(baseLocation);
		
		//Open Paren
		openParenLocation.x = baseLocation.x + base.getWIDTH() + defaultHorizontalSpace_TAs_Parens;
		openParenLocation.y = parent.getBaseLineY() - openParen_upperHeight;
		
		
		Point logExpressionLocation = new Point();
		logExpressionLocation.x = openParenLocation.x + openParenBounds.width() + defaultHorizontalSpace_TAs_Parens;
		logExpressionLocation.y = parent.getBaseLineY() - logExpression.getUpperHeight();		
		logExpression.setLocation(logExpressionLocation);
		
		//Close Paren:
		closeParenLocation.x = logExpressionLocation.x + logExpression.getWIDTH() + defaultHorizontalSpace_TAs_Parens;
		closeParenLocation.y = parent.getBaseLineY() - closeParen_upperHeight;
		
		base.update_pass_2();
		logExpression.update_pass_2();
	}

	@Override
	public void update_pass_1_chain_down() {
		// TODO Auto-generated method stub
		base.update_pass_1_chain_down();
		logExpression.update_pass_1_chain_down();
		update_pass_1(false);
	}
	
	@Override
	public void updateOnParentTextSizeChanged() {
		// TODO Auto-generated method stub
		base.setTextSize(TextArea.TEXT_SIZE_SMALL);
		base.updateOnTextSizeChanged();
		
		if (parent.isRoot()){
			logExpression.setTextSize(TextArea.TEXT_SIZE_NORMAL);
		}else{
			logExpression.setTextSize(TextArea.TEXT_SIZE_SMALL);
		}
		logExpression.updateOnTextSizeChanged();
		
		updateUpperLowerHeight();
		updateWidthHeight();
	}

	@Override
	public void updateOnParentPaintChanged() {
		// TODO Auto-generated method stub
		base.setPaint(parent.getPaint());
		logExpression.setPaint(parent.getPaint());
		
		base.updateOnPaintChanged();
		logExpression.updateOnPaintChanged();
	}

	@Override
	public int getNumberTextArea() {
		// TODO Auto-generated method stub
		return 2;
	}

	@Override
	public TextArea getTextArea(int index) {
		// TODO Auto-generated method stub
		if (index == 0) return base;
		if (index == 1) return logExpression;
		return null;
	}

	@Override
	public int getIndexOfTextArea(TextArea textArea) {
		// TODO Auto-generated method stub
		if (textArea == base) return 0;
		if (textArea == logExpression) return 1;
		return -1;
	}

	@Override
	public void drawSelf(Canvas canvas) {
		// TODO Auto-generated method stub
		if (isVisible == false) return;
		
		Paint paint = new Paint(parent.getPaint());
		paint.setTextSize(parent.getTextSize());
		
		//draw identifier Text
		if (identifierTextVisibility == true){
			Rect bounds = new Rect();
			
			paint.getTextBounds(identifierText, 0, identifierText.length(), bounds);
			int width = bounds.width();

			String string2 = identifierText + identifierText;
			Rect bounds2 = new Rect();
			paint.getTextBounds(string2, 0, string2.length(), bounds2);
			int width2 = bounds2.width();

			int shift_distance = (width2 - 2 * width) / 2;
			canvas.drawText(identifierText, identifierTextLocation.x - shift_distance,
					identifierTextLocation.y + bounds.height(), paint);
		}
		
		//Draw DEBUG info
		//Draw Location:
		Paint tempPaint = new Paint();
		tempPaint.setColor(Color.RED);
		canvas.drawCircle(location.x, location.y, 4, tempPaint);
		
		//Draw line between lower part && upper part
		float lineY = location.y + upperHeight;
		canvas.drawLine(location.x, lineY, location.x + WIDTH, lineY, tempPaint);
		
		//Draw open, close paren
		Rect openParenBounds = new Rect();
		Rect closeParenBounds = new Rect();
		
		paint.getTextBounds(openParen, 0, openParen.length(), openParenBounds);
		paint.getTextBounds(closeParen, 0, closeParen.length(), closeParenBounds);
		
		int openParenWidth = openParenBounds.width();
		int closeParenWidth = closeParenBounds.width();
		
		String openParen_tempString = openParen + openParen;
		String closeParen_tempString = closeParen + closeParen;
		
		Rect openParenBounds2 = new Rect();
		Rect closeParenBounds2 = new Rect();
		
		paint.getTextBounds(openParen_tempString, 0, openParen_tempString.length(), openParenBounds2);
		paint.getTextBounds(closeParen_tempString, 0, closeParen_tempString.length(), closeParenBounds2);
		
		int openParenWidth2 = openParenBounds2.width();
		int closeParenWidth2 = closeParenBounds2.width();
		
		int openParen_shift_distance = (openParenWidth2 - 2 * openParenWidth) / 2;
		int closeParen_shift_distance = (closeParenWidth2 - 2 * closeParenWidth) / 2;
		
		canvas.drawText(openParen, openParenLocation.x - openParen_shift_distance,
				openParenLocation.y + openParenBounds.height(), paint);
		canvas.drawText(closeParen, closeParenLocation.x - closeParen_shift_distance,
				closeParenLocation.y + closeParenBounds.height(), paint);
		
		base.drawSelf(canvas);
		logExpression.drawSelf(canvas);
	}
	
	//------------------------- GETTERS SETTERS--------------------------

	public TextArea getBase() {
		return base;
	}

	public void setBase(TextArea base) {
		this.base = base;
	}

	public TextArea getLogExpression() {
		return logExpression;
	}

	public void setLogExpression(TextArea logExpression) {
		this.logExpression = logExpression;
	}

	public Point getIdentifierTextLocation() {
		return identifierTextLocation;
	}

	public void setIdentifierTextLocation(Point identifierTextLocation) {
		this.identifierTextLocation = identifierTextLocation;
	}

	public Point getOpenParenLocation() {
		return openParenLocation;
	}

	public void setOpenParenLocation(Point openParenLocation) {
		this.openParenLocation = openParenLocation;
	}

	public Point getCloseParenLocation() {
		return closeParenLocation;
	}

	public void setCloseParenLocation(Point closeParenLocation) {
		this.closeParenLocation = closeParenLocation;
	}

	public boolean isIdentifierTextVisibility() {
		return identifierTextVisibility;
	}

	public void setIdentifierTextVisibility(boolean identifierTextVisibility) {
		this.identifierTextVisibility = identifierTextVisibility;
	}

	
}
