package visualToken;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;

public class CombinatoricAndPolRectToken extends VisualToken {
	
	public static final String seperator = ",";
	public static final String openParen = "(";
	public static final String closeParen = ")";
	
	public static final int defaultHorizontalSpace = 5;
	
	private String identifierText;
	private TextArea n,r;
	private Point identifierTextLocation, openParenLocation, closeParenLocation, seperatorLocation;
	
	private boolean identifierTextVisibility;
	
	public CombinatoricAndPolRectToken(TextArea parent, String identifierText) {
		super(COMBINATORIC_POL_RECT_TOKEN, parent);
		// TODO Auto-generated constructor stub
		identifierTextVisibility = true; 
		
		this.identifierText = identifierText;
		n = new TextArea(this, !parent.isRoot(), parent.getPaint());
		r = new TextArea(this, !parent.isRoot(), parent.getPaint());	
		
		identifierTextLocation = new Point();
		openParenLocation = new Point();
		closeParenLocation = new Point();
		seperatorLocation = new Point();
		
		Paint paint = new Paint(parent.getPaint());
		paint.setTextSize(parent.getTextSize());
		Rect identifierTextBounds = new Rect();
		paint.getTextBounds(identifierText, 0, identifierText.length(), identifierTextBounds);
		
		Rect openParenBounds = new Rect();
		Rect closeParenBounds = new Rect();
		Rect seperatorBounds = new Rect();
		
		paint.getTextBounds(openParen, 0, openParen.length(), openParenBounds);
		paint.getTextBounds(closeParen, 0, closeParen.length(), closeParenBounds);
		paint.getTextBounds(seperator, 0, seperator.length(), seperatorBounds);
		
		int identifierText_upperHeight = (int) (StringToken.upper_lower_ratio	/ (StringToken.upper_lower_ratio + 1.0f) * identifierTextBounds.height());
		int openParen_upperHeight = (int) (StringToken.upper_lower_ratio	/ (StringToken.upper_lower_ratio + 1.0f) * openParenBounds.height());
		int closeParen_upperHeight = (int) (StringToken.upper_lower_ratio	/ (StringToken.upper_lower_ratio + 1.0f) * closeParenBounds.height());
		int seperator_upperHeight = (int)(StringToken.upper_lower_ratio / StringToken.upper_lower_ratio + 1.0f) * seperatorBounds.height();
		int n_upperHeight =  n.getUpperHeight();
		int r_upperHeight =  r.getUpperHeight();
		
		upperHeight = Math.max(identifierText_upperHeight, openParen_upperHeight);
		upperHeight = Math.max(upperHeight, closeParen_upperHeight);
		upperHeight = Math.max(upperHeight, n_upperHeight);
		upperHeight = Math.max(upperHeight, r_upperHeight);
		upperHeight = Math.max(upperHeight, seperator_upperHeight);

		int identifierText_lowerHeight = identifierTextBounds.height() - identifierText_upperHeight;
		int openParen_lowerHeight = openParenBounds.height() - openParen_upperHeight;
		int closeParen_lowerHeight = closeParenBounds.height() - closeParen_upperHeight;
		int seperator_lowerHeight = seperatorBounds.height() - seperator_upperHeight;
		int n_lowerHeight = n.getLowerHeight();
		int r_lowerHeight = r.getLowerHeight();
		
		lowerHeight = Math.max(identifierText_lowerHeight, openParen_lowerHeight);
		lowerHeight = Math.max(lowerHeight, closeParen_lowerHeight);
		lowerHeight = Math.max(lowerHeight, seperator_lowerHeight);
		lowerHeight = Math.max(lowerHeight, n_lowerHeight);
		lowerHeight = Math.max(lowerHeight, r_lowerHeight);
		
		//init WIDTH && HEIGHT
		WIDTH = identifierTextBounds.width() + openParenBounds.width() + closeParenBounds.width() + seperatorBounds.width()
				+ n.getWIDTH() + r.getWIDTH() + 5 * defaultHorizontalSpace;
		HEIGHT = upperHeight + lowerHeight;
	}

	@Override
	public void updateWidthHeight() {
		// TODO Auto-generated method stub
		Paint paint = new Paint(parent.getPaint());
		paint.setTextSize(parent.getTextSize());
		Rect identifierTextBounds = new Rect();
		paint.getTextBounds(identifierText, 0, identifierText.length(), identifierTextBounds);
		
		Rect openParenBounds = new Rect();
		Rect closeParenBounds = new Rect();
		Rect seperatorBounds = new Rect();
		
		paint.getTextBounds(openParen, 0, openParen.length(), openParenBounds);
		paint.getTextBounds(closeParen, 0, closeParen.length(), closeParenBounds);
		paint.getTextBounds(seperator, 0, seperator.length(), seperatorBounds);
		
		//init WIDTH && HEIGHT
		WIDTH = identifierTextBounds.width() + openParenBounds.width() + closeParenBounds.width() + seperatorBounds.width()
				+ n.getWIDTH() + r.getWIDTH() + 5 * defaultHorizontalSpace;
		HEIGHT = upperHeight + lowerHeight;
	}

	@Override
	public void updateUpperLowerHeight() {
		// TODO Auto-generated method stub
		Paint paint = new Paint(parent.getPaint());
		paint.setTextSize(parent.getTextSize());
		Rect identifierTextBounds = new Rect();
		paint.getTextBounds(identifierText, 0, identifierText.length(), identifierTextBounds);
		
		Rect openParenBounds = new Rect();
		Rect closeParenBounds = new Rect();
		Rect seperatorBounds = new Rect();
		
		paint.getTextBounds(openParen, 0, openParen.length(), openParenBounds);
		paint.getTextBounds(closeParen, 0, closeParen.length(), closeParenBounds);
		paint.getTextBounds(seperator, 0, seperator.length(), seperatorBounds);
		
		int identifierText_upperHeight = (int) (StringToken.upper_lower_ratio	/ (StringToken.upper_lower_ratio + 1.0f) * identifierTextBounds.height());
		int openParen_upperHeight = (int) (StringToken.upper_lower_ratio	/ (StringToken.upper_lower_ratio + 1.0f) * openParenBounds.height());
		int closeParen_upperHeight = (int) (StringToken.upper_lower_ratio	/ (StringToken.upper_lower_ratio + 1.0f) * closeParenBounds.height());
		int seperator_upperHeight = (int)(StringToken.upper_lower_ratio / StringToken.upper_lower_ratio + 1.0f) * seperatorBounds.height();
		int n_upperHeight =  n.getUpperHeight();
		int r_upperHeight =  r.getUpperHeight();
		
		upperHeight = Math.max(identifierText_upperHeight, openParen_upperHeight);
		upperHeight = Math.max(upperHeight, closeParen_upperHeight);
		upperHeight = Math.max(upperHeight, n_upperHeight);
		upperHeight = Math.max(upperHeight, r_upperHeight);
		upperHeight = Math.max(upperHeight, seperator_upperHeight);

		int identifierText_lowerHeight = identifierTextBounds.height() - identifierText_upperHeight;
		int openParen_lowerHeight = openParenBounds.height() - openParen_upperHeight;
		int closeParen_lowerHeight = closeParenBounds.height() - closeParen_upperHeight;
		int seperator_lowerHeight = seperatorBounds.height() - seperator_upperHeight;
		int n_lowerHeight = n.getLowerHeight();
		int r_lowerHeight = r.getLowerHeight();
		
		lowerHeight = Math.max(identifierText_lowerHeight, openParen_lowerHeight);
		lowerHeight = Math.max(lowerHeight, closeParen_lowerHeight);
		lowerHeight = Math.max(lowerHeight, seperator_lowerHeight);
		lowerHeight = Math.max(lowerHeight, n_lowerHeight);
		lowerHeight = Math.max(lowerHeight, r_lowerHeight);
	}

	@Override
	public void update_pass_1(boolean bCallParent) {
		// TODO Auto-generated method stub
		updateUpperLowerHeight();
		updateWidthHeight();
		checkTokenToTheRight_ForPowerToToken();
		if (bCallParent) parent.update_pass_1(bCallParent);
	}

	@Override
	public void update_pass_2() {
		// TODO Auto-generated method stub
		Paint paint = new Paint(parent.getPaint());
		paint.setTextSize(parent.getTextSize());
		Rect identifierTextBounds = new Rect();
		paint.getTextBounds(identifierText, 0, identifierText.length(), identifierTextBounds);
		
		Rect openParenBounds = new Rect();
		Rect closeParenBounds = new Rect();
		Rect seperatorBounds = new Rect();
		
		paint.getTextBounds(openParen, 0, openParen.length(), openParenBounds);
		paint.getTextBounds(closeParen, 0, closeParen.length(), closeParenBounds);
		paint.getTextBounds(seperator, 0, seperator.length(), seperatorBounds);
		
		int identifierText_upperHeight = (int) (StringToken.upper_lower_ratio	/ (StringToken.upper_lower_ratio + 1.0f) * identifierTextBounds.height());
		int openParen_upperHeight = (int) (StringToken.upper_lower_ratio	/ (StringToken.upper_lower_ratio + 1.0f) * openParenBounds.height());
		int closeParen_upperHeight = (int) (StringToken.upper_lower_ratio	/ (StringToken.upper_lower_ratio + 1.0f) * closeParenBounds.height());
		int seperator_upperHeight = (int)(StringToken.upper_lower_ratio / StringToken.upper_lower_ratio + 1.0f) * seperatorBounds.height();
		
		//IdentifierText
		identifierTextLocation.x = location.x;
		identifierTextLocation.y = parent.getBaseLineY() - identifierText_upperHeight;
		
		//OpenParen
		openParenLocation.x = identifierTextLocation.x + identifierTextBounds.width() + defaultHorizontalSpace;
		openParenLocation.y = parent.getBaseLineY() - openParen_upperHeight;
		
		//n
		Point nLocation = new Point();
		nLocation.x = openParenLocation.x + openParenBounds.width() + defaultHorizontalSpace;
		nLocation.y = parent.getBaseLineY() - n.getUpperHeight();
		n.setLocation(nLocation);
		
		//seperator
		seperatorLocation.x = nLocation.x + n.getWIDTH() + defaultHorizontalSpace;
		seperatorLocation.y = parent.getBaseLineY() - seperator_upperHeight;
		
		//r
		Point rLocation = new Point();
		rLocation.x = seperatorLocation.x + seperatorBounds.width() + defaultHorizontalSpace;
		rLocation.y = parent.getBaseLineY() - r.getUpperHeight();
		r.setLocation(rLocation);
		
		//close paren
		closeParenLocation.x = rLocation.x + r.getWIDTH() + defaultHorizontalSpace;
		closeParenLocation.y = parent.getBaseLineY() - openParen_upperHeight;
		
		n.update_pass_2();
		r.update_pass_2();
	}

	@Override
	public void update_pass_1_chain_down() {
		// TODO Auto-generated method stub
		update_pass_1(false);
	}
	
	@Override
	public void updateOnParentTextSizeChanged() {
		// TODO Auto-generated method stub
		if (parent.isRoot()){
			n.setTextSize(TextArea.TEXT_SIZE_NORMAL);
			r.setTextSize(TextArea.TEXT_SIZE_NORMAL);
		}else{
			n.setTextSize(TextArea.TEXT_SIZE_SMALL);
			r.setTextSize(TextArea.TEXT_SIZE_SMALL);
		}
		n.updateOnTextSizeChanged();
		r.updateOnTextSizeChanged();
		
		updateUpperLowerHeight();
		updateWidthHeight();
	}

	@Override
	public void updateOnParentPaintChanged() {
		// TODO Auto-generated method stub
		n.setPaint(parent.getPaint());
		r.setPaint(parent.getPaint());
		
		n.updateOnPaintChanged();
		r.updateOnPaintChanged();
	}

	@Override
	public int getNumberTextArea() {
		// TODO Auto-generated method stub
		return 2;
	}

	@Override
	public TextArea getTextArea(int index) {
		// TODO Auto-generated method stub
		if (index == 0) return n;
		if (index == 1) return r;
		return null;
	}

	@Override
	public int getIndexOfTextArea(TextArea textArea) {
		// TODO Auto-generated method stub
		if (textArea == n) return 0;
		if (textArea == r) return 1;
		return -1;
	}

	@Override
	public void drawSelf(Canvas canvas) {
		// TODO Auto-generated method stub
if (isVisible == false) return;
		
		Paint paint = new Paint(parent.getPaint());
		paint.setTextSize(parent.getTextSize());
		
		//draw identifier Text
		if (identifierTextVisibility == true){
			Rect bounds = new Rect();
			
			paint.getTextBounds(identifierText, 0, identifierText.length(), bounds);
			int width = bounds.width();

			String string2 = identifierText + identifierText;
			Rect bounds2 = new Rect();
			paint.getTextBounds(string2, 0, string2.length(), bounds2);
			int width2 = bounds2.width();

			int shift_distance = (width2 - 2 * width) / 2;
			canvas.drawText(identifierText, identifierTextLocation.x - shift_distance,
					identifierTextLocation.y + bounds.height(), paint);
		}
		
		//Draw DEBUG info
		//Draw Location:
		Paint tempPaint = new Paint();
		tempPaint.setColor(Color.RED);
		canvas.drawCircle(location.x, location.y, 4, tempPaint);
		
		//Draw line between lower part && upper part
		float lineY = location.y + upperHeight;
		canvas.drawLine(location.x, lineY, location.x + WIDTH, lineY, tempPaint);
		
		//Draw open, close paren, seperator
		Rect openParenBounds = new Rect();
		Rect closeParenBounds = new Rect();
		Rect seperatorBounds = new Rect();		
		
		paint.getTextBounds(openParen, 0, openParen.length(), openParenBounds);
		paint.getTextBounds(closeParen, 0, closeParen.length(), closeParenBounds);
		paint.getTextBounds(seperator, 0, seperator.length(), seperatorBounds);
		
		int openParenWidth = openParenBounds.width();
		int closeParenWidth = closeParenBounds.width();
		int seperatorWidth = seperatorBounds.width();
		
		String openParen_tempString = openParen + openParen;
		String closeParen_tempString = closeParen + closeParen;
		String seperator_tempString = seperator + seperator;
		
		Rect openParenBounds2 = new Rect();
		Rect closeParenBounds2 = new Rect();
		Rect seperatorBounds2 = new Rect();
		
		paint.getTextBounds(openParen_tempString, 0, openParen_tempString.length(), openParenBounds2);
		paint.getTextBounds(closeParen_tempString, 0, closeParen_tempString.length(), closeParenBounds2);
		paint.getTextBounds(seperator_tempString, 0, seperator_tempString.length(), seperatorBounds2);
		
		int openParenWidth2 = openParenBounds2.width();
		int closeParenWidth2 = closeParenBounds2.width();
		int seperatorWidth2 = closeParenBounds2.width();
		
		int openParen_shift_distance = (openParenWidth2 - 2 * openParenWidth) / 2;
		int closeParen_shift_distance = (closeParenWidth2 - 2 * closeParenWidth) / 2;
		int seperator_shift_distance = (seperatorWidth2 - 2 * seperatorWidth) / 2;
		
		canvas.drawText(openParen, openParenLocation.x - openParen_shift_distance,
				openParenLocation.y + openParenBounds.height(), paint);
		canvas.drawText(closeParen, closeParenLocation.x - closeParen_shift_distance,
				closeParenLocation.y + closeParenBounds.height(), paint);
		canvas.drawText(seperator, seperatorLocation.x - seperator_shift_distance,
				seperatorLocation.y + seperatorBounds.height(), paint);
		n.drawSelf(canvas);
		r.drawSelf(canvas);
	}

	public boolean isIdentifierTextVisibility() {
		return identifierTextVisibility;
	}

	public void setIdentifierTextVisibility(boolean identifierTextVisibility) {
		this.identifierTextVisibility = identifierTextVisibility;
	}

	
}
