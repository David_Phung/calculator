package visualToken;

public class PowerToFixedBase extends PowerToVisualToken {

	public PowerToFixedBase(TextArea parent, String base) {
		super(parent);
		// TODO Auto-generated constructor stub
		tempBase.setPowerToTokenTempBase(false);
		
		StringToken st = new StringToken(tempBase, base);
		tempBase.getTokenList().add(st);
		st.update_pass_1(false);
		tempBase.update_pass_1(false);
	}
	
	@Override
	public VisualToken findBase() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public int getNumberTextArea() {
		// TODO Auto-generated method stub
		return 1;
	}
	
	@Override
	public TextArea getTextArea(int index) {
		// TODO Auto-generated method stub
		if (index == 0) return exponent;
		return null;
	}
	
	@Override
	public int getIndexOfTextArea(TextArea textArea) {
		// TODO Auto-generated method stub
		if (textArea == exponent) return 0;
		return -1;
	}

}
