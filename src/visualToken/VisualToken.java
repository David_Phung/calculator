package visualToken;

import com.example.calculator03.TextAreaEditor;

import android.graphics.Canvas;
import android.graphics.Point;

public abstract class VisualToken {
	
	public static final int STRING_TOKEN = 0;
	public static final int FRACTION_TOKEN = 1;
	public static final int POWER_TO_TOKEN = 2;
	public static final int TEMP_BASE_FOR_POWER_TO_TOKEN = 3;
	public static final int ROOT_CUSTOM_DEGREE_TOKEN = 4;
	public static final int ROOT_Nth_TOKEN = 5;
	public static final int ROOT_CUBE_TOKEN = 6;
	public static final int LOGARITHM_CUSTOM_BASE = 7;
	public static final int TRIG_FUNCTION_POWER_TO__1 = 8;
	
	public static final int SQUARE_ROOT = 9;
	public static final int CUBE_ROOT = 10;
	public static final int SQUARE = 11;
	public static final int CUBE = 12;
	public static final int INVERSE_TRIG = 13;
	
	public static final int COMBINATORIC_POL_RECT_TOKEN = 14;
	
	public static final int POWERTO_FIXEDBASE = 15;
	public static final int LOGARITHM_FIXED_BASE = 16;
	
	public static final int EXP_TOKEN = 17;
	
	public int type;
	
	protected TextArea parent;
	protected Point location;
	protected int WIDTH, HEIGHT, upperHeight, lowerHeight;
	protected boolean isVisible;

	public VisualToken(int type, TextArea parent) {
		// TODO Auto-generated constructor stub
		this.type = type;
		this.parent = parent;
		location = new Point();
		isVisible = true;
	}
	
	public void checkTokenToTheRight_ForPowerToToken(){
		//check token the right
		int currentIndex = parent.getTokenList().indexOf(this);
		if ( currentIndex < parent.getTokenList().size() - 1){
			VisualToken tokenToTheRight = parent.getTokenList().get(currentIndex + 1);
			if (tokenToTheRight.type == VisualToken.POWER_TO_TOKEN){
				tokenToTheRight.update_pass_1(false);
			}
		}
	}
	
	//-------------------------------------
	
	public abstract void updateWidthHeight();
	
	public abstract void updateUpperLowerHeight();
	
	public abstract void update_pass_1(boolean bCallParent);
	
	public abstract void update_pass_2();
	
	public abstract void update_pass_1_chain_down();
	
	public abstract void updateOnParentTextSizeChanged();
	
	public abstract void updateOnParentPaintChanged();
	
	//---------------------------------
	
	public abstract int getNumberTextArea();
	
	/**
	 * <p><b><i>  public abstract TextArea getTextArea(int index) </i></b></p>
	 * Return null if invalid index
	 * @param index
	 * @return
	 */
	public abstract TextArea getTextArea(int index);
	
	/**
	 * 
	 * <p><b><i> public abstract int getIndexOfTextArea(TextArea textArea)  </i></b></p>
	 * return -1 if textArea not found
	 * @param textArea
	 * @return
	 */
	public abstract int getIndexOfTextArea(TextArea textArea);

	//----------------------------------
	
	public abstract void drawSelf(Canvas canvas);
	
	//-----------------------------------
	
	public TextArea getParent() {
		return parent;
	}

	public void setParent(TextArea parent) {
		this.parent = parent;
	}

	public Point getLocation() {
		return location;
	}

	public void setLocation(Point location) {
		this.location = location;
	}


	public int getWIDTH() {
		return WIDTH;
	}

	public void setWIDTH(int wIDTH) {
		WIDTH = wIDTH;
	}

	public int getHEIGHT() {
		return HEIGHT;
	}

	public void setHEIGHT(int hEIGHT) {
		HEIGHT = hEIGHT;
	}

	public int getUpperHeight() {
		return upperHeight;
	}

	public void setUpperHeight(int upperHeight) {
		this.upperHeight = upperHeight;
	}

	public int getLowerHeight() {
		return lowerHeight;
	}

	public void setLowerHeight(int lowerHeight) {
		this.lowerHeight = lowerHeight;
	}

	public boolean isVisible() {
		return isVisible;
	}

	public void setVisible(boolean isVisible) {
		this.isVisible = isVisible;
	}
	
	
}
