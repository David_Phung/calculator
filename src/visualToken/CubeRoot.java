package visualToken;

public class CubeRoot extends RootToken {

	public CubeRoot(TextArea parent) {
		super(parent);
		// TODO Auto-generated constructor stub
		StringToken token = new StringToken(degree, "3");
		degree.getTokenList().add(token);
		token.update_pass_1(false);
		degree.update_pass_1(false);
	}

	@Override
	public int getNumberTextArea() {
		// TODO Auto-generated method stub
		return 1;
	}
	
	@Override
	public TextArea getTextArea(int index) {
		// TODO Auto-generated method stub
		if (index == 0) return radicand;
		return null;
	}
	
	@Override
	public int getIndexOfTextArea(TextArea textArea) {
		// TODO Auto-generated method stub
		if (textArea == radicand) return 0;
		return -1;
	}
}
