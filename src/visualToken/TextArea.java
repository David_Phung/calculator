package visualToken;

import java.util.ArrayList;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.Paint.Style;
import android.util.Log;

public class TextArea {
	
	public static int TEXT_SIZE_NORMAL = 40;
	public static int TEXT_SIZE_SMALL = 20;
	public static float TEXT_SIZE_SMALLER_RATIO = 3 / 5f;

	private VisualToken parent;
	private Point location;
	private int WIDTH, HEIGHT, upperHeight, lowerHeight, baseLineY, space,
			textSize;
	private ArrayList<VisualToken> tokenList;
	private boolean isVisible, isRoot, isPowerToTokenTempBase, bDrawBoundsWhenEmpty;
	private Paint paint;
	
	private boolean bDisabledBySize;
	
	public TextArea(VisualToken parent, boolean bSmallTextSize, Paint paint) {
		// TODO Auto-generated constructor stub
		if (parent == null){
			isRoot = true;
		}else{
			this.parent = parent;
		}
		
		location = new Point();
		if (bSmallTextSize){
			textSize = TEXT_SIZE_SMALL;
		}else{
			textSize = TEXT_SIZE_NORMAL;
		}
		
		tokenList = new ArrayList<VisualToken>();
		isVisible = true;
		this.paint = paint;  
		WIDTH = HEIGHT = textSize;
		upperHeight = (int) (StringToken.upper_lower_ratio	/ (StringToken.upper_lower_ratio + 1.0f) * textSize);
		lowerHeight = textSize - this.upperHeight;
		
		isPowerToTokenTempBase = false;
		bDrawBoundsWhenEmpty = true;
		bDisabledBySize = false;
	}
	
	//-------------------------------------
	
	public void updateWidthHeight(){
		if (tokenList.isEmpty()) {
			WIDTH = HEIGHT = textSize;
			Log.d("","In TextArea updateWidthHeight(): WIDTH = " + WIDTH + " HEIGHT = " + HEIGHT);
		} else {
			float maxUpperHeight = tokenList.get(0).getUpperHeight();
			float maxLowerHeight = tokenList.get(0).getLowerHeight();

			for (int i = 0; i < tokenList.size(); i++) {
				VisualToken tempToken = tokenList.get(i);
				maxUpperHeight = Math.max(maxUpperHeight,
						tempToken.getUpperHeight());
				maxLowerHeight = Math.max(maxLowerHeight,
						tempToken.getLowerHeight());
			}

			HEIGHT = (int) (maxUpperHeight + maxLowerHeight);

			int w = 0;
			for (int i = 0; i < tokenList.size(); i++) {
				VisualToken tempToken = tokenList.get(i);
				w += tempToken.getWIDTH();
				w += space;
			}
			w -= space;
			WIDTH = w;
		}
	}
	
	public void updateUpperLowerHeight(){
		if (tokenList.isEmpty()) {
			this.upperHeight = (int) (StringToken.upper_lower_ratio	/ (StringToken.upper_lower_ratio + 1.0f) * textSize);
			this.lowerHeight = textSize - this.upperHeight;
		} else {
			float maxUpperHeight = tokenList.get(0).getUpperHeight();
			float maxLowerHeight = tokenList.get(0).getLowerHeight();

			for (int i = 0; i < tokenList.size(); i++) {
				VisualToken tempToken = tokenList.get(i);
				maxUpperHeight = Math.max(maxUpperHeight,
						tempToken.getUpperHeight());
				maxLowerHeight = Math.max(maxLowerHeight,
						tempToken.getLowerHeight());
			}
			this.upperHeight = (int) maxUpperHeight;
			this.lowerHeight = (int) maxLowerHeight;
		}
	}
	
	public void updateBaseLineY(){
		if (tokenList.isEmpty()) {
			baseLineY = location.y + textSize;
		} else {
			float maxUpperHeight = tokenList.get(0).getUpperHeight();

			for (int i = 0; i < tokenList.size(); i++) {
				VisualToken tempToken = tokenList.get(i);
				maxUpperHeight = Math.max(maxUpperHeight,
						tempToken.getUpperHeight());
			}
			baseLineY = (int) (location.y + maxUpperHeight);
		}
	}
	
	public void update_pass_1(boolean bCallParent){
		updateWidthHeight();
		updateUpperLowerHeight();
		if (isRoot && bCallParent){
			for (VisualToken token:tokenList){
				if (token.type == VisualToken.POWER_TO_TOKEN){
					token.update_pass_1(false);
				}
			}
			update_pass_2();
		}else if (bCallParent){
			for (VisualToken token:tokenList){
				if (token.type == VisualToken.POWER_TO_TOKEN){
					token.update_pass_1(false);
				}
			}
			parent.update_pass_1(bCallParent);
		}else if (!bCallParent){
			/*for (int i = 0; i < tokenList.size(); i++){
				tokenList.get(i).update_pass_1(bCallParent);
			}*/
		}
	}
	
	public void update_pass_2(){
		updateBaseLineY();
		//Loop through children to set locations and call updates
		float currentXToAssign = this.location.x;
		for (int i = 0; i < tokenList.size(); i++) {
			VisualToken tempToken = tokenList.get(i);
			float y = baseLineY - tempToken.getUpperHeight();

			Point newLocation = new Point((int)currentXToAssign,(int) y);

			currentXToAssign += tempToken.getWIDTH();
			currentXToAssign += space;

			tempToken.setLocation(newLocation);
			tempToken.update_pass_2();
		}
	}

	public void update_pass_1_chain_down(){
		for (VisualToken token:tokenList){
			token.update_pass_1_chain_down();
		}
		update_pass_1(false);
	}
	
	public void updateOnTextSizeChanged(){
		paint.setTextSize(textSize);
		//Loop through token list to call updateOnParentTextSizeChanged()
		for (int i = 0; i < tokenList.size(); i++){
			tokenList.get(i).updateOnParentTextSizeChanged();
		}
		//update size
		updateWidthHeight();
		
		if (isRoot){
			update_pass_2();
		}
	}
	
	public void updateOnPaintChanged(){
		//Loop through token list to call updateOnParentPaintChanged()
		for (int i = 0; i < tokenList.size(); i++){
			tokenList.get(i).updateOnParentPaintChanged();
		}
		//update size
		updateWidthHeight();
		
		if (isRoot){
			update_pass_2();
		}		
	}
	
	public void drawSelf(Canvas canvas){
		if (isVisible == false || bDisabledBySize){
			return;
		}
		
		//------------------------
		if (tokenList.isEmpty() && bDrawBoundsWhenEmpty && !isRoot) {
			Paint tempPaint = new Paint(paint);
			tempPaint.setStyle(Style.STROKE);
			tempPaint.setStrokeWidth(2);
			canvas.drawRect(location.x, location.y, location.x + WIDTH,
					location.y + HEIGHT, tempPaint);
			//Log.d("","In TextArea drawself(): location: " + location.x + "," + location.y + " WDITH = " + WIDTH + " HEIGHT = " + HEIGHT);
		}else {
			for (int i = 0; i < tokenList.size(); i++) {
				VisualToken tempToken = tokenList.get(i);
				tempToken.drawSelf(canvas);
			}
		}
	}
	
	
	//--------------------------------------------

	public VisualToken getParent() {
		return parent;
	}

	public void setParent(VisualToken parent) {
		this.parent = parent;
	}

	public Point getLocation() {
		return location;
	}

	public void setLocation(Point location) {
		this.location = location;
	}

	public int getWIDTH() {
		if (bDisabledBySize) return 0;
		return WIDTH;
	}

	public void setWIDTH(int wIDTH) {
		WIDTH = wIDTH;
	}

	public int getHEIGHT() {
		if (bDisabledBySize) return 0;
		return HEIGHT;
	}

	public void setHEIGHT(int hEIGHT) {
		HEIGHT = hEIGHT;
	}

	public int getUpperHeight() {
		return upperHeight;
	}

	public void setUpperHeight(int upperHeight) {
		this.upperHeight = upperHeight;
	}

	public int getLowerHeight() {
		return lowerHeight;
	}

	public void setLowerHeight(int lowerHeight) {
		this.lowerHeight = lowerHeight;
	}

	public int getBaseLineY() {
		return baseLineY;
	}

	public void setBaseLineY(int baseLineY) {
		this.baseLineY = baseLineY;
	}

	public int getSpace() {
		return space;
	}

	public void setSpace(int space) {
		this.space = space;
	}

	public int getTextSize() {
		return textSize;
	}

	public void setTextSize(int textSize) {
		this.textSize = textSize;
	}

	public ArrayList<VisualToken> getTokenList() {
		return tokenList;
	}

	public void setTokenList(ArrayList<VisualToken> tokenList) {
		this.tokenList = tokenList;
	}

	public boolean isVisible() {
		return isVisible;
	}

	public void setVisible(boolean isVisible) {
		this.isVisible = isVisible;
	}

	public boolean isRoot() {
		return isRoot;
	}

	public void setRoot(boolean isRoot) {
		this.isRoot = isRoot;
	}

	public Paint getPaint() {
		return paint;
	}

	public void setPaint(Paint paint) {
		this.paint = paint;
	}

	public boolean isPowerToTokenTempBase() {
		return isPowerToTokenTempBase;
	}

	public void setPowerToTokenTempBase(boolean isPowerToTokenTempBase) {
		this.isPowerToTokenTempBase = isPowerToTokenTempBase;
	}

	public boolean isbDrawBoundsWhenEmpty() {
		return bDrawBoundsWhenEmpty;
	}

	public void setbDrawBoundsWhenEmpty(boolean bDrawBoundsWhenEmpty) {
		this.bDrawBoundsWhenEmpty = bDrawBoundsWhenEmpty;
	}

	public boolean isbDisabledBySize() {
		return bDisabledBySize;
	}

	public void setbDisabledBySize(boolean bDisabledBySize) {
		this.bDisabledBySize = bDisabledBySize;
	}

	
}
