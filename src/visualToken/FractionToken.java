package visualToken;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.RectF;
import android.graphics.Paint.Style;
import android.util.Log;

public class FractionToken extends VisualToken {
	
	public static final float scale_ratio_if_parent_is_root = 75.0f / 100;
	public static final float spaceY_textSize_ratio = 1.0f / 15;
	public static final int defaultFactionBarThickness = 2;
	public static final int defaultSpaceX = 3;
	public static final int defaultSpaceY = 3;

	private TextArea numerator, denominator;
	private int spaceX_numerator, spaceX_denominator, spaceY;
	private int fractionBarThickness;
	private int fractionBarLength;
	private Point fractionBarLocation;
	private boolean fractionBarVisibility;

	public FractionToken(TextArea parent) {
		super(FRACTION_TOKEN, parent);
		// TODO Auto-generated constructor stub
		int textSize = TextArea.TEXT_SIZE_SMALL;

		// initialize numerator and denominator
		numerator = new TextArea(this, true, parent.getPaint());
		denominator = new TextArea(this, true, parent.getPaint());

		// initialize spaces
		spaceX_numerator = spaceX_denominator = defaultSpaceX;
		spaceY = defaultSpaceY;

		// initializeWIDTH HEIGHT:
		WIDTH = textSize + spaceX_numerator * 2;
		HEIGHT = textSize + spaceY * 2;

		// initialize upperPartHeight lowerPartHeight:
		upperHeight = lowerHeight = textSize + spaceY;

		// initialize factionBar:
		fractionBarThickness = defaultFactionBarThickness;
		fractionBarLength = WIDTH;
		fractionBarLocation = new Point();
		fractionBarVisibility = true;
	}
	
	public void updateSpaces() {
		int tempWidth = 0;
		if (numerator.getWIDTH() < denominator.getWIDTH()) {
			spaceX_denominator = defaultSpaceX;
			tempWidth = spaceX_denominator * 2 + denominator.getWIDTH();
			spaceX_numerator =(tempWidth - numerator.getWIDTH()) / 2;
		} else {
			spaceX_numerator = defaultSpaceX;
			tempWidth = spaceX_numerator * 2 + numerator.getWIDTH();
			spaceX_denominator = (tempWidth - denominator.getWIDTH()) / 2;
		}
		
		spaceY = defaultSpaceY + fractionBarThickness / 2;
	}

	@Override
	public void updateWidthHeight() {
		// TODO Auto-generated method stub
		HEIGHT = upperHeight + lowerHeight;

		if (numerator.getWIDTH() < denominator.getWIDTH()) {
			WIDTH = spaceX_denominator * 2 + denominator.getWIDTH();
		} else {
			WIDTH = spaceX_numerator * 2 + numerator.getWIDTH();
		}
	}

	@Override
	public void updateUpperLowerHeight() {
		// TODO Auto-generated method stub
		upperHeight = numerator.getHEIGHT() + spaceY;
		lowerHeight = denominator.getHEIGHT() + spaceY;
	}
	
	/**
	 * 
	 * <p><b><i> public void updateFractionBar()  </i></b></p>
	 * Must be called before updateVisibility()
	 */
	public void updateFractionBar(){
		fractionBarLength = WIDTH;
		int baseLine = parent.getBaseLineY();
		int upperThickness = fractionBarThickness / 2;
		fractionBarLocation = new Point(location.x, baseLine
				- upperThickness);
		fractionBarVisibility = true;
	}

	@Override
	public void update_pass_1(boolean bCallParent) {
		// TODO Auto-generated method stub
		updateSpaces();
		updateUpperLowerHeight();
		updateWidthHeight();
		checkTokenToTheRight_ForPowerToToken();
		if (bCallParent){
			parent.update_pass_1(bCallParent);
		}
	}

	@Override
	public void update_pass_2() {
		// TODO Auto-generated method stub
		updateFractionBar();
		
		Point newNumeratorLocation = new Point(location.x + spaceX_numerator,
				location.y);
		Point newDenominatorLocation = new Point(location.x
				+ spaceX_denominator, location.y + numerator.getHEIGHT() + 2
				* spaceY);
		numerator.setLocation(newNumeratorLocation);
		denominator.setLocation(newDenominatorLocation);
		numerator.update_pass_2();
		denominator.update_pass_2();
	}

	@Override
	public void update_pass_1_chain_down() {
		// TODO Auto-generated method stub
		numerator.update_pass_1_chain_down();
		denominator.update_pass_1_chain_down();
		update_pass_1(false);
	}
	
	@Override
	public void updateOnParentTextSizeChanged() {
		// TODO Auto-generated method stub
		numerator.setTextSize(TextArea.TEXT_SIZE_SMALL);
		numerator.updateOnTextSizeChanged();
		
		denominator.setTextSize(TextArea.TEXT_SIZE_SMALL);
		denominator.updateOnTextSizeChanged();
		
		updateSpaces();
		updateUpperLowerHeight();
		updateWidthHeight();
	}

	@Override
	public void updateOnParentPaintChanged() {
		// TODO Auto-generated method stub
		numerator.setPaint(new Paint(parent.getPaint()));
		numerator.updateOnPaintChanged();
		
		denominator.setPaint(new Paint(parent.getPaint()));
		denominator.updateOnPaintChanged();
	}

	@Override
	public int getNumberTextArea() {
		// TODO Auto-generated method stub
		return 2;
	}

	@Override
	public TextArea getTextArea(int index) {
		// TODO Auto-generated method stub
		if (index == 0) return numerator;
		if (index == 1) return denominator;
		return null;
	}

	@Override
	public int getIndexOfTextArea(TextArea textArea) {
		// TODO Auto-generated method stub
		if (textArea == numerator) return 0;
		if (textArea == denominator) return 1;
		return -1;
	}

	@Override
	public void drawSelf(Canvas canvas) {
		// TODO Auto-generated method stub
		if (isVisible == false) {
			return;
		}
		if (fractionBarVisibility == true){
			RectF factionBarRect = new RectF(fractionBarLocation.x,
					fractionBarLocation.y, fractionBarLocation.x + fractionBarLength,
					fractionBarLocation.y + fractionBarThickness);
			Paint paint = new Paint(parent.getPaint());
			paint.setStyle(Style.FILL);
			canvas.drawRect(factionBarRect, paint);	
		}
		numerator.drawSelf(canvas);
		denominator.drawSelf(canvas);
	}
	
	//********************* GETTERS SETTERS ********************

	public TextArea getNumerator() {
		return numerator;
	}

	public void setNumerator(TextArea numerator) {
		this.numerator = numerator;
	}

	public TextArea getDenominator() {
		return denominator;
	}

	public void setDenominator(TextArea denominator) {
		this.denominator = denominator;
	}

	public int getSpaceX_numerator() {
		return spaceX_numerator;
	}

	public void setSpaceX_numerator(int spaceX_numerator) {
		this.spaceX_numerator = spaceX_numerator;
	}

	public int getSpaceX_denominator() {
		return spaceX_denominator;
	}

	public void setSpaceX_denominator(int spaceX_denominator) {
		this.spaceX_denominator = spaceX_denominator;
	}

	public int getSpaceY() {
		return spaceY;
	}

	public void setSpaceY(int spaceY) {
		this.spaceY = spaceY;
	}

	public int getFractionBarThickness() {
		return fractionBarThickness;
	}

	public void setFractionBarThickness(int fractionBarThickness) {
		this.fractionBarThickness = fractionBarThickness;
	}

	public int getFractionBarLength() {
		return fractionBarLength;
	}

	public void setFractionBarLength(int fractionBarLength) {
		this.fractionBarLength = fractionBarLength;
	}

	public Point getFractionBarLocation() {
		return fractionBarLocation;
	}

	public void setFractionBarLocation(Point fractionBarLocation) {
		this.fractionBarLocation = fractionBarLocation;
	}

	public boolean isFractionBarVisibility() {
		return fractionBarVisibility;
	}

	public void setFractionBarVisibility(boolean fractionBarVisibility) {
		this.fractionBarVisibility = fractionBarVisibility;
	}

	
}
