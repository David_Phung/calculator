### Calculator
A scientific calculator with a realistic look written in Java for Android phones.

![alt text](https://bitbucket.org/David_Phung/calculator/raw/10a89909b83289f19e0da9820fda2bbfa807fdf8/Screenshot.png)

The project was created using Eclipse with the Android plugin and Google's Android SDK.